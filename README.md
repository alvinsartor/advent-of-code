# Advent Of Code 

[Advent Of Code](https://adventofcode.com/)  is an annual event in which hundred of thousands of programmers from all over the world put their skills and knowledge in use for the greater good: saving Christmas 🎅. 

## Why Doing it?

Each dev has different reasons to put a lot of time into completing the AoC challenges;    

I do it because I want to push my skills over the limit, learn new things, refine my knowledge about algorithms and data structures and train my *Clean & Efficient Code* glands.

## Which languages are admitted?

Any! Pick your favorite, your most hated, the one you want to learn or the one you had forgotten!   
The beauty of the challenge is that you can pick your own tools.    

I chose C#, simply because it is TheBestLanguage™ around (just my humble opinion :P) and I can never get enough of it.

## Current state

Year          | 2015 | 2016 | 2017 | 2018 | 2019 | 2020 | 2021 
------------- |:----:|:----:|:----:|:----:|:----:|:----:|:----:
State         | 0/50 | 0/50 | 50/50⭐ | 50/50⭐ | 50/50⭐ | 50/50⭐ | 26/50 

## My solutions with execution times


### 2021

```bash
Year 2021 Day 01 Part 1: 1676 | elapsed: 0.1577298ms
Year 2021 Day 01 Part 2: 1706 | elapsed: 0.1701406ms
Year 2021 Day 02 Part 1: 1698735 | elapsed: 0.4711052ms
Year 2021 Day 02 Part 2: 1594785890 | elapsed: 0.3940544ms
Year 2021 Day 03 Part 1: 3374136 | elapsed: 0.2074154ms
Year 2021 Day 03 Part 2: 4432698 | elapsed: 0.1897344ms
Year 2021 Day 04 Part 1: 58412 | elapsed: 0.6329611ms
Year 2021 Day 04 Part 2: 10030 | elapsed: 1.1546131ms
Year 2021 Day 05 Part 1: 17420 | elapsed: 38.640968ms
Year 2021 Day 05 Part 2: 41345 | elapsed: 73.670766ms
Year 2021 Day 06 Part 1: 387413 | elapsed: 0.0917839ms
Year 2021 Day 06 Part 2: 1738377086345 | elapsed: 0.0557613ms
Year 2021 Day 07 Part 1: 333755 | elapsed: 36.38486ms
Year 2021 Day 07 Part 2: 94017638 | elapsed: 32.18165ms
Year 2021 Day 08 Part 1: 476 | elapsed: 0.2382612ms
Year 2021 Day 08 Part 2: 1011823 | elapsed: 3.7661815ms
Year 2021 Day 09 Part 1: 633 | elapsed: 0.2883208ms
Year 2021 Day 09 Part 2: 1050192 | elapsed: 4.33476ms
Year 2021 Day 10 Part 1: 215229 | elapsed: 0.3329618ms
Year 2021 Day 10 Part 2: 1105996483 | elapsed: 0.2857652ms
Year 2021 Day 11 Part 1: 1659 | elapsed: 0.5743521ms
Year 2021 Day 11 Part 2: 227 | elapsed: 1.2686849ms
Year 2021 Day 12 Part 1: 3410 | elapsed: 41.96215ms
Year 2021 Day 12 Part 2: 98796 | elapsed: 2232.5784ms
Year 2021 Day 13 Part 1: 827 | elapsed: 0.1279944ms
Year 2021 Day 13 Part 2:
████  ██  █  █ █  █ ███  ████  ██  ███
█    █  █ █  █ █ █  █  █ █    █  █ █  █
███  █  █ ████ ██   █  █ ███  █    █  █
█    ████ █  █ █ █  ███  █    █    ███
█    █  █ █  █ █ █  █ █  █    █  █ █
████ █  █ █  █ █  █ █  █ ████  ██  █

 | elapsed: 1.1530516ms

Executed 13 days
Total execution time: 2471.3144267ms
```

### 2020

```bash
Year 2020 Day 01 Part 1: 786811 | elapsed: 0.0086779ms
Year 2020 Day 01 Part 2: 199068980 | elapsed: 0.0145027ms
Year 2020 Day 02 Part 1: 393 | elapsed: 0.4662213ms
Year 2020 Day 02 Part 2: 690 | elapsed: 0.2836846ms
Year 2020 Day 03 Part 1: 169 | elapsed: 0.0064722ms
Year 2020 Day 03 Part 2: 7560370818 | elapsed: 0.0300265ms
Year 2020 Day 04 Part 1: 256 | elapsed: 0.4052475ms
Year 2020 Day 04 Part 2: 198 | elapsed: 0.6364017ms
Year 2020 Day 05 Part 1: 970 | elapsed: 0.2967471ms
Year 2020 Day 05 Part 2: 587 | elapsed: 0.3683262ms
Year 2020 Day 06 Part 1: 6387 | elapsed: 0.4032194ms
Year 2020 Day 06 Part 2: 3039 | elapsed: 0.4764231ms
Year 2020 Day 07 Part 1: 372 | elapsed: 1.3428478ms
Year 2020 Day 07 Part 2: 8015 | elapsed: 0.4414277ms
Year 2020 Day 08 Part 1: 1487 | elapsed: 0.0258767ms
Year 2020 Day 08 Part 2: 1607 | elapsed: 0.9974862ms
Year 2020 Day 09 Part 1: 15353384 | elapsed: 0.1071802ms
Year 2020 Day 09 Part 2: 2466556 | elapsed: 0.1240964ms
Year 2020 Day 10 Part 1: 2592 | elapsed: 0.0066972ms
Year 2020 Day 10 Part 2: 198428693313536 | elapsed: 0.0068953ms
Year 2020 Day 11 Part 1: 2321 | elapsed: 366.8245ms
Year 2020 Day 11 Part 2: 2102 | elapsed: 602.8692ms
Year 2020 Day 12 Part 1: 1133 | elapsed: 0.0939886ms
Year 2020 Day 12 Part 2: 61053 | elapsed: 0.0470192ms
Year 2020 Day 13 Part 1: 23 | elapsed: 0.002684ms
Year 2020 Day 13 Part 2: 530015546283687 | elapsed: 0.0119545ms
Year 2020 Day 14 Part 1: 11884151942312 | elapsed: 0.7581982ms
Year 2020 Day 14 Part 2: 2625449018811 | elapsed: 59.212372ms
Year 2020 Day 15 Part 1: 758 | elapsed: 0.0684455ms
Year 2020 Day 15 Part 2: 814 | elapsed: 2236.2143ms
Year 2020 Day 16 Part 1: 23925 | elapsed: 0.0413177ms
Year 2020 Day 16 Part 2: 964373157673 | elapsed: 0.984199ms
Year 2020 Day 17 Part 1: 317 | elapsed: 35.5917ms
Year 2020 Day 17 Part 2: 1692 | elapsed: 1879.439ms
Year 2020 Day 18 Part 1: 209335026987 | elapsed: 2.6822717ms
Year 2020 Day 18 Part 2: 33331817392479 | elapsed: 2.7907225ms
Year 2020 Day 19 Part 1: 291 | elapsed: 74.8331ms
Year 2020 Day 19 Part 2: 409 | elapsed: 515.4075ms
Year 2020 Day 20 Part 1: 17032646100079 | elapsed: 0.476807ms
Year 2020 Day 20 Part 2: 2006 | elapsed: 9.628895ms
Year 2020 Day 21 Part 1: 1930 | elapsed: 0.4615ms
Year 2020 Day 21 Part 2: spcqmzfg,rpf,dzqlq,pflk,bltrbvz,xbdh,spql,bltzkxx | elapsed: 0.2643ms
Year 2020 Day 22 Part 1: 33694 | elapsed: 0.0382198ms
Year 2020 Day 22 Part 2: 31835 | elapsed: 1359.105ms
Year 2020 Day 23 Part 1: 82635947 | elapsed: 0.0357333ms
Year 2020 Day 23 Part 2: 157047826689 | elapsed: 30666.955ms
Year 2020 Day 24 Part 1: 434 | elapsed: 0.8704741ms
Year 2020 Day 24 Part 2: 3955 | elapsed: 274.928155ms
Year 2020 Day 25 Part 1: 5414549 | elapsed: 132.00218ms
Year 2020 Day 25 Part 2: None | elapsed: 0.000022ms

Executed 25 days
Total execution time: 38229.0872168ms
```

### 2019

```bash
Year 2019 Day 01 Part 1: 3349352 | elapsed: 0.0044072ms
Year 2019 Day 01 Part 2: 5021154 | elapsed: 0.0171842ms
Year 2019 Day 02 Part 1: 7210630 | elapsed: 0.0009791ms
Year 2019 Day 02 Part 2: 3892 | elapsed: 2.102143ms
Year 2019 Day 03 Part 1: 207 | elapsed: 114.31422ms
Year 2019 Day 03 Part 2: 21196 | elapsed: 117.83687ms
Year 2019 Day 04 Part 1: 2090 | elapsed: 19.858804ms
Year 2019 Day 04 Part 2: 1419 | elapsed: 21.429708ms
Year 2019 Day 05 Part 1: 13787043 | elapsed: 0.0073625ms
Year 2019 Day 05 Part 2: 3892695 | elapsed: 0.0130535ms
Year 2019 Day 06 Part 1: 158090 | elapsed: 2.9636498ms
Year 2019 Day 06 Part 2: 241 | elapsed: 27.089236ms
Year 2019 Day 07 Part 1: 38500 | elapsed: 1.367435ms
Year 2019 Day 07 Part 2: 33660560 | elapsed: 3.833035ms
Year 2019 Day 08 Part 1: 2375 | elapsed: 0.1454444ms
Year 2019 Day 08 Part 2: RKHRY | elapsed: 0.0400435ms
Year 2019 Day 09 Part 1: 3765554916 | elapsed: 0.0331515ms
Year 2019 Day 09 Part 2: 76642 | elapsed: 43.325904ms
Year 2019 Day 10 Part 1: 260 | elapsed: 71.925848ms
Year 2019 Day 10 Part 2: 608 | elapsed: 72.471558ms
Year 2019 Day 11 Part 1: 1564 | elapsed: 18.423316ms
Year 2019 Day 11 Part 2: RFEPCFEB | elapsed: 1.215635ms
Year 2019 Day 12 Part 1: 9743 | elapsed: 1.508265ms
Year 2019 Day 12 Part 2: 288684633706728 | elapsed: 186.957124ms
Year 2019 Day 13 Part 1: 296 | elapsed: 2.5787844ms
Year 2019 Day 13 Part 2: 13824 | elapsed: 90.078464ms
Year 2019 Day 14 Part 1: 899155 | elapsed: 0.2606767ms
Year 2019 Day 14 Part 2: 2390226 | elapsed: 9.5061372ms
Year 2019 Day 15 Part 1: 272 | elapsed: 14.003932ms
Year 2019 Day 15 Part 2: 398 | elapsed: 44.746735ms
Year 2019 Day 16 Part 1: 94960436 | elapsed: 133.94498ms
Year 2019 Day 16 Part 2: 57762756 | elapsed: 421.98583ms
Year 2019 Day 17 Part 1: 5972 | elapsed: 6.1179335ms
Year 2019 Day 17 Part 2: 933214 | elapsed: 19.427004ms
Year 2019 Day 18 Part 1: 5392 | elapsed: 5673.79465ms
Year 2019 Day 18 Part 2: 1684 | elapsed: 3193.2401ms
Year 2019 Day 19 Part 1: 215 | elapsed: 129.23172ms
Year 2019 Day 19 Part 2: 7720975 | elapsed: 568.27494ms
Year 2019 Day 20 Part 1: 588 | elapsed: 28.77302ms
Year 2019 Day 20 Part 2: 6834 | elapsed: 796.08235ms
Year 2019 Day 21 Part 1: 19359969 | elapsed: 3.905232ms
Year 2019 Day 21 Part 2: 1140082748 | elapsed: 87.629925ms
Year 2019 Day 22 Part 1: 7860 | elapsed: 4.64876ms
Year 2019 Day 22 Part 2: 61256063148970 | elapsed: 0.3739987ms
Year 2019 Day 23 Part 1: 17740 | elapsed: 2.386264ms
Year 2019 Day 23 Part 2: 12567 | elapsed: 31.946586ms
Year 2019 Day 24 Part 1: 13500447 | elapsed: 0.2742583ms
Year 2019 Day 24 Part 2: 2120 | elapsed: 398.74368ms
Year 2019 Day 25 Part 1: 1090617344 | elapsed: 427.11892ms
Year 2019 Day 25 Part 2: None | elapsed: 0ms

Executed 25 days
Total execution time: 12795.9592575ms
```

### 2018

```bash
Year 2018 Day 01 Part 1: 543 | elapsed: 0.0515901ms
Year 2018 Day 01 Part 2: 621 | elapsed: 4.909224ms
Year 2018 Day 02 Part 1: 6200 | elapsed: 1.93746ms
Year 2018 Day 02 Part 2: xpysnnkqrbuhefmcajodplyzw | elapsed: 0.8463708ms
Year 2018 Day 03 Part 1: 108961 | elapsed: 9.645128ms
Year 2018 Day 03 Part 2: 681 | elapsed: 5.407625ms
Year 2018 Day 04 Part 1: 72925 | elapsed: 1.7227756ms
Year 2018 Day 04 Part 2: 49137 | elapsed: 1.650128ms
Year 2018 Day 05 Part 1: 10132 | elapsed: 4.822787ms
Year 2018 Day 05 Part 2: 4572 | elapsed: 132.990384ms
Year 2018 Day 06 Part 1: 3890 | elapsed: 151.609038ms
Year 2018 Day 06 Part 2: 40284 | elapsed: 29.832877ms
Year 2018 Day 07 Part 1: JDEKPFABTUHOQSXVYMLZCNIGRW | elapsed: 0.4008654ms
Year 2018 Day 07 Part 2: 1048 | elapsed: 0.3273877ms
Year 2018 Day 08 Part 1: 44893 | elapsed: 2.5961902ms
Year 2018 Day 08 Part 2: 27433 | elapsed: 2.0595968ms
Year 2018 Day 09 Part 1: 386018 | elapsed: 4.1090338ms
Year 2018 Day 09 Part 2: 3085518618 | elapsed: 1753.6176ms
Year 2018 Day 10 Part 1: GFANEHKJ | elapsed: 2.42283ms
Year 2018 Day 10 Part 2: 10086 | elapsed: 4.15142ms
Year 2018 Day 11 Part 1: 233,36 | elapsed: 20.602678ms
Year 2018 Day 11 Part 2: 231,107,14 | elapsed: 693.95294ms
Year 2018 Day 12 Part 1: 1184 | elapsed: 0.1554424ms
Year 2018 Day 12 Part 2: 250000000219 | elapsed: 1.279797ms
Year 2018 Day 13 Part 1: 139,65 | elapsed: 1.986422ms
Year 2018 Day 13 Part 2: 40,77 | elapsed: 32.149624ms
Year 2018 Day 14 Part 1: 6985103122 | elapsed: 35.157968ms
Year 2018 Day 14 Part 2: 20182290 | elapsed: 2982.88416ms
Year 2018 Day 15 Part 1: 208960 | elapsed: 6721.5408ms
Year 2018 Day 15 Part 2: YAY! Score: 49863 | elapsed: 3851.0673ms
Year 2018 Day 16 Part 1: 560 | elapsed: 5.3461405ms
Year 2018 Day 16 Part 2: 622 | elapsed: 5.6885785ms
Year 2018 Day 17 Part 1: 38364 | elapsed: 9.79965ms
Year 2018 Day 17 Part 2: 30551 | elapsed: 9.360566ms
Year 2018 Day 18 Part 1: 515496 | elapsed: 5.422366ms
Year 2018 Day 18 Part 2: 233058 | elapsed: 324.62638ms
Year 2018 Day 19 Part 1: 1228 | elapsed: 0.0007942ms
Year 2018 Day 19 Part 2: 15285504 | elapsed: 0.0429107ms
Year 2018 Day 20 Part 1: 3669 | elapsed: 74.57828ms
Year 2018 Day 20 Part 2: 8369 | elapsed: 71.563948ms
Year 2018 Day 21 Part 1: 9959629 | elapsed: 0.000082ms
Year 2018 Day 21 Part 2: 12691260 | elapsed: 0.93506ms
Year 2018 Day 22 Part 1: 7743 | elapsed: 0.3281431ms
Year 2018 Day 22 Part 2: 1029 | elapsed: 8700.9276ms
Year 2018 Day 23 Part 1: 172 | elapsed: 0.856021ms
Year 2018 Day 23 Part 2: 125532607 | elapsed: 41.684714ms
Year 2018 Day 24 Part 1: Infection won with 21070 units left. | elapsed: 5.5124785ms
Year 2018 Day 24 Part 2: ImmuneSystem won with 7500 units left. | elapsed: 157.81212ms
Year 2018 Day 25 Part 1: 383 | elapsed: 49.766086ms
Year 2018 Day 25 Part 2: None | elapsed: 0ms

Executed 25 days
Total execution time: 25920.1393613ms
```


### 2017

```bash
Year 2017 Day 01 Part 1: 1047 | elapsed: 0.1166683ms
Year 2017 Day 01 Part 2: 982 | elapsed: 0.1325896ms
Year 2017 Day 02 Part 1: 36766 | elapsed: 0.0275171ms
Year 2017 Day 02 Part 2: 261 | elapsed: 0.0294527ms
Year 2017 Day 03 Part 1: 326 | elapsed: 2.863259ms
Year 2017 Day 03 Part 2: 363010 | elapsed: 0.1364698ms
Year 2017 Day 04 Part 1: 325 | elapsed: 0.428465ms
Year 2017 Day 04 Part 2: 119 | elapsed: 1.691427ms
Year 2017 Day 05 Part 1: 375042 | elapsed: 1.914606ms
Year 2017 Day 05 Part 2: 28707598 | elapsed: 292.42309ms
Year 2017 Day 06 Part 1: 4074 | elapsed: 7.721882ms
Year 2017 Day 06 Part 2: 2793 | elapsed: 5.930668ms
Year 2017 Day 07 Part 1: eugwuhl | elapsed: 3.7592985ms
Year 2017 Day 07 Part 2: 420 | elapsed: 5.817002ms
Year 2017 Day 08 Part 1: 4163 | elapsed: 0.9499582ms
Year 2017 Day 08 Part 2: 5347 | elapsed: 0.9368482ms
Year 2017 Day 09 Part 1: 16021 | elapsed: 7.767455ms
Year 2017 Day 09 Part 2: 7685 | elapsed: 7.746585ms
Year 2017 Day 10 Part 1: 9656 | elapsed: 0.0227906ms
Year 2017 Day 10 Part 2: 20B7B54C92BF73CF3E5631458A715149 | elapsed: 1.5952278ms
Year 2017 Day 11 Part 1: 808 | elapsed: 1.02083ms
Year 2017 Day 11 Part 2: 1556 | elapsed: 1.1439058ms
Year 2017 Day 12 Part 1: 288 | elapsed: 7.424278ms
Year 2017 Day 12 Part 2: 211 | elapsed: 16.04008ms
Year 2017 Day 13 Part 1: 1904 | elapsed: 0.009163ms
Year 2017 Day 13 Part 2: 3833504 | elapsed: 309.41788ms
Year 2017 Day 14 Part 1: 8316 | elapsed: 77.09383ms
Year 2017 Day 14 Part 2: 1074 | elapsed: 295.63583ms
Year 2017 Day 15 Part 1: 594 | elapsed: 1480.4198666666666666666666667ms
Year 2017 Day 15 Part 2: 328 | elapsed: 1369.5109ms
Year 2017 Day 16 Part 1: ionlbkfeajgdmphc | elapsed: 6.617634ms
Year 2017 Day 16 Part 2: fdnphiegakolcmjb | elapsed: 100.66167ms
Year 2017 Day 17 Part 1: 355 | elapsed: 5.42743ms
Year 2017 Day 17 Part 2: 6154117 | elapsed: 0.07914ms
Year 2017 Day 18 Part 1: 2951 | elapsed: 0.0091044ms
Year 2017 Day 18 Part 2: 7366 | elapsed: 33.03809ms
Year 2017 Day 19 Part 1: PBAZYFMHT | elapsed: 1.393356ms
Year 2017 Day 19 Part 2: 16072 | elapsed: 1.451147ms
Year 2017 Day 20 Part 1: 157 | elapsed: 29.31118ms
Year 2017 Day 20 Part 2: 499 | elapsed: 35.40463ms
Year 2017 Day 21 Part 1: 164 | elapsed: 1.068074ms
Year 2017 Day 21 Part 2: 2355110 | elapsed: 980.54224ms
Year 2017 Day 22 Part 1: 5433 | elapsed: 2.074893ms
Year 2017 Day 22 Part 2: 2512599 | elapsed: 3657.8596ms
Year 2017 Day 23 Part 1: 9409 | elapsed: 16.464606ms
Year 2017 Day 23 Part 2: 913 | elapsed: 0.1765301ms
Year 2017 Day 24 Part 1: 1656 | elapsed: 9066.95145ms
Year 2017 Day 24 Part 2: 1642 | elapsed: 61.698892ms
Year 2017 Day 25 Part 1: 3099 | elapsed: 395.119464ms
Year 2017 Day 25 Part 2: Nothing to do! Yay! | elapsed: 0ms

Executed 25 days
Total execution time: 18295.076953766666666666666667ms
```


### 2016

```bash
Loading...
```


### 2015

```bash
Loading...
```


### Notes

Note1: do you have a way to improve the algorithms? Amazing! Send a pull request or send me a message, I'll be happy to discuss!       
Note2: the elapsed time is the average time of N executions of the challenge on my machine. The N value defaults to 1000 and is defined on each challenge.

