﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/21

    public class Day21 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 1;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 1;

        /// <summary>
        /// Count the times ingredients with allergens appear in the list.
        /// <para> Avg execution time: 3.918ms | O(n) </para>
        /// </summary>        
        public override string Part1()
        {
            var parsedList = ParseInputToIngredients(input);
            var mappedAllergens = MapAllergensToIngredients(parsedList);
            var allergensInStrangeLanguage = mappedAllergens.Select(a => a.Value).ToHashSet();

            var allIngredients = parsedList.SelectMany(x => x.ingredients);
            return allIngredients.Count(i => !allergensInStrangeLanguage.Contains(i)).ToString();
        }

        /// <summary>
        /// Print the list of ingredients with allergens.
        /// <para> Avg execution time: 3.090ms | O(n) </para>
        /// </summary>
        public override string Part2()
        {
            var parsedList = ParseInputToIngredients(input);
            var mappedAllergens = MapAllergensToIngredients(parsedList);
            var canonicalDangerousIngredientsList = mappedAllergens
                .Select(x => (allergen: x.Key, ingredient: x.Value))
                .OrderBy(x => x.allergen)
                .Select(x => x.ingredient);

            return string.Join(',', canonicalDangerousIngredientsList);
        }

        private IEnumerable<(string[] ingredients, string[] allergens)> ParseInputToIngredients(string[] input)
        {
            var result = new List<(string[] ingredients, string[] allergens)>();
            foreach (string line in input)
            {
                var separator = line.IndexOf(" (contains");
                var ingredients = line[..separator].Trim().Split(" ");
                var allergens = line[separator..].Replace(" (contains", "").Replace(")", "").Trim().Split(", ");
                result.Add((ingredients, allergens));
            }
            return result;
        }

        private static Dictionary<string, string> MapAllergensToIngredients(
            IEnumerable<(string[] ingredients, string[] allergens)> parsedList)
        {
            var associations = new Dictionary<string, HashSet<string>>();
            foreach ((string[] ingredients, string[] allergens) in parsedList)
                foreach (string allergen in allergens)
                    if (associations.ContainsKey(allergen))
                        associations[allergen].IntersectWith(ingredients);
                    else
                        associations[allergen] = ingredients.ToHashSet();

            List<(string, HashSet<string>)> associationsList = associations
                .Select(x => (allergen: x.Key, ingredients: x.Value)).ToList();

            var allergensMapping = new Dictionary<string, string>();
            while (associationsList.Count > 0)
            {
                var sureMapping = associationsList.First(x => x.Item2.Count == 1);
                var allergen = sureMapping.Item1;
                var ingredient = sureMapping.Item2.First();

                allergensMapping.Add(allergen, ingredient);

                associationsList.Remove(sureMapping);
                associationsList.ForEach(e => e.Item2.Remove(ingredient));
            }

            return allergensMapping;
        }

    }
}