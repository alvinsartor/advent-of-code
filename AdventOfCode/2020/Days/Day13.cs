﻿using AdventOfCode.helpers;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/13

    public class Day13 : DailyChallenge
    {
        /// <summary>
        /// Find the number of the first departing bus.
        /// <para> Avg 1000 executions: 0.007ms | O(n) </para>
        /// </summary>
        public override string Part1() {
            var minutes = int.Parse(input[0]);
            var busses = input[1].Split(",").Where(x => x != "x").Select(int.Parse);
            var timesToWait = busses.Select(bus => (busNumber:bus, timeToWait:bus - minutes % bus));
            var (busNumber, timeToWait) = timesToWait.MinBy(x => x.timeToWait);
            return busNumber.ToString();
        }

        /// <summary>
        /// Find the first timestamp such as each bus departs at I minute difference from each other.
        /// <para> Avg 1000 executions: 0.015ms | O(n) </para>
        /// </summary>
        public override string Part2()
        {
            var busses = input[1]
                .Split(",")
                .Select((bus, order) => (bus, order))
                .Where(bus => bus.bus != "x")
                .Select(bus => (busNumber: int.Parse(bus.bus), bus.order))
                .ToList();

            var nValues = busses.Select(x => x.busNumber).ToList();
            var aValues = busses.Select(x => x.busNumber - x.order % x.busNumber).ToList();

            return ChineseReminderTheorem(nValues, aValues).ToString();
        }

        private static long ChineseReminderTheorem(List<int> nValues, List<int> aValues)
        {
            long N = nValues.Aggregate((long)1, (curr, n) => curr * n);
            long sum = 0;
            for (int i = 0; i < nValues.Count; i++)
            {
                long y = N / nValues[i];
                sum += aValues[i] * ModularMultiplicativeInverse(y, nValues[i]) * y;
            }

            return sum % N;
        }

        private static long ModularMultiplicativeInverse(long a, int mod)
        {
            long b = a % mod;
            for (long x = 1; x < mod; x++)
                if (b * x % mod == 1)
                    return x;
            return 1;
        }
    }
}