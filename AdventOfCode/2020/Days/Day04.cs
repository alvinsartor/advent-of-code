﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/4

    public class Day04 : DailyChallenge
    {
        // The data file contains passport fragments. Each fragment has the format: "CODE:VALUE"
        // Passports are separated by newlines.
        // Count the number of valid passports.

        /// <summary>
        /// A passport is valid if it contains all the required fields.
        /// <para> Avg 1000 executions: 0.701ms | O(n) </para>
        /// </summary>
        public override string Part1()
        {
            var adjustedInput = input.Append("");

            int counter = 0;
            var passportValidator = new SimplePassportValidator();
            foreach (string line in adjustedInput)
                counter += AddLineToPassoportValidator(line, passportValidator);

            return counter.ToString();
        }

        /// <summary>
        /// A passport is valid if it contains all the required fields AND the each field is correctly validated.
        /// <para> Avg 1000 executions: 0.783ms | O(n) </para>
        /// </summary>
        public override string Part2()
        {
            var adjustedInput = input.Append("");

            int counter = 0;
            var passportValidator = new FieldPassportValidator();
            foreach (string line in adjustedInput)
                counter += AddLineToPassoportValidator(line, passportValidator);

            return counter.ToString();
        } 

        private static int AddLineToPassoportValidator(string line, PassportValidator validator)
        {
            if (!string.IsNullOrEmpty(line))
            {
                foreach (string entry in line.Split(" ")) validator.AddEntry(entry);
                return 0;
            }

            var result = validator.IsValid() ? 1 : 0;
            validator.Clear();
            return result;
        }

        abstract class PassportValidator
        {
            private readonly string[] _requiredFields = new string[] { "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid" };
            private HashSet<string> _missingFields;

            public PassportValidator() => _missingFields = _requiredFields.ToHashSet();

            public void Clear() => _missingFields = _requiredFields.ToHashSet();
            public bool IsValid() => _missingFields.Count == 0;

            protected void ValidateField(string field) => _missingFields.Remove(field);
            public abstract void AddEntry(string entry);
        }

        /// <summary>
        /// Validator implementing a simple method: if a field is passed in, it is validated.
        /// </summary>
        /// <seealso cref="PassportValidator" />
        private class SimplePassportValidator : PassportValidator {
            /// <summary>
            /// Adds an entry to the passport. Format: 'CODE:VALUE'.
            /// </summary>
            public override void AddEntry(string entry) => ValidateField(entry.Split(":")[0]);
        }

        /// <summary>
        /// Validator implementing a more complex method: a field must be present and its value must pass certain rules in order to be considered valid.
        /// </summary>
        /// <seealso cref="PassportValidator" />
        private class FieldPassportValidator : PassportValidator
        {

            public override void AddEntry(string entry)
            {
                var split = entry.Split(":");
                var code = split[0];
                var value = split[1];
                if (validations.ContainsKey(code) && validations[code](value))
                    ValidateField(code);
            }

            private readonly Dictionary<string, Func<string, bool>> validations = new Dictionary<string, Func<string, bool>>
            {
                { "ecl", ValidateEcl },
                { "byr", ValidateByr },
                { "iyr", ValidateIyr },
                { "eyr", ValidateEyr },
                { "hgt", ValidateHgt },
                { "hcl", ValidateHcl },
                { "pid", ValidatePid },
            };

            private static readonly HashSet<string> eclPossibilities = new HashSet<string>(new string[] { "amb", "blu", "brn", "gry", "grn", "hzl", "oth" });
            private static bool ValidateEcl(string ecl) => eclPossibilities.Contains(ecl);
            private static bool ValidateByr(string byr) => int.TryParse(byr, out int res) && res >= 1920 && res <= 2002; 
            private static bool ValidateIyr(string iyr) => int.TryParse(iyr, out int res) && res >= 2010 && res <= 2020; 
            private static bool ValidateEyr(string eyr) => int.TryParse(eyr, out int res) && res >= 2020 && res <= 2030; 
            
            private static bool ValidateHgt(string hgt)
            {
                if (hgt.Length < 4) return false;
                var unit = hgt[^2..];
                var value = hgt[0..^2];
                return unit == "cm" ? ValidateHgtCm(value) : unit == "in" ? ValidateHgtIn(value) : false; 
            } 
            private static bool ValidateHgtCm(string hgt) => int.TryParse(hgt, out int res) && res >= 150 && res <= 193; 
            private static bool ValidateHgtIn(string hgt) => int.TryParse(hgt, out int res) && res >= 59 && res <= 76;

            private static bool ValidateHcl(string hcl) => hcl[0] == '#'
                && hcl.Skip(1).All(c => (c >= '0' && c <= '9') || (c >= 'a' && c <= 'f'));
            private static bool ValidatePid(string pid) => pid.Length == 9
                && pid.All(c => c >= '0' && c <= '9');
        }
    }
}
