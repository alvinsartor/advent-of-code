using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/22

    public class Day22 : DailyChallenge
    {
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 1;

        private (LinkedList<int> deck1, LinkedList<int> deck2) GetDecks()
        {
            var endOfFirstDeck = 0;
            while (input[endOfFirstDeck] != "") endOfFirstDeck++;
            var beginningOfSecondDeck = endOfFirstDeck + 2;

            var deck1 = input[1..endOfFirstDeck].Select(int.Parse).ToLinkedList();
            var deck2 = input[beginningOfSecondDeck..].Select(int.Parse).ToLinkedList();

            return (deck1, deck2);
        }

        /// <summary>
        /// Find the winner of a card game.
        /// <para> Avg execution time: 0.053ms </para>
        /// </summary>        
        public override string Part1()
        {
            var (deck1, deck2) = GetDecks();
            var winner = Play(deck1, deck2);

            var score = winner.Select((x, i) => x * (winner.Length - i)).Sum();
            return score.ToString();
        }

        private int[] Play(LinkedList<int> p1, LinkedList<int> p2)
        {
            if (p1.Count == 0) return p2.ToArray();
            if (p2.Count == 0) return p1.ToArray();

            var p1Card = p1.First!.Value; p1.RemoveFirst();
            var p2Card = p2.First!.Value; p2.RemoveFirst();

            var winner = p1Card > p2Card ? p1 : p2;
            winner.AddLast(Math.Max(p1Card, p2Card));
            winner.AddLast(Math.Min(p1Card, p2Card));

            return Play(p1, p2);
        }

        /// <summary>
        /// Same as before, but now the card game has recursive rules.
        /// <para> Avg execution time: 1465ms </para>
        /// </summary>
        public override string Part2()
        {
            var (deck1, deck2) = GetDecks();
            var (winnerDeck, _) = PlayRecursive(deck1, deck2);

            var score = winnerDeck.Select((x, i) => x * (winnerDeck.Length - i)).Sum();
            return score.ToString();
        }

        private (int[] deck, bool wasP1) PlayRecursive(LinkedList<int> p1, LinkedList<int> p2)
        {
            var playedMatches = new HashSet<string>();
            while(p1.Count > 0 && p2.Count > 0)
            {
                string configuration = ConfigurationToString(p1, p2);
                if (playedMatches.Contains(configuration)) return (Array.Empty<int>(), true);
                playedMatches.Add(configuration);

                var p1Card = p1.First!.Value; p1.RemoveFirst();
                var p2Card = p2.First!.Value; p2.RemoveFirst();

                if (p1.Count >= p1Card && p2.Count >= p2Card)
                {
                    // recursive combat!
                    var deck1ForRecursive = p1.Take(p1Card).ToLinkedList();
                    var deck2ForRecursive = p2.Take(p2Card).ToLinkedList();
                    var (_, p1WonTheGame) = PlayRecursive(deck1ForRecursive, deck2ForRecursive);

                    (LinkedList<int> winnerDeck, var winnerCard, var loserCard) = p1WonTheGame
                        ? (p1, p1Card, p2Card)
                        : (p2, p2Card, p1Card);

                    winnerDeck.AddLast(winnerCard);
                    winnerDeck.AddLast(loserCard);
                }
                else
                {
                    var winnerDeck = p1Card > p2Card ? p1 : p2;
                    winnerDeck.AddLast(Math.Max(p1Card, p2Card));
                    winnerDeck.AddLast(Math.Min(p1Card, p2Card));
                }
            }

            return p1.Count == 0 ? (p2.ToArray(), false) : (p1.ToArray(), true);
        }

        private static string ConfigurationToString(LinkedList<int> p1, LinkedList<int> p2) =>
            string.Join(".", p1) + " - " + string.Join(".", p2);
    }
}