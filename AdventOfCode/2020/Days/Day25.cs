﻿
namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/25

    public class Day25 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 10;

        /// <summary>
        /// Given a couple of public keys, find the value to unlock the door.
        /// <para> Avg execution time: 141ms | O(L) [L = loop size;] </para>
        /// </summary>        
        public override string Part1()
        {
            var publicKeyCard = int.Parse(input[0]);
            var publicKeyDoor = int.Parse(input[1]);
            var doorLoopSize = GetLoopSize(publicKeyDoor);
            long handshakeValue = RepeatTransformation(publicKeyCard, doorLoopSize);
            return handshakeValue.ToString();
        }

        private static int GetLoopSize(int publicKey)
        {
            long n = 1;
            var counter = 1;
            while ((n = TransformWithSubjectNumber(n, 7)) != publicKey) counter++;
            return counter;
        }

        private static long TransformWithSubjectNumber(long number, long subjectNumber) => 
            number * subjectNumber % 20201227;

        private static long RepeatTransformation(long subjectNumber, int loopSize)
        {
            long number = 1;
            while (loopSize-- > 0) number = TransformWithSubjectNumber(number, subjectNumber);
            return number;
        }

        /// <summary> Not needed. The star is given if all the other challenges are solved. </summary>
        public override string Part2() => "None";
    }
}