﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/11

    public class Day11 : DailyChallenge
    {
        private readonly int columns;
        private readonly int rows;
        private readonly char[][] seats;
        public Day11()
        {
            rows = input.Length;
            columns = input[0].Length;
            seats = input.Select(x => x.ToCharArray()).ToArray();
        }

        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 1;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 1;

        /// <summary>
        /// Given a map N*M of a waiting room and some rules people use to choose a seat, 
        /// compute the rules until the situation stabilizes and count the number of taken seats.
        /// /// <para> Avg execution time: 614ms | O(nmk) </para>
        /// </summary>
        public override string Part1() => SimulateSeatsFlow(ImmediatelyAdjacentSeats, 4).ToString();

        /// <summary>
        /// Like part 1, but with slightly modified rules.
        /// /// <para> Avg execution time: 696ms | O(nmkq) </para>
        /// </summary>
        public override string Part2() => SimulateSeatsFlow(FirstVisibleAdjacentSeats, 5).ToString();

        private int SimulateSeatsFlow(
            Func<int, int, char[][], IEnumerable<char?>> adjacentSeatsFunction,
            int filledAdjacentsForPeopleToLeave)
        {
            bool hasChanged = false;
            var inputCopy = seats.ToArray();

            do
            {
                hasChanged = false;
                var newInputState = inputCopy
                    .Select((line, row) => line
                        .Select((cell, col) =>
                            {
                                var adjacent = adjacentSeatsFunction(row, col, inputCopy);
                                var newState = GetNewSeatState(inputCopy[row][col], adjacent, filledAdjacentsForPeopleToLeave);
                                if (cell != newState) hasChanged = true;
                                return newState;
                            })
                        .AsParallel().ToArray())
                    .AsParallel().ToArray();

                inputCopy = newInputState.ToArray();
            } while (hasChanged);

            return inputCopy.Sum(x => x.Count(s => s == '#'));
        }

        private IEnumerable<char?> ImmediatelyAdjacentSeats(int row, int col, char[][] input)
        {
            for (int i = Math.Max(0, row - 1); i <= Math.Min(row + 1, rows - 1); i++)
                for (int j = Math.Max(0, col - 1); j <= Math.Min(col + 1, columns - 1); j++)
                    if (!(i == row && j == col))
                        yield return (input[i][j]);
        }

        private static char GetNewSeatState(char currentState, IEnumerable<char?> adjacent, int limit = 4)
        {
            var l = adjacent.ToList();
            return currentState switch
            {
                '.' => '.',
                'L' => adjacent.All(s => s != '#') ? '#' : 'L',
                '#' => adjacent.Count(s => s == '#') >= limit ? 'L' : '#',
                _ => throw new Exception("")
            };
        }

        private static readonly Vector2[] directions =
            {
                new Vector2(-1,-1), new Vector2(-1, 0), new Vector2(-1, 1), new Vector2( 0,-1),
                new Vector2( 0, 1), new Vector2( 1,-1), new Vector2( 1, 0), new Vector2( 1, 1),
            };

        private IEnumerable<char?> FirstVisibleAdjacentSeats(int row, int col, char[][] input)
        {
            foreach (var d in directions)
                yield return FirstSeatInDirection(d, row, col, input);
        }

        private static char? FirstSeatInDirection(Vector2 direction, int row, int col, char[][] input)
        {
            bool isOutsideRange(int r, int c) => r < 0 || c < 0 || r >= input.Length || c >= input[r].Length;
            bool isV2OutsideRange(Vector2 pos) => isOutsideRange((int)pos.Y, (int)pos.X);

            var currentPos = direction + new Vector2(col, row);
            while (!isV2OutsideRange(currentPos))
            {
                var seat = input[(int)currentPos.Y][(int)currentPos.X];
                if (seat != '.') return seat;
                currentPos += direction;
            }

            return null;
        }
    }
}
