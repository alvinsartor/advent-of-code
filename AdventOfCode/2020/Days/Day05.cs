﻿using System;
using System.Linq;

namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/5

    public class Day05 : DailyChallenge
    {
        /// <summary>
        /// Finds the row and column of each ticket, calculates the id and returns the maximum ID.
        /// <para> Avg 1000 executions: 0.510ms | O(n) </para>
        /// </summary>
        public override string Part1() => input.Select(code => (GetRow(code) * 8) + GetColumn(code)).Max().ToString();

        /// <summary>
        /// By checking all the other ticket's IDs, it finds the missing ID (surrounded by existing ones).
        /// <para> Avg 1000 executions: 0.600ms | O(n) </para>
        /// </summary>
        public override string Part2()
        {
            var ids = input.Select(code => (GetRow(code) * 8) + GetColumn(code)).OrderBy(x => x).ToArray();
            for (var i = 0; i < ids.Length - 1; i++)
                if (ids[i + 1] - ids[i] == 2) 
                    return (ids[i] + 1).ToString();

            return "?";
        }

        private static int GetRow(string fullCode) => GetRow(0, 127, fullCode[..7]);
        private static int GetRow(int lowerBound, int upperBound, string code)
        {
            if (lowerBound == upperBound) return lowerBound;
            var portion = (int)Math.Ceiling((upperBound - lowerBound) / 2.0);
            if (code[0] == 'F') return GetRow(lowerBound, upperBound - portion, code[1..]); 
            if (code[0] == 'B') return GetRow(lowerBound + portion, upperBound, code[1..]);

            return -1;
        }

        private static int GetColumn(string fullCode) => GetColumn(0, 7, fullCode[7..]);
        private static int GetColumn(int lowerBound, int upperBound, string code)
        {
            if (lowerBound == upperBound) return lowerBound;
            var portion = (int)Math.Ceiling((upperBound - lowerBound) / 2.0);
            if (code[0] == 'L') return GetColumn(lowerBound, upperBound - portion, code[1..]);
            if (code[0] == 'R') return GetColumn(lowerBound + portion, upperBound, code[1..]);

            return -1;
        }
    }

}
