﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/9

    public class Day09 : DailyChallenge
    {
        private readonly long[] intInput;
        public Day09() : base() => intInput = input.Select(long.Parse).ToArray();

        /// <summary>
        /// Given a list of numbers, find the first number that cannot be express as a sum of 2 of the previous 'k' numbers.
        /// <para>We use a linkedlist to queue and track the 'k' elements and a hashset (in sync with the list) to access the numbers in the fastest possible way.</para>
        /// <para> Avg 1000 executions: 0.113ms | O(nk) </para>
        /// </summary>
        public override string Part1()
        {
            var preamble = 25;
            var hashList = new XMasCracker();
            return hashList.FindNumberBreakingXMasCypher(intInput, preamble).ToString();
        }

        /// <summary>
        /// Find the interval of contiguous numbers that summed gives the number found in Part1. 
        /// Once the interval is found, return the sum between first and the last number.
        /// <para> A simple linkedlist and an accumulator allows us to track the moving interval without 
        /// having to sum it all the times, granting a low complexity. </para>
        /// <para> Avg 1000 executions: 0.127ms | O(nk) (O(nk) to compute the number + O(n) to find the interval) </para>
        /// </summary>
        public override string Part2()
        {
            long cypherBreakingNumber = long.Parse(Part1());
            var accumulatorList = new LinkedList<long>();
            long accumulator = 0;
            int i = 0;

            do
            {
                if (accumulator < cypherBreakingNumber || accumulatorList.Count < 2)
                {
                    var number = intInput[i++];
                    accumulatorList.AddLast(number);
                    accumulator += number;
                }                
                else if (accumulator > cypherBreakingNumber)
                {
                    accumulator -= accumulatorList.First!.Value;
                    accumulatorList.RemoveFirst();
                }
                else
                {
                    return (accumulatorList.Min() + accumulatorList.Max()).ToString();
                } 
            } while (i < intInput.Length);

            return "?";
        }

        /// <summary>
        /// Class combining a linkedlist and a hashset to use the strong suits of both data structures at once: 
        /// constant complexity hash access and queueing of elements.
        /// </summary>
        private class XMasCracker
        {
            protected readonly LinkedList<long> list = new LinkedList<long>();
            protected readonly HashSet<long> hash = new HashSet<long>();

            private void Add(long value)
            {
                list.AddLast(value);
                hash.Add(value);
            }
            private void RemoveFirst()
            {
                hash.Remove(list.First!.Value);
                list.RemoveFirst();
            }
            private bool HasNumbersSummingTo(long value)
            {
                foreach (var element in list)
                    if (hash.Contains(value - element))
                        return true;
                return false;
            }
            public long FindNumberBreakingXMasCypher(long[] input, int preamble)
            {
                for (int i = 0; i < input.Length; i++)
                {
                    var number = input[i];
                    if (i > preamble)
                    {
                        if (!HasNumbersSummingTo(number)) return number;
                        RemoveFirst();
                    }
                    Add(number);
                }
                return -1;
            }
        }
    }
}
