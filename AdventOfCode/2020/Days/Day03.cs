﻿using System.Linq;
using System.Numerics;

namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/3

    class Day03 : DailyChallenge
    {
        // Given a map in which '.' is an empty space and '#' is a tree, an initial position (position 0,0) 
        // and a direction (??); count the number of trees ('#') on your way.

        /// <summary>
        /// Count the trees on your way if your direction was [3,1].
        /// <para> Avg 1000 executions: 0.012ms | O(n) </para>
        /// </summary>
        public override string Part1()
        {
            Vector2 direction = new Vector2(3, 1);            
            return CountTreesOnTheGivenDirection(direction).ToString();
        }

        /// <summary>
        /// Given multiple directions, count the trees on your way and multiply the result of each direction.
        /// <para> Avg 1000 executions: 0.012ms | O(n) </para>
        /// </summary>
        public override string Part2()
        {
            Vector2[] directions =
            {
                new Vector2(1, 1),
                new Vector2(3, 1),
                new Vector2(5, 1),
                new Vector2(7, 1),
                new Vector2(1, 2),
            };

            var treesByDirection = directions.Select(CountTreesOnTheGivenDirection);
            var result = treesByDirection.Aggregate(1.0, (curr, next) => curr * next);
            return result.ToString();
        }

        private int CountTreesOnTheGivenDirection(Vector2 direction)
        {
            int step = 1;
            int counter = 0;
            while (direction.Y * step < input.Length)
                if (GetCharAtCoordinate(direction * step++) == '#')
                    counter++;
            return counter;
        }

        private char GetCharAtCoordinate(Vector2 v) => input[(int)v.Y][(int)v.X % input[0].Length];        
    }
}