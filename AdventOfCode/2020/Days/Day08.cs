﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/8

    public class Day08 : DailyChallenge
    {
        /// <summary>
        /// Given a set of instruction, print the value of Accumulator before the excution enters to an infinite loop.
        /// <para> Avg 1000 executions: 0.037ms | O(n) </para>
        /// </summary>
        public override string Part1()
        {
            var computer = new Computer();
            var aliasInput = new AliasInput(input);
            computer.Execute(aliasInput);
            return computer.Accumulator.ToString();
        }

        /// <summary>
        /// Repair the instruction that corrupted the program and print the value of Accumulator at the end of the execution.
        /// The corrupted instruction is either a nop (that must be changed to jmp) or jmp (that must be changed to nop).
        /// <para> Avg 1000 executions: 1.589ms | O(n) </para>
        /// </summary>
        public override string Part2()
        {
            var computer = new Computer();
            var aliasInput = new AliasInput(input);

            computer.Execute(aliasInput);
            var jmp = computer.EcecutedInstructions.Where(i => input[i].Contains("jmp")).ToList();
            var nop = computer.EcecutedInstructions.Where(i => input[i].Contains("nop")).ToList();

            foreach (int i in jmp)
            {
                aliasInput.SetAlias(i, input[i].Replace("jmp", "nop"));
                computer.Execute(aliasInput);
                if (!computer.TerminatedForcefully) return computer.Accumulator.ToString();
            }
            foreach(int i in nop)
            {
                aliasInput.SetAlias(i, input[i].Replace("nop", "jmp"));
                computer.Execute(aliasInput);
                if (!computer.TerminatedForcefully) return computer.Accumulator.ToString();
            }

            return "?";
        }

        /// <summary>
        /// Class able to execute the text instructions.
        /// </summary>
        private class Computer
        {
            public int Accumulator { get; private set; } = 0;
            public int LastInstruction { get; private set; } = 0;
            public bool TerminatedForcefully { get; private set; } = false;
            public HashSet<int> EcecutedInstructions => executedLines.ToHashSet();
            
            public HashSet<int> executedLines = new HashSet<int>();
            private int index = 0;

            public void Execute(AliasInput instructions)
            {
                Reinitialize();

                while (!executedLines.Contains(index) && index < instructions.Length)
                {
                    executedLines.Add(index);
                    LastInstruction = index;
                    var currentInstruction = instructions[index].Split(" ");
                    var action = currentInstruction[0];

                    switch (action)
                    {
                        case "acc":
                            Accumulator += int.Parse(currentInstruction[1]);
                            index++;
                            break;
                        case "nop":
                            index++;
                            break;
                        case "jmp":
                            index += int.Parse(currentInstruction[1]);
                            break;
                        default:
                            throw new Exception("Unrecognized command: " + action);
                    }
                }

                TerminatedForcefully = executedLines.Contains(index);
            }

            private void Reinitialize()
            {
                Accumulator = 0;
                LastInstruction = 0;
                TerminatedForcefully = false;
                index = 0;
                executedLines = new HashSet<int>();
            }
        }

        /// <summary>
        /// Class encapsulating the input array that allows to swap 1 line with an arbitrary string, 
        /// without modifying (or copying over) the original array.
        /// </summary>
        private class AliasInput
        {
            private readonly string[] collection;
            private int aliasIndex = -1;
            private string aliasValue = "";

            public AliasInput(string[] originalInput) => collection = originalInput;
            public void SetAlias(int index, string value) => (aliasIndex, aliasValue) = (index, value);

            public string this[int i] => aliasIndex == i ? aliasValue : collection[i];
            public int Length => collection.Length;
        }
    }
}
