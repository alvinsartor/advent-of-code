﻿using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/18

    public class Day18 : DailyChallenge
    {
        /// <summary>
        /// Resolve a bunch of expressions with the following rule: all operators have the same precedence.
        /// <para> Avg execution time: 3.918ms | O(n) </para>
        /// </summary>        
        public override string Part1() => input.Sum(l => ResolvePolishNotation(ToPolishNotation(l))).ToString();

        /// <summary>
        /// Resolve a bunch of expressions with the following rule: addition has more precendence than multiplication.
        /// <para> Avg execution time: 3.090ms | O(n) </para>
        /// </summary>
        public override string Part2()
        {
            Dictionary<string, int> priorities = new Dictionary<string, int>();
            priorities["+"] = 1;
            return input.Sum(l => ResolvePolishNotation(ToPolishNotation(l, priorities))).ToString();
        }

        private static Stack<string> ToPolishNotation(string line, Dictionary<string, int>? operatorsPriorities = null)
        {
            line = line.Replace("(", " ( ").Replace(")", " ) ");
            Dictionary<string, int> priorities = operatorsPriorities ?? new Dictionary<string, int>();
            Stack<string> output = new Stack<string>();
            Stack<string> operators = new Stack<string>();
            int GetPriority(string token) => priorities.ContainsKey(token) ? priorities[token] : 0;

            while (line.Length > 0)
            {
                (string token, string rest, TokenType tokenType) = GetNextToken(line);
                switch (tokenType)
                {
                    case TokenType.number:
                        output.Push(token);
                        break;
                    case TokenType.operation:
                        var currPriority = GetPriority(token);
                        while (operators.Count > 0
                                && operators.Peek() != "("
                                && GetPriority(operators.Peek()) >= currPriority)
                            output.Push(operators.Pop());
                        operators.Push(token);
                        break;
                    case TokenType.openParenthesis:
                        operators.Push(token);
                        break;
                    case TokenType.closeParenthesis:
                        while (operators.Count > 0 && operators.Peek() != "(") output.Push(operators.Pop());
                        operators.Pop();
                        break;
                    default:
                        throw new NotImplementedException();
                }
                line = rest;
            }

            while (operators.Count > 0) output.Push(operators.Pop());
            return output;
        }

        private enum TokenType { number, operation, openParenthesis, closeParenthesis }
        private static readonly HashSet<char> signs = new HashSet<char>(new[] { '(', ')', '+', '*' });
        private static (string token, string rest, TokenType tokenType) GetNextToken(string line)
        {
            line = line.Trim();
            var firstElement = line[0];
            var indexOfSpace = line.IndexOf(' ');

            return firstElement switch
            {
                '(' => ("(", line[1..], TokenType.openParenthesis),
                ')' => (")", line[1..], TokenType.closeParenthesis),
                '+' => ("+", line[1..], TokenType.operation),
                '*' => ("*", line[1..], TokenType.operation),
                _ => indexOfSpace > -1
                    ? (line[0..indexOfSpace], line[indexOfSpace..].Trim(), TokenType.number)
                    : (line, "", TokenType.number)
            };
        }

        private static double ResolvePolishNotation(Stack<string> pn)
        {
            var rpn = pn.ToReversedStack();
            Stack<double> stack = new Stack<double>();
            while (rpn.Count > 0)
            {
                var token = rpn.Pop();
                if (double.TryParse(token, out double result))
                    stack.Push(result);
                else
                    stack.Push(PerformOperation(stack.Pop(), stack.Pop(), token));
            }
            return stack.Pop();
        }

        private static double PerformOperation(double x, double y, string operation) =>
            operation switch
            {
                "*" => x * y,
                "+" => x + y,
                _ => throw new NotImplementedException(),
            };
    }
}