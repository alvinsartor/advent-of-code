﻿using System.Linq;
using System.Collections.Generic;

namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/7

    public class Day07: DailyChallenge
    {
        /// <summary>
        /// Considering all "shiny gold" leaves, climb the tree and count all the bags that can contain it.
        /// <para> Dictionaries save from having to search in the whole input each interation. </para>
        /// <para> Avg 1000 executions: 1.886ms | O(n) </para>
        /// </summary>
        public override string Part1()
        {
            var bags = new HashSet<string> { "shiny gold" };
            var countedBags = bags.ToHashSet();
            var bagsDictionary = CreateContainedBagDictionary();

            do
            {
                var otherbags = bags
                    .SelectMany(bag => bagsDictionary.ContainsKey(bag) ? bagsDictionary[bag] : new List<string>())
                    .Where(bag => !countedBags.Contains(bag))
                    .ToHashSet();

                countedBags.UnionWith(otherbags);
                bags = otherbags;
            } while (bags.Count > 0);

            return (countedBags.Count - 1).ToString();
        }

        /// <summary>
        /// Considering "shiny gold" the root, descend the tree and count how many bags it must contain.
        /// <para> Dictionaries save from having to search in the whole input each interation. </para>
        /// <para> Avg 1000 executions: 0.608ms | O(n) </para>
        /// </summary>
        public override string Part2()
        {
            var bagsDictionary = CreateContainingBagsDictionary();
            List<(int, string)> bags = new List<(int, string)>
            {
                (1, "shiny gold")
            };

            int c0 = 0;
            do
            {
                var otherbags = bags
                    .SelectMany(bag => bagsDictionary[bag.Item2].Select(t => (t.Item1 * bag.Item1, t.Item2)))
                    .ToList();
                c0 += otherbags.Select(bag => bag.Item1).Sum();
                bags = otherbags.Where(bag => bag.Item1 > 0).ToList();

            } while (bags.Count > 0);

            return c0.ToString();
        }

        /// <summary>
        /// Creates a dictionary in which each bag is associated to the list of bags that can contain it.
        /// </summary>
        private Dictionary<string, List<string>> CreateContainedBagDictionary()
        {
            Dictionary<string, List<string>> result = new Dictionary<string, List<string>>();

            foreach (string line in input)
            {
                var (container, contained) = ExtractLineInfo(line);
                foreach ((_, string bag) in contained)
                {
                    if (result.ContainsKey(bag)) result[bag].Add(container);
                    else result.Add(bag, new List<string> { container });
                }
            }

            return result;
        }

        /// <summary>
        /// Creates a dictionary with the containing bags as keys 
        /// and the list of contained bags with the relative amount as value.
        /// </summary>
        private Dictionary<string, IEnumerable<(int, string)>> CreateContainingBagsDictionary() => 
            input.Select(ExtractLineInfo).ToDictionary(info => info.container, info => info.contained);

        /// <summary>
        /// Given a rule, it extracts the containing bag and the contained ones (with relative amounts).
        /// </summary>
        private (string container, IEnumerable<(int, string)> contained) ExtractLineInfo(string line)
        {
            var container = line.Substring(0, line.IndexOf(" bags contain"));
            line = line.Remove(0, line.IndexOf("bags contain ") + 13);

            var contained = line.Contains("no other bag")
                ? new List<(int, string)>()
                : line.Split(",")
                .Select(ss => ss.Trim())
                .Select(ss => (int.Parse(ss.Split(" ")[0]), ss.Split(" ")[1] + " " + ss.Split(" ")[2]));
            return (container, contained);
        }
    }
}