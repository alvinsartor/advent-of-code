﻿using System.Linq;

namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/2

    class Day02 : DailyChallenge
    {
        /// <summary>
        /// Given a string with the following format: "RULE : PASSWORD"; determine how many passwords follow the rule.
        /// EXAMPLE: "15-16 f: ffffffffffffffhf" (15 <= count(password, 'f') <= 16).
        /// <para>Using an refillable object seems the most performant and cleanest way.</para>
        /// <para> Avg 1000 executions: 0.539ms | O(n) </para>
        /// </summary>
        public override string Part1()
        {
            var rule = new RuleAndPassword();
            return input.Count(passwordRecord => rule.Fill(passwordRecord).AppliesPart1()).ToString();
        }

        /// <summary>
        /// Given a string with the following format: "RULE : PASSWORD"; determine how many passwords follow the rule.
        /// EXAMPLE: "15-16 f: ffffffffffffffhf" (15 <= count(password, 'f') <= 16).
        /// <para>Using an refillable object seems the most performant and cleanest way.</para>
        /// <para> Avg 1000 executions: 0.539ms | O(n) </para>
        /// </summary>
        public override string Part2()
        {
            var rule = new RuleAndPassword();
            return input.Count(passwordRecord => rule.Fill(passwordRecord).AppliesPart2()).ToString();
        }

        /// <summary>
        /// Class encapsulating a rule, a password and the various interpretations of the rule.
        /// </summary>
        private class RuleAndPassword
        {
            private string _password;
            private char _letter;
            private int _lBound;
            private int _uBound;

            public RuleAndPassword Fill(string passwordRecord)
            {
                var split = passwordRecord.Split(":");
                _password = split[1].Trim();

                var rule = split[0].Split(" ");
                _letter = rule[1].ToCharArray()[0];

                var occurrences = rule[0].Split("-");
                _lBound = int.Parse(occurrences[0]);
                _uBound = int.Parse(occurrences[1]);

                return this;
            }

            /// <summary>
            /// Returns TRUE whether the password follows the first interpretation of the rule.
            /// </summary>
            public bool AppliesPart1()
            {
                var letterOccurrences = _password.Count(c => c == _letter);
                return _lBound <= letterOccurrences && letterOccurrences <= _uBound;
            }

            /// <summary>
            /// Returns TRUE whether the password follows the second interpretation of the rule.
            /// </summary>
            public bool AppliesPart2() =>
                _password[_lBound - 1] == _letter ^ _password[_uBound - 1] == _letter;
        }
    }
}
