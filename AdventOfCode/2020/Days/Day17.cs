﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/17

    public class Day17 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 1;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 1;

        /// <summary>
        /// Calculate the number of active cubes after 6 iterations in a 3d space.
        /// <para> Avg execution time: 40ms | O(n3) </para>
        /// </summary>
        public override string Part1()
        {
            var activeCells = GetActivePointsFromInput().Cast<IPoint>().ToHashSet();
            var iterations = 6;

            while(iterations-- > 0)
            {
                var wholeSpace = activeCells.SelectMany(c => c.SurroundingSpace());
                var newActiveCells = wholeSpace.Where(c => c.ShouldBeActive(activeCells)).ToHashSet();
                activeCells = newActiveCells;
            }

            return activeCells.Count.ToString();
        }

        private IEnumerable<Point3d> GetActivePointsFromInput() =>
            input
                .SelectMany((row, y) => row
                    .Select((cell, x) => (cell, p: new Point3d(x, y, 0))))
                .Where(x => x.cell == '#')
                .Select(x => x.p);

        /// <summary>
        /// Like part1, but this time the space is 4d.
        /// <para> Avg execution time: 1893ms | O(n4) </para>
        /// </summary>
        public override string Part2()
        {
            var activeCells = GetActivePointsFromInput().Select(Point4d.From3dPoint).Cast<IPoint>().ToHashSet();
            var iterations = 6;

            while (iterations-- > 0)
            {
                var wholeSpace = activeCells.SelectMany(c => c.SurroundingSpace());
                var newActiveCells = wholeSpace.Where(c => c.ShouldBeActive(activeCells)).ToHashSet();
                activeCells = newActiveCells;
            }

            return activeCells.Count.ToString();
        }

        interface IPoint {
            IEnumerable<IPoint> SurroundingSpace();
            bool ShouldBeActive(HashSet<IPoint> currentlyActive);
        }

        class Point3d : IPoint
        {
            public readonly int x;
            public readonly int y;
            public readonly int z;

            public Point3d(int x, int y, int z) =>
                (this.x, this.y, this.z) = (x, y, z);

            public IEnumerable<IPoint> Neighbors() =>
                SurroundingSpace().Where(p => !p.Equals(this));

            public virtual IEnumerable<IPoint> SurroundingSpace()
            {
                for (int xi = x - 1; xi <= x + 1; xi++)
                    for (int yi = y - 1; yi <= y + 1; yi++)
                        for (int zi = z - 1; zi <= z + 1; zi++)
                            yield return new Point3d(xi, yi, zi);
            }

            public bool ShouldBeActive(HashSet<IPoint> currentlyActive)
            {
                var isActive = currentlyActive.Contains(this);
                var activeNeighbors = Neighbors().Where(currentlyActive.Contains).Count();
                return activeNeighbors == 3 || (isActive && activeNeighbors == 2);
            }

            public override bool Equals(object? obj) => obj is Point3d point &&
                       x == point.x &&
                       y == point.y &&
                       z == point.z;

            public override int GetHashCode() => HashCode.Combine(x, y, z);
        }

        private class Point4d : Point3d
        {
            public readonly int w;
            public Point4d(int x, int y, int z, int w): base(x, y, z) => this.w = w;
            public static Point4d From3dPoint(Point3d p) => new Point4d(p.x, p.y, p.z, 0);

            public override IEnumerable<IPoint> SurroundingSpace()
            {
                for (int xi = x - 1; xi <= x + 1; xi++)
                    for (int yi = y - 1; yi <= y + 1; yi++)
                        for (int zi = z - 1; zi <= z + 1; zi++)
                            for (int wi = w - 1; wi <= w + 1; wi++)
                            yield return new Point4d(xi, yi, zi, wi);
            }

            public override bool Equals(object? obj) => obj is Point4d point &&
                       x == point.x &&
                       y == point.y &&
                       z == point.z &&
                       w == point.w;

            public override int GetHashCode() => HashCode.Combine(x, y, z, w);
        }
    }
}