﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/15

    public class Day15 : DailyChallenge
    {
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 1;

        /// <summary>
        /// The elves invented a memory game: each round say 0 if it is the first appearance of the previous number, 
        /// its age otherwise. What is the 2020th number?
        /// <para> Avg 1000 executions: 0.091ms | O(n) </para>
        /// </summary>
        public override string Part1() => NthMemoryNumber(2020).ToString();

        /// <summary>
        /// Like before, but they now want to know what the 30000000th number is.
        /// <para> Avg execution time: 3s | O(n) </para>
        /// </summary>
        public override string Part2() => NthMemoryNumber(30000000).ToString();

        private int NthMemoryNumber(int nth)
        {
            int count = 0;
            var numbers = new Dictionary<int, int>();
            int current = -1;
            int next = -1;

            foreach (var n in input[0].Split(",").Select(int.Parse))
            {
                if (current > -1) numbers[current] = count;
                current = n;
                count++;
            }

            while (count < nth)
            {
                next = !numbers.ContainsKey(current) ? 0 : count - numbers[current];
                numbers[current] = count++;
                current = next;
            }

            return current;
        }

    }
}