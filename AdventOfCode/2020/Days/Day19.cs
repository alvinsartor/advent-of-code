﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/19

    public class Day19 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 1;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 1;

        /// <summary>
        /// Given a set of recursive rules and some strings, count the strings that satisfy the rule 0.
        /// <para> Avg execution time: 200ms </para>
        /// </summary>        
        public override string Part1()
        {
            var (rules, messages) = ParseInput(input);
            return messages.Count(m => rules[0].Matches(m).Any(x => x.IsFullMatch)).ToString();
        }        

        /// <summary>
        /// Same as before, but now with infinite cycles added to the rules.
        /// <para> Avg execution time: 700ms </para>
        /// </summary>
        public override string Part2()
        {
            var newRules = new[] { "8: 42 | 42 8", "11: 42 31 | 42 11 31" };
            var adaptedInput = newRules.Concat(input.Where(line => line != "8: 42" && line != "11: 42 31")).ToArray();

            var (rules, messages) = ParseInput(adaptedInput);
            return messages.Count(m => rules[0].Matches(m).Any(x => x.IsFullMatch)).ToString();
        }

        private static (Dictionary<int, Rule> rules, string[] entries) ParseInput(string[] input)
        {
            var i = 0;
            while (input[i] != "") i++;

            var rawRules = input[..i];
            var rules = rawRules.Select(Rule.ParseFromString).ToDictionary(rule => rule.RuleId);
            foreach (var rule in rules.Values) rule.Resolve(rules);

            var entries = input[++i..];

            return (rules, entries);
        }

        public abstract record Rule(int RuleId)
        {
            public virtual void Resolve(Dictionary<int, Rule> allRules) { }
            public abstract Match[] Matches(string input);

            public static Rule ParseFromString(string rule)
            {
                var ruleSplit = rule.Split(": ");
                var ruleId = int.Parse(ruleSplit[0]);
                var ruleBody = ruleSplit[1];

                if (ruleBody[0] == '"') 
                    return new CharRule(ruleId, ruleBody[1]);

                if (!ruleBody.Contains('|'))
                    return CreateLinkRule(ruleId, ruleBody);

                var rules = ruleBody.Split(" | ").Select(body => CreateLinkRule(ruleId, body)).ToArray();
                return new CompoundRule(ruleId, rules);
            }

            private static LinkRule CreateLinkRule(int ruleId, string ruleBody) =>
                new LinkRule(ruleId, ruleBody.Split(" ").Select(n => int.Parse(n)).ToArray());
        }

        record LinkRule(int RuleId, int[] LinkedRuleNumbers) : Rule(RuleId)
        {
            public Rule[]? LinkedRules { get; private set; } 

            public override Match[] Matches(string input) =>
                LinkedRules.Aggregate(new[] { new Match(input) },
                                      (acc, rule) => acc.SelectMany(m => rule.Matches(m.Remaining))
                                                        .ToArray());

            public override void Resolve(Dictionary<int, Rule> allRules) =>
                LinkedRules = LinkedRuleNumbers.Select(x => allRules[x]).ToArray();
        }

        record CompoundRule(int RuleId, params Rule[] Rules) : Rule(RuleId)
        {
            public override Match[] Matches(string input) => (
                from rule in Rules
                from match in rule.Matches(input)
                select match
            ).ToArray();

            public override void Resolve(Dictionary<int, Rule> allRules)
            {
                foreach (var rule in Rules)
                    rule.Resolve(allRules);
            }
        }

        record CharRule(int RuleId, char Char) : Rule(RuleId)
        {
            public override Match[] Matches(string input) =>
                input.FirstOrDefault() == Char
                    ? new[] { new Match(input.Substring(1)) }
                    : Array.Empty<Match>();
        }

        public record Match(string Remaining)
        {
            public bool IsFullMatch => string.IsNullOrEmpty(Remaining);
        }
    }
}