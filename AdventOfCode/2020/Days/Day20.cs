﻿using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/20

    public class Day20 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 100;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 100;

        /// <summary>
        /// Given a set of randomly rotated and flipped images, find the images representing the corners of the puzzle 
        /// and return the product of their ids.
        /// <para> Avg execution time: 0.649ms </para>
        /// </summary>        
        public override string Part1()
        {
            var puzzlePieces = ParsePiecesFromInput(input);
            var connections = FindConnections(puzzlePieces);
            var cornerIds = connections
                .Select(pair => (piece: pair.Key, nrConnections: pair.Value.Count))
                .Where(connection => connection.nrConnections == 2)
                .Select(connection => connection.piece.id);

            return cornerIds.Aggregate((double)1, (curr, next) => curr * next).ToString();
        }

        /// <summary>
        /// Put the images together and count the patterns representing sea monsters in them.
        /// Then return the number of '#' NOT belonging to sea monsters.
        /// <para> Avg execution time: 12.21ms </para>
        /// </summary>
        public override string Part2()
        {
            var puzzlePieces = ParsePiecesFromInput(input);
            var connections = FindConnections(puzzlePieces);
            var puzzle = ComposePuzzle(connections);
            var lines = ExtractImagesFromPuzzle(puzzle);
            var numberSeaMonsters = TagSeaMonsters(lines);            
            var numberWaves = lines.Select(l => l.Count(c => c == '#')).Sum();
            var result = numberWaves - (numberSeaMonsters * 15);
            return result.ToString();
        }

        private static List<PuzzlePiece> ParsePiecesFromInput(string[] input)
        {
            List<PuzzlePiece> pieces = new List<PuzzlePiece>();
            const int pieceHeight = 12;
            for (int i = pieceHeight; i <= input.Length + 1; i += pieceHeight)
            {
                var lBound = i - pieceHeight;
                var uBound = i - 1;
                pieces.Add(PuzzlePiece.CreatePieceFromString(input[lBound..uBound]));
            }
            return pieces;
        }

        private static Dictionary<PuzzlePiece, List<PuzzlePiece>> FindConnections(IEnumerable<PuzzlePiece> puzzlePieces)
        {
            Dictionary<string, PuzzlePiece> sides = new Dictionary<string, PuzzlePiece>();
            Dictionary<PuzzlePiece, List<PuzzlePiece>> connections = new Dictionary<PuzzlePiece, List<PuzzlePiece>>();

            void AddConnection(PuzzlePiece p1, PuzzlePiece p2)
            {
                if (!connections.ContainsKey(p1)) connections.Add(p1, new List<PuzzlePiece>());
                if (!connections.ContainsKey(p2)) connections.Add(p2, new List<PuzzlePiece>());
                connections[p1].Add(p2);
                connections[p2].Add(p1);
            }

            foreach (var piece in puzzlePieces)
                foreach ((string original, string flipped) in piece.SidesWithFlippedPaired.Value)
                {
                    if (sides.ContainsKey(original))
                    {
                        var otherPiece = sides[original];
                        AddConnection(piece, otherPiece);
                    }
                    else if (sides.ContainsKey(flipped))
                    {
                        var otherPiece = sides[flipped];
                        AddConnection(piece, otherPiece);
                    }
                    else
                    {
                        sides.Add(original, piece);
                        sides.Add(flipped, piece);
                    }
                }

            return connections;
        }

        private static PuzzlePiece[][] ComposePuzzle(Dictionary<PuzzlePiece, List<PuzzlePiece>> connections)
        {
            var sideSize = (int)Math.Sqrt(connections.Count);
            HashSet<PuzzlePiece> unprocessed = connections.Keys.ToHashSet();

            // step 0: initialize puzzle array
            PuzzlePiece[][] puzzle = new PuzzlePiece[sideSize][];
            for (int i = 0; i < puzzle.Length; i++) puzzle[i] = new PuzzlePiece[sideSize];

            // step1: take one of the angles (this will be our 0,0) and find its true orientation/side
            var angle = connections.First(x => x.Value.Count == 2).Key;
            puzzle[0][0] = RotatePieceToMatch00Position(angle, connections);
            unprocessed.Remove(angle);

            // step2: fill the first column
            for (int i = 1; i < puzzle.Length; i++)
            {
                var previousPiece = puzzle[i - 1][0];
                var bottomPiece = connections[previousPiece]
                    .Where(p => unprocessed.Contains(p))
                    .First(p => p.AllSidesWithFlipped.Value.Contains(previousPiece.BottomSide.Value));
                puzzle[i][0] = bottomPiece.TransformSoTopMatchesWith(previousPiece.BottomSide.Value);
                unprocessed.Remove(bottomPiece);
            }

            // step3: fill each row using the first value as starting point
            for (int r = 0; r < puzzle.Length; r++)
                for (int c = 1; c < puzzle[r].Length; c++)
                {
                    var previousPiece = puzzle[r][c - 1];
                    var rightPiece = connections[previousPiece]
                        .Where(p => unprocessed.Contains(p))
                        .First(p => p.AllSidesWithFlipped.Value.Contains(previousPiece.RightSide.Value));
                    puzzle[r][c] = rightPiece.TransformSoLeftMatchesWith(previousPiece.RightSide.Value);
                    unprocessed.Remove(rightPiece);
                }

            return puzzle;
        }

        private static char[][] ExtractImagesFromPuzzle(PuzzlePiece[][] puzzle)
        {
            int pieceHeight = 10;
            List<string> lines = new List<string>();

            for (int r = 0; r < puzzle.Length; r++)
                for (int line = 1; line < pieceHeight - 1; line++)
                {
                    string concatenatedLine = "";
                    for (int c = 0; c < puzzle[r].Length; c++)
                        concatenatedLine += puzzle[r][c].GetLine(line)[1..^1];
                    lines.Add(concatenatedLine);
                }
         
            return lines.Select(line => line.ToCharArray()).ToArray();
        }

        private static PuzzlePiece RotatePieceToMatch00Position(
            PuzzlePiece angle,
            Dictionary<PuzzlePiece, List<PuzzlePiece>> connections)
        {
            var conn1 = connections[angle][0];
            var conn2 = connections[angle][1];

            var angleSides = angle.Sides.Value.ToHashSet();
            var connectionsSides = conn1.SidesWithFlippedPaired.Value
                .Concat(conn2.SidesWithFlippedPaired.Value)
                .SelectMany(t => new[] { t.Item1, t.Item2 });

            angleSides.ExceptWith(connectionsSides);
            return angle.RotateUntilSidesCorrespondToTopLeft(angleSides);
        }

        private static int TagSeaMonsters(char[][] lines)
        {
            var images = new List<char[][]>(); images.Add(lines);
            lines = lines.Rotate(); images.Add(lines);
            lines = lines.Rotate(); images.Add(lines);
            lines = lines.Rotate(); images.Add(lines);
            lines = lines.FlipHorizontally(); images.Add(lines);
            lines = lines.Rotate(); images.Add(lines);
            lines = lines.Rotate(); images.Add(lines);
            lines = lines.Rotate(); images.Add(lines);

            var monstersEachImage = images.Select(CountSeaMonstersInImage);
            return monstersEachImage.Sum();
        }

        private static int CountSeaMonstersInImage(char[][] lines)
        {
            var pattern = @"(?<=#.{77})#.{4}#{2}.{4}#{2}.{4}#{3}(?=.{77}#.{2}#.{2}#.{2}#.{2}#.{2}#)";
            var singleLine = lines.Aggregate("", (curr, next) => curr + new string(next));
            var matches = Regex.Matches(singleLine, pattern);
            return matches.Count;
        }

        private class PuzzlePiece
        {
            public readonly long id;
            public readonly char[][] piece;
            public readonly Lazy<string> TopSide;
            public readonly Lazy<string> RightSide;
            public readonly Lazy<string> BottomSide;
            public readonly Lazy<string> LeftSide;
            public readonly Lazy<string[]> Sides;
            public readonly Lazy<HashSet<string>> AllSidesWithFlipped;
            public readonly Lazy<(string, string)[]> SidesWithFlippedPaired;

            public static PuzzlePiece CreatePieceFromString(string[] pieceWithId)
            {
                var id = long.Parse(pieceWithId[0][5..^1]);
                var piece = pieceWithId[1..].Select(x => x.ToCharArray()).ToArray();
                return new PuzzlePiece(id, piece);
            }

            public PuzzlePiece(long id, char[][] piece)
            {
                this.id = id;
                this.piece = piece;

                TopSide = new Lazy<string>(() => new string(piece[0]));
                RightSide = new Lazy<string>(() => new string(piece.Select(line => line[^1]).ToArray()));
                BottomSide = new Lazy<string>(() => new string(piece[^1].Reverse().ToArray()));
                LeftSide = new Lazy<string>(() => new string(piece.Select(line => line[0]).Reverse().ToArray()));
                Sides = new Lazy<string[]>(() => new string[] { TopSide.Value, RightSide.Value, BottomSide.Value, LeftSide.Value });
                SidesWithFlippedPaired = new Lazy<(string, string)[]>(() => CalculateSidesWithFlipped(this));
                AllSidesWithFlipped = new Lazy<HashSet<string>>(() => CalculateAllSidesWithFlipped(this));
            }

            public override bool Equals(object? obj) => obj is PuzzlePiece piece && id == piece.id;
            public override int GetHashCode() => HashCode.Combine(id);
            public override string ToString() => id.ToString();

            [Pure]
            public PuzzlePiece TransformSoTopMatchesWith(string sideToMatch) => 
                TransformSoSideMatchesWith(sideToMatch.StringReverse(), (PuzzlePiece p) => p.TopSide.Value);

            [Pure]
            public PuzzlePiece TransformSoLeftMatchesWith(string sideToMatch) => 
                TransformSoSideMatchesWith(sideToMatch.StringReverse(), (PuzzlePiece p) => p.LeftSide.Value);

            [Pure]
            private PuzzlePiece TransformSoSideMatchesWith(string sideToMatch, Func<PuzzlePiece, string> getSide)
            {
                var side = getSide(this);
                if (side == sideToMatch) return this;
                if (Sides.Value.ToHashSet().Contains(sideToMatch)) return Rotated().TransformSoSideMatchesWith(sideToMatch, getSide);
                else return Flipped().TransformSoSideMatchesWith(sideToMatch, getSide);
            }

            [Pure]
            public PuzzlePiece RotateUntilSidesCorrespondToTopLeft(HashSet<string> sides) =>
                sides.Contains(LeftSide.Value) && sides.Contains(TopSide.Value)
                    ? this
                    : Rotated().RotateUntilSidesCorrespondToTopLeft(sides);

            [Pure]
            public PuzzlePiece Rotated() => new PuzzlePiece(id, piece.Rotate());

            [Pure]
            public PuzzlePiece Flipped() => new PuzzlePiece(id, piece.FlipHorizontally());

            [Pure]
            public string GetLine(int l) => new string(piece[l]);

            private static (string, string)[] CalculateSidesWithFlipped(PuzzlePiece piece) =>
                new (string, string)[] {
                    (piece.TopSide.Value, piece.TopSide.Value.StringReverse()),
                    (piece.RightSide.Value, piece.RightSide.Value.StringReverse()),
                    (piece.BottomSide.Value, piece.BottomSide.Value.StringReverse()),
                    (piece.LeftSide.Value, piece.LeftSide.Value.StringReverse()),
                };

            private static HashSet<string> CalculateAllSidesWithFlipped(PuzzlePiece piece) =>
                piece.Sides.Value.Concat(piece.Sides.Value.Select(s => s.StringReverse())).ToHashSet();
        }
    }
}