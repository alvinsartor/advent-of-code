﻿using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/24

    public class Day24 : DailyChallenge
    {
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 100;

        private static readonly Vector3 East = new Vector3(1, -1, 0);
        private static readonly Vector3 West = new Vector3(-1, 1, 0);
        private static readonly Vector3 SouthEast = new Vector3(0, -1, -1);
        private static readonly Vector3 SouthWest = new Vector3(-1, 0, -1);
        private static readonly Vector3 NorthEast = new Vector3(1, 0, 1);
        private static readonly Vector3 NorthWest = new Vector3(0, 1, 1);
        
        /// <summary>
        /// Given a list corresponding to steps in a hexagonal grid; Determine the final destination of each step and return 
        /// the number of tiles reached (reaching the same tile twice will flip it back to its initial state, making it count as 0).
        /// <para> Avg execution time: 0.704ms | O(NM) [N = number of strings; M = length of strings] </para>
        /// </summary>        
        public override string Part1()
        {
            var flippedTiles = CalculateInitialFlippedTiles();
            return flippedTiles.Count.ToString();
        }

        private HashSet<Vector3> CalculateInitialFlippedTiles()
        {
            HashSet<Vector3> reachedTiles = new HashSet<Vector3>();
            input.Select(InstructionListToFinalPosition).ForEach(pt =>
            {
                if (reachedTiles.Contains(pt)) reachedTiles.Remove(pt);
                else reachedTiles.Add(pt);
            });

            return reachedTiles;
        }

        private Vector3 InstructionListToFinalPosition(string instructionsList)
        {
            Vector3 currentPosition = Vector3.Zero;
            for (int i = 0; i < instructionsList.Length; i++)
            {
                var currentStep = instructionsList[i].ToString();
                if (currentStep == "n" || currentStep == "s")
                {
                    currentStep += instructionsList[i + 1];
                    i++;
                }
                currentPosition += StepToPoint(currentStep);
            }
            return currentPosition;
        }

        private static Vector3 StepToPoint(string step) =>
            step switch
            {
                "e" => East,
                "w" => West,
                "se" => SouthEast,
                "sw" => SouthWest,
                "ne" => NorthEast,
                "nw" => NorthWest,
                _ => throw new NotImplementedException(),
            };

        /// <summary>
        /// Given the initial configuration, iterate 100 times applying certain rules and count how many tiles should be flipped.
        /// <para> Avg execution time: 308ms | O(KN) </para>
        /// </summary>
        public override string Part2()
        {
            var Surroundings = new Vector3[] { East, West, SouthEast, SouthWest, NorthEast, NorthWest };
            var SurroundingsAndSelf = Surroundings.Append(Vector3.Zero).ToArray();

            var flippedTiles = CalculateInitialFlippedTiles();
            var counter = 0;
            
            while (++counter <= 100)
            {
                var tilesToCheck = flippedTiles.SelectMany(pt => SurroundingsAndSelf.Select(p2 => pt + p2)).ToHashSet();
                HashSet<Vector3> newFlippedTiles = new HashSet<Vector3>();
                tilesToCheck.ForEach(tile =>
                {
                    var blackSurroundingTiles = Surroundings.Select(pt => tile + pt).Count(tile => flippedTiles.Contains(tile));
                    bool shouldBeBlack = blackSurroundingTiles == 2 || (blackSurroundingTiles == 1 && flippedTiles.Contains(tile));
                    if (shouldBeBlack) newFlippedTiles.Add(tile);
                });

                flippedTiles = newFlippedTiles;
            }

            return flippedTiles.Count.ToString();
        }
    }
}