﻿using AdventOfCode.helpers;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/16

    public class Day16 : DailyChallenge
    {
        private readonly TicketField[] fields;
        private readonly HashSet<int> validNumbers;
        private readonly int[] myTicket;
        private readonly int[][] otherTickets;

        public Day16() 
        {
            fields = GetFields(input);
            validNumbers = GetValidNumbers(input);
            myTicket = GetMyTicket(input);
            otherTickets = GetAllTickets(input);
        }
        
        private static HashSet<int> GetValidNumbers(string[] input)
        {
            var includedNumbers = new HashSet<int>();
            var i = 0;
            while (input[i].Trim() != "")
            {
                var ranges = input[i++]
                    .Split(": ")[1]
                    .Split(" or ")
                    .Select(r => r.Split("-").Select(int.Parse).ToArray())
                    .ToArray();

                for (int n = ranges[0][0]; n < ranges[0][1]; n++) includedNumbers.Add(n);
                for (int n = ranges[1][0]; n < ranges[1][1]; n++) includedNumbers.Add(n);
            }
            return includedNumbers;
        }

        private static TicketField[] GetFields(string[] input)
        {
            var i = 0;
            while (input[i].Trim() != "") i++;
            return input.Take(i).Select(field => new TicketField(field)).ToArray();
        }

        private static int[] GetMyTicket(string[] input)
        {
            var i = 0;
            while (input[i++] != "your ticket:") ;
            return input[i].Split(",").Select(int.Parse).ToArray();
        }

        private static int[][] GetAllTickets(string[] input)
        {
            var i = 0;
            while (input[i++] != "nearby tickets:") ;
            return input[i..].Select(ticket => ticket.Split(",").Select(int.Parse).ToArray()).ToArray();
        }

        /// <summary>
        /// Count the number of valid tickets: tickets in which each value is valid in at least 1 field range.
        /// <para> Avg 1000 executions: 0.056ms | O(len(tickets)) </para>
        /// </summary>
        public override string Part1()
        {
            var validTickets = 0;
            foreach (var ticket in otherTickets)
                foreach (int n in ticket) 
                    if (!validNumbers.Contains(n)) 
                        validTickets += n;
            return validTickets.ToString();
        }

        /// <summary>
        /// Use the valid tickets to associate each column to the relative field.
        /// Once this is done, return the product the fields belonging to your ticket that have 'destination' in the name.
        /// <para> Avg 1000 executions: 1.563ms </para>
        /// </summary>
        public override string Part2()
        {
            // get valid tickets and transpose them, to obtain a list of single-field-values
            var validTickets = otherTickets.Where(ticket => ticket.All(n => validNumbers.Contains(n))).ToArray();
            var columns = validTickets.Transposed();

            // create dictionary containing all the fields that might work for each column
            var columnsWithAssociatedFields = new Dictionary<int, HashSet<int>>();
            var fieldsWithIndex = fields.Select((f, i) => (indx: i, field: f)).ToArray();
            for (int i = 0; i < columns.Length; i++)
                columnsWithAssociatedFields[i] = fieldsWithIndex
                    .Where((x) => columns[i].All(n => x.field.IsIncluded(n)))
                    .Select(x => x.indx)
                    .ToHashSet();

            // exclude 1 by 1 the lists that surely belong to a certain column
            var columnsWithField = new Dictionary<int, int>();
            while(columnsWithAssociatedFields.Count > 0)
            {
                // there should be at least 1 column with 1 only possible field (otherwise the puzzle has no solution)
                var firstColumnWeAreSureOf = columnsWithAssociatedFields.First(x => x.Value.Count == 1);

                (int columnId, int fieldId) = (firstColumnWeAreSureOf.Key, firstColumnWeAreSureOf.Value.First());
                columnsWithField.Add(columnId, fieldId);
                columnsWithAssociatedFields.Remove(columnId);
                foreach (var pair in columnsWithAssociatedFields) pair.Value.Remove(fieldId);
            }

            // finally we have 1 field for 1 column. We can decipher our own ticket 
            IEnumerable<(int value, string name)>? myTicketDeciphered = columnsWithField
                .Select((KeyValuePair<int, int> pair) => (myTicket[pair.Key], fields[pair.Value].name));

            var productOfValuesRelativeToDeparture = myTicketDeciphered
                .Where(x => x.name.ToLower().Contains("departure"))
                .Select(x => x.value)
                .Aggregate(1.0, (curr, next) => curr * next);

            return productOfValuesRelativeToDeparture.ToString();
        }

        private class DoubleRange
        {
            private readonly int lb1;
            private readonly int ub1;
            private readonly int lb2;
            private readonly int ub2;

            public DoubleRange(string ranges)
            {
                var split = ranges.Split(" or ").Select(r => r.Split("-").Select(int.Parse).ToArray()).ToArray();
                (lb1, ub1, lb2, ub2) = (split[0][0], split[0][1], split[1][0], split[1][1]);
            }
            public bool IsIncluded(int n) => (lb1 <= n && n <= ub1) || (lb2 <= n && n <= ub2);
        }

        private class TicketField
        {
            public readonly DoubleRange ranges;
            public readonly string name;
                
            public TicketField(string field)
            {
                var split = field.Split(": ");
                name = split[0];
                ranges = new DoubleRange(split[1]);
            }

            public bool IsIncluded(int n) => ranges.IsIncluded(n);
        }
    }
}