﻿using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/23

    public class Day23 : DailyChallenge
    {
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 1;

        /// <summary>
        /// Apply a series of operations to a circular list K times and display the final result.
        /// <para> Avg execution time: 0.045ms | O(K) </para>
        /// </summary>        
        public override string Part1()
        {
            var circularList = input[0].Select(c => int.Parse(c.ToString())).ToLinkedList();
            var nodesDictionary = ExtractNodeDictionary(circularList);

            var max = circularList.Max();
            var min = circularList.Min();
            var counter = 0;
            LinkedListNode<int> currentNode = circularList.First!;

            while (++counter <= 100)
            {                
                var poppedElements = RemoveElementsFromList(ref circularList, ref nodesDictionary, currentNode);
                var destinationNode = FindDestinationNode(poppedElements, nodesDictionary, currentNode.Value, min, max);
                AddElementsToList(ref circularList, ref nodesDictionary, destinationNode, poppedElements);
                                
                currentNode = currentNode!.CircularNext()!;
                if (currentNode is null) throw new ArgumentNullException(nameof(currentNode));
            }

            RepositionCircularList(ref circularList, 0, 1);
            return circularList.ToArray().Skip(1).Aggregate("", (curr, next) => curr + next);
        }

        private static Dictionary<int, LinkedListNode<int>> ExtractNodeDictionary(LinkedList<int> list)
        {
            var nodesDictionary = new Dictionary<int, LinkedListNode<int>>();
            var node = list.First;
            do
            {
                nodesDictionary.Add(node!.Value, node);
                node = node.Next;
            } while (node != null);

            return nodesDictionary;
        }

        private static void RepositionCircularList(ref LinkedList<int> circularList, int currentIndex, int currentValue)
        {
            while (circularList.ElementAt(currentIndex) != currentValue)
            {
                var first = circularList.First!.Value;
                circularList.RemoveFirst();
                circularList.AddLast(first);
            }
        }

        private static LinkedListNode<int> FindDestinationNode(
            IList<int> poppedElements,             
            Dictionary<int, LinkedListNode<int>> nodesDictionary,
            int current, int min, int max)
        {
            do current = current - 1 >= min ? current - 1 : max;
            while (current == poppedElements[0] || current == poppedElements[1] || current == poppedElements[2]);
            return nodesDictionary[current];
        }

        private static List<int> RemoveElementsFromList(
            ref LinkedList<int> circularList, 
            ref Dictionary<int, LinkedListNode<int>> nodesDictionary, 
            LinkedListNode<int> currentNode)
        {
            var result = new List<int>();
            for(int i = 0; i < 3; i++)
            {
                var nodeToRemove = currentNode.CircularNext()!;
                result.Add(nodeToRemove.Value);
                nodesDictionary.Remove(nodeToRemove.Value);
                circularList.Remove(nodeToRemove);
            }

            return result;
        }

        private static void AddElementsToList(
            ref LinkedList<int> circularList,
            ref Dictionary<int, LinkedListNode<int>> nodesDictionary, 
            LinkedListNode<int> node,
            IList<int> valuesToAdd)
        {
            foreach(var value in valuesToAdd)
            {
                circularList.AddAfter(node, value);
                node = node.CircularNext()!;
                nodesDictionary.Add(value, node);
            }
        }

        /// <summary>
        /// Same as before, but now with 1M elements and 10M iterations!
        /// <para> Avg execution time: 30 seconds | O(K) </para>
        /// </summary>
        public override string Part2()
        {
            var circularList = input[0].Select(c => int.Parse(c.ToString())).ToLinkedList();
            var max = circularList.Max();
            for (int i = max + 1; i <= 1000000; i++) circularList.AddLast(i);
            max = 1000000;
            var nodesDictionary = ExtractNodeDictionary(circularList);

            var min = circularList.Min();
            var counter = 0;

            LinkedListNode<int> currentNode = circularList.First!;

            while (++counter <= 10000000)
            {
                var poppedElements = RemoveElementsFromList(ref circularList, ref nodesDictionary, currentNode);
                var destinationNode = FindDestinationNode(poppedElements, nodesDictionary, currentNode.Value, min, max);
                AddElementsToList(ref circularList, ref nodesDictionary, destinationNode, poppedElements);

                currentNode = currentNode!.CircularNext()!;
            }

            var node1 = nodesDictionary[1];
            var n1 = node1.CircularNext()!;
            var n2 = n1.CircularNext()!;
            var result = (long)n1.Value * n2.Value;
            return result.ToString();
        }
    }
}