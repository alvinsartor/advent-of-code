﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/14

    public class Day14 : DailyChallenge
    {
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 50;

        /// <summary>
        /// Given a series of masks and numbers, apply the masks, place the numbers to the right 
        /// memory addresses and, finally, return the sum of all saved numbers.
        /// <para> Avg 1000 executions: 1.201ms | O(n) </para>
        /// </summary>
        public override string Part1() {
            var memory = new Dictionary<string, ulong>();
            var mask = "";
                
            foreach (var instruction in input.Select(line => line.Split(" = ")))
            {
                var type = instruction[0];
                var value = instruction[1];
                if (type == "mask")
                {
                    mask = value;
                    continue;
                }
                
                var binary = Convert.ToString(long.Parse(value), 2).PadLeft(mask.Length, '0').ToCharArray();
                var masked = ApplyMaskOnValue(binary, mask);
                string memAddress = type[4..type.IndexOf(']')];
                memory[memAddress] = Convert.ToUInt64(masked, 2);                
            }

            return ((ulong) memory.Values.Sum(x => (decimal)x)).ToString();
        }

        private static string ApplyMaskOnValue(char[] value, string mask) => 
            new string(value.Select((c, i) => mask[i] != 'X' ? mask[i] : c).ToArray());

        /// <summary>
        /// Now the mask should be applied to the addresses instead of the numbers. Each application produces multiple addresses.
        /// The result is still the sum of all saved numbers.
        /// <para> Avg 50 executions: 84ms | O(nk) </para>
        /// </summary>
        public override string Part2()
        {
            var memory = new Dictionary<string, ulong>();
            var mask = "";

            foreach (var instruction in input.Select(line => line.Split(" = ")))
            {
                var type = instruction[0];
                var value = instruction[1];
                if (type == "mask")
                {
                    mask = value;
                    continue;
                }

                var binaryMemAddress = Convert.ToString(long.Parse(type[4..type.IndexOf(']')]), 2).PadLeft(mask.Length, '0');
                var maskedAddresses = ApplyFloatingMask(binaryMemAddress.ToCharArray(), mask);
                foreach (var address in maskedAddresses) memory[address] = ulong.Parse(value);                    
            }

            return ((ulong)memory.Values.Sum(x => (decimal)x)).ToString();
        }

        private static string[] ApplyFloatingMask(char[] value, string mask)
        {
            var floating = value.Select((c, i) => mask[i] == '0' ? c : mask[i]).ToArray();
            return SubstituteXWith01s(new string(floating));
        }

        private static string[] SubstituteXWith01s(string binary)
        {
            if (!binary.Contains('X')) return new[] { binary };

            var indexOfX = binary.IndexOf('X');
            var bin1 = binary.ToCharArray();
            bin1[indexOfX] = '0';
            var bin2 = binary.ToCharArray();
            bin2[indexOfX] = '1';

            var arr1 = SubstituteXWith01s(new string(bin1));
            var arr2 = SubstituteXWith01s(new string(bin2));
            return arr1.Concat(arr2).ToArray();
        }
    }
}