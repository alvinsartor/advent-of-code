﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/10

    public class Day10 : DailyChallenge
    {
        private readonly int[] intInput;
        public Day10() : base() => intInput = input.Select(int.Parse).ToArray();

        /// <summary>
        /// Given a list of connected adapters, find the joltage difference between them and return 
        /// the number of 1-jolt differences multiplied to the number of 3-jolts differences.
        /// <para>Once the list is sorted, just compute the difference between each individual adapter.</para>
        ///  <para> Avg 1000 executions: 0.007ms | O(nlogn) </para>
        /// </summary>
        public override string Part1()
        {
            var sortedInput = intInput.Append(0).OrderBy(x => x).ToArray();
            var oneJoltDifferences = 0;
            var threeJoltsDifferences = 1;

            for (int i = 0; i < sortedInput.Length - 1; i++)
            {
                var diff = sortedInput[i + 1] - sortedInput[i];
                if (diff == 1) oneJoltDifferences++;
                else if (diff == 3) threeJoltsDifferences++;
            }

            var result = oneJoltDifferences * threeJoltsDifferences;
            return result.ToString();
        }

        /// <summary>
        /// Find the number of combinations of adapters in which there is <= 3 jolts of difference 
        /// between an adapter and the following one.
        /// <para>With 2^n possible solutions, computing through bruteforce is not possible. 
        /// We'll better use some dynamic programming to break down the problem in small segments.</para>
        /// <para> Avg 1000 executions: 0.009ms | O(nlogn) </para>
        /// </summary>
        public override string Part2()
        {
            var sortedInput = intInput.Append(0).Append(intInput.Max()).OrderBy(x => x).ToArray();
            return CalculateAdaptersCombinations(sortedInput, 0).ToString(); 
        }

        private readonly Dictionary<int, long> AlreadyCalculatedNrOfPaths = new Dictionary<int, long>();
        private long CalculateAdaptersCombinations(int[] input, int i)
        {
            // we reached the second-last adapter. Only 1 path from here till the end.
            if (i == input.Length - 2)
                return 1;

            // to save time, we don't need to recalculate the number of paths each time.
            // when we find it, we save it and we make it available for the future.
            if (AlreadyCalculatedNrOfPaths.ContainsKey(i))
                return AlreadyCalculatedNrOfPaths[i];

            // paths(i, max) == paths(i, i+1) + paths(i+1, max)
            // so we recursively push I till Length-1 (where we have 1 path only)
            // and then descend, adding paths at each step.
            long answer = 0;
            for (int j = i + 1; j < Math.Min(input.Length, i + 4); j++)
                if (input[j] - input[i] <= 3)
                    answer += CalculateAdaptersCombinations(input, j);

            AlreadyCalculatedNrOfPaths[i] = answer;
            return answer;
        }
    }
}