﻿using System.Linq;

namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/1
    
    public class Day01: DailyChallenge
    {

        private readonly int[] intInput;
        public Day01() : base() => intInput = input.Select(int.Parse).ToArray();
  
        /// <summary>
        /// Given a list of number, find the product of the #_two_# number that summed give 2020.
        /// <para> Hashtables are great for this as it allows us to just run through the array once and check for the existence of the complementary member. </para>
        /// <para> Avg 1000 executions: 0.012ms | O(n) </para>
        /// </summary>
        public override string Part1()
        {
            var hash = intInput.ToHashSet();
            foreach (var n in hash)
                if (hash.Contains(2020- n))
                    return (n * (2020 - n)).ToString();
            
            return "?";
        }

        /// <summary>
        /// Given a list of number, find the product of the #_three_# number that summed give 2020.
        /// <para> Through sorting and smart capping the loops, we can achieve a pretty good result.</para>
        /// <para> Avg 1000 executions: 0.023ms | O(nlogn)+ </para>
        /// </summary>
        public override string Part2()
        {
            var sorted = intInput.OrderBy(x => x).ToArray();
            for (int i = 0; i < sorted.Length - 2; i++)
                for (int j = i + 1; j < sorted.Length - 1 && sorted[i] + sorted[j] <= 2020; j++)
                    for (int k = j + 1; k < sorted.Length && sorted[i] + sorted[j] + sorted[k] <= 2020; k++)
                        if (sorted[i] + sorted[j] + sorted[k] == 2020)
                            return (sorted[i] * sorted[j] * sorted[k]).ToString();
            return "?";
        }
    }
}