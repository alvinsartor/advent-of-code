﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/6

    public class Day06 : DailyChallenge
    {
        /// <summary>
        /// Count the number of answers ANY of the member of the group answered yes to.
        /// <para> Avg 1000 executions: 0.688ms | O(n) </para>
        /// </summary>
        public override string Part1()
        {
            var adjustedInput = input.Append("");
            Group group = new Group();
            int counter = 0;
            foreach (string line in adjustedInput)
                counter += AddLineToGroup(line, group);

            return counter.ToString();
        }

        /// <summary>
        /// Count the number of answers ALL of the member of the group answered yes to.
        /// <para> Avg 1000 executions: 0.720ms | O(n) </para>
        /// </summary>
        public override string Part2()
        {
            var adjustedInput = input.Append("");
            Group group = new Group();
            int counter = 0;
            foreach (string line in adjustedInput)
                counter += AddLineToGroup(line, group, false);

            return counter.ToString();
        }

        private static int AddLineToGroup(string line, Group group, bool union = true)
        {
            if (string.IsNullOrEmpty(line))
            {
                var count = group.Count();
                group.Clear();
                return count;
            }

            if (union) group.Union(line);
            else group.Intersect(line);
            return 0;
        }

        /// <summary>
        /// Object representing the answers of a group of passengers.
        /// </summary>
        class Group
        {
            private HashSet<char> answers = new HashSet<char>();
            private bool newGroup = true;
            public void Clear() => newGroup = true;
            public int Count() => answers.Count;

            /// <summary>
            /// Intersects the specified line with the other answers from the group.
            /// </summary>
            public void Intersect(string line)
            {
                if (newGroup) Reinitialize(line);
                else answers.IntersectWith(line);
                newGroup = false;
            }

            /// <summary>
            /// Performs a union operation between the other answrs from the group and the specified line.
            /// </summary>
            public void Union(string line)
            {
                if (newGroup) Reinitialize(line);
                else answers.UnionWith(line);                
            }

            private void Reinitialize(string line)
            {
                answers = line.ToHashSet();
                newGroup = false;
            }
        }
    }
}
