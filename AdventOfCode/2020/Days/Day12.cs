﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace AdventOfCode._2020.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2020/day/12

    public class Day12 : DailyChallenge
    {
        private static readonly Vector2 NORTH = new Vector2(0, 1);
        private static readonly Vector2 EAST = new Vector2(1, 0);
        private static readonly Vector2 SOUTH = new Vector2(0, -1);
        private static readonly Vector2 WEST = new Vector2(-1, 0);

        /// <summary>
        /// Given a set of navigation instructions, calculate the Manhattan distance to the final destination.
        /// <para> Avg 1000 executions: 0.128ms | O(n) </para>
        /// </summary>
        public override string Part1()
        {
            Vector2 current = new Vector2(0, 0);
            char facing = 'E';

            foreach (var instruction in input)
                (facing, current) = ApplyInstruction(instruction, facing, current);

            return ((int)(Math.Abs(current.X) + Math.Abs(current.Y))).ToString();
        }

        private static (char facing, Vector2 pos) ApplyInstruction(string instruction, char facing, Vector2 currentPos)
        {
            char direction = instruction[0];
            int amount = int.Parse(instruction[1..]);
            return direction switch
            {
                'F' => ApplyInstruction(facing.ToString() + amount.ToString(), facing, currentPos),
                'N' => (facing, currentPos + NORTH * amount),
                'E' => (facing, currentPos + EAST * amount),
                'S' => (facing, currentPos + SOUTH * amount),
                'W' => (facing, currentPos + WEST * amount),
                'L' => (CalculateNewDirectionAfterTurning(facing, direction, amount), currentPos),
                'R' => (CalculateNewDirectionAfterTurning(facing, direction, amount), currentPos),
                _ => throw new NotImplementedException(),
            };
        }

        private static char CalculateNewDirectionAfterTurning(char current, char turningSide, int amount)
        {
            List<char> directions = (new[] { 'N', 'E', 'S', 'W' }).ToList();
            int steps = amount / 90;
            var dir = turningSide == 'R' ? +1 : -1;
            var currentDirection = directions.IndexOf(current);
            while (steps-- > 0)
            {
                currentDirection += dir;
                currentDirection = currentDirection < 0 ? 3 : currentDirection > 3 ? 0 : currentDirection;
            }
            return directions[currentDirection];
        }

        /// <summary>
        /// Same as before, but now the directions are relative to a waypoint.
        /// <para> Avg 1000 executions: 0.065ms | O(n) </para>
        /// </summary>
        public override string Part2()
        {
            Vector2 current = new Vector2(0, 0);
            Vector2 waypoint = new Vector2(10, 1);

            foreach (var instruction in input)
                (waypoint, current) = ApplyInstructionWithWaypoint(instruction, waypoint, current);

            return ((int)(Math.Abs(current.X) + Math.Abs(current.Y))).ToString();
        }

        private static (Vector2 waypoint, Vector2 current) ApplyInstructionWithWaypoint(string instruction, Vector2 waypoint, Vector2 current)
        {
            char direction = instruction[0];
            int amount = int.Parse(instruction[1..]);

            return direction switch
            {
                'F' => (waypoint, current + waypoint * amount),
                'N' => (waypoint + NORTH * amount, current),
                'E' => (waypoint + EAST * amount, current),
                'S' => (waypoint + SOUTH * amount, current),
                'W' => (waypoint + WEST * amount, current),
                'R' => (CalculateNewWaypointAfterTurning(waypoint, direction, amount), current),
                'L' => (CalculateNewWaypointAfterTurning(waypoint, direction, amount), current),
                _ => throw new NotImplementedException(),
            };
        }

        private static Vector2 CalculateNewWaypointAfterTurning(Vector2 waypoint, char direction, int amount)
        {
            if (amount == 0) return waypoint;

            Vector2 newWaypoint = direction switch
            {
                'R' => TurnWaypointToRight(waypoint),
                'L' => TurnWaypointToLeft(waypoint),
                _ => throw new NotImplementedException(),
            };
            return CalculateNewWaypointAfterTurning(newWaypoint, direction, amount - 90);
        }

        private static Vector2 TurnWaypointToLeft(Vector2 waypoint) => new Vector2(waypoint.Y * -1, waypoint.X);
        private static Vector2 TurnWaypointToRight(Vector2 waypoint) => new Vector2(waypoint.Y, waypoint.X * -1);
    }
}