﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/16

    class Day16 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 10;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 10;

        /// <summary>
        /// Apply the FFT algorithm 100 times to the given sequence and return the first 8 digits of the final result.
        /// </summary>
        public override string Part1()
        {   
            var sequence = input[0].Select(c => int.Parse(c.ToString())).ToArray();
            var (addends, subtraends) = CalculateAddendsAndSubtraends(sequence.Length);

            var counter = 100;
            while (counter-- > 0) sequence = FFTAlgorithm(sequence, addends, subtraends);

            return string.Join("", sequence.Take(8));
        }


        private static int[] FFTAlgorithm(int[] input, int[][] addends, int[][] subtraends)
        {
            var newInput = new int[input.Length];
            for (int i = 0; i < input.Length; i++)
            {
                var rowAddends = addends[i].Select(i => input[i]).Sum();
                var rowSubtraends = subtraends[i].Select(i => input[i]).Sum();
                newInput[i] = Math.Abs(rowAddends - rowSubtraends) % 10;
            }

            return newInput;
        }

        private static (int[][] addends, int[][] subtrahends) CalculateAddendsAndSubtraends(int length)
        {
            int[][] addends = new int[length][];
            int[][] subtrahends = new int[length][];
            for (int i = 0; i < length; i++)
            {
                (int[] rowAddends, int[] rowSubtraends) = CalculateAddendsAndSubtraendsForARow(i, length);
                addends[i] = rowAddends;
                subtrahends[i] = rowSubtraends;
            }

            return (addends, subtrahends);
        }

        private static (int[] addends, int[] subtraends) CalculateAddendsAndSubtraendsForARow(int row, int length)
        {
            var singleRepetitions = row + 1;
            var groupRepetitions = singleRepetitions * 4;
            var initialOffsetAddends = singleRepetitions - 1;
            var initialOffsetSubtraends = initialOffsetAddends + singleRepetitions * 2;

            var addends = GetIndexesOfRepetition(initialOffsetAddends, singleRepetitions, groupRepetitions, length).ToArray();
            var subtraends = GetIndexesOfRepetition(initialOffsetSubtraends, singleRepetitions, groupRepetitions, length).ToArray();
            return (addends, subtraends);
        }

        private static IEnumerable<int> GetIndexesOfRepetition(int initialOffset, int timesToRepeat, int intervals, int maxIndex)
        {
            int n = initialOffset;
            int round = 0;
            while (n < maxIndex)
            {
                for (var i = 0; i < timesToRepeat; i++)
                {
                    n = initialOffset + i + round * intervals;
                    if (n < maxIndex) yield return n;
                }
                round++;
            }
        }

        /// <summary>
        /// Same as before, but repeat the input 10'000 times!
        /// </summary>
        public override string Part2()
        {
            var sequence = input[0].Select(c => int.Parse(c.ToString())).ToArray();
            var offset = int.Parse(string.Join("", sequence.Take(7).ToArray()));
                        
            // being the offset way over the half of the array, 
            // we can remove all the previous numbers and simply calculate the result as a series of sums
            sequence = Enumerable.Repeat(sequence, 10000).SelectMany(n => n).Skip(offset).ToArray();
            
            var counter = 100;
            while (counter-- > 0) sequence = FFTAlgorithm_V2(sequence);

            return string.Join("", sequence.Take(8));
        }

        private static int[] FFTAlgorithm_V2(int[] input)
        {
            for (int i = input.Length - 2; i >= 0; i--)
                input[i] += input[i + 1];
            return input.Select(n => n % 10).ToArray();
        }
    }
}
