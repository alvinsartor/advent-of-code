﻿using AdventOfCode._2019.Days.IntcodeComputer;
using AdventOfCode.helpers;
using Graph;
using Graph.PathFinding;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/25

    class Day25 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 5;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 0;

        private readonly long[] program;
        public Day25() : base() => program = input[0].Split(",").Select(long.Parse).ToArray();

        /// <summary>
        /// Navigate a droid to the spaceship of santa and pass the pressure sensor to finally find Santa!
        /// </summary>
        public override string Part1()
        {
            var exploringDroid = new ExploringDroid(program, false);            
            return exploringDroid.AutomaticExploration();

            // #-*-#-*-#   Uncomment this block (and comment the previous one) to manually play the game!   #-*-#-*-# //
            //var exploringDroid = new ExploringDroid(program, false);
            //exploringDroid.Manual() 
            //return "You tell me!";
        }

        /// <summary> 
        /// Not needed. The star is given if all the other challenges are solved. 
        /// </summary>
        public override string Part2() => "None";

        private class ExploringDroid : ASCIIComputer
        {
            private readonly HashSet<string> DangerousObjects = new HashSet<string> { 
                "infinite loop", 
                "molten lava", 
                "escape pod",
                "giant electromagnet",
                "photons", 
            };
            private readonly Graph<Point> spaceshipMap;
            private readonly Dictionary<Point, string> roomsPositions;
            private readonly HashSet<Point> horizon;
            private readonly Queue<string> directions;
            private Point currentPosition;

            public ExploringDroid(long[] program, bool displayConsoleOutputs) : base(program, displayConsoleOutputs, true)
            {
                currentPosition = Point.Origin;
                spaceshipMap = new Graph<Point>();
                spaceshipMap.Add(currentPosition);
                roomsPositions = new Dictionary<Point, string>();
                horizon = new HashSet<Point> { currentPosition };
                directions = new Queue<string>();
            }

            public string AutomaticExploration()
            {
                MapSpaceshipAndRecollectObjects();
                return CrackThePressureSensitiveFloor();                
            }

            private void MapSpaceshipAndRecollectObjects()
            {
                while (horizon.Count > 0)
                {
                    var outputs = ExecuteCurrentInstruction();
                    horizon.Remove(currentPosition);
                    AddCurrentRoom(outputs);
                    AddHorizonPointsToGraph(outputs);
                    PickupObjects(outputs);
                    if (horizon.Count > 0)
                    {
                        DefineNextDirections();
                        InputDirectionCommand();
                    }
                }
            }

            private string CrackThePressureSensitiveFloor()
            {
                NavigateToRoom("Security Checkpoint");
                var pressureRoomPosition = GetRoomPosition("Pressure-Sensitive Floor");
                var pressureRoomDirection = PointToDirection(pressureRoomPosition - currentPosition) + "\n";

                var winningMessage = TryAllCombinationsUntilPressureSensitiveFloorIsCracked(pressureRoomDirection);
                return winningMessage[11][51..61];
            }

            private List<string> TryAllCombinationsUntilPressureSensitiveFloorIsCracked(string pressureRoomDirection)
            {
                var itemsInInventory = GetItemsInYourInventory().ToArray();
                var itemsCombinations = itemsInInventory.Combinations();

                foreach (var combination in itemsCombinations)
                {
                    var hashCombination = combination.ToHashSet();
                    foreach (var item in itemsInInventory)
                    {
                        string command;
                        if (hashCombination.Contains(item)) command = $"take {item}\n";
                        else command = $"drop {item}\n";

                        command.ForEach(c => computer.Inputs.Enqueue(c));
                        ExecuteCurrentInstruction();
                    }

                    pressureRoomDirection.ForEach(c => computer.Inputs.Enqueue(c));
                    var outputs = ExecuteCurrentInstruction();

                    if (!outputs.Any(line => line.Contains("heavier") || line.Contains("lighter"))) 
                        return outputs;
                }

                throw new Exception("I tried all the combinations but none worked 🤖");
            }

            private void NavigateToRoom(string roomName)
            {
                var roomPosition = GetRoomPosition(roomName);
                if (currentPosition.Equals(roomPosition)) return;

                var path = spaceshipMap.AStarSearch(currentPosition, roomPosition, (p) => p.Distance(roomPosition)).Result;
                EnqueuePathDirections(path);
                while(directions.Count > 0)
                {
                    InputDirectionCommand();
                    ExecuteCurrentInstruction();
                }
            }

            private Point GetRoomPosition(string roomName) => roomsPositions.Single(pair => pair.Value == roomName).Key;

            private List<string> ExecuteCurrentInstruction()
            {
                ExecuteTillHalts();
                var outputs = new List<string>();
                while (ProcessedOutputs.Count > 0) outputs.Add(ProcessedOutputs.Dequeue());
                return outputs;
            }

            private void InputDirectionCommand()
            {
                var direction = directions.Dequeue();
                (direction + "\n").ForEach(c => computer.Inputs.Enqueue(c));
                currentPosition += DirectionToPoint(direction);
            }

            private void AddCurrentRoom(List<string> outputs)
            {
                if (roomsPositions.ContainsKey(currentPosition)) return;
                var roomName = outputs.First(line => line.Contains("== ")).Trim('=').Trim();
                roomsPositions.Add(currentPosition, roomName);
            }

            private void PickupObjects(List<string> outputs)
            {
                var takeInstructions = GetCurrentRoomObjects(outputs)
                    .Where(obj => !DangerousObjects.Contains(obj))
                    .Select(obj => $"take {obj}\n");

                foreach (var instruction in takeInstructions)
                {
                    instruction.ForEach(c => computer.Inputs.Enqueue(c));
                }
            }

            private static IEnumerable<string> GetCurrentRoomObjects(List<string> outputs)
            {
                var objectsStart = outputs.IndexOf("Items here:") + 1;
                if (objectsStart > -1)
                    while (!string.IsNullOrWhiteSpace(outputs[objectsStart]))
                        yield return outputs[objectsStart++][2..];
            }

            private IEnumerable<string> GetItemsInYourInventory()
            {
                "inv\n".ForEach(c => computer.Inputs.Enqueue(c));
                var outputs = ExecuteCurrentInstruction();
                var objectsStart = outputs.IndexOf("Items in your inventory:") + 1;
                if (objectsStart > -1)
                    while (!string.IsNullOrWhiteSpace(outputs[objectsStart]))
                        yield return outputs[objectsStart++][2..];
            }

            private void AddHorizonPointsToGraph(List<string> outputs)
            {
                var currentRoomDirections = GetCurrentRoomDirections(outputs);
                var currentRoomNode = spaceshipMap.GetNode(currentPosition);
                foreach (var direction in currentRoomDirections)
                {
                    var roomPosition = currentPosition + direction;
                    if (!spaceshipMap.Contains(roomPosition))
                    {
                        var addedNode = spaceshipMap.Add(roomPosition);
                        addedNode.AddBidirectionalConnection(currentRoomNode);

                        if (!roomsPositions.ContainsKey(roomPosition)) horizon.Add(roomPosition);
                    }
                }
            }

            private void DefineNextDirections()
            {
                if (directions.Count > 0) return;
                
                var pathToClosestHorizon = horizon
                    .Select(pt => spaceshipMap.AStarSearch(currentPosition, pt, (p) => p.Distance(pt)).Result)
                    .MinBy(path => path.Cost);

                EnqueuePathDirections(pathToClosestHorizon);
            }

            private void EnqueuePathDirections(Path<Point> path)
            {
                var pathPositions = path.Nodes.Select(node => node.Value).ToArray();
                for (int i = 1; i < pathPositions.Length; i++)
                {
                    var relativeDirection = pathPositions[i] - pathPositions[i - 1];
                    directions.Enqueue(PointToDirection(relativeDirection));
                }
            }

            private static IEnumerable<Point> GetCurrentRoomDirections(List<string> outputs)
            {
                var doorsDirectionsStart = outputs.IndexOf("Doors here lead:") + 1;
                while (!string.IsNullOrWhiteSpace(outputs[doorsDirectionsStart]))
                    yield return DirectionToPoint(outputs[doorsDirectionsStart++][2..]);
            }

            private static Point DirectionToPoint(string direction) => direction switch
            {
                "north" => Point.Up,
                "east" => Point.Right,
                "south" => Point.Down,
                "west" => Point.Left,
                _ => throw new NotImplementedException($"Direction not recognized: {direction}"),
            };

            private static string PointToDirection(Point point) => point switch
            {
                Point(0, -1) => "north",
                Point(1, 0) => "east",
                Point(0, 1) => "south",
                Point(-1, 0) => "west",
                _ => throw new NotImplementedException($"Direction not recognized: {point}"),
            };

            public void Manual()
            {
                ExecuteTillHalts();
                (Console.ReadLine() + "\n").ForEach(c => computer.Inputs.Enqueue(c));
                Manual();
            }
        }
    }
}
