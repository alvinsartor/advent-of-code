﻿using AdventOfCode.helpers;
using System;
using System.Linq;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/12

    class Day12 : DailyChallenge
    {
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 50;
        
        /// <summary>
        /// Simulate the position of 4 Jupiter moons over time, using each other position to influence the velocity.
        /// </summary>
        public override string Part1()
        {
            var planets = input.Select(ExtractPointFromInputLine).Select(pos => new Planet(pos)).ToArray();
            int counter = 1000;
            while (counter-- > 0)
            {
                for (int i = 0; i < planets.Length - 1; i++)
                    for (int j = i + 1; j < planets.Length; j++)
                    {
                        planets[i].UpdateVelocityByGravityTowards(planets[j]);
                        planets[j].UpdateVelocityByGravityTowards(planets[i]);
                    }
                planets.ForEach(planet => planet.ApplyVelocity());
            }

            return planets.Sum(planet => planet.TotalEnergy).ToString();
        }

        /// <summary>
        /// After how many cycles do planets go back to the initial position? Calculate!
        /// </summary>
        public override string Part2()
        {
            var positions = input.Select(ExtractPointFromInputLine).ToArray();

            var NumberOfCyclesForXToRepeat = NumberOfCyclesForAxisToRepeat(positions.Select(p => p.X).ToArray());
            var NumberOfCyclesForYToRepeat = NumberOfCyclesForAxisToRepeat(positions.Select(p => p.Y).ToArray());
            var NumberOfCyclesForZToRepeat = NumberOfCyclesForAxisToRepeat(positions.Select(p => p.Z).ToArray());

            var lcm1 = MathExtensions.LeastCommonMultiple(NumberOfCyclesForXToRepeat, NumberOfCyclesForYToRepeat);
            var lcm2 = MathExtensions.LeastCommonMultiple(lcm1, NumberOfCyclesForZToRepeat);

            return lcm2.ToString();
        }

        private static int NumberOfCyclesForAxisToRepeat(double[] initialValues)
        {
            var planets = initialValues.Select(v => new MonodimensionalPlanet(v)).ToArray();
            var counter = 0;
            do
            {
                for (int i = 0; i < planets.Length - 1; i++)
                    for (int j = i + 1; j < planets.Length; j++)
                    {
                        planets[i].UpdateVelocityByGravityTowards(planets[j]);
                        planets[j].UpdateVelocityByGravityTowards(planets[i]);
                    }
                planets.ForEach(planet => planet.ApplyVelocity());
                counter++;
            } while (!IsInitialState(initialValues, planets));

            return counter;
        }

        private static bool IsInitialState(double[] initialValues, MonodimensionalPlanet[] planets)
        {
            for (int i = 0; i < initialValues.Length; i++)
                if (planets[i].Velocity != 0 || planets[i].Position != initialValues[i])
                    return false;
            return true;
        }

        private static Point3D ExtractPointFromInputLine(string line)
        {
            var axis = line[1..^1].Split(", ").Select(axis => double.Parse(axis[2..])).ToArray();
            return new Point3D(axis[0], axis[1], axis[2]);
        }

        private class Planet
        {
            public Point3D Position { get; private set; }
            public Point3D Velocity { get; set; }
            public double PotentialEnergy => Math.Abs(Position.X) + Math.Abs(Position.Y) + Math.Abs(Position.Z);
            public double KineticEnergy => Math.Abs(Velocity.X) + Math.Abs(Velocity.Y) + Math.Abs(Velocity.Z);
            public double TotalEnergy => PotentialEnergy * KineticEnergy;

            public Planet(Point3D planetPosition) => (Position, Velocity) = (planetPosition, Point3D.Origin);
            public void ApplyVelocity() => Position += Velocity;
            public void UpdateVelocityByGravityTowards(Planet otherPlanet)
            {
                var delta = otherPlanet.Position - Position;
                var sign = delta.Sign();
                Velocity += sign;
            }

            public override string ToString() => $"pos=<{Position}>, vel=<{Velocity}>";
        }

        private class MonodimensionalPlanet
        {
            public double Position { get; private set; }
            public double Velocity { get; set; }
            
            public MonodimensionalPlanet(double initialPosition) => (Position, Velocity) = (initialPosition, 0);
            public void ApplyVelocity() => Position += Velocity;
            public void UpdateVelocityByGravityTowards(MonodimensionalPlanet otherPlanet) => 
                Velocity += Math.Sign(otherPlanet.Position - Position);
        }
    }
}
