﻿using System.Linq;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/1

    class Day01: DailyChallenge
    {
        /// <summary>
        /// Calculate the amount of fuel needed to propel the spaceship.
        /// </summary>
        public override string Part1() => input.Select(x => (int.Parse(x) / 3) - 2).Sum().ToString();

        /// <summary>
        /// Calculate the amount of fuel needed to propel the spaceship, taking into account the weight of the same fuel as well.
        /// </summary>
        public override string Part2() => input.Select(x => RecursiveFueling(int.Parse(x))).Sum().ToString();

        private int RecursiveFueling(int mass)
        {
            var neededFuel = mass / 3 - 2;
            return neededFuel <= 0 ? 0 : neededFuel + RecursiveFueling(neededFuel);
        }
    }
}
