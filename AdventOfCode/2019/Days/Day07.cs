﻿using AdventOfCode._2019.Days.IntcodeComputer;
using AdventOfCode.helpers;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/7

    class Day07 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 100;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 100;

        private readonly long[] program;
        public Day07() : base() => program = input[0].Split(",").Select(long.Parse).ToArray();

        /// <summary>
        /// Use the Intercode Computer of Day5 to run amplify a signal. 
        /// What is the highest number we can obtain using any possible permutation of the settings array?.
        /// </summary>
        public override string Part1()
        {
            var possibleSettingsValues = new[] { 0, 1, 2, 3, 4 }.Permutations();
            var amplifiersPermutations = possibleSettingsValues.Select(GetSimpleAmplifiersFromConfiguration);
            var outputResults = amplifiersPermutations.Select(amplifiers => RunAmplifierPrograms(amplifiers));
            return outputResults.Max().ToString();
        }

        private Amplifier[] GetSimpleAmplifiersFromConfiguration(int[] configuration) =>
            configuration.Select(setting => new Amplifier(program, setting)).ToArray();

        private static long RunAmplifierPrograms(Amplifier[] amplifiersChain)
        {
            long input = 0;
            foreach (var amplifier in amplifiersChain) {
                amplifier.Inputs.Enqueue(input);
                amplifier.RunProgram();
                long output = amplifier.Outputs.Dequeue();
                input = output; 
            } 
            return input;
        }

        /// <summary>
        /// Now use a feedback loop to continuously re-execute the program. 
        /// What is the highest value we can obtain now?
        /// </summary>
        public override string Part2()
        {
            var possibleSettingsValues = new[] { 9, 8, 7, 6, 5 }.Permutations();
            var amplifiersPermutations = possibleSettingsValues.Select(GetInterconnectedAmplifiersFromConfiguration);
            var outputResults = amplifiersPermutations.Select(amplifiers => RunAmplifierProgramsWithFeedbackLoop(amplifiers));
            return outputResults.Max().ToString();
        }

        private Amplifier[] GetInterconnectedAmplifiersFromConfiguration(int[] settings)
        {
            var connection01 = new Queue<long>();
            var connection12 = new Queue<long>();
            var connection23 = new Queue<long>();
            var connection34 = new Queue<long>();
            var connection40 = new Queue<long>();

            var amp0 = new Amplifier(program, settings[0], connection40, connection01);
            var amp1 = new Amplifier(program, settings[1], connection01, connection12);
            var amp2 = new Amplifier(program, settings[2], connection12, connection23);
            var amp3 = new Amplifier(program, settings[3], connection23, connection34);
            var amp4 = new Amplifier(program, settings[4], connection34, connection40);

            return new[] { amp0, amp1, amp2, amp3, amp4 };
        }

        private static long RunAmplifierProgramsWithFeedbackLoop(Amplifier[] amplifiersChain)
        {
            amplifiersChain[0].Inputs.Enqueue(0);
            while(!amplifiersChain[4].computer.HasEnded)
                foreach(var amplifier in amplifiersChain)
                    amplifier.RunUntilItHalts();
                
            return amplifiersChain[4].computer.StoredOutputs.Pop();
        }

        private class Amplifier
        {
            private readonly int phaseSetting;

            public readonly IntcodeComputer_V4 computer;
            public readonly Queue<long> Inputs;
            public readonly Queue<long> Outputs;

            public Amplifier(
                long[] program, 
                int phaseSetting,
                Queue<long>? inputChannel = null,
                Queue<long>? outputChannel = null)
            {
                Inputs = inputChannel ?? new Queue<long>();
                Outputs = outputChannel ?? new Queue<long>();
                this.phaseSetting = phaseSetting;
                computer = new IntcodeComputer_V4(program.ToArray(), Inputs, Outputs);
                Inputs.Enqueue(phaseSetting);
            }

            public void RunProgram() => computer.Execute();
            public void RunUntilItHalts()
            {
                bool shouldHalt = false;
                while(!shouldHalt) shouldHalt = computer.ExecuteNextInstruction();
            }

            public override string ToString() => $"Settings: {phaseSetting}";
        }
    }
}
