﻿using AdventOfCode._2019.Days.IntcodeComputer;
using AdventOfCode.helpers;
using Graph;
using Graph.PathFinding;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/14

    class Day15 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 100;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 100;

        private readonly long[] program;
        public Day15() : base() => program = input[0].Split(",").Select(long.Parse).ToArray();

        /// <summary>
        /// Find the length of the shortest path to the section of the ship where the oxigen is leaking.
        /// </summary>
        public override string Part1()
        {
            var map = new Map();
            var robot = new RepairingRobot(program, Map.StartingPoint());
            map.DiscoverPosition(robot.CurrentPosition, 1);

            bool stopCondition() => map.LeakPosition != null;
            AutomaticControl(robot, map, stopCondition, false); // set to 'true' to print the map each step.

            var pathToLeak = map.PathTo(Map.StartingPoint(), map.LeakPosition!);
            return pathToLeak.Edges.Count.ToString();
        }

        private static void AutomaticControl(RepairingRobot robot, Map map, Func<bool> StopCondition, bool printMap = true)
        {
            while (!StopCondition())
            {
                Point closestFrontier = map.ClosestFrontierTo(robot.CurrentPosition);
                Path<Point> pathToClosestFrontier = map.PathTo(robot.CurrentPosition, closestFrontier);
                pathToClosestFrontier.Nodes.Select(n => n.Value).ForEach(point => robot.MoveTowards(point));
                var surroundingsToExplore = map.UnexploredNodesSurrounding(closestFrontier);

                foreach(Point pointToExplore in surroundingsToExplore)
                {
                    var direction = robot.DestinationToDirection(pointToExplore);
                    var output = robot.ExecuteStep(direction);
                    map.DiscoverPosition(pointToExplore, output);
                    robot.MoveTowards(closestFrontier);
                    if (printMap) map.PrintMap(robot.CurrentPosition);
                }
            }
        }

        /// <summary>
        /// Calculate how long it will take for the oxigen to fill the whole ship compartiment.
        /// </summary>
        public override string Part2()
        {
            var map = new Map();
            var robot = new RepairingRobot(program, Map.StartingPoint());
            map.DiscoverPosition(robot.CurrentPosition, 1);

            bool stopCondition() => !map.HasUnexploredAreas;
            AutomaticControl(robot, map, stopCondition, false); // set to 'true' to print the map each step.

            return map.TurnsToFillTheMapStartingFrom(map.LeakPosition!).ToString();
        }

        private class RepairingRobot
        {
            private readonly IntcodeComputer_V5 computer;
            private readonly Queue<long> inputs;
            private readonly Queue<long> outputs;
            public Point CurrentPosition { get; private set; }

            public RepairingRobot(long[] program, Point startingPoint)
            {
                inputs = new Queue<long>();
                outputs = new Queue<long>();
                computer = new IntcodeComputer_V5(program, inputs, outputs);
                CurrentPosition = startingPoint;
            }

            public long ExecuteStep(long direction)
            {
                inputs.Enqueue(direction);
                while (!computer.ExecuteNextInstruction()) ;
                var outputtedValue = outputs.Dequeue();

                if (outputtedValue > 0) CurrentPosition = DirectionToDestination(direction);
                return outputtedValue;
            }

            [Pure]
            public Point DirectionToDestination(long direction)
            {
                return CurrentPosition + direction switch
                {
                    1 => Point.Up,
                    2 => Point.Down,
                    3 => Point.Left,
                    4 => Point.Right,
                    _ => throw new NotImplementedException(),
                };
            }

            [Pure]
            public long DestinationToDirection(Point destination)
            {
                var direction = destination - CurrentPosition;
                if (direction.Equals(Point.Up)) return 1;
                if (direction.Equals(Point.Down)) return 2;
                if (direction.Equals(Point.Left)) return 3;
                if (direction.Equals(Point.Right)) return 4;
                throw new NotImplementedException();
            }

            public void MoveTowards(Point point)
            {
                if (point.Equals(CurrentPosition)) return;

                if (point.ManhattanDistance(CurrentPosition) > 1) 
                    throw new Exception("Cannot move towards non-adjacent point");

                var direction = (point - CurrentPosition).RelativeDirection();
                ExecuteStep(direction);
            }
        }

        private class Map
        {
            private const int mapSideLength = 50;
            private readonly char[][] rawMap;

            private readonly Graph<Point> graph;
            private readonly HashSet<Point> discovered;
            private readonly HashSet<Point> frontier;

            public bool HasUnexploredAreas { get; private set; } = true;
            public Point? LeakPosition { get; private set; } = null;
            public static Point StartingPoint() => new Point(mapSideLength / 2, mapSideLength / 2);

            public Map()
            {
                rawMap = new char[mapSideLength][];
                for (int i = 0; i < mapSideLength; i++) rawMap[i] = Enumerable.Repeat(' ', mapSideLength).ToArray();

                discovered = new HashSet<Point>();
                frontier = new HashSet<Point>();
                graph = new Graph<Point>();
            }

            public void DiscoverPosition(Point position, long value)
            {
                char symbol = value switch
                {
                    0 => '#',
                    1 => '.',
                    2 => '@',
                    _ => throw new NotImplementedException(),
                };

                rawMap[(int)position.Y][(int)position.X] = symbol;
                discovered.Add(position);

                if (value > 0 && position.Surroundings().Any(pt => !discovered.Contains(pt))) frontier.Add(position);
                if (value > 0) AddPointToGraph(position);
                CleanFrontier();

                if (frontier.Count == 0) HasUnexploredAreas = false;
                if (value == 2) LeakPosition = position;
            }

            private void AddPointToGraph(Point position)
            {
                var node = graph.Add(position);
                foreach (var neighbor in position.Surroundings())
                    if (graph.Contains(neighbor)) 
                        graph.GetNode(neighbor).AddBidirectionalConnection(node);
            }

            private void CleanFrontier()
            {
                var pointsToRemoveFromFrontier = frontier.Where(pt => pt.Surroundings().All(pt2 => discovered.Contains(pt2)));
                pointsToRemoveFromFrontier.ForEach(p => frontier.Remove(p));
            }

            public void PrintMap(Point robotPosition)
            {
                var mapCopy = rawMap.Select(line => line.ToArray()).ToArray();
                mapCopy[(int)robotPosition.Y][(int)robotPosition.X] = 'R';
                Console.Clear();
                mapCopy.Select(line => new string(line)).ForEach(Console.WriteLine);
                Console.WriteLine();
                Task.Delay(1).Wait();
            }

            public Point ClosestFrontierTo(Point point) => frontier.MinBy(pt => pt.Distance(point));
            public Path<Point> PathTo(Point initial, Point final) => graph.AStarSearch(initial, final, initial.Distance).Result;
            public IEnumerable<Point> UnexploredNodesSurrounding(Point p) => p.Surroundings().Where(pt => !discovered.Contains(pt));

            public int TurnsToFillTheMapStartingFrom(Point startingPoint)
            {
                var counter = -1; // origin is instantly filled
                var fillableArea = graph.Nodes.Select(n => n.Value).ToHashSet();
                var localFrontier = new List<Point> { startingPoint };
                var filledPoints = new HashSet<Point>();

                while(localFrontier.Count > 0)
                {
                    localFrontier.ForEach(p => filledPoints.Add(p));
                    localFrontier = localFrontier
                        .SelectMany(p => p.Surroundings())
                        .Where(p => fillableArea.Contains(p))
                        .Where(p => !filledPoints.Contains(p))
                        .ToList();
                    counter++;
                }

                return counter;
            }
        }
    }

    public static class PointExtensions{                
        public static long RelativeDirection(this Point direction)
        {
            if (direction.Equals(Point.Up)) return 1;
            if (direction.Equals(Point.Down)) return 2;
            if (direction.Equals(Point.Left)) return 3;
            if (direction.Equals(Point.Right)) return 4;
            throw new NotImplementedException();
        }
    }
}
