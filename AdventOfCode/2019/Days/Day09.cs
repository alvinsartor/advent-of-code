﻿using AdventOfCode._2019.Days.IntcodeComputer;
using System.Linq;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/9

    class Day09 : DailyChallenge
    {
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 50;

        private readonly long[] program;
        public Day09() : base() => program = input[0].Split(",").Select(long.Parse).ToArray();

        /// <summary>
        /// Add some more instructions to the Intcode computer and run with input 1.
        /// </summary>
        public override string Part1()
        {
            var computer = new IntcodeComputer_V5(program);
            computer.Inputs.Enqueue(1);
            computer.Execute();
            return computer.StoredOutputs.Pop().ToString();
        }

        /// <summary>
        /// To verify it really works, run with input 2.
        /// </summary>
        public override string Part2()
        {
            var computer = new IntcodeComputer_V5(program);
            computer.Inputs.Enqueue(2);
            computer.Execute();
            return computer.StoredOutputs.Pop().ToString();
        }
    }
}
