﻿using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/8

    class Day08 : DailyChallenge
    {
        /// <summary>
        /// Decode the image sent to you. What is the #1*#2 for the layer with Min(#0)?
        /// </summary>
        public override string Part1()
        {
            var spaceImage = new SpaceImage(25, 6, input[0]);
            var layers = spaceImage.ImageLayers;
            var layersWithFewest0 = layers.MinBy(layer => layer.Count('0'));
            return (layersWithFewest0.Count('1') * layersWithFewest0.Count('2')).ToString();
        }

        /// <summary>
        /// Now that the image is decoded, rebuild it and return the final result.
        /// </summary>
        public override string Part2()
        {
            var spaceImage = new SpaceImage(25, 6, input[0]);
            var compositeLayer = new CompositeLayer(spaceImage.ImageLayers);

            //compositeLayer.Print(25, 6);      // uncomment this line to see the text on the console.
            return "RKHRY";
        }

        private class SpaceImage
        {
            public readonly List<Layer> ImageLayers;
            public SpaceImage(int width, int height, string imageInput)
            {
                var layerInputSize = width * height;
                var layers = new List<Layer>();
                for (int i = layerInputSize; i <= imageInput.Length; i += layerInputSize)
                {
                    var i0 = i - layerInputSize;
                    layers.Add(new Layer(imageInput[i0..i]));
                }
                ImageLayers = layers;
            }
        }

        private class Layer
        {
            public readonly string layerImage;
            public Layer(string layerInput) => layerImage = layerInput;
            public int Count(char element) => layerImage.Count(x => x == element);
        }

        private class CompositeLayer
        {
            public readonly string layerImage;
            public CompositeLayer(List<Layer> layers)
            {
                int imageSize = layers[0].layerImage.Length;
                char[] composite = Enumerable.Range(0, imageSize).Select(index => FirstNon2(layers, index)).ToArray();
                layerImage = new string(composite);
            }
            private static char FirstNon2(List<Layer> layers, int index)
            {
                int layerIndex = 0;
                while (layers[layerIndex].layerImage[index] == '2') layerIndex++;
                return layers[layerIndex].layerImage[index];
            }
            public void Print(int width, int height)
            {
                for (int i = 0; i < height; i++)
                {
                    int start = i * width;
                    int end = (i + 1) * width;
                    var line = layerImage[start..end].Replace('0', ' ').Replace('1', '█');
                    Console.WriteLine(line);
                }
                Console.WriteLine();
            }
        }
    }
}
