﻿using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using static AdventOfCode.helpers.GeneralHelpers;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/14

    class Day14 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 1000;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 500;       

        /// <summary>
        /// Calculate how much ORE you need to produce 1 unit of FUEL.
        /// </summary>
        public override string Part1()
        {
            Dictionary<string, Reaction> reactions = input
                .Select(Reaction.FromString)
                .ToDictionary(reaction => reaction.Output.Name);            
            var oreQuantityForFuel = SimplifyReaction("FUEL", 1, reactions);
            return oreQuantityForFuel.ToString();
        }

        private static long SimplifyReaction(string componentName, long requiredQty, Dictionary<string, Reaction> reactions)
        {
            BucketOfLeftovers leftovers = new();
            long oreNeeded = 0;

            var reactionToSimplify = reactions[componentName].UpdateToObtain(requiredQty);
            var inputComponents = reactionToSimplify.Input.ToDictionary(comp => comp.Name);

            while (inputComponents.Count > 0)
            {
                var componentToSimpify = inputComponents.First().Value;
                inputComponents.Remove(componentToSimpify.Name);
                if (componentToSimpify.Name == "ORE")
                {
                    oreNeeded += componentToSimpify.Amount;
                    continue;
                }

                var quantityInLeftovers = leftovers.TakeFromLeftovers(componentToSimpify.Name, componentToSimpify.Amount);
                if (componentToSimpify.Amount == quantityInLeftovers)
                {
                    // there was plenty in the leftovers! YAY!
                    continue;
                }

                componentToSimpify = new Component(componentToSimpify.Amount - quantityInLeftovers, componentToSimpify.Name);
                var reactionToObtainComponent = reactions[componentToSimpify.Name].UpdateToObtain(componentToSimpify.Amount);
                if (reactionToObtainComponent.Output.Amount > componentToSimpify.Amount)
                    leftovers.Add(componentToSimpify.Name, reactionToObtainComponent.Output.Amount - componentToSimpify.Amount);

                var neededInputs = reactionToObtainComponent.Input;
                neededInputs.ForEach(component =>
                {
                    if (inputComponents.ContainsKey(component.Name))
                        inputComponents[component.Name] = inputComponents[component.Name].Added(component.Amount);
                    else
                        inputComponents.Add(component.Name, component);
                });
            }

            return oreNeeded;
        }

        /// <summary>
        /// Calculate how much FUEL you can produce with 1 Trillion ORE.
        /// </summary>
        public override string Part2()
        {
            Dictionary<string, Reaction> reactions = input
                .Select(Reaction.FromString)
                .ToDictionary(reaction => reaction.Output.Name);

            long functionToInterpolate(long amount) => 
                SimplifyReaction("FUEL", amount, reactions);
            
            InterpolatorDirection direction(long oreQty) => 
                oreQty > 1000000000000 ? InterpolatorDirection.Down : InterpolatorDirection.Up;

            var interpolationResult = LinearInterpolation(functionToInterpolate, direction, 1, 10000000000);
            if (SimplifyReaction("FUEL", interpolationResult, reactions) > 1000000000000)
                interpolationResult--; // correct interpolation error

            return interpolationResult.ToString();
        }    


        private record Component(long Amount, string Name)
        {
            public static Component FromString(string s)
            {
                var split = s.Split(" ");
                return new Component(long.Parse(split[0]), split[1]);
            }
            public Component Multiplied(long n) => new Component(Amount * n, Name);
            public Component Added(long n) => new Component(Amount + n, Name);
            public override string ToString() => $"{Amount} {Name}";
        }

        private record Reaction(IList<Component> Input, Component Output)
        {
            public static Reaction FromString(string s)
            {
                var split = s.Split(" => ");
                var input = split[0].Split(", ").Select(Component.FromString).ToList();
                var output = Component.FromString(split[1]);
                return new Reaction(input, output);
            }
            public Reaction UpdateToObtain(long requiredQuantity)
            {
                var factor = (long)Math.Ceiling(requiredQuantity / (double)Output.Amount);
                var newInputs = Input.Select(component => component.Multiplied(factor)).ToList();
                var newOutput = Output.Multiplied(factor);
                return new Reaction(newInputs, newOutput);
            }
            public override string ToString() => $"{string.Join(", ", Input)} => {Output}";
        }

        private class BucketOfLeftovers
        {
            private readonly Dictionary<string, long> leftovers = new();
            public void Add(string comp, long qty)
            {
                if (leftovers.ContainsKey(comp)) leftovers[comp] += qty;
                else leftovers.Add(comp, qty);
            }
            public long TakeFromLeftovers(string comp, long qty)
            {
                if (!leftovers.ContainsKey(comp)) return 0;
                long amountTaken = Math.Min(qty, leftovers[comp]);
                leftovers[comp] -= amountTaken;
                return amountTaken;
            }
        }
    }
}
