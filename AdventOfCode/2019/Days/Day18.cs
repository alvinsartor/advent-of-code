﻿using AdventOfCode.helpers;
using Graph;
using Graph.Interfaces;
using Graph.PathFinding;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/18

    class Day18 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 2;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 2;

        /// <summary>
        /// Calculate the shortest path to recollect all the keys in the Triton maze.
        /// </summary>
        public override string Part1() => FindPathCostForGivenInput(input);

        /// <summary>
        /// Divide the maze in 4 non-comunicant sections and deploy 4 robots at the same time.
        /// </summary>
        public override string Part2() => FindPathCostForGivenInput(AdjustInputForPart2(input));

        private string FindPathCostForGivenInput(string[] input)
        {
            var (graph, keys, doors, positions) = DecodeInput(input);
            Environment.Initialize(keys, doors, graph);

            var stateSearchGraph = new DynamicGraph<State>(state => State.ExpandState(state));
            var initialState = State.InitialState(positions);
            bool hasArrived(State s) => keys.Count == s.KeysCount;
            double heuristic(State s) => s.DistanceToMissingKeys();

            var n = stateSearchGraph.Add(initialState);
            var path = stateSearchGraph.AStarSearch(initialState, hasArrived, heuristic).Result;

            return path.Cost.ToString();
        }

        private static (Graph<Point> graph, Dictionary<char, Point> keys, Dictionary<char, Point> doors, Point[] positions)
            DecodeInput(string[] input)
        {
            Graph<Point> graph = new Graph<Point>();
            Dictionary<char, Point> keys = new();
            Dictionary<char, Point> doors = new();
            List<Point> positions = new();

            for (var i = 0; i < input.Length; i++)
                for (var j = 0; j < input[i].Length; j++)
                {
                    var value = input[i][j];
                    if (value == '#') continue;

                    var point = new Point(j, i);
                    graph.Add(point);
                    if (value == '.') continue;
                    else if (value == '@') positions.Add(point);
                    else if (char.IsLower(value)) keys.Add(value, point);
                    else if (char.IsUpper(value)) doors.Add(value, point);
                    else throw new NotImplementedException($"Unexpected char: {value}");
                }

            foreach (Point point in graph.Nodes.Select(n => n.Value))
            {
                if (graph.Contains(point + Point.Down))
                    graph.GetNode(point).AddBidirectionalConnection(graph.GetNode(point + Point.Down));

                if (graph.Contains(point + Point.Right))
                    graph.GetNode(point).AddBidirectionalConnection(graph.GetNode(point + Point.Right));
            }

            return (graph, keys, doors, positions.ToArray());
        }

        private static string[] AdjustInputForPart2(string[] input)
        {
            char[][] inputCopy = input.Select(line => line.ToCharArray()).ToArray();

            inputCopy[40][39] = '#';
            inputCopy[40][40] = '#';
            inputCopy[40][41] = '#';
            inputCopy[41][40] = '#';
            inputCopy[39][40] = '#';

            inputCopy[39][39] = '@';
            inputCopy[39][41] = '@';
            inputCopy[41][39] = '@';
            inputCopy[41][41] = '@';

            return inputCopy.Select(line => new string(line)).ToArray();
        }

        private class Environment
        {
            private static Environment? inst = null;
            public static Environment Instance => inst ?? throw new Exception("Environment has not been initialized yet.");
            public static void Initialize(Dictionary<char, Point> keys, Dictionary<char, Point> doors, Graph<Point> graph) =>
                inst = new Environment(keys, doors, graph);

            public readonly IReadOnlyDictionary<char, Point> allKeys;
            public readonly IReadOnlyDictionary<char, Point> allDoors;
            public readonly IGraph<Point> graph;
            private readonly Dictionary<(Point fro, Point to), Path<Point>> CachedPaths = new();

            private Environment(Dictionary<char, Point> keys, Dictionary<char, Point> doors, Graph<Point> graph)
            {
                allKeys = keys;
                allDoors = doors;
                this.graph = graph;
            }

            public Path<Point> PathTo(Point from, Point destination)
            {
                if (!CachedPaths.ContainsKey((from, destination)))
                    CachedPaths.Add((from, destination), graph.AStarSearch(from, destination, (p) => p.ManhattanDistance(destination)).Result);
                return CachedPaths[(from, destination)];
            }
        }

        private class State
        {
            private readonly List<char> sortedKeys;
            private readonly Dictionary<char, Point> keysToRecollect;
            private readonly HashSet<Point> unrecollectedKeysPositions;
            private readonly HashSet<Point> closedDoorsPositions;
            private readonly HashSet<char> keys;
            private readonly Point[] positions;
            private readonly string strPositions;

            private string RecollectedSortedKeys { get; init; }
            private string RecollectedKeys { get; init; }
            public int KeysCount => keys.Count;
            
            public static State InitialState(Point[] positions) => new State(Array.Empty<char>(), positions);

            private State(IEnumerable<char> keys, Point[] positions)
            {
                sortedKeys = keys.ToList();
                this.keys = keys.ToHashSet();
                this.positions = positions;
                strPositions = string.Join("|", this.positions.Select(p => p.ToString()));

                RecollectedSortedKeys = string.Join("", sortedKeys);
                RecollectedKeys = string.Join("", keys.OrderBy(k => k));

                var allKeys = Environment.Instance.allKeys;
                var allDoors = Environment.Instance.allDoors;
                keysToRecollect = allKeys.Where(pair => !this.keys.Contains(pair.Key)).ToDictionary(p => p.Key, p => p.Value);
                unrecollectedKeysPositions = keysToRecollect.Values.ToHashSet();
                closedDoorsPositions = keysToRecollect.Keys.Where(k => allDoors.ContainsKey(char.ToUpper(k))).Select(k => allDoors[char.ToUpper(k)]).ToHashSet();
            }

            protected static IEnumerable<(char key, Path<Point> path)> GetValidPathsToRemainingKeys(State state, Point position) =>
                state.keysToRecollect
                    .Select(pair => (key: pair.Key, path: Environment.Instance.PathTo(position, pair.Value)))
                    .Where(pathAndKey => !pathAndKey.path.IsEmpty && PathDoesNotCrossClosedDoorsOrKeys(state, pathAndKey.path));

            private static bool PathDoesNotCrossClosedDoorsOrKeys(State state, Path<Point> path)
            {
                // last node is not checked as it is our destination
                foreach (var point in path.Nodes.Reverse().Skip(1).Select(n => n.Value))
                    if (state.unrecollectedKeysPositions.Contains(point) || state.closedDoorsPositions.Contains(point))
                        return false;
                return true;
            }

            public static Task<ImmutableList<(State, double)>> ExpandState(State state)
            {
                var accessibleStates = state.positions
                    .SelectMany((pos, i) => StatesAccessibleFromGivenPosition(state, i))
                    .ToImmutableList();
                return Task.FromResult(accessibleStates);
            }

            private static IEnumerable<(State, double)> StatesAccessibleFromGivenPosition(State state, int positionIndex)
            {
                var position = state.positions[positionIndex];
                var validPathsToRemainingKeys = GetValidPathsToRemainingKeys(state, position);
                return validPathsToRemainingKeys.Select(pathAndKey =>
                {
                    var newPositions = state.positions.ToArray();
                    newPositions[positionIndex] = Environment.Instance.allKeys[pathAndKey.key];
                    var newKeys = state.keys.Append(pathAndKey.key).ToHashSet();
                    var pathCost = pathAndKey.path.Cost;
                    return (new State(newKeys, newPositions), pathCost);
                });
            }

            public double DistanceToMissingKeys()
            {
                var distancesToKeys = keysToRecollect
                    .Select(key => key.Value)
                    .SelectMany(key => positions.Select(position => Environment.Instance.PathTo(position, key)))
                    .Select(path => path.Cost).ToList();

                return distancesToKeys.Count > 0 ? distancesToKeys.Max() : 0;
            }

            public override string ToString() => RecollectedSortedKeys + " " + strPositions;
            public override int GetHashCode() => HashCode.Combine(RecollectedKeys, strPositions);
            public override bool Equals(object? obj) 
                => obj is State state && RecollectedKeys == state.RecollectedKeys && strPositions == strPositions;
        }        
    }
}
