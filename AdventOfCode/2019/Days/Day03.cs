﻿using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/3

    partial class Day03 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 10;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 10;

        /// <summary>
        /// The input represent a path of 2 cables. Calculate the Manhattan distance between O and the closest intersection.
        /// </summary>
        public override string Part1()
        {
            var (_, _, intersections) = GetIntersections(input);
            return intersections.Min(p => p.ManhattanDistanceFromOrigin).ToString();
        }

        /// <summary>
        /// Now calculate the minimum amount of steps for a cable to reach the closest intersection.
        /// </summary>
        public override string Part2()
        {
            var (cable1, cable2, intersections) = GetIntersections(input);
            return intersections.Min(p => StepsToGetThere(p, cable1) + StepsToGetThere(p, cable2)).ToString();
        }

        private static (List<Point> cable1, List<Point> cable2, HashSet<Point> intersections) GetIntersections(string[] input)
        {
            var cable1 = CablePathFinder(input[0].Split(","));
            var cable2 = CablePathFinder(input[1].Split(","));

            var intersections = cable1.ToHashSet();
            intersections.IntersectWith(cable2.ToHashSet());
            intersections.ExceptWith(new[] { Point.Origin });

            return (cable1, cable2, intersections);
        }

        private static List<Point> CablePathFinder(string[] cableSteps)
        {
            var cablePath = new List<Point>() { Point.Origin };
            var currentPosition = Point.Origin;
            foreach (var step in cableSteps)
            {
                AddPointsToPath(currentPosition, cablePath, step);
                currentPosition = cablePath.Last();
            }

            return cablePath;
        }

        private static void AddPointsToPath(Point startingPoint, List<Point> currentPath, string direction)
        {
            Point trajectory = GetDirectionFromStep(direction[0]);
            int distance = int.Parse(direction[1..]);
            for (int i = 1; i <= distance; i++) currentPath.Add(trajectory * i + startingPoint);
        }

        private static double StepsToGetThere(Point p, List<Point> l) => l.IndexOf(p);        

        private static Point GetDirectionFromStep(char directionCode) =>
            directionCode switch
            {
                'U' => new Point(0, 1),
                'R' => new Point(1, 0),
                'D' => new Point(0, -1),
                'L' => new Point(-1, 0),
                _ => throw new Exception("Unrecognized direction " + directionCode),
            };       
    }
}
