﻿using AdventOfCode._2019.Days.IntcodeComputer;
using System.Linq;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/5

    class Day05 : DailyChallenge
    {
        private readonly int[] program;
        public Day05() : base() => program = input[0].Split(",").Select(int.Parse).ToArray();

        /// <summary>
        /// Extend the Intercode Computer of Day2 to execute more instructions, run the input and print the latest program output.
        /// </summary>
        public override string Part1()
        {
            var programCopy = program.ToArray();
            var computer = new IntcodeComputer_V2();
            computer.Inputs.Push(1);

            computer.Execute(programCopy);
            return computer.Outputs.Pop().ToString();
        }

        /// <summary>
        /// Extend even more the computer to support jumps. Run the input and print the latest program output.
        /// </summary>
        public override string Part2()
        {
            var programCopy = program.ToArray();
            var computer = new IntcodeComputer_V2();
            computer.Inputs.Push(5);

            computer.Execute(programCopy);
            return computer.Outputs.Pop().ToString();
        }
    }
}
