﻿using AdventOfCode._2019.Days.IntcodeComputer;
using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/17

    class Day17 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 200;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 100;

        private readonly long[] program;
        public Day17() : base() => program = input[0].Split(",").Select(long.Parse).ToArray();

        /// <summary>
        /// Get the image from the camera and find all the scaffolding intersections.
        /// </summary>
        public override string Part1()
        {
            //new Camera(program).PrintImage(); // uncomment to print the image

            var camera = new Camera(program);
            var intersections = camera.GetScaffoldIntersectionsFromImage();
            return intersections.Sum().ToString();
        }

        /// <summary>
        /// Send a sequence to the robot representing the path it has to follow to walk over all the scaffoldings.
        /// </summary>
        public override string Part2()
        {
            var camera = new Camera(program);
            var robot = new Robot(program, camera.GetImage());
            var dustRemoved = robot.CalculateAndExecuteFullPath(false); // set to true to see the image
            return dustRemoved.ToString();
        }

        private class Camera
        {
            private readonly IntcodeComputer_V5 computer;
            private readonly Queue<long> inputs;
            private readonly Queue<long> outputs;

            public Camera(long[] program)
            {
                inputs = new Queue<long>();
                outputs = new Queue<long>();
                computer = new IntcodeComputer_V5(program, inputs, outputs);
            }

            public void PrintImage() => GetImage().ForEach(Console.WriteLine);

            public string[] GetImage()
            {
                while (!computer.ExecuteNextInstruction()) ;
                List<long> buffer = new();
                while (outputs.Count > 0) buffer.Add(outputs.Dequeue());
                var stringedOutput = new string(buffer.Select(n => (char)n).ToArray());

                return stringedOutput.Split("\n").Where(s => !string.IsNullOrEmpty(s)).ToArray();
            }

            public IEnumerable<int> GetScaffoldIntersectionsFromImage()
            {
                string[] image = GetImage();
                char[][] decomposedImage = image.Select(line => line.ToCharArray()).ToArray();
                bool isIntersection(int rw, int cl) =>
                    decomposedImage[rw][cl] == '#' &&
                    decomposedImage[rw - 1][cl] == '#' &&
                    decomposedImage[rw + 1][cl] == '#' &&
                    decomposedImage[rw][cl - 1] == '#' &&
                    decomposedImage[rw][cl + 1] == '#';

                for (int r = 1; r < decomposedImage.Length - 1; r++)
                    for (int c = 1; c < decomposedImage[r].Length - 1; c++)
                        if (isIntersection(r, c))
                            yield return r * c;
            }
        }

        private class Robot
        {
            enum Rotation { north, east, south, west }
            private readonly IntcodeComputer_V5 computer;
            private readonly Queue<long> inputs;
            private readonly Queue<long> outputs;
            private readonly string[] map;

            public Robot(long[] program, string[] map)
            {
                program = program.ToArray();
                program[0] = 2;
                inputs = new Queue<long>();
                outputs = new Queue<long>();
                computer = new IntcodeComputer_V5(program, inputs, outputs);
                this.map = map;
            }

            public long CalculateAndExecuteFullPath(bool print = true)
            {
                var instructions = CalculateFullPath();
                var (subA, subB, subC, main) = CompressInstructions(instructions);

                RunUntilHalts(print); main.ForEach(c => inputs.Enqueue(c));
                RunUntilHalts(print); subA.ForEach(c => inputs.Enqueue(c));
                RunUntilHalts(print); subB.ForEach(c => inputs.Enqueue(c));
                RunUntilHalts(print); subC.ForEach(c => inputs.Enqueue(c));
                RunUntilHalts(print); "n\n".ForEach(c => inputs.Enqueue(c));

                while (!computer.ExecuteNextInstruction()) ;
                long latestValue = outputs.Last();
                return latestValue;
            }

            private string[] RunUntilHalts(bool print)
            {
                while (!computer.ExecuteNextInstruction()) ;
                List<long> buffer = new();
                while (outputs.Count > 0) buffer.Add(outputs.Dequeue());
                var outputContent = new string(buffer.Select(n => (char)n).ToArray()).Split("/n");
                if (print) outputContent.ForEach(Console.WriteLine);
                return outputContent;
            }

            private static (string subA, string subB, string subC, string main) CompressInstructions(string[] instructions)
            {
                // the result is processed manually to obtain the below lines
                string subA = InstructionsToString(new string[] { "R", "8", "L", "12", "R", "8" }) + "\n";
                string subB = InstructionsToString(new string[] { "L", "10", "L", "10", "R", "8" }) + "\n";
                string subC = InstructionsToString(new string[] { "L", "12", "L", "12", "L", "10", "R", "10" }) + "\n";
                string main = InstructionsToString(new string[] { "A", "A", "B", "C", "B", "C", "B", "A", "C", "A" }) + "\n";

                return (subA, subB, subC, main);
            }

            private static string InstructionsToString(string[] instructions) => string.Join(",", instructions);

            private string[] CalculateFullPath()
            {
                var path = new List<string>();
                var (position, direction) = GetRobotPositionAndRotation();

                while (!HasFinished(position, direction))
                {
                    (string turningDirection, Point newDirection) = GetTurningDirection(position, direction);
                    path.Add(turningDirection);
                    direction = newDirection;
                    (string stepsForward, Point newPosition) = GetStepsForward(position, direction);
                    path.Add(stepsForward);
                    position = newPosition;
                }

                return path.ToArray();
            }

            private bool IsPointOnMap(Point pos) => pos.X >= 0 && pos.Y >= 0 && pos.X < map[0].Length && pos.Y < map.Length;
            private bool IsPointOnPath(Point pos) => IsPointOnMap(pos) && map[(int)pos.Y][(int)pos.X] == '#';
            private bool IsThereAPathOnTheRight(Point pos, Point dir) => IsPointOnPath(pos + dir.RotatedClockwise());
            private bool IsThereAPathOnTheLeft(Point pos, Point dir) => IsPointOnPath(pos + dir.RotatedCounterclockwise());
            private bool HasFinished(Point pos, Point dir) => !IsThereAPathOnTheLeft(pos, dir) && !IsThereAPathOnTheRight(pos, dir);

            private (string turningDirection, Point newDirection) GetTurningDirection(Point pos, Point dir) =>
                IsThereAPathOnTheRight(pos, dir) ? ("R", dir.RotatedClockwise()) : ("L", dir.RotatedCounterclockwise());

            private (string stepsForward, Point newPosition) GetStepsForward(Point pos, Point dir)
            {
                int counter = 0;
                while (IsPointOnPath(pos + dir))
                {
                    pos += dir;
                    counter++;
                }
                return (counter.ToString(), pos);
            }

            private (Point position, Point direction) GetRobotPositionAndRotation()
            {
                bool isRobot(char ch) => ch != '.' && ch != '#';
                Point getDirection(char ch) => ch switch
                {
                    '^' => Point.Up,
                    '>' => Point.Right,
                    'v' => Point.Down,
                    '<' => Point.Left,
                    _ => throw new NotImplementedException(),
                };

                for (int r = 0; r < map.Length; r++)
                    for (int c = 0; c < map[r].Length; c++)
                        if (isRobot(map[r][c]))
                            return (new Point(c, r), getDirection(map[r][c]));

                return (Point.Origin, Point.Up);
            }
        }
    }
}
