﻿using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/24

    class Day24 : DailyChallenge
    {
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 10;
        private readonly Dictionary<Point3D, List<Point3D>> cachedP2Results = new();

        /// <summary>
        /// Watch bugs live and die until the pattern repeats itself.
        /// </summary>
        public override string Part1()
        {
            HashSet<Point> bugs = DecodeInput(input).ToHashSet();
            HashSet<string> generations = new();
            string currentGenerationString = BugsToString(bugs);

            do
            {
                generations.Add(currentGenerationString);
                bugs = GetNextGeneration(bugs).ToHashSet();
                currentGenerationString = BugsToString(bugs);
            } while (!generations.Contains(currentGenerationString));

            return bugs.Select(pt => Math.Pow(2, pt.X + pt.Y * 5)).Sum().ToString();
        }

        private static IEnumerable<Point> GetNextGeneration(HashSet<Point> bugs)
        {
            for (int x = 0; x < 5; x++)
                for (int y = 0; y < 5; y++)
                {
                    var pt = new Point(x, y);
                    var adjacentBugs = pt.Surroundings().Count(neighbor => bugs.Contains(neighbor));

                    if (adjacentBugs == 1 || (adjacentBugs == 2 && !bugs.Contains(pt)))
                        yield return pt;
                }            
        }

        private static IEnumerable<Point> DecodeInput(string[] input) =>
            input.SelectMany((line, y) => line.Select((ch, x) => (new Point(x, y), ch)))
                .Where(tuple => tuple.ch == '#')
                .Select(tuple => tuple.Item1);

        private static string BugsToString(HashSet<Point> bugs) =>
            string.Join("|", bugs.OrderBy(pt => pt.ManhattanDistanceFromOrigin).Select(pt => pt.ToString()));

        /// <summary>
        /// Count the number of bugs after 200 iterations, considering, thought, 
        /// that each 5x5 is the central tile of a recursive, vertically stacked set of grids.
        /// </summary>
        public override string Part2()
        {
            HashSet<Point3D> bugs = DecodeInput(input).Select(pt => new Point3D(pt.X, pt.Y, 0)).ToHashSet();
            cachedP2Results.Clear();

            for (int i = 0; i < 200; i++) bugs = GetNext3DGeneration(bugs).ToHashSet();
            return bugs.Count.ToString();
        }

        private IEnumerable<Point3D> GetNext3DGeneration(HashSet<Point3D> bugs)
        {
            var bugsAndNeigbors = bugs.SelectMany(pt => RecursiveNeighbors(pt)).Concat(bugs).ToHashSet();
            foreach(Point3D pt in bugsAndNeigbors)
            {
                var adjacentBugs = RecursiveNeighbors(pt).Count(neighbor => bugs.Contains(neighbor));
                if (adjacentBugs == 1 || (adjacentBugs == 2 && !bugs.Contains(pt)))
                    yield return pt;
            }
        }

        private IEnumerable<Point3D> RecursiveNeighbors(Point3D point)
        {
            if (!cachedP2Results.ContainsKey(point))
            {
                var neighbors = PointUp(point).Concat(PointRight(point)).Concat(PointDown(point)).Concat(PointLeft(point));
                cachedP2Results.Add(point, neighbors.ToList());
            }
            return cachedP2Results[point];
        }

        private static IEnumerable<Point3D> PointUp(Point3D point) =>
            point.Y == 0 ? new[] { new Point3D(2, 1, point.Z + 1) }
                : point.Y == 3 && point.X == 2 ? GetRow(4, point.Z - 1)
                    : new[] { point + Point.Up };

        private static IEnumerable<Point3D> PointDown(Point3D point) =>
            point.Y == 4 ? new[] { new Point3D(2, 3, point.Z + 1) }
                : point.Y == 1 && point.X == 2 ? GetRow(0, point.Z - 1)
                    : new[] { point + Point.Down };

        private static IEnumerable<Point3D> PointRight(Point3D point) =>
            point.X == 4 ? new[] { new Point3D(3, 2, point.Z + 1) }
                : point.X == 1  && point.Y == 2? GetColumn(0, point.Z - 1)
                    : new[] { point + Point.Right };

        private static IEnumerable<Point3D> PointLeft(Point3D point) =>
            point.X == 0 ? new[] { new Point3D(1, 2, point.Z + 1) }
                : point.X == 3 && point.Y == 2 ? GetColumn(4, point.Z - 1)
                    : new[] { point + Point.Left };

        private static IEnumerable<Point3D> GetRow(double y, double z) => 
            Enumerable.Range(0, 5).Select(x => new Point3D(x, y, z));

        private static IEnumerable<Point3D> GetColumn(double x, double z) => 
            Enumerable.Range(0, 5).Select(y => new Point3D(x, y, z));
    }
}
