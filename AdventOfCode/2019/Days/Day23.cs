﻿using AdventOfCode._2019.Days.IntcodeComputer;
using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/23

    class Day23 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 100;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 50;

        private readonly long[] program;
        public Day23() : base() => program = input[0].Split(",").Select(long.Parse).ToArray();

        /// <summary>
        /// Connect multiple computers using a network. What is the first package.Y value the computers send to the address 255?
        /// </summary>
        public override string Part1()
        {
            Network.Instance.CleanNetwork();
            var computers = Enumerable.Range(0, 50).Select(id => new ComputerNode(id, program)).ToArray();
            var snooper = new Snooper(255);

            while (!snooper.HasSnoopedPackage())
                computers.ForEach(compu => compu.ExecuteTillHalts());

            return snooper.SnoopedPackage!.Y.ToString();
        }

        /// <summary>
        /// Place a NAT on 255. What is the first repited value the NAT sends to the computer at address 0?
        /// </summary>
        public override string Part2()
        {
            Network.Instance.CleanNetwork();
            var computers = Enumerable.Range(0, 50).Select(id => new ComputerNode(id, program)).ToArray();
            var nat = new NAT(255, computers);

            int currentComputer = 0;
            int currentRound = 0;
            while (!nat.HasResult())
            {
                computers[currentComputer].ExecuteTillHalts(currentRound);
                currentComputer++;
                if (currentComputer >= computers.Length)
                {
                    currentComputer = 0;
                    currentRound++;
                    nat.ResumeActivityIfNetworkIsIdle(currentRound);
                }
            }

            return nat.DuplicateY!.Value.ToString();
        }

        private interface INetworkNode
        {
            public long Id { get; }
            public void EnqueuePackage(Package package);
        }

        private class Snooper : INetworkNode
        {
            public long Id { get; init; }
            public Package? SnoopedPackage { get; private set; } = null;
            public Snooper(long id)
            {
                Id = id;
                Network.Instance.RegisterNodeToNetwork(this);
            }
            public void EnqueuePackage(Package package) => SnoopedPackage = package;
            public bool HasSnoopedPackage() => SnoopedPackage != null;
        }

        private class NAT : INetworkNode
        {
            public long Id { get; init; }
            private readonly HashSet<long> sentYValues = new();
            public long? DuplicateY { get; private set; } = null;
            private Package? lastReceivedPackage = null;
            private readonly ComputerNode[] computers;

            public NAT(long id, ComputerNode[] computers)
            {
                Id = id;
                this.computers = computers;
                Network.Instance.RegisterNodeToNetwork(this);
            }
            public void EnqueuePackage(Package package) => lastReceivedPackage = package;
            public void ResumeActivityIfNetworkIsIdle(int currentRound)
            {
                if (lastReceivedPackage == null || !AreComputersIdle(currentRound)) return;
                computers[0].EnqueuePackage(lastReceivedPackage.ChangeDestination(0));

                if (sentYValues.Contains(lastReceivedPackage.Y)) DuplicateY = lastReceivedPackage.Y;
                sentYValues.Add(lastReceivedPackage.Y);
            }
            public bool HasResult() => DuplicateY != null;
            private static bool AreComputersIdle(int currentRound) => 
                Network.Instance.LatestRoundPackageWasReceived < currentRound - 2;
        }

        private class ComputerNode : INetworkNode
        {
            private readonly IntcodeComputer_V5 computer;
            public long Id { get; init; }

            public ComputerNode(long id, long[] program)
            {
                computer = new IntcodeComputer_V5(program);
                Id = id;
                computer.Inputs.Enqueue(Id);
                Network.Instance.RegisterNodeToNetwork(this);
            }

            public void ExecuteTillHalts(int currentExecutionRound = 0)
            {
                if (computer.Inputs.Count == 0) computer.Inputs.Enqueue(-1);
                while (!computer.ExecuteNextInstruction()) ;
                if (computer.Outputs.Count == 0) return;

                long packageDestination = computer.Outputs.Dequeue();
                long x = computer.Outputs.Dequeue();
                long y = computer.Outputs.Dequeue();
                var package = new Package(packageDestination, x, y, currentExecutionRound);
                Network.Instance.AddPackageToNetwork(package);
            }

            public void EnqueuePackage(Package package)
            {
                if (package.NodeId != Id) throw new Exception("It seems that this package was not for this computer!");
                computer.Inputs.Enqueue(package.X);
                computer.Inputs.Enqueue(package.Y);
            }
        }

        private class Network
        {
            private static readonly Network inst = new Network();
            private Network() { }
            public static Network Instance => inst;

            private readonly Dictionary<long, INetworkNode> RegisteredNodes = new();
            public int LatestRoundPackageWasReceived { get; private set; } = int.MaxValue;

            public void CleanNetwork()
            {
                LatestRoundPackageWasReceived = int.MaxValue;
                RegisteredNodes.Clear();
            }
            public void RegisterNodeToNetwork(INetworkNode node) => RegisteredNodes.Add(node.Id, node);
            public void AddPackageToNetwork(Package package)
            {
                LatestRoundPackageWasReceived = package.ExecutionRound;
                if (!RegisteredNodes.ContainsKey(package.NodeId))
                    throw new Exception("Specified node is not registered to the network");
                RegisteredNodes[package.NodeId].EnqueuePackage(package);
            }
        }

        private record Package(long NodeId, long X, long Y, int ExecutionRound) {
            
            [Pure]
            public Package ChangeDestination(long newId) => new Package(newId, X, Y, ExecutionRound);        
        }
    }
}
