﻿using AdventOfCode._2019.Days.IntcodeComputer;
using AdventOfCode.helpers;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/21

    class Day21 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 100;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 20;

        private readonly long[] program;
        public Day21() : base() => program = input[0].Split(",").Select(long.Parse).ToArray();

        /// <summary>
        /// Program the SpringDroid to jump over all the holes and scan the hull for the damages amount.
        /// </summary>
        public override string Part1()
        {
            var droid = new SpringDroid(program, false);
                
            // D && (!A || !B || !C)
            var instructions = new[] {
                "NOT A J\n",
                "NOT B T\n",
                "OR T J\n",
                "NOT C T\n",
                "OR T J\n",
                "AND D J\n",
                "WALK\n",
            };
            return droid.ExecuteInstructions(instructions);
        }

        /// <summary>
        /// Same as before, but now using the RUN command, so to increase the view.
        /// </summary>
        public override string Part2()
        {
            var droid = new SpringDroid(program, false);
            // (D && (!A || !B || !C) && H) || !A
            var instructions = new[]
            {
                "NOT A J\n",
                "NOT B T\n",
                "OR T J\n",
                "NOT C T\n",
                "OR T J\n",
                "AND D J\n",
                "AND H J\n", // but only if H is also ground
                "NOT A T\n", // if next tile is a hole we have to jump
                "OR T J\n",
                "RUN\n",
            };
            return droid.ExecuteInstructions(instructions);
        }

        private class SpringDroid : ASCIIComputer
        {
            public SpringDroid(long[] program, bool displayConsoleOutputs) : base(program, displayConsoleOutputs) { }
            public string ExecuteInstructions(IEnumerable<string> instructions)
            {
                ExecuteTillHalts();
                instructions.ForEach(instruction => instruction.ForEach(c => computer.Inputs.Enqueue(c)));
                ExecuteTillHalts();

                var result = NonCharResults.Count > 0 ? NonCharResults.Dequeue().ToString() : "No result 🤖";
                return result;
            }
        }
    }
}
