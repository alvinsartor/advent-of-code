﻿using Graph;
using Graph.Interfaces;
using Graph.PathFinding;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/6

    class Day06 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 500;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 50;

        /// <summary>
        /// Given an input containing the orbits of some celestial bodies, count the total number of orbits (direct and indirect).
        /// </summary>
        public override string Part1()
        {
            var graph = InstantiatePlanetsGraph(input);
            FillPlanetsGraph(graph, input);

            AlreadyDiscoveredNeighbors.Clear();
            var orbits = graph.Nodes.Select(planet => StepsToRoot(planet)).Sum();

            return orbits.ToString();
        }

        /// <summary>
        /// Find the shortest path to get to the planet orbited by SAN
        /// </summary>
        public override string Part2()
        {
            var graph = InstantiatePlanetsGraph(input);
            FillPlanetsGraph(graph, input, true);

            var planetOrbitedByYou = graph.GetNode("YOU").CurrentNeighbors[0];
            var planetOrbitedBySanta = graph.GetNode("SAN").CurrentNeighbors[0];

            var path = graph.FirstDepthSearch(planetOrbitedByYou, planetOrbitedBySanta).Result;
            return (path.Nodes.Count - 1).ToString();
        }

        private static Graph<string> InstantiatePlanetsGraph(string[] input)
        {
            var graph = new Graph<string>();
            var allPlanets = input.SelectMany(x => x.Split(")")).ToHashSet();
            foreach (var planet in allPlanets) graph.Add(planet);
            return graph;
        }

        private static void FillPlanetsGraph(Graph<string> graph, string[] input, bool bidirectional = false)
        {
            foreach (var orbit in input)
            {
                var split = orbit.Split(")");
                var planet1 = graph.GetNode(split[0]);
                var planet2 = graph.GetNode(split[1]);
                if (bidirectional) planet2.AddBidirectionalConnection(planet1);
                else planet2.AddConnection(planet1);
            }
        }

        private readonly Dictionary<Node<string>, int> AlreadyDiscoveredNeighbors = new Dictionary<Node<string>, int>();
        private int StepsToRoot(Node<string> node)
        {
            if (AlreadyDiscoveredNeighbors.ContainsKey(node)) return AlreadyDiscoveredNeighbors[node];
            var neighbors = node.CurrentNeighbors;
            var result = neighbors.Count == 0 ? 0 : 1 + StepsToRoot(neighbors[0]);
            AlreadyDiscoveredNeighbors.Add(node, result);
            return result;
        }


    }
}
