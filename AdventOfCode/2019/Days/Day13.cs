﻿using AdventOfCode._2019.Days.IntcodeComputer;
using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/13

    class Day13 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 500;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 50;        

        private readonly long[] program;
        public Day13() : base() => program = input[0].Split(",").Select(long.Parse).ToArray();

        /// <summary>
        /// Count the number of tiles the game draws at first.
        /// </summary>
        public override string Part1()
        {
            var output = new Queue<long>();
            var computer = new IntcodeComputer_V5(program, outputChannel: output);
            computer.Execute();
            var counter = 0;

            while (output.Count > 0)
            {
                output.Dequeue(); //x
                output.Dequeue(); //y
                if (output.Dequeue() == 2) counter++; //2==tile
            }

            return counter.ToString();
        }

        /// <summary>
        /// Play the game and return the score when the ball hits the latest block.
        /// </summary>
        public override string Part2()
        {
            var hackedProgramToPlayForFree = program.ToArray();
            hackedProgramToPlayForFree[0] = 2;

            long score = 0;
            long latestScore = 0;
            char[][]? screen = null;
            var output = new Queue<long>();
            var input = new Queue<long>();
            var computer = new IntcodeComputer_V5(hackedProgramToPlayForFree, input, output);
            
            while (!computer.HasEnded)
            {
                while (!computer.ExecuteNextInstruction()) ;
                var (newScreen, currentScore, ballX, paddleX) = ProgramOutputsToScreenStrings(output, screen);

                latestScore = currentScore;
                score += currentScore;
                //PrintScreen(newScreen, score); // uncomment this line to see the minigame.

                var joysticTilt = Math.Sign(ballX - paddleX);
                input.Enqueue(joysticTilt);
                screen = newScreen;
            }

            return latestScore.ToString();
        }

        private static void PrintScreen(char[][] screen, long score)
        {
            Console.Clear();
            screen.Select(line => new string(line)).ForEach(Console.WriteLine);
            Console.WriteLine($"Score: {score}");
            Console.WriteLine();
            Task.Delay(1).Wait();
        }

        private static (char[][] screen, long score, int ballX, int paddleX)  
            ProgramOutputsToScreenStrings(Queue<long> output, char[][]? screen = null)
        {
            var field = FetchResults(output).ToList();
            screen ??= InitializeScreen(field.Select(x => x.position));
            
            long currentScore = 0;
            int ballPosition = 0;
            int paddlePosition = 0;
            foreach(var (position, value) in field)
            {
                if (position.X == -1) currentScore = value;
                else
                {
                    screen[(int)position.Y][(int)position.X] = ExtractSymbolFromOutputValue(value);
                    if (value == 4) ballPosition = (int)position.X;
                    else if (value == 3) paddlePosition = (int)position.X;
                }
            }

            return (screen, currentScore, ballPosition, paddlePosition);
        }

        private static char[][] InitializeScreen(IEnumerable<Point> screenPoints)
        {
            var screenSize = new Point(screenPoints.Max(point => point.X) + 1, screenPoints.Max(point => point.Y) + 1);
            char[][] screen = new char[(int)screenSize.Y][];
            for (var i = 0; i < screenSize.Y; i++) screen[i] = new char[(int)screenSize.X];
            return screen;
        }

        private static IEnumerable<(Point position, long value)> FetchResults(Queue<long> output)
{
            while (output.Count > 0)
            {
                var x = output.Dequeue();
                var y = output.Dequeue();
                var value = output.Dequeue();
                yield return (new Point(x, y), value);
            }
        }

        private static char ExtractSymbolFromOutputValue(long value) => value switch
        {
            0 => ' ',
            1 => '#',
            2 => '═',
            3 => '_',
            4 => 'o',
            _ => throw new NotImplementedException(),
        };            
    }
}
