﻿using AdventOfCode._2019.Days.IntcodeComputer;
using System.Linq;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/2

    class Day02 : DailyChallenge
    {
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 100;

        private readonly int[] program;
        public Day02() : base() => program = input[0].Split(",").Select(int.Parse).ToArray();
        
        /// <summary>
        /// Execute the program in input and print the content of the cell 0.
        /// </summary>
        public override string Part1()
        {
            var programCopy = program.ToArray();
            programCopy[1] = 12;
            programCopy[2] = 2;

            IntcodeComputer_V1.Execute(programCopy);
            return programCopy[0].ToString();
        }

        /// <summary>
        /// Determine what pair of inputs produces the output 19690720.
        /// </summary>
        public override string Part2()
        {
            for(int i = 0; i<=99; i++)
                for(int j = 0; j<=99; j++)
                {
                    var programCopy = program.ToArray();
                    programCopy[1] = i;
                    programCopy[2] = j;

                    IntcodeComputer_V1.Execute(programCopy);
                    if (programCopy[0] == 19690720) return (100 * i + j).ToString();
                }
            return "None";
        }       
    }
}
