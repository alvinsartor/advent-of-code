﻿using AdventOfCode._2019.Days.IntcodeComputer;
using AdventOfCode.helpers;
using System.Collections.Generic;
using System.Linq;
using static AdventOfCode.helpers.GeneralHelpers;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/19

    class Day19 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 5;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 5;

        private readonly Drone drone;
        public Day19() : base()
        {
            var program = input[0].Split(",").Select(long.Parse).ToArray();
            drone = new Drone(program);
        }

        /// <summary>
        /// Investigate the shape of the tractor beam by counting how many cells it affects.
        /// </summary>
        public override string Part1()
        {
            var pointsWhereBeamIsActive = GetQuadrant(50).Where(pt => drone.IsBeamWorkingOnGivenPosition(pt)).Count();
            return pointsWhereBeamIsActive.ToString();
        }

        private static IEnumerable<Point> GetQuadrant(int size)
        {
            for (double x = 0; x < size; x++)
                for (double y = 0; y < size; y++)
                    yield return new Point(x, y);
        }

        /// <summary>
        /// When does the beam get large enough to include a 100x100 box? Get the nearest point of that box.
        /// </summary>
        public override string Part2()
        {
            var distanceToCheck = GetDistanceWhereBeamIsWideEnough();
            Point[] angles;

            do { angles = GetAnglesAtDistance(distanceToCheck++); }
            while (!drone.IsBeamWorkingOnGivenPosition(angles[2])); // we can just check the bottom left 

            var topLeftAngle = angles[0];
            return (topLeftAngle.X * 10000 + topLeftAngle.Y).ToString();
        }

        private int GetDistanceWhereBeamIsWideEnough()
        {
            double beamRatio = 1.6;
            long functionToInterpolate(long y) => CalculateBeamWidthAtCertainDistance((int)y);
            InterpolatorDirection fitnessFunction(long width) =>
                width < 100 * beamRatio
                ? InterpolatorDirection.Up : width > 100 * beamRatio
                ? InterpolatorDirection.Down : InterpolatorDirection.GoodEnough;

            return (int)LinearInterpolation(functionToInterpolate, fitnessFunction, 250, 1500);
        }

        private static IEnumerable<Point> PointsAtCertainDistanceLeftToRight(int y) =>
            Enumerable.Range(0, y).Select(x => new Point(x, y));

        private static IEnumerable<Point> PointsAtCertainDistanceRightToLeft(int y) =>
            Enumerable.Range(0, y)
                .Select(n => y + 1 - n) // inverted [Y -> 0]
                .Select(x => new Point(x, y));

        private Point FirstPointInsideBeam(IEnumerable<Point> points) =>
            points.First(pt => drone.IsBeamWorkingOnGivenPosition(pt));

        private int CalculateBeamWidthAtCertainDistance(int y)
        {
            var leftPoint = FirstPointInsideBeam(PointsAtCertainDistanceLeftToRight(y));
            var rightPoint = FirstPointInsideBeam(PointsAtCertainDistanceRightToLeft(y));
            return (int)(rightPoint.X - leftPoint.X);
        }

        private Point[] GetAnglesAtDistance(int distance)
        {
            var topRightPointAtDistance = PointsAtCertainDistanceRightToLeft(distance).First(pt => drone.IsBeamWorkingOnGivenPosition(pt));
            var topLeftPoint = topRightPointAtDistance - new Point(99, 0);
            var bottomLeftPoint = topLeftPoint + new Point(0, 99);
            var bottomRightPoint = topRightPointAtDistance + new Point(0, 99);
            return new[] { topLeftPoint, topRightPointAtDistance, bottomLeftPoint, bottomRightPoint };
        }

        public class Drone
        {
            private readonly long[] program;
            public Drone(long[] program) => this.program = program;

            public bool IsBeamWorkingOnGivenPosition(Point position)
            {
                var computer = new IntcodeComputer_V5(program);
                computer.Inputs.Enqueue((long)position.X);
                computer.Inputs.Enqueue((long)position.Y);
                while (!computer.ExecuteNextInstruction()) ;
                return computer.Outputs.Dequeue() > 0;
            }
        }
    }
}
