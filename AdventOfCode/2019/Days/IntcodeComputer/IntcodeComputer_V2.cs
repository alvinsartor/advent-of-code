﻿using System;
using System.Collections.Generic;

namespace AdventOfCode._2019.Days.IntcodeComputer
{
    public class IntcodeComputer_V2
    {
        public Stack<int> Inputs = new Stack<int>();
        public Stack<int> Outputs = new Stack<int>();

        public void Execute(int[] program)
        {
            var index = 0;
            while (program[index] != 99) index = HandleInstruction(index, program);
        }

        private int HandleInstruction(int instructionIndex, int[] program)
        {
            var instruction = program[instructionIndex].ToString().PadLeft(5, '0');
            var operation = instruction[^2..];
            var parameterModes = instruction[..3];

            switch (operation)
            {
                case "01":
                    Add(program, instructionIndex, parameterModes);
                    return instructionIndex + 4;
                case "02":
                    Mult(program, instructionIndex, parameterModes);
                    return instructionIndex + 4;
                case "03":
                    InputInstruction(program, instructionIndex, Inputs);
                    return instructionIndex + 2;
                case "04":
                    OutputInstruction(program, instructionIndex, Outputs);
                    return instructionIndex + 2;
                case "05":
                    return JumpIf(program, instructionIndex, parameterModes, true);
                case "06":
                    return JumpIf(program, instructionIndex, parameterModes, false);
                case "07":
                    LessThan(program, instructionIndex, parameterModes);
                    return instructionIndex + 4;
                case "08":
                    EqualTo(program, instructionIndex, parameterModes);
                    return instructionIndex + 4;
                default:
                    throw new NotImplementedException();
            }
        }

        private static int GetParameter(char parameterMode, int parameterIndex, int[] program) =>
            parameterMode switch
            {
                '0' => program[program[parameterIndex]],
                '1' => program[parameterIndex],
                _ => throw new NotImplementedException(),
            };

        private static void Add(int[] program, int index, string instructionPrefix)
        {
            var firstMember = GetParameter(instructionPrefix[2], index + 1, program);
            var secondMember = GetParameter(instructionPrefix[1], index + 2, program);
            program[program[index + 3]] = firstMember + secondMember;
        }

        private static void Mult(int[] program, int index, string instructionPrefix)
        {
            var firstMember = GetParameter(instructionPrefix[2], index + 1, program);
            var secondMember = GetParameter(instructionPrefix[1], index + 2, program);
            program[program[index + 3]] = firstMember * secondMember;
        }

        private static void InputInstruction(int[] program, int index, Stack<int> Inputs) =>
            program[program[index + 1]] = Inputs.Pop();

        private static void OutputInstruction(int[] program, int index, Stack<int> Outputs) =>
            Outputs.Push(program[program[index + 1]]);

        private static int JumpIf(int[] program, int index, string instructionPrefix, bool jumpIfNon0)
        {
            var firstMember = GetParameter(instructionPrefix[2], index + 1, program);
            var secondMember = GetParameter(instructionPrefix[1], index + 2, program);
            if (firstMember != 0 && jumpIfNon0 || firstMember == 0 && !jumpIfNon0)
                return secondMember;
            else
                return index + 3;
        }

        private static void LessThan(int[] program, int index, string instructionPrefix)
        {
            var firstMember = GetParameter(instructionPrefix[2], index + 1, program);
            var secondMember = GetParameter(instructionPrefix[1], index + 2, program);
            program[program[index + 3]] = firstMember < secondMember ? 1 : 0;
        }

        private static void EqualTo(int[] program, int index, string instructionPrefix)
        {
            var firstMember = GetParameter(instructionPrefix[2], index + 1, program);
            var secondMember = GetParameter(instructionPrefix[1], index + 2, program);
            program[program[index + 3]] = firstMember == secondMember ? 1 : 0;
        }
    }
}
