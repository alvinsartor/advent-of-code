using NUnit.Framework;
using System.Linq;

namespace AdventOfCode._2019.Days.IntcodeComputer.NUnit
{
    [TestFixture]
    internal sealed class IntcodeComputer_V5Fixture
    {

        [TestCase("1,0,0,0,99", ExpectedResult = "2,0,0,0,99")] //1 + 1 = 2
        [TestCase("2,3,0,3,99", ExpectedResult = "2,3,0,6,99")] //3 * 2 = 6
        [TestCase("2,4,4,5,99,0", ExpectedResult = "2,4,4,5,99,9801")]// 99 * 99 = 9801
        [TestCase("1,1,1,4,99,5,6,0,99", ExpectedResult = "30,1,1,4,2,5,6,0,99")]
        public string ComputerSuccessfullyPerformsAdditionsAndMultiplications(string encodedProgram)
        {
            var program = encodedProgram.Split(",").Select(long.Parse).ToArray();
            var computer = new IntcodeComputer_V5(program);

            computer.Execute();

            var outputProgram = computer.ProgramSnapshot.Take(program.Length);
            return string.Join(',', outputProgram);
        }

        [TestCase(8, 1)]
        [TestCase(0, 0)]
        [TestCase(99, 0)]
        public void TestEqualInstructionWithPositionMode(int input, int expectedOutput)
        {
            // Using position mode, consider whether the input is equal to 8; output 1 (if it is) or 0 (if it is not).
            var encodedProgram = "3,9,8,9,10,9,4,9,99,-1,8";
            var program = encodedProgram.Split(",").Select(long.Parse).ToArray();
            var computer = new IntcodeComputer_V5(program);

            computer.Inputs.Enqueue(input);
            computer.Execute();

            var output = computer.Outputs.Peek();
            Assert.AreEqual(output, expectedOutput);
        }

        [TestCase(8, 1)]
        [TestCase(0, 0)]
        [TestCase(99, 0)]
        public void TestEqualInstructionWithImmediateMode(int input, int expectedOutput)
        {
            // Using immediate mode, consider whether the input is equal to 8; output 1 (if it is) or 0 (if it is not).
            var encodedProgram = "3,3,1108,-1,8,3,4,3,99";
            var program = encodedProgram.Split(",").Select(long.Parse).ToArray();
            var computer = new IntcodeComputer_V5(program);

            computer.Inputs.Enqueue(input);
            computer.Execute();

            var output = computer.Outputs.Peek();
            Assert.AreEqual(output, expectedOutput);
        }

        [TestCase(7, 1)]
        [TestCase(0, 1)]
        [TestCase(99, 0)]
        public void TestLessThanInstructionWithPositionMode(int input, int expectedOutput)
        {
            // Using position mode, consider whether the input is less than 8; output 1 (if it is) or 0 (if it is not).
            var encodedProgram = "3,9,7,9,10,9,4,9,99,-1,8";
            var program = encodedProgram.Split(",").Select(long.Parse).ToArray();
            var computer = new IntcodeComputer_V5(program);

            computer.Inputs.Enqueue(input);
            computer.Execute();

            var output = computer.Outputs.Peek();
            Assert.AreEqual(output, expectedOutput);
        }

        [TestCase(7, 1)]
        [TestCase(0, 1)]
        [TestCase(99, 0)]
        public void TestLessThanInstructionWithImmediateMode(int input, int expectedOutput)
        {
            // Using immediate mode, consider whether the input is less than 8; output 1 (if it is) or 0 (if it is not).
            var encodedProgram = "3,3,1107,-1,8,3,4,3,99";
            var program = encodedProgram.Split(",").Select(long.Parse).ToArray();
            var computer = new IntcodeComputer_V5(program);

            computer.Inputs.Enqueue(input);
            computer.Execute();

            var output = computer.Outputs.Peek();
            Assert.AreEqual(output, expectedOutput);
        }

        [TestCase(0, 0)]
        [TestCase(1, 1)]
        public void TestJumpInstructionWithPositionMode(int input, int expectedOutput)
        {
            // Using position mode, output 0 if the input was zero or 1 if the input was non-zero.
            var encodedProgram = "3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9";
            var program = encodedProgram.Split(",").Select(long.Parse).ToArray();
            var computer = new IntcodeComputer_V5(program);

            computer.Inputs.Enqueue(input);
            computer.Execute();

            var output = computer.Outputs.Peek();
            Assert.AreEqual(output, expectedOutput);
        }

        [TestCase(0, 0)]
        [TestCase(1, 1)]
        public void TestJumpInstructionWithImmediateMode(int input, int expectedOutput)
        {
            // Using immediate mode, output 0 if the input was zero or 1 if the input was non-zero.
            var encodedProgram = "3,3,1105,-1,9,1101,0,0,12,4,12,99,1";
            var program = encodedProgram.Split(",").Select(long.Parse).ToArray();
            var computer = new IntcodeComputer_V5(program);

            computer.Inputs.Enqueue(input);
            computer.Execute();

            var output = computer.Outputs.Peek();
            Assert.AreEqual(output, expectedOutput);
        }

        [Test]
        public void TestComputerHandlesBigNumbers()
        {
            var encodedProgram = "104,1125899906842624,99";
            var program = encodedProgram.Split(",").Select(long.Parse).ToArray();
            var computer = new IntcodeComputer_V5(program);

            computer.Execute();

            var output = computer.Outputs.Peek();
            Assert.AreEqual(output, 1125899906842624);
        }

        [TestCase("109,-1,4,1,99", ExpectedResult = -1)]
        [TestCase("109,-1,104,1,99", ExpectedResult = 1)]
        [TestCase("109,-1,204,1,99", ExpectedResult = 109)]
        [TestCase("109,1,9,2,204,-6,99", ExpectedResult = 204)]
        [TestCase("109,1,109,9,204,-6,99", ExpectedResult = 204)]
        [TestCase("109,1,209,-1,204,-106,99", ExpectedResult = 204)]
        public long ParameterPositionIsCorrectlyRetrieved(string encodedProgram)
        {
            var program = encodedProgram.Split(",").Select(long.Parse).ToArray();
            var computer = new IntcodeComputer_V5(program);

            computer.Execute();

            var output = computer.Outputs.Peek();
            return output;
        }
    }
}