﻿using System;

namespace AdventOfCode._2019.Days.IntcodeComputer
{
    public class IntcodeComputer_V1
    {
        public static void Execute(int[] program)
        {
            for (var i = 0; i < program.Length && program[i] != 99; i += 4)
                if (program[i] == 1) Add(program, i);
                else if (program[i] == 2) Mult(program, i);
                else throw new Exception("Unrecognized command: " + program[i].ToString());
        }

        private static void Add(int[] prg, int i) => prg[prg[i + 3]] = prg[prg[i + 1]] + prg[prg[i + 2]];
        private static void Mult(int[] prg, int i) => prg[prg[i + 3]] = prg[prg[i + 1]] * prg[prg[i + 2]];
    }
}
