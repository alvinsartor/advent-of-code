﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2019.Days.IntcodeComputer
{
    public class IntcodeComputer_V4
    {
        private static int _IDCounter = 0;
        public readonly int Identifier;
        public readonly Queue<long> Inputs;
        public readonly Queue<long> Outputs;
        public readonly Stack<long> StoredOutputs;

        private long pointer;
        private readonly long[] program;

        public IntcodeComputer_V4(
            long[] program,
            Queue<long>? inputChannel = null,
            Queue<long>? outputChannel = null)
        {
            Identifier = _IDCounter++;
            this.program = program.ToArray();
            pointer = 0;

            Inputs = inputChannel ?? new Queue<long>();
            Outputs = outputChannel ?? new Queue<long>();
            StoredOutputs = new Stack<long>();
        }

        /// <summary>
        /// Gets a value indicating whether this instance has ended to compute the program.
        /// </summary>
        public bool HasEnded { get; private set; } = false;

        /// <summary>
        /// Executes the specified program.
        /// </summary>
        public void Execute()
        {
            bool shouldHalt = false;

            while (program[pointer] != 99 || !shouldHalt) 
                (pointer, shouldHalt) = HandleInstruction(pointer, program);

            HasEnded = program[pointer] == 99;
        }

        /// <summary>
        /// Executes the next instruction of the program.
        /// </summary>
        /// <exception cref="Exception">This program has already been executed.</exception>
        public bool ExecuteNextInstruction()
        {
            if (HasEnded) throw new Exception("This program has already been executed.");

            (long newPointer, bool shouldHalt) = HandleInstruction(pointer, program);
            pointer = newPointer;
            HasEnded = program[pointer] == 99;

            return shouldHalt || HasEnded;
        }

        private (long pointer, bool shouldHalt) HandleInstruction(long instructionIndex, long[] program)
        {
            var instruction = program[instructionIndex].ToString().PadLeft(5, '0');
            var operation = instruction[^2..];
            var parameterModes = instruction[..3];
            //Console.WriteLine($"Instruction {instruction}, computer # {Identifier}");

            switch (operation)
            {
                case "01":
                    Add(program, instructionIndex, parameterModes);
                    return (instructionIndex + 4, false);
                case "02":
                    Mult(program, instructionIndex, parameterModes);
                    return (instructionIndex + 4, false);
                case "03":
                    return InputInstruction(program, instructionIndex);
                case "04":
                    OutputInstruction(program, instructionIndex);
                    return (instructionIndex + 2, false);
                case "05":
                    return (JumpIf(program, instructionIndex, parameterModes, true), false);
                case "06":
                    return (JumpIf(program, instructionIndex, parameterModes, false), false);
                case "07":
                    LessThan(program, instructionIndex, parameterModes);
                    return (instructionIndex + 4, false);
                case "08":
                    EqualTo(program, instructionIndex, parameterModes);
                    return (instructionIndex + 4, false);
                case "99":
                    return (instructionIndex, true);
                default:
                    throw new NotImplementedException();
            }
        }

        private static long GetParameter(char parameterMode, long parameterIndex, long[] program) =>
            parameterMode switch
            {
                '0' => program[program[parameterIndex]],
                '1' => program[parameterIndex],
                _ => throw new NotImplementedException(),
            };
        private static void Add(long[] program, long index, string instructionPrefix)
        {
            var firstMember = GetParameter(instructionPrefix[2], index + 1, program);
            var secondMember = GetParameter(instructionPrefix[1], index + 2, program);
            program[program[index + 3]] = firstMember + secondMember;
        }
        private static void Mult(long[] program, long index, string instructionPrefix)
        {
            var firstMember = GetParameter(instructionPrefix[2], index + 1, program);
            var secondMember = GetParameter(instructionPrefix[1], index + 2, program);
            program[program[index + 3]] = firstMember * secondMember;
        }
        private (long pointer, bool shouldHalt) InputInstruction(long[] program, long index)
        {
            if (Inputs.Count == 0) 
            {
                return (index, true);
            }
            
            program[program[index + 1]] = Inputs.Dequeue();
            return (index + 2, false);
        }

        private void OutputInstruction(long[] program, long index)
        {
            long valueToOutput = program[program[index + 1]];
            Outputs.Enqueue(valueToOutput);
            StoredOutputs.Push(valueToOutput);
        }
        private static long JumpIf(long[] program, long index, string instructionPrefix, bool jumpIfNon0)
        {
            var firstMember = GetParameter(instructionPrefix[2], index + 1, program);
            var secondMember = GetParameter(instructionPrefix[1], index + 2, program);
            if (firstMember != 0 && jumpIfNon0 || firstMember == 0 && !jumpIfNon0)
                return secondMember;
            else
                return index + 3;
        }
        private static void LessThan(long[] program, long index, string instructionPrefix)
        {
            var firstMember = GetParameter(instructionPrefix[2], index + 1, program);
            var secondMember = GetParameter(instructionPrefix[1], index + 2, program);
            program[program[index + 3]] = firstMember < secondMember ? 1 : 0;
        }
        private static void EqualTo(long[] program, long index, string instructionPrefix)
        {
            var firstMember = GetParameter(instructionPrefix[2], index + 1, program);
            var secondMember = GetParameter(instructionPrefix[1], index + 2, program);
            program[program[index + 3]] = firstMember == secondMember ? 1 : 0;
        }
    }
}
