﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2019.Days.IntcodeComputer
{
    public class IntcodeComputer_V5
    {
        private static int _IDCounter = 0;
        public readonly int Identifier;
        public readonly Queue<long> Inputs;
        public readonly Queue<long> Outputs;
        public readonly Stack<long> StoredOutputs;

        private long pointer;
        private long relativeBase;
        private readonly long[] program;

        /// <summary>
        /// Initializes a new instance of the <see cref="IntcodeComputer_V5"/> class.
        /// </summary>
        /// <param name="program">The program to be ran on this computer.</param>
        /// <param name="inputChannel">The input channel (optional).</param>
        /// <param name="outputChannel">The output channel (optional).</param>
        public IntcodeComputer_V5(
            long[] program,
            Queue<long>? inputChannel = null,
            Queue<long>? outputChannel = null)
        {
            Identifier = _IDCounter++;
            this.program = program.Concat(new long[5000]).ToArray();
            pointer = 0;
            relativeBase = 0;

            Inputs = inputChannel ?? new Queue<long>();
            Outputs = outputChannel ?? new Queue<long>();
            StoredOutputs = new Stack<long>();
        }

        /// <summary>
        /// Gets a value indicating whether this instance has ended to compute the program.
        /// </summary>
        public bool HasEnded { get; private set; } = false;

        /// <summary>
        /// Gets a snapshot of the program.
        /// </summary>
        public long[] ProgramSnapshot => program.ToArray();

        /// <summary>
        /// Executes the specified program.
        /// </summary>
        public void Execute()
        {
            bool shouldHalt = false;

            while (program[pointer] != 99 || !shouldHalt) 
                (pointer, shouldHalt) = HandleInstruction(pointer, program);

            HasEnded = program[pointer] == 99;
        }

        /// <summary>
        /// Executes the next instruction of the program.
        /// </summary>
        /// <exception cref="Exception">This program has already been executed.</exception>
        public bool ExecuteNextInstruction()
        {
            if (HasEnded) throw new Exception("This program has already been executed.");

            (long newPointer, bool shouldHalt) = HandleInstruction(pointer, program);
            pointer = newPointer;
            HasEnded = program[pointer] == 99;

            return shouldHalt || HasEnded;
        }

        private (long pointer, bool shouldHalt) HandleInstruction(long instructionIndex, long[] program)
        {
            var instruction = program[instructionIndex].ToString().PadLeft(5, '0');
            var operation = instruction[^2..];
            var parameterModes = instruction[..3];
            
            switch (operation)
            {
                case "01":
                    Add(program, instructionIndex, parameterModes);
                    return (instructionIndex + 4, false);
                case "02":
                    Mult(program, instructionIndex, parameterModes);
                    return (instructionIndex + 4, false);
                case "03":
                    return InputInstruction(program, instructionIndex, parameterModes);
                case "04":
                    OutputInstruction(program, instructionIndex, parameterModes);
                    return (instructionIndex + 2, false);
                case "05":
                    return (JumpIf(program, instructionIndex, parameterModes, true), false);
                case "06":
                    return (JumpIf(program, instructionIndex, parameterModes, false), false);
                case "07":
                    LessThan(program, instructionIndex, parameterModes);
                    return (instructionIndex + 4, false);
                case "08":
                    EqualTo(program, instructionIndex, parameterModes);
                    return (instructionIndex + 4, false);
                case "09":
                    AdjustRelativeBase(program, instructionIndex, parameterModes);
                    return (instructionIndex + 2, false);
                case "99":
                    return (instructionIndex, true);
                default:
                    throw new NotImplementedException();
            }
        }

        private long GetParameterPosition(char parameterMode, long parameterIndex, long[] program) =>
            parameterMode switch
            {
                '0' => program[parameterIndex],
                '1' => parameterIndex,
                '2' => relativeBase + program[parameterIndex],
                _ => throw new NotImplementedException(),
            };
        private void Add(long[] program, long index, string instructionPrefix)
        {
            var firstMember = program[GetParameterPosition(instructionPrefix[2], index + 1, program)];
            var secondMember = program[GetParameterPosition(instructionPrefix[1], index + 2, program)];
            var savingDestination = GetParameterPosition(instructionPrefix[0], index + 3, program);
            program[savingDestination] = firstMember + secondMember;
        }
        private void Mult(long[] program, long index, string instructionPrefix)
        {
            var firstMember = program[GetParameterPosition(instructionPrefix[2], index + 1, program)];
            var secondMember = program[GetParameterPosition(instructionPrefix[1], index + 2, program)];
            var savingDestination = GetParameterPosition(instructionPrefix[0], index + 3, program);
            program[savingDestination] = firstMember * secondMember;
        }
        private (long pointer, bool shouldHalt) InputInstruction(long[] program, long index, string instructionPrefix)
        {
            if (Inputs.Count == 0) return (index, true);

            long indexOfValueToInput = GetParameterPosition(instructionPrefix[2], index + 1, program);
            program[indexOfValueToInput] = Inputs.Dequeue();
            return (index + 2, false);
        }
        private void OutputInstruction(long[] program, long index, string instructionPrefix)
        {
            long indexOfValueToOutput = GetParameterPosition(instructionPrefix[2], index + 1, program);
            long valueToOutput = program[indexOfValueToOutput];
            Outputs.Enqueue(valueToOutput);
            StoredOutputs.Push(valueToOutput);
        }
        private long JumpIf(long[] program, long index, string instructionPrefix, bool jumpIfNon0)
        {
            var firstMember = program[GetParameterPosition(instructionPrefix[2], index + 1, program)];
            var secondMember = program[GetParameterPosition(instructionPrefix[1], index + 2, program)];
            if (firstMember != 0 && jumpIfNon0 || firstMember == 0 && !jumpIfNon0)
                return secondMember;
            else
                return index + 3;
        }
        private void LessThan(long[] program, long index, string instructionPrefix)
        {
            var firstMember = program[GetParameterPosition(instructionPrefix[2], index + 1, program)];
            var secondMember = program[GetParameterPosition(instructionPrefix[1], index + 2, program)];
            var savingDestination = GetParameterPosition(instructionPrefix[0], index + 3, program);
            program[savingDestination] = firstMember < secondMember ? 1 : 0;
        }
        private void EqualTo(long[] program, long index, string instructionPrefix)
        {
            var firstMember = program[GetParameterPosition(instructionPrefix[2], index + 1, program)];
            var secondMember = program[GetParameterPosition(instructionPrefix[1], index + 2, program)];
            var savingDestination = GetParameterPosition(instructionPrefix[0], index + 3, program);
            program[savingDestination] = firstMember == secondMember ? 1 : 0;
        }
        private void AdjustRelativeBase(long[] program, long index, string instructionPrefix)
        {
            var indexOfAdjustment = GetParameterPosition(instructionPrefix[2], index + 1, program);
            var amountToAdjustWith = program[indexOfAdjustment];
            relativeBase += amountToAdjustWith;
        }
    }
}