﻿using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdventOfCode._2019.Days.IntcodeComputer
{
    public class ASCIIComputer
    {
        protected readonly IntcodeComputer_V5 computer;
        private readonly bool displayConsoleOutputs;
        private readonly bool addOutputsToQueue;

        protected Queue<long> NonCharResults { get; init; }
        protected Queue<string> ProcessedOutputs { get; init; }

        public ASCIIComputer(
            long[] program, 
            bool displayConsoleOutputs = true,
            bool addOutputsToQueue = true)
        {
            computer = new IntcodeComputer_V5(program);
            this.displayConsoleOutputs = displayConsoleOutputs;
            this.addOutputsToQueue = addOutputsToQueue;
            NonCharResults = new Queue<long>();
            ProcessedOutputs = new Queue<string>();
        }

        protected void ExecuteTillHalts()
        {
            while (!computer.ExecuteNextInstruction()) ;
            ProcessOutput();
        }

        private void ProcessOutput()
        {
            StringBuilder buffer = new();
            while (computer.Outputs.Count > 0)
            {
                var output = computer.Outputs.Dequeue();
                if (output >= 256) NonCharResults.Enqueue(output);
                else
                {
                    if (displayConsoleOutputs) Console.Write((char)output);
                    if (addOutputsToQueue) buffer.Append((char)output);
                }
            }
            buffer.ToString().Split("\n").ForEach(line => ProcessedOutputs.Enqueue(line));
        }
    }
}
