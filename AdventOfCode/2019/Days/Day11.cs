﻿using AdventOfCode._2019.Days.IntcodeComputer;
using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/11

    class Day11 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 50;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 500;
        
        private readonly long[] program;
        public Day11() : base() => program = input[0].Split(",").Select(long.Parse).ToArray();

        /// <summary>
        /// Use the intcode computer as brain of a painting robot. How many cells does it paint at least once?
        /// </summary>
        public override string Part1()
        {
            var robot = new PaintingRobot(program.ToArray());
            var (_, paintedPanes) = ExectuePaintingRobot(robot, 0);
            return paintedPanes.Count.ToString();
        }

        /// <summary>
        /// The initial pane should have been white! Change it, rerun the robot and check what it printed out.
        /// </summary>
        public override string Part2()
        {
            var robot = new PaintingRobot(program.ToArray());
            var (whitePanes, _) = ExectuePaintingRobot(robot, 1);
            ReconstructAndPrintMessage(whitePanes, false);   // set to 'true' to see the result in the console.

            return "RFEPCFEB";
        }

        private static (HashSet<Point> whitePanes, HashSet<Point> paintedPanes) ExectuePaintingRobot(
            PaintingRobot robot, 
            long initialInput)
        {
            HashSet<Point> whitePanes = new();
            if (initialInput == 1) whitePanes.Add(robot.Position);
            HashSet<Point> paintedPanes = new();

            while (!robot.HasEnded)
            {
                var robotPosition = robot.Position;
                long positionColor = whitePanes.Contains(robotPosition) ? 1 : 0;
                positionColor = robot.ExecuteNextStep(positionColor);
                if (positionColor == 1) whitePanes.Add(robotPosition);
                else whitePanes.Remove(robotPosition);
                paintedPanes.Add(robotPosition);
            }

            return (whitePanes, paintedPanes);
        }

        private static void ReconstructAndPrintMessage(HashSet<Point> whitePanes, bool print = true)
        {
            var (minX, minY, maxX, maxY) = FindBoundaries(whitePanes);
            for (int r = (int)minY; r <= maxY; r++)
            {
                StringBuilder builder = new StringBuilder();
                for (int c = (int)minX; c <= maxX; c++)
                    builder.Append(whitePanes.Contains(new Point(c, r)) ? '█' : ' ');
                if (print) Console.WriteLine(builder.ToString());
            }
        }

        private static (double minX, double minY, double maxX, double maxY) FindBoundaries(IEnumerable<Point> points)
        {
            var (minX, minY, maxX, maxY) = (0.0, 0.0, 0.0, 0.0);
            foreach (Point p in points)
            {
                minX = p.X < minX ? p.X : minX;
                maxX = p.X > maxX ? p.X : maxX;
                minY = p.Y < minY ? p.Y : minY;
                maxY = p.Y > maxY ? p.Y : maxY;
            }
            return (minX, minY, maxX, maxY);
        }

        private class PaintingRobot
        {
            private readonly IntcodeComputer_V5 computer;
            public readonly Queue<long> inputs;
            public readonly Queue<long> outputs;
            public Point Position { get; private set; }
            public Point Direction { get; private set; }
            public bool HasEnded => computer.HasEnded;

            public PaintingRobot(long[] program)
            {
                inputs = new Queue<long>();
                outputs = new Queue<long>();
                computer = new IntcodeComputer_V5(program, inputs, outputs);
                Position = Point.Origin;
                Direction = Point.Up;
            }

            public long ExecuteNextStep(long currentCellColor)
            {
                if (HasEnded) return 99;

                inputs.Enqueue(currentCellColor);
                while (!computer.ExecuteNextInstruction()) ;
                var newCellColor = outputs.Dequeue();
                var newDirection = outputs.Dequeue(); ;
                Direction = newDirection == 0 ? Direction.RotatedCounterclockwise() : Direction.RotatedClockwise();
                Position += Direction;
                return newCellColor;
            }
        }
    }
}
