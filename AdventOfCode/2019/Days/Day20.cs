﻿using AdventOfCode.helpers;
using Graph;
using Graph.PathFinding;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/20

    class Day20 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 10;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 2;

        /// <summary>
        /// Find the shortest path to complete the maze taking into account the teleporting doors.
        /// </summary>
        public override string Part1()
        {
            var graph = BuildBaseGraph(input);
            var externalDoors = GetExternalDoors(input);
            var internalDoors = GetInternalDoors(input);

            AddDoorsToGraph(graph, externalDoors, internalDoors);

            var door1Position = externalDoors["AA"];
            var door2Position = externalDoors["ZZ"];
            var pathAZ = graph.AStarSearch(door1Position, door2Position, (pt) => 1).Result;

            return pathAZ.Edges.Count.ToString();
        }

        private static Graph<Point> BuildBaseGraph(string[] input, string graphName = "Maze")
        {
            Graph<Point> graph = new Graph<Point>(graphName);
            for (var i = 2; i < input.Length - 2; i++)
                for (var j = 2; j < input[i].Length - 2; j++)
                    if (input[i][j] == '.')
                        graph.Add(new Point(j, i));

            foreach (Point point in graph.Nodes.Select(n => n.Value))
            {
                if (graph.Contains(point + Point.Down))
                    graph.GetNode(point).AddBidirectionalConnection(graph.GetNode(point + Point.Down));

                if (graph.Contains(point + Point.Right))
                    graph.GetNode(point).AddBidirectionalConnection(graph.GetNode(point + Point.Right));
            }

            return graph;
        }

        private static void AddDoorsToGraph(
            Graph<Point> graph,
            Dictionary<string, Point> externalDoors,
            Dictionary<string, Point> internalDoors)
        {
            foreach (var doorName in internalDoors.Keys)
            {
                var door1 = internalDoors[doorName];
                var door2 = externalDoors[doorName];
                graph.GetNode(door1).AddBidirectionalConnection(graph.GetNode(door2));
            }
        }

        private static Dictionary<string, Point> GetExternalDoors(string[] input)
        {
            var topSide = FetchDoorsFromLine(input, new Point(2, 2), Point.Right, Point.Up);
            var bottomSide = FetchDoorsFromLine(input, new Point(2, input.Length - 3), Point.Right, Point.Down);
            var leftSide = FetchDoorsFromLine(input, new Point(2, 2), Point.Down, Point.Left);
            var rightSide = FetchDoorsFromLine(input, new Point(input[0].Length - 3, 2), Point.Down, Point.Right);
            var doors = topSide.Concat(bottomSide).Concat(leftSide).Concat(rightSide);
            return doors.ToDictionary(x => x.doorName, x => x.doorPosition);
        }

        private static Dictionary<string, Point> GetInternalDoors(string[] input)
        {
            var centerTop = FetchDoorsFromLine(input, new Point(32, 32), Point.Right, Point.Down, 53);
            var centerBottom = FetchDoorsFromLine(input, new Point(32, 92), Point.Right, Point.Up, 53);
            var centerLeft = FetchDoorsFromLine(input, new Point(32, 32), Point.Down, Point.Right, 60);
            var centerRight = FetchDoorsFromLine(input, new Point(84, 32), Point.Down, Point.Left, 60);
            var doors = centerTop.Concat(centerBottom).Concat(centerLeft).Concat(centerRight);
            return doors.ToDictionary(x => x.doorName, x => x.doorPosition);
        }

        private static IEnumerable<(string doorName, Point doorPosition)> FetchDoorsFromLine(
            string[] input,
            Point startingPoint,
            Point direction,
            Point doorSide,
            int wallLength = int.MaxValue)
        {
            bool IsInInput(Point pt) => pt.X >= 0 && pt.X < input[0].Length && pt.Y >= 0 && pt.Y < input.Length;

            int count = 0;
            while (IsInInput(startingPoint) && count < wallLength)
            {
                if (input[(int)startingPoint.Y][(int)startingPoint.X] == '.')
                    yield return (FetchSingleDoorName(input, startingPoint, doorSide), startingPoint);
                startingPoint += direction;
                count++;
            }
        }

        private static string FetchSingleDoorName(string[] input, Point position, Point doorSide)
        {
            var letterPositions = new[] { position + doorSide, position + doorSide + doorSide };
            var sortedLetters = letterPositions.OrderBy(pt => pt.ManhattanDistanceFromOrigin).ToArray();

            var letter1 = input[(int)sortedLetters[0].Y][(int)sortedLetters[0].X];
            var letter2 = input[(int)sortedLetters[1].Y][(int)sortedLetters[1].X];

            return $"{letter1}{letter2}";
        }

        /// <summary>
        /// New rule: inner doors teleport to external doors of a lower level and so on. The graph has potentially ∞ levels.
        /// </summary>
        public override string Part2()
        {
            var multigraph = BuildMultigraph(input, 26);
            var externalDoors = GetExternalDoors(input);
            var internalDoors = GetInternalDoors(input);

            AddDoorsToMultiGraph(multigraph, externalDoors, internalDoors);

            var door1Position = externalDoors["AA"];
            var door2Position = externalDoors["ZZ"];

            // as all graphs have a node corresponding to ZZ, we might end up to the wrong level.
            // I will add an extra node only on the first level and this will be subtracted 
            // from the final path count.
            var extraNodePosition = new Point(-1, -1);
            var extraNode = multigraph[0].Add(extraNodePosition);
            extraNode.AddBidirectionalConnection(multigraph[0].GetNode(door2Position));

            var pathAZ = multigraph[0].AStarSearch(door1Position, extraNodePosition, (pt) => 1).Result;

            return (pathAZ.Edges.Count - 1).ToString();
        }

        private static Graph<Point>[] BuildMultigraph(string[] input, int levels)
        {
            Graph<Point>[] graphs = Enumerable.Range(1, levels).Select(level => new Graph<Point>($"level{level}")).ToArray();
            for (var i = 2; i < input.Length - 2; i++)
                for (var j = 2; j < input[i].Length - 2; j++)
                    if (input[i][j] == '.')
                        foreach(var graph in graphs)
                            graph.Add(new Point(j, i));

            foreach (Point point in graphs[0].Nodes.Select(n => n.Value))
            {
                if (graphs[0].Contains(point + Point.Down))
                    foreach (var graph in graphs)
                        graph.GetNode(point).AddBidirectionalConnection(graph.GetNode(point + Point.Down));

                if (graphs[0].Contains(point + Point.Right))
                    foreach (var graph in graphs)
                        graph.GetNode(point).AddBidirectionalConnection(graph.GetNode(point + Point.Right));
            }

            return graphs;
        }

        private static void AddDoorsToMultiGraph(
            Graph<Point>[] multigraph,
            Dictionary<string, Point> externalDoors,
            Dictionary<string, Point> internalDoors)
        {
            foreach (var doorName in internalDoors.Keys)
            {
                var door1 = internalDoors[doorName];
                var door2 = externalDoors[doorName];
                for (int level = 0; level < multigraph.Length - 1; level++)
                {
                    var upperLevelNode = multigraph[level].GetNode(door1);
                    var lowerLevelNode = multigraph[level + 1].GetNode(door2);
                    upperLevelNode.AddBidirectionalConnection(lowerLevelNode);
                }
            }
        }
    }
}
