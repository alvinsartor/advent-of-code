﻿using System.Linq;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/5

    class Day04 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 50;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 50;

        /// <summary>
        /// Given some rules and a range, determine how many numbers in the range follow the rules.
        /// </summary>
        public override string Part1()
        {
            var rangeString = input[0].Split("-");
            var start = int.Parse(rangeString[0]);
            var end = int.Parse(rangeString[1]);

            return Enumerable
                .Range(start, end - start)
                .Count(n => FollowsP1Rule(n.ToString()))
                .ToString();
        }

        /// <summary>
        /// Calculate the amount of fuel needed to propel the spaceship, taking into account the weight of the same fuel as well.
        /// </summary>
        public override string Part2()
        {
            var rangeString = input[0].Split("-");
            var start = int.Parse(rangeString[0]);
            var end = int.Parse(rangeString[1]);

            return Enumerable
                .Range(start, end - start)
                .Count(n => FollowsP2Rule(n.ToString()))
                .ToString();
        }

        private static bool FollowsP1Rule(string number) => IsNotDecreasing(number) && HasAdjacentEqualNumbers(number);
        private static bool FollowsP2Rule(string number) => IsNotDecreasing(number) && HasAtLeastOneAdjacentPair(number);

        private static bool IsNotDecreasing(string number)
        {
            for (int i = 1; i < number.Length; i++)
                if (number[i] < number[i - 1])
                    return false;
            return true;
        }

        private static bool HasAdjacentEqualNumbers(string number)
        {
            for (int i = 1; i < number.Length; i++)
                if (number[i - 1] == number[i])
                    return true;
            return false;
        }

        private static bool HasAtLeastOneAdjacentPair(string number)
        {
            var occurrences = number.GroupBy(c => c).GroupBy(c => c.Count()).Select(x => x.Key).ToHashSet();
            return occurrences.Contains(2);
        }
    }
}
