﻿using System;
using System.Linq;
using System.Numerics;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/22

    class Day22 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 100;

        /// <summary>
        /// Perform a series of shuffling on a 10007 cards deck and print the result.
        /// </summary>
        public override string Part1()
        {
            int[] deck = Enumerable.Range(0, 10007).ToArray();
            foreach (string instruction in input) deck = ApplyInstructionToDeck(deck, instruction);
            return Array.IndexOf(deck, 2019).ToString();
        }

        private static int[] DealIntoNewStack(int[] deck) => deck.Reverse().ToArray();
        private static int[] CutDeck(int[] deck, int cut) => cut > 0
                ? deck[cut..].Concat(deck[..cut]).ToArray()
                : deck[^-cut..].Concat(deck[..^-cut]).ToArray();
        private static int[] DealWithIncrement(int[] deck, int increment)
        {
            int[] newDeck = new int[deck.Length];
            var oldDeckCursor = 0;
            var newDeckCursor = 0;
            while (oldDeckCursor < deck.Length)
            {
                newDeck[newDeckCursor] = deck[oldDeckCursor];
                oldDeckCursor++;
                newDeckCursor = (newDeckCursor + increment) % deck.Length;
            }
            return newDeck;
        }

        private static int[] ApplyInstructionToDeck(int[] deck, string instruction)
        {
            if (instruction.Contains("deal into")) return DealIntoNewStack(deck);
            if (instruction.Contains("cut")) return CutDeck(deck, int.Parse(instruction[4..]));
            return DealWithIncrement(deck, int.Parse(instruction[20..]));
        }

        /// <summary>
        /// Now the same, with an immense deck, an immense number of times! [Thanks mcpower from Reddit!]
        /// </summary>
        public override string Part2()
        {
            BigInteger deckSize = 119315717514047;
            BigInteger times = 101741582076661;
            BigInteger cardPosition = 2020;
            BigInteger modularInverse(BigInteger n) => BigInteger.ModPow(n, deckSize - 2, deckSize);

            BigInteger increment_mul = 1;
            BigInteger offset_diff = 0;
            foreach (string instruction in input)
            {
                if (instruction.Contains("deal into"))
                {
                    increment_mul *= -1;
                    increment_mul = Mod(increment_mul, deckSize);
                    offset_diff += increment_mul;
                    offset_diff = Mod(offset_diff, deckSize); 
                }
                else if (instruction.Contains("cut"))
                {
                    var value = BigInteger.Parse(instruction[4..]);
                    offset_diff += value * increment_mul;
                    offset_diff = Mod(offset_diff, deckSize);
                }
                else
                {
                    var value = BigInteger.Parse(instruction[20..]);
                    increment_mul *= modularInverse(value);
                    increment_mul = Mod(increment_mul, deckSize);
                }
            }

            var increment = BigInteger.ModPow(increment_mul, times, deckSize);
            var offset = offset_diff * (1 - increment) * modularInverse(Mod(1 - increment_mul, deckSize));
            offset = Mod(offset, deckSize);
            var result = Mod((offset + cardPosition * increment), deckSize);
            return result.ToString();
        }

        private static BigInteger Mod(BigInteger x, BigInteger m)
        {
            BigInteger r = x % m;
            return r < 0 ? r + m : r;
        }
    }
}