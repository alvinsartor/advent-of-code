﻿using AdventOfCode.helpers;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2019.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2019/day/10

    class Day10 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 50;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 50;
       
        /// <summary>
        /// Given a map representing an asteroid field, find the number of asteroids visible from the one with the best visibility.
        /// </summary>
        public override string Part1()
        {
            Point viewSize = new(input[0].Length, input.Length);
            HashSet<Point> asteroids = ExtractAsteroidsCoordinatesFromInput(input);
            var (_, numberOfAsteroids) = FetchBestPositionInAsteroidField(asteroids, viewSize);
            return numberOfAsteroids.ToString();
        }

        /// <summary>
        /// By vaporizing all visible asteroids from the observation point found in P1, using a clockwise motion, 
        /// what is the 200th asteroid being hit?
        /// </summary>
        public override string Part2()
        {
            Point viewSize = new(input[0].Length, input.Length);
            HashSet<Point> asteroids = ExtractAsteroidsCoordinatesFromInput(input);
            var (position, _) = FetchBestPositionInAsteroidField(asteroids, viewSize);
            var lucky200 = VaporizeAsteroids(position, asteroids, viewSize).Take(200).Last();

            return (lucky200.X * 100 + lucky200.Y).ToString();
        }

        private static IEnumerable<Point> VaporizeAsteroids(Point laserPosition, HashSet<Point> asteroids, Point viewSize)
        {
            var laserUp = laserPosition + new Point(0, -1);
            asteroids.Remove(laserPosition);
            while (asteroids.Count > 0)
            {
                var currentlyVisible = AsteroidsVisibleFromPosition(laserPosition, asteroids, viewSize)
                    .OrderBy(pt => MathExtensions.AngleToClockwiseValue(MathExtensions.Angle(laserPosition, laserUp, pt)))
                    .ToList();

                foreach (var asteroid in currentlyVisible)
                {
                    asteroids.Remove(asteroid);
                    yield return asteroid;
                }
            }
        }

        private static (Point position, int numberOfAsteroids) FetchBestPositionInAsteroidField(
            HashSet<Point> asteroids,
            Point viewSize) => asteroids
                .Select(pos => (pos, count: AsteroidsVisibleFromPosition(pos, asteroids, viewSize).ToList().Count))
                .MaxBy(posAndCount => posAndCount.count);

        private static HashSet<Point> ExtractAsteroidsCoordinatesFromInput(string[] input)
        {
            HashSet<Point> asteroids = new HashSet<Point>();
            for (int i = 0; i < input.Length; i++)
                for (int j = 0; j < input[i].Length; j++)
                    if (input[i][j] == '#')
                        asteroids.Add(new Point(j, i));
            return asteroids;
        }

        private static IEnumerable<Point> AsteroidsVisibleFromPosition(Point position, HashSet<Point> asteroidsField, Point viewSize)
        {
            var availableAsteroids = asteroidsField.ToHashSet();
            availableAsteroids.Remove(position);
            var sortedAsteroids = availableAsteroids.OrderBy(pt => Point.Distance(pt, position)).ToList();

            foreach(var asteroid in sortedAsteroids)
                if (availableAsteroids.Contains(asteroid))
                {
                    yield return asteroid;
                    PointsInTrajectory(position, asteroid, viewSize)
                        .ForEach(hiddenAsteroid => availableAsteroids.Remove(hiddenAsteroid));
                }
        }

        private static IEnumerable<Point> PointsInTrajectory(Point origin, Point p1, Point viewSize)
        {
            Point vector = (p1 - origin).Reduced();
            Point cursor = origin + vector;
            do
            {
                yield return cursor;
                cursor += vector;
            } while (IsPointInTheView(cursor, viewSize));
        }

        private static bool IsPointInTheView(Point p, Point viewSize) => 
            p.X >= 0 && p.X < viewSize.X && p.Y >= 0 && p.Y < viewSize.Y;
    }
}
