﻿using System;
using System.Diagnostics;

namespace AdventOfCode
{
    public static class Stopwatcher
    {
        private static readonly Stopwatch stopwatch = new Stopwatch();

        /// <summary>
        /// Executes the specified function and measures how long it took.
        /// </summary>
        /// <param name="func">The function to execute.</param>
        /// <param name="precision">The number of times the function will be executed.</param>
        public static (string result, decimal timeInMs) Track(Func<string> func, int precision = 1000)
        {
            var result = func();
            decimal elapsedTime = precision > 0 ? RepeatFunction(func, precision) : 0;
            return (result, elapsedTime);
        }

        private static decimal RepeatFunction<T>(Func<T> func, int precision)
        {
            decimal amount = 0;
            
            for (var i = 0; i < precision; i++)
            {
                stopwatch.Reset();
                stopwatch.Start();
                func();
                stopwatch.Stop();
                amount += (decimal) stopwatch.Elapsed.TotalMilliseconds;
            }
            return amount / precision;
        }
    }
}
