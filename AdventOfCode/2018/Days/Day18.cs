﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/18

    class Day18: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 100;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 10;

        /// <summary>
        /// Calculate how the landscape will change after 10 minutes, given the puzzle rules.
        /// </summary>
        public override string Part1()
        {
            var map = DecodeInitialMap(input);
            
            int turn = -1;
            while (++turn < 10) map = GetNewMap(map);
            
            return GetResourceValue(map).ToString();
        }

        /// <summary>
        /// Check the resource value after 1000000000 minutes.
        /// </summary>
        public override string Part2()
        {
            var map = DecodeInitialMap(input);
            (int cycleStartIndex, int[] cycleValues) = FindCycleInResourceValues(map);

            var cycleElementIndex = (1000000000 - cycleStartIndex) % cycleValues.Length - 1; // -1 as we start with 0.
            return cycleValues[cycleElementIndex].ToString();
        }

        private static char[][] DecodeInitialMap(string[] input) => 
            input.Select(line => line.ToCharArray()).ToArray();

        private static (int cycleStartIndex, int[] cycleValues) FindCycleInResourceValues(char[][] map)
        {
            int turn = 0;
            List<int> resourceValues = new();
            Dictionary<int, List<int>> resourceValueOccurrences = new();

            bool cycleFound = false;
            while (!cycleFound)
            {
                map = GetNewMap(map);
                var currentMapValue = GetResourceValue(map);
                resourceValues.Add(currentMapValue);

                if (!resourceValueOccurrences.ContainsKey(currentMapValue)) 
                    resourceValueOccurrences.Add(currentMapValue, new List<int>());
                resourceValueOccurrences[currentMapValue].Add(turn++);

                if (resourceValueOccurrences[currentMapValue].Count == 5) cycleFound = true;
            }

            var cycleStartValue = resourceValues[^1];
            var cycleStartIndex = resourceValueOccurrences[cycleStartValue][^2];
            var cycleEndIndex = resourceValueOccurrences[cycleStartValue][^1];
            var cycle = resourceValues.ToArray()[cycleStartIndex..cycleEndIndex];
            return (cycleStartIndex, cycle);
        }

        private static int GetResourceValue(char[][] map)
        {
            var woodTiles = map.Sum(line => line.Count(c => c == '|'));
            var lumberyardTiles = map.Sum(line => line.Count(c => c == '#'));
            return woodTiles * lumberyardTiles;
        }

        private static char[][] GetNewMap(char[][] map)
        {
            return map
                .AsParallel()
                .Select((line, r) => line.Select((tile, c) => CalculateNewTile(tile, GetSurroundingTiles(map, r, c))).ToArray())
                .ToArray();            
            
            // equivalent to:
            //var newMap = map.Select(line => line.ToArray()).ToArray();
            //for (int r = 0; r < newMap.Length; r++)
            //    for (int c = 0; c < newMap[r].Length; c++)
            //        newMap[r][c] = CalculateNewTile(map[r][c], GetSurroundingTiles(map, r, c));
        }

        private static IEnumerable<char> GetSurroundingTiles(char[][] map, int r, int c)
        {
            for (int rr = r - 1; rr <= r + 1; rr++)
                for (int cc = c - 1; cc <= c + 1; cc++)
                    if (rr >= 0 && rr < map.Length && cc >= 0 && cc < map[rr].Length)
                        if (rr != r || cc != c)
                            yield return map[rr][cc];
        }

        private static char CalculateNewTile(char tile, IEnumerable<char> surroundings) => tile switch
        {
            '.' => surroundings.Count(t => t == '|') >= 3 ? '|' : '.',
            '|' => surroundings.Count(t => t == '#') >= 3 ? '#' : '|',
            '#' => surroundings.Any(t => t == '#') && surroundings.Any(t => t == '|') ? '#' : '.',
            _ => throw new NotImplementedException($"Unrecognized tile: {tile}"),
        };
    }
}