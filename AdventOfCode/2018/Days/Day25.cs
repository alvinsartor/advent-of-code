﻿using AdventOfCode.helpers;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/25

    class Day25: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 50;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 0;

        /// <summary>
        /// 
        /// </summary>
        public override string Part1()
        {
            var points = input.Select(PointFromString).ToList();
            var clusters = ClusterByDistance(points);
            return clusters.Count.ToString();
        }

        private static Point4D PointFromString(string line) => Point4D.FromArray(line.Split(",").Select(double.Parse).ToArray());

        private static HashSet<HashSet<Point4D>> ClusterByDistance(List<Point4D> points)
        {
            HashSet<HashSet<Point4D>> clusters = new();
            foreach (var pt in points) AddPointToClusters(clusters, pt);
            return clusters;
        }

        private static void AddPointToClusters(HashSet<HashSet<Point4D>> clusters, Point4D point)
        {
            var pointClusters = clusters.Where(cluster => cluster.Any(p => p.ManhattanDistance(point) <= 3)).ToList();
            if (pointClusters.Count == 0)
            {
                clusters.Add(new HashSet<Point4D> { point });
            }
            else if (pointClusters.Count == 1)
            {
                pointClusters[0].Add(point);
            }
            else
            {
                pointClusters.ForEach(cluster => clusters.Remove(cluster));
                clusters.Add(pointClusters.SelectMany(p => p).Append(point).ToHashSet());
            }
        }

        /// <summary> Not needed. The star is given if all the other challenges are solved. </summary>
        public override string Part2() => "None";
    }
}
