﻿using AdventOfCode._2018.Days.TimeTravelingDevice;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/16

    class Day16: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 200;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 200;

        /// <summary>
        /// Try to assign each instruction by using reverse engineered operations.
        /// </summary>
        public override string Part1()
        {
            var compu = new TimeTravelingDevice_V1();
            var reverseEngineeredInstructions = DecodeInput(input).reverseEngineeredInstructions;
            var matchingInstructions = reverseEngineeredInstructions.Select(instruction => 
                (instruction, matchingCount: instruction.GetPossibleMatchingInstructions(compu).ToList().Count));
            
            return matchingInstructions.Count(instruction => instruction.matchingCount >= 3).ToString();
        }

        /// <summary>
        /// Assign each code to the relative instruction and execute the program.
        /// </summary>
        public override string Part2()
        {
            var compu = new TimeTravelingDevice_V1();
            (List<ReverseEngineeredInstruction> reverseEngineeredInstructions, List<int[]> instructions) = DecodeInput(input);
            var matchingInstructions = reverseEngineeredInstructions.Select(instruction =>
                (instruction.OperationCode, possibleOperations: instruction.GetPossibleMatchingInstructions(compu).ToHashSet()))
                .ToList();

            var operationsByCode = OperationsByCode(matchingInstructions);
            compu.ExecuteOperationsList(new int[] { 0, 0, 0, 0 }, instructions, operationsByCode);

            return compu.register[0].ToString();
        }

        private static Dictionary<int, string> OperationsByCode(List<(int opCode, HashSet<string> possibleOperations)> reverseEngineeringOutcome)
        {
            Dictionary<int, string> result = new();
            while (reverseEngineeringOutcome.Count > 0)
            {
                var (opCode, possibleOperations) = reverseEngineeringOutcome.First(x => x.possibleOperations.Count == 1);
                var certainOperation = possibleOperations.First();
                result.Add(opCode, certainOperation);

                reverseEngineeringOutcome = reverseEngineeringOutcome.Where(x => x.opCode != opCode).ToList();
                reverseEngineeringOutcome.ForEach(x => x.possibleOperations.Remove(certainOperation));
            }

            return result;
        }

        private static (List<ReverseEngineeredInstruction> reverseEngineeredInstructions, List<int[]> instructions) 
            DecodeInput(string[] input)
        {
            List<ReverseEngineeredInstruction> reverseEngineeredInstructions = new();
            int cursor = 0;
            while(!string.IsNullOrEmpty(input[cursor]))
            {
                var endOfInstructionSet = cursor + 3;
                reverseEngineeredInstructions.Add(ReverseEngineeredInstruction.FromString(input[cursor..endOfInstructionSet]));
                cursor += 4;
            }

            var restOfInput = input[cursor..].Where(line => !string.IsNullOrEmpty(line));
            var instructions = restOfInput.Select(line => line.Split(" ").Select(int.Parse).ToArray()).ToList();

            return (reverseEngineeredInstructions, instructions);
        }

        private record ReverseEngineeredInstruction(int[] Before, int[] After, int[] Operation, int OperationCode) { 
            public static ReverseEngineeredInstruction FromString(string[] instructionSet)
            {
                var before = instructionSet[0][9..^1].Split(", ").Select(int.Parse).ToArray();
                var after = instructionSet[2][9..^1].Split(", ").Select(int.Parse).ToArray();
                var operation = instructionSet[1].Split(" ").Select(int.Parse).ToArray();
                if (operation[0] > 16) throw new Exception("Suspicious operation code!");
                return new ReverseEngineeredInstruction(before, after, operation[1..], operation[0]);
            }  
            
            public IEnumerable<string> GetPossibleMatchingInstructions(TimeTravelingDevice_V1 compu)
            {
                var encodedAfter = string.Join(", ", After);
                foreach(var command in compu.SupportedOperations)
                {
                    compu.Initialize(Before);
                    command.Value(Operation); 
                    var encodedRegisters = string.Join(", ", compu.register);
                    if (encodedRegisters == encodedAfter) yield return command.Key;
                }
            }
        }
    }
}
