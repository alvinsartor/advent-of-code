﻿using AdventOfCode._2018.Days.TimeTravelingDevice;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/19

    class Day19: DailyChallenge
    {
        /// <summary>
        /// Implement flow control in the TimeTravelingDevice and execute the given program.
        /// </summary>
        public override string Part1()
        {
            //return ExecuteTheProgramAsAsked(input, new long[6], true);         // works! But slow (250ms)
            return Divisors(867).Sum().ToString();                              // much faster! (0.001ms) see notes for translation
        }

        /// <summary>
        /// Same as before, but this time initialize register 0 to 1.
        /// </summary>
        public override string Part2()
        {
            // return ExecuteTheProgramAsAsked(input, new long[] { 1, 0, 0, 0, 0, 0 });      // works! But WAY TOO slow (a year?)
            return Divisors(10551267).Sum().ToString();                                     // much faster! (<0.1ms) see notes for translation
        }

        private static string ExecuteTheProgramAsAsked(string[] input, long[] registers, bool debugMode = false)
        {
            (int registryNumber, Instruction[] instructions) = DecodeInput(input);
            var timeTravelingDevice = new TimeTravelingDevice_V2();
            timeTravelingDevice.ExecuteProgram(instructions, registryNumber, registers, debugMode);
            return timeTravelingDevice.register[0].ToString();
        }

        private static (int registryNumber, Instruction[] instructions) DecodeInput(string[] input)
        {
            int registryNumber = int.Parse(input[0][4..]);
            var instructions = input[1..].Select(Instruction.FromString).ToArray();
            return (registryNumber, instructions);
        }

        private static IEnumerable<long> Divisors(long n)
        {
            for(int i = 1; i < Math.Sqrt(n); i++)
                if (n % i == 0)
                {
                    yield return i;
                    if (n / i != i) yield return n / i;
                }
        }
    }
}
