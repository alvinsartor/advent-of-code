﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/1

    class Day01: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 1000;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 100;

        /// <summary>
        /// Apply a series of operations to stabilize the frequency.
        /// </summary>
        public override string Part1() => input.Select(int.Parse).Sum().ToString();

        /// <summary>
        /// Keep performing the operations until you find the first frequency that repeats itself.
        /// </summary>
        public override string Part2()
        {
            var reachedFrequencies = new HashSet<int>();
            var frequencyChanges = input.Select(int.Parse).ToArray();
            var cursor = 0;
            var currentFrequency = 0;

            do
            {
                currentFrequency += frequencyChanges[cursor];
                cursor = (cursor + 1) % frequencyChanges.Length;
                if (reachedFrequencies.Contains(currentFrequency)) return currentFrequency.ToString();
                reachedFrequencies.Add(currentFrequency);
            } while (true);
        }
    }
}
