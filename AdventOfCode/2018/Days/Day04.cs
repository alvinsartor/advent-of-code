﻿using AdventOfCode.helpers;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/4

    class Day04 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 250;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 250;

        /// <summary>
        /// Find the guard that slept the most and the minute he slept more often.
        /// </summary>
        public override string Part1()
        {
            var log = input.OrderBy(x => x).ToArray();
            var sleepingIntervals = ExtractSleepingIntervals(log);

            var guardThatSleptTheMost = sleepingIntervals.MaxBy(pair => pair.Value.Sum(tuple => tuple.amount)).Key;
            var sleepingDistribution = CalculateSleepingDistribution(sleepingIntervals[guardThatSleptTheMost]);
            var minuteWithHigestSleepFrequency = sleepingDistribution.Select((min, i) => (min, i)).MaxBy(t => t.min).i;

            return (int.Parse(guardThatSleptTheMost) * minuteWithHigestSleepFrequency).ToString();
        }

        /// <summary>
        /// Find the guard that slept the most on a single minute.
        /// </summary>
        public override string Part2()
        {
            var log = input.OrderBy(x => x).ToArray();
            var sleepingIntervals = ExtractSleepingIntervals(log);
            var sleepingDistributions = sleepingIntervals.Select(x => (id: x.Key, distribution: CalculateSleepingDistribution(x.Value)));
            var guardThatSleptMostOnAMinute = sleepingDistributions
                .Select(x =>
                {
                    var id = x.id;
                    var minutesAndAmount = x.distribution.Select((amount, i) => (minute: i, amount));
                    var minuteWithMaxAmount = minutesAndAmount.MaxBy(tuple => tuple.amount);
                    return (id, minuteWithMaxAmount.minute, minuteWithMaxAmount.amount);
                })
                .MaxBy(x => x.amount);

            return (int.Parse(guardThatSleptMostOnAMinute.id) * guardThatSleptMostOnAMinute.minute).ToString();
        }

        private static Dictionary<string, List<(int start, int end, int amount)>> ExtractSleepingIntervals(string[] log)
        {
            var guardsIdsAndIndexes = log
                .Select((line, index) => (line, index))
                .Where(pair => pair.line.Contains("Guard"))
                .Select(pair => (id: pair.line.Split(" ")[3][1..], index: pair.index))
                .ToArray();

            Dictionary<string, List<(int start, int end, int amount)>> sleepingIntervals = new();
            for (int i = 0; i < guardsIdsAndIndexes.Length; i++)
            {
                var (id, index) = guardsIdsAndIndexes[i];
                if (!sleepingIntervals.ContainsKey(id)) sleepingIntervals.Add(id, new List<(int, int, int)>());

                var shiftsList = sleepingIntervals[id];
                var shiftStartIndex = index + 1;
                var shiftEndIndex = i < guardsIdsAndIndexes.Length - 1 ? guardsIdsAndIndexes[i + 1].index : log.Length;
                var shiftEntries = log[shiftStartIndex..shiftEndIndex];
                for (int j = 0; j < shiftEntries.Length; j += 2)
                {
                    var sleepingStart = ExtractMinutesFromShift(shiftEntries[j]);
                    var sleepingEnd = ExtractMinutesFromShift(shiftEntries[j + 1]);
                    shiftsList.Add((sleepingStart, sleepingEnd, sleepingEnd - sleepingStart));
                }
            }

            return sleepingIntervals;
        }

        private static int ExtractMinutesFromShift(string shiftLine) => int.Parse(shiftLine[15..17]);

        private static int[] CalculateSleepingDistribution(List<(int start, int end, int amount)> sleepingShifts)
        {
            var minutesInWhichGuardWasAsleep = new int[60];

            foreach (var (start, end, _) in sleepingShifts)
                for (int i = start; i < end; i++)
                    minutesInWhichGuardWasAsleep[i]++;

            return minutesInWhichGuardWasAsleep;
        }
    }
}
