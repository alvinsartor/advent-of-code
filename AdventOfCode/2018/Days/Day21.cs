﻿using System;
using System.Collections.Generic;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/21


    class Day21: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 100;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 100;

        /// <summary>
        /// Crack the traveling machine code and find the value of reg[0] that yields the MIN number of instructions.
        /// </summary>
        public override string Part1() => FindNumberThatGrantsMinimumNumberOfOperations().ToString();

        /// <summary>
        /// Crack the traveling machine code and find the value of reg[0] that yields the MAX number of instructions (no endless loop).
        /// </summary>
        public override string Part2() => FindNumberThatGrantsMaximumNumberOfOperations().ToString();

        /// <summary>
        /// To get the lowest number of instructions, we just have to set A 
        /// to the value D contains the first time the A==D check is performed.
        /// </summary>
        private static long FindNumberThatGrantsMinimumNumberOfOperations()
        {
            long F = 65536;
            long D = 5557974;            
            while (true)
            {
                D += F & 255;
                D &= 16777215;
                D *= 65899;
                D &= 16777215;
                if (F < 256) return D;
                F = (long)Math.Floor(F / 256.0);
            }
        }

        /// <summary>
        /// To get the highest number of instructions, we just have to set A 
        /// to the value D contains the first time the A==D check is performed.
        /// </summary>
        private static long FindNumberThatGrantsMaximumNumberOfOperations()
        {
            HashSet<long> DValues = new();
            long previousD = 0;

            long D = 0;
            long F = 0;

        line_5:
            F = D | 65536;
            D = 5557974;
        line_7:
            D += F & 255;
            D &= 16777215;
            D *= 65899;
            D &= 16777215;

            if (256 > F)
            {
                if (DValues.Contains(D)) return previousD;
                DValues.Add(D);
                previousD = D;                
                goto line_5;
            }
            else
            {
                F = (long)Math.Floor(F / 256.0);
                goto line_7;
            }
        }
    }
}
