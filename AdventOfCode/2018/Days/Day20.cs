﻿using AdventOfCode.helpers;
using Graph;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/20

    class Day20: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 50;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 50;

        /// <summary>
        /// Build the map using the given regex string and find the distance of the farthest room
        /// </summary>
        public override string Part1() => 
            GetDistances(BuildMapFromInput(input[0][1..^1]))
            .Values
            .Max()
            .ToString();

        /// <summary>
        /// Count the number of rooms
        /// </summary>
        public override string Part2() =>
            GetDistances(BuildMapFromInput(input[0][1..^1]))
            .Values
            .Count(d => d >= 1000)
            .ToString();


        private static Graph<Point> BuildMapFromInput(string regex)
        {
            Graph<Point> map = new();
            map.Add(Point.Origin);

            var currentPosition = map[Point.Origin];
            RecursiveExploration(currentPosition, map, regex, 0);
            return map;
        }

        private static (Node<Point> current, int index) RecursiveExploration(Node<Point> current, Graph<Point> map, string regex, int index)
        {
            var initialNode = current;
            while (index < regex.Length)
            {
                var currentSymbol = regex[index];
                if (char.IsUpper(currentSymbol)) (current, index) = AddBlock(current, map, regex, index);
                else if (currentSymbol == '(') (current, index) = RecursiveExploration(current, map, regex, index+1);
                else if (currentSymbol == ')') return (initialNode, index + 1);
                else if (currentSymbol == '|' && regex[index+1] == ')') return (current, index + 2);
                else if (currentSymbol == '|') (current, index) = (initialNode, index + 1);
            }

            return (current, index);
        }

        private static (Node<Point> current, int index) AddBlock(Node<Point> startingPoint, Graph<Point> map, string regex, int index)
        {
            while (index < regex.Length && char.IsUpper(regex[index]))
            {
                startingPoint = TryAddRoomToMap(regex[index], startingPoint, map);
                index++;
            }            
            return (startingPoint, index);
        }

        private static Node<Point> TryAddRoomToMap(char direction, Node<Point> currentNode, Graph<Point> map)
        {
            var nextPosition = CharDirectionToPosition(direction, currentNode.Value);
            var nextPositionNode = map.TryAdd(nextPosition);
            nextPositionNode.TryAddBidirectionalConnection(currentNode);
            return nextPositionNode;
        }

        private static Point CharDirectionToPosition(char dir, Point current) => current + CharDirectionToPointDirection(dir);
        private static Point CharDirectionToPointDirection(char dir) => dir switch
        {
            'N' => Point.Up,
            'E' => Point.Right,
            'S' => Point.Down,
            'W' => Point.Left,
            _ => throw new NotImplementedException($"Unrecognized direction '{dir}'"),
        };

        private static Dictionary<Point, int> GetDistances(Graph<Point> map)
        {
            Dictionary<Point, int> discoveredNodes = new();
            List<Point> fronteer = new() { Point.Origin };
            int currentDistance = 0;

            while(fronteer.Count > 0)
            {
                fronteer.ForEach(position => discoveredNodes.Add(position, currentDistance));                
                fronteer = fronteer
                    .SelectMany(position => map[position].CurrentNeighbors.Where(neighbor => !discoveredNodes.ContainsKey(neighbor.Value)))
                    .Select(n => n.Value)
                    .ToList();

                currentDistance++;
            }

            return discoveredNodes;
        }
    }
}
