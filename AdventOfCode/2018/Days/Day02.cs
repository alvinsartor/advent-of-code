﻿using System;
using System.Linq;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/2

    class Day02: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 500;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 500;

        /// <summary>
        /// Scan the IDs and calculate the checksum using the given algorithm.
        /// </summary>
        public override string Part1()
        {
            bool anyLetterRepitesTwice(string line) => line.Any(letter => line.Count(x => x == letter) == 2);
            bool anyLetterRepitesThree(string line) => line.Any(letter => line.Count(x => x == letter) == 3);
            return (input.Count(anyLetterRepitesTwice) * input.Count(anyLetterRepitesThree)).ToString();
        }

        /// <summary>
        /// Find the boxes whose IDs only differ by 1 char.
        /// </summary>
        public override string Part2()
        {
            (string word1, string word2) = FindSimilarIds(input);
            var intersection = new string(word1.Where((x, i) => word2[i] == x).ToArray());            
            return intersection;
        }

        private static (string, string) FindSimilarIds(string[] input)
        {
            for (int i = 0; i < input.Length - 1; i++)
                for (int j = i + 1; j < input.Length; j++)
                    if (DiffIsAtMost1Letter(input[i], input[j]))
                        return (input[i], input[j]);
            throw new Exception("No similar IDs were found");
        }

        private static bool DiffIsAtMost1Letter(string s1, string s2)
        {
            var tolerance = 1;
            var index = -1;
            while (++index < s1.Length && tolerance >= 0)
                if (s1[index] != s2[index])
                    tolerance--;

            return tolerance >= 0;
        }
    }
}
