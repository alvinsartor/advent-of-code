﻿using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/23

    class Day23 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 100;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 100;

        private static readonly List<Point3D> DiagonalsFromTopLeft = new List<Point3D>() {
                new Point3D(0,0,0), new Point3D(0,0,1), new Point3D(0,1,0), new Point3D(0,1,1),
                new Point3D(1,0,0), new Point3D(1,0,1), new Point3D(1,1,0), new Point3D(1,1,1),
            };

        /// <summary>
        /// Find the nanobot that has the greatest number of other bots in its range.
        /// </summary>
        public override string Part1()
        {
            var bots = DecodeInput(input).ToList();
            var botWithGreaterRange = bots.MaxBy(b => b.Range);
            var botsInRange = bots.Count(bot => bot.Position.ManhattanDistance(botWithGreaterRange.Position) < botWithGreaterRange.Range);

            return botsInRange.ToString();
        }

        /// <summary>
        /// Find the position that is in range of the highest amount of nanobots.
        /// </summary>
        public override string Part2()
        {
            var bots = DecodeInput(input).ToList();
            var mostCoveredPoint = MostCoveredPoint(bots);
            return mostCoveredPoint.ManhattanDistanceFromOrigin.ToString();
        }


        private static IEnumerable<Bot> DecodeInput(string[] input)
        {
            foreach (var line in input)
            {
                var startIndex = 5;
                var endIndex = line.IndexOf('>');
                var rangeIndex = endIndex + 5;
                var positionSplit = line[startIndex..endIndex].Split(",").Select(double.Parse).ToArray();
                var radius = int.Parse(line[rangeIndex..]);

                var position = new Point3D(positionSplit[0], positionSplit[1], positionSplit[2]);
                yield return new Bot(position, radius);
            }
        }

        private static Point3D MostCoveredPoint(List<Bot> bots)
        {
            (Point3D topLeftCorner, long side) = GetInitialCoordinates(bots);
            var initialCube = new Cube(topLeftCorner, side, bots);
            var sortedcubes = new SortedSet<Cube>() { initialCube };

            while (sortedcubes.Min!.side > 1)
            {
                Cube bestCube = sortedcubes.Min!;
                sortedcubes.Remove(bestCube);
                bestCube.SplitInSubcubes(bots).Where(c => c.coveredBots > 0).ForEach(c => sortedcubes.Add(c));
            }

            return sortedcubes.Min!.topLeftCorner;
        }

        private static (Point3D topLeftCorner, long side) GetInitialCoordinates(List<Bot> bots)
        {
            var minX = bots.Min(b => b.Position.X);
            var minY = bots.Min(b => b.Position.Y);
            var minZ = bots.Min(b => b.Position.Z);
            var maxX = bots.Max(b => b.Position.X);
            var maxY = bots.Max(b => b.Position.Y);
            var maxZ = bots.Max(b => b.Position.Z);
            var topLeftCorner = new Point3D(minX, minY, minZ);
            var longestDelta = (long)Math.Max(maxX - minX, Math.Max(maxY - minY, maxZ - minZ));
            return (topLeftCorner, longestDelta);
        }
        
        private record Bot(Point3D Position, int Range)
        {
            public bool Covers(Point3D point) => Position.ManhattanDistance(point) <= Range;
        }

        private class Cube : IComparable<Cube>
        {
            public readonly Point3D topLeftCorner;
            public readonly long side;
            public readonly long halfSide;
            public readonly int coveredBots;
            public readonly double distance;

            public Cube(Point3D topLeftCorner, long side, List<Bot> bots)
            {
                this.topLeftCorner = topLeftCorner;
                this.side = side;
                halfSide = (long)Math.Ceiling(side / 2.0);
                distance = topLeftCorner.ManhattanDistanceFromOrigin;
                coveredBots = CoveredBots(bots);
            }

            public IEnumerable<Cube> SplitInSubcubes(List<Bot> bots) => 
                DiagonalsFromTopLeft.Select(d => new Cube((d * halfSide) + topLeftCorner, halfSide, bots));

            private int CoveredBots(List<Bot> bots)
            {
                (double minX, double maxX) = (topLeftCorner.X, topLeftCorner.X + side - 1);
                (double minY, double maxY) = (topLeftCorner.Y, topLeftCorner.Y + side - 1);
                (double minZ, double maxZ) = (topLeftCorner.Z, topLeftCorner.Z + side - 1);
                return bots.Count(bot =>
                {
                    double distance = 0;
                    distance += GetDistanceFromBox(bot.Position.X, minX, maxX);
                    distance += GetDistanceFromBox(bot.Position.Y, minY, maxY);
                    distance += GetDistanceFromBox(bot.Position.Z, minZ, maxZ);
                    return distance <= bot.Range;
                });
            }

            static double GetDistanceFromBox(double value, double lowerBound, double upperBound)
            {
                if (value < lowerBound) return lowerBound - value;
                if (value > upperBound) return value - upperBound;
                return 0;
            }

            public int CompareTo(Cube? other)
            {
                if (other == null) return -1;
                if (coveredBots != other.coveredBots) return coveredBots > other.coveredBots ? -1 : 1;
                if (distance != other.distance) return distance < other.distance ? -1 : 1;
                if (side != other.side) return side < other.side ? -1 : 1;
                return 0;
            }

            public override bool Equals(object? obj) => obj is Cube cube && topLeftCorner.Equals(cube.topLeftCorner) && side == cube.side;
            public override int GetHashCode() => HashCode.Combine(topLeftCorner, side);
            public override string ToString() => $"c:{topLeftCorner}; s:{side}, covered:{coveredBots}";
        }
    }
}
