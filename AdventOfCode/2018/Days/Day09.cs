﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/9

    class Day09: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 500;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 1;

        /// <summary>
        /// Play the marble game of the elves and find the winner's score.
        /// </summary>
        public override string Part1()
        {
            (int nrPlayers, int lastMarble) = DecodeInput(input[0]);
            return PlayMarbleGame(nrPlayers, lastMarble).ToString();
        }

        /// <summary>
        /// Find the score if the game was 100x longer.
        /// </summary>
        public override string Part2()
        {
            (int nrPlayers, int lastMarble) = DecodeInput(input[0]);
            return PlayMarbleGame(nrPlayers, lastMarble * 100).ToString();
        }

        private static (int nrPlayers, int lastMarble) DecodeInput(string input)
        {
            var split = input.Split(" ");
            return (int.Parse(split[0]), int.Parse(split[6]));
        }

        private static long PlayMarbleGame(int nrPlayers, int lastMarble)
        {
            var scores = new long[nrPlayers];
            LinkedList<int> marbleCircle = new LinkedList<int>();
            marbleCircle.AddLast(0);
            var currentMarble = marbleCircle.First!;

            for (int i = 1; i <= lastMarble; i++)
            {
                if (i % 23 == 0)
                {
                    var player = (i - 1) % nrPlayers;
                    var marbleToRemove = currentMarble;
                    var nrOfMarblesToGoBackTo = 7;
                    while (nrOfMarblesToGoBackTo-- > 0) marbleToRemove = MoveCounterclockwise(marbleToRemove);

                    scores[player] += i;
                    scores[player] += marbleToRemove.Value;

                    currentMarble = MoveClockwise(marbleToRemove);
                    marbleCircle.Remove(marbleToRemove);
                }
                else
                {
                    var next = MoveClockwise(currentMarble);
                    currentMarble = marbleCircle.AddAfter(next, i);
                }
            }

            return scores.Max();
        }

        private static LinkedListNode<int> MoveClockwise(LinkedListNode<int> node) => node.Next ?? node.List!.First!;
        private static LinkedListNode<int> MoveCounterclockwise(LinkedListNode<int> node) => node.Previous ?? node.List!.Last!;
    }
}
