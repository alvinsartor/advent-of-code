﻿using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/3

    class Day03 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 100;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 100;

        /// <summary>
        /// Find the area representing the intersections of the given rectangles.
        /// </summary>
        public override string Part1()
        {
            var fabric = new char[1000, 1000];
            input.ForEach(line => MarkFabric(fabric, line));

            int intersectionsCount = 0;
            for (int i = 0; i < 1000; i++) for (int j = 0; j < 1000; j++) if (fabric[i, j] == 'X') intersectionsCount++;
            return intersectionsCount.ToString();
        }

        private static void MarkFabric(char[,] fabric, string line)
        {
            var split = line.Split(' ');
            var pos = split[2][..^1].Split(',').Select(int.Parse).ToArray();
            var size = split[3].Split('x').Select(int.Parse).ToArray();

            for (int c = pos[0]; c < pos[0] + size[0]; c++)
                for (int r = pos[1]; r < pos[1] + size[1]; r++)
                    fabric[r, c] = fabric[r, c] == 0 ? '#' : 'X';
        }

        /// <summary>
        /// Find the rectangle that does not overlap with anyone else.
        /// </summary>
        public override string Part2()
        {
            var rectangles = input.Select(ExtractRectangle).ToArray();
            var toBeDiscarded = new HashSet<int>();

            for(int i = 0; i < rectangles.Length; i++)
            {
                if (toBeDiscarded.Contains(i)) continue;

                var rectangle = rectangles[i];
                bool intersects = false;
                for(var j = rectangles.Length - 1; j >= 0; j--)
                    if (i != j && Rectangle.Intersect(rectangle, rectangles[j]))
                    {
                        toBeDiscarded.Add(j);
                        intersects = true;
                        break;
                    }

                if (!intersects) return rectangle.Id;
            }

            throw new Exception("It seems that all rectangles intersect.");
        }

        private static Rectangle ExtractRectangle(string line)
        {
            var split = line.Split(' ');
            var id = split[0][1..];
            var pos = split[2][..^1].Split(',').Select(int.Parse).ToArray();
            var size = split[3].Split('x').Select(int.Parse).ToArray();
            return new Rectangle(id, pos[0], pos[1], size[0], size[1]);
        }

        private record Rectangle(string Id, int X, int Y, int Width, int Height)
        {
            public static bool Intersect(Rectangle r1, Rectangle r2) => 
                !(r1.X > r2.X + r2.Width || r2.X > r1.X + r1.Width) && 
                !(r1.Y > r2.Y + r2.Height || r2.Y > r1.Y + r1.Height);            
        }
    }
}
