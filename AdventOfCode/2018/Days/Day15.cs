﻿using AdventOfCode.helpers;
using Graph;
using Graph.PathFinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/15

    class Day15: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 1;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 1;

        /// <summary>
        /// Spectate the war between elves and goblins. Who will win?
        /// </summary>
        public override string Part1()
        {
            var environment = Environment.BuildFromInput(input);
            environment.WageWar(false); // set to true to print the screen after each turn
            return environment.Score.ToString();
        }

        /// <summary>
        /// Give the elves some mighty weapons and see them slaughter the goblins! 
        /// Find the minimum HitPoints the elves need to suffer no losses.
        /// </summary>
        public override string Part2()
        {
            const int minimumHitPointsToGiveToElvesToSufferNoLosses = 23; // found by gradually incr. until no losses are registered.

            var environment = Environment.BuildFromInput(input, minimumHitPointsToGiveToElvesToSufferNoLosses);
            var numberOfElves = environment.elves.Count;
            environment.WageWar(false); // set to true to print the screen after each turn
            bool hadLosses = environment.elves.Count != numberOfElves;
            
            return !hadLosses 
                ? $"YAY! Score: {environment.Score}" 
                : $":( {numberOfElves - environment.elves.Count} losses";
        }

        private class Environment
        {
            private readonly char[][] originalMap;
            public readonly Graph<Point> map;
            public readonly HashSet<Elf> elves;
            public readonly HashSet<Goblin> goblins;
            public readonly HashSet<Player> allPlayers;

            private int turn;
            public int Score => (elves.Sum(p => p.Health) + goblins.Sum(p => p.Health)) * turn;
            public bool IsWarOver => elves.Count == 0 || goblins.Count == 0;

            private Environment(char[][] originalMap, Graph<Point> map, HashSet<Elf> elves, HashSet<Goblin> goblins)
            {
                turn = 0;
                this.originalMap = originalMap;
                this.map = map;
                this.elves = elves;
                this.goblins = goblins;
                allPlayers = elves.Cast<Player>().Concat(goblins.Cast<Player>()).ToHashSet();
            }

            public static Environment BuildFromInput(string[] input, int attackForElves = 3)
            {
                Graph<Point> map = new();
                HashSet<Elf> elves = new();
                HashSet<Goblin> goblins = new();

                for (int r = 0; r < input.Length; r++)
                    for (int c = 0; c < input[r].Length; c++)
                        if (input[r][c] != '#')
                        {
                            var value = input[r][c];
                            var position = new Point(c, r);

                            map.Add(position);
                            if (map.Contains(position + Point.Up)) map[position].AddBidirectionalConnection(map[position + Point.Up]);
                            if (map.Contains(position + Point.Left)) map[position].AddBidirectionalConnection(map[position + Point.Left]);

                            if (value == 'E') elves.Add(new Elf(position, attackForElves));
                            else if (value == 'G') goblins.Add(new Goblin(position));
                        }

                var originalMap = input.Select(line => line.Replace('E', '.').Replace('G', '.').ToCharArray()).ToArray();
                return new Environment(originalMap, map, elves, goblins);
            }

            public void WageWar(bool printScreenAfterEachTurn = true)
            {
                while (!IsWarOver)
                {
                    var sortedPlayers = allPlayers.OrderBy(p => p.Position.Y).ThenBy(p => p.Position.X);
                    foreach (var player in sortedPlayers)
                    {
                        if (!player.IsAlive) continue;
                        player.MoveIfPossible(this);
                        player.AttackIfPossible(this);
                    }
                    if (!IsWarOver) turn++;
                    if (printScreenAfterEachTurn) Print();
                }                
            }

            public void RemoveDeadPlayer(Player player)
            {
                allPlayers.Remove(player);
                if (player.PlayerType == "Elf") elves.Remove((Elf)player);
                else goblins.Remove((Goblin)player);
            }

            public Task<Path<Point>> GetPathTo(Point start, Point end, HashSet<Point> unwalkableNodes) => 
                map.AStarSearch(start, end, (pt) => end.Distance(pt), unwalkableNodes);

            public void Print()
            {
                Console.WriteLine($"Turn: {turn}");
                var mapCopy = originalMap.Select(line => line.ToArray()).ToArray();
                foreach (var goblin in goblins) mapCopy[(int)goblin.Position.Y][(int)goblin.Position.X] = 'G';
                foreach (var elf in elves) mapCopy[(int)elf.Position.Y][(int)elf.Position.X] = 'E';
                mapCopy.ForEach(line => Console.WriteLine(new string(line)));
                Console.WriteLine();
                allPlayers.OrderBy(p => p.Position.Y).ThenBy(p => p.Position.X).ForEach(Console.WriteLine);
                Console.WriteLine();
            }
        }

        private abstract class Player
        {
            private static int ID = 0;
            private const int MaxNumberOfPathsSearched = 50;
            
            private readonly int id;
            private readonly int hitPoints;

            public abstract string PlayerType { get; }
            public Point Position { get; private set; }
            public int Health { get; private set; }
            public bool IsAlive => Health > 0;

            public Player(Point initialPosition, int attackPower = 3)
            {
                id = ID++;
                Position = initialPosition;
                Health = 200;
                hitPoints = attackPower;
            }

            public override bool Equals(object? obj) => obj is Player player && id == player.id;
            public override int GetHashCode() => HashCode.Combine(id);
            public override string ToString() => $"{PlayerType} {id}, Pos:{Position}, HP:{Health}";
            public abstract IEnumerable<Point> EnemiesPositions(Environment environment);
            private IEnumerable<Player> EnemiesList(IEnumerable<Player> allPlayers) => allPlayers.Where(p => p.PlayerType != PlayerType);
            private void HitPlayer(Player otherPlayer, Environment environment)
            {
                if (!otherPlayer.IsAlive) throw new Exception("You're trying to hit a dead body!");

                otherPlayer.Health -= hitPoints;
                if (!otherPlayer.IsAlive) environment.RemoveDeadPlayer(otherPlayer);
            }

            public void MoveIfPossible(Environment environment)
            {
                var enemiesPositions = EnemiesPositions(environment).SelectMany(p => p.Surroundings()).ToHashSet();
                if (enemiesPositions.Contains(Position)) return; // player already in position;

                var unwalkablePositions = environment.allPlayers.Select(p => p.Position).ToHashSet();
                var pathToClosestEnemy = enemiesPositions
                    .AsParallel()
                    .Where(pos => !unwalkablePositions.Contains(pos))
                    .Select(pos => environment.GetPathTo(Position, pos, unwalkablePositions).Result)
                    .Where(path => !path.IsEmpty)
                    .Select(path => (destination: path.Nodes[^1].Value, path))
                    .OrderBy(tuple => tuple.path.Cost)
                    .ThenBy(tuple => tuple.destination.Y)
                    .ThenBy(tuple => tuple.destination.X)
                    .ToList();

                if (pathToClosestEnemy.Count == 0) return;

                // now, I need to recalculate the paths as there might be multiple movements that bring the player
                // to the destination and we then should choose the first one in reading order.

                var destination = pathToClosestEnemy[0].destination;
                var surroundings = Position.Surroundings().ToHashSet();
                if (surroundings.Contains(destination))
                {
                    Position = destination;
                    return;
                }
                
                var closestSurrounding = surroundings
                    .AsParallel()
                    .Where(pt => environment.map.Contains(pt) && !unwalkablePositions.Contains(pt))
                    .Select(pt => (destination: pt, path: environment.GetPathTo(pt, destination, unwalkablePositions).Result))
                    .Where(tuple => !tuple.path.IsEmpty)
                    .OrderBy(tuple => tuple.path.Cost)
                    .ThenBy(tuple => tuple.destination.Y)
                    .ThenBy(tuple => tuple.destination.X);

                Position = closestSurrounding.First().destination;
            }

            public void AttackIfPossible(Environment environment)
            {
                var surroundings = Position.Surroundings().ToHashSet();
                var closestEnemy = EnemiesList(environment.allPlayers)
                    .Where(e => surroundings.Contains(e.Position))
                    .OrderBy(e => e.Health)
                    .ThenBy(e => e.Position.Y)
                    .ThenBy(e => e.Position.X)
                    .FirstOrDefault();

                if (closestEnemy != null) HitPlayer(closestEnemy, environment);
            }
        }

        private class Elf : Player
        {
            public override string PlayerType => "Elf";
            public Elf(Point initialPosition, int attackPower = 3) : base(initialPosition, attackPower) { }
            public override IEnumerable<Point> EnemiesPositions(Environment environment) => environment.goblins.Select(p => p.Position);
        }

        private class Goblin : Player
        {
            public override string PlayerType => "Goblin";
            public Goblin(Point initialPosition) : base(initialPosition) { }
            public override IEnumerable<Point> EnemiesPositions(Environment environment) => environment.elves.Select(p => p.Position);
        }
    }
}
