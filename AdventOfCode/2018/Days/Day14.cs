﻿using AdventOfCode.helpers;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/14

    class Day14: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 50;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 5;

        /// <summary>
        /// Combine recipes to obtain the perfect hot choco! Return the last 10 recipes after iterating N times. 
        /// </summary>
        public override string Part1()
        {
            List<int> recipes = new List<int> { 3, 7 };
            int elf1 = 0;
            int elf2 = 1;
            var target = 10 + int.Parse(input[0]);

            while (recipes.Count < target)
            {
                (recipes[elf1] + recipes[elf2])
                    .ToString().ToCharArray()
                    .ForEach(c => recipes.Add(int.Parse(c.ToString())));

                elf1 = (elf1 + recipes[elf1] + 1) % recipes.Count;
                elf2 = (elf2 + recipes[elf2] + 1) % recipes.Count;              
            }

            var last10 = recipes.Skip(recipes.Count - 10).ToArray();
            return string.Join("", last10);
        }

        /// <summary>
        /// Now the other way around. Return the number of iterations before N appears in the recipes.
        /// </summary>
        public override string Part2()
        {
            List<int> recipes = new List<int> { 3, 7 };
            int elf1 = 0;
            int elf2 = 1;

            string target = input[0];
            int targetLength = target.Length;
            int looseTargetLength = targetLength + 1;
            string latestAdded = "XXXXXX37";

            while (true)
            {
                var newRecipe = (recipes[elf1] + recipes[elf2]).ToString();
                newRecipe.ToCharArray().ForEach(c => recipes.Add(int.Parse(c.ToString())));

                elf1 = (elf1 + recipes[elf1] + 1) % recipes.Count;
                elf2 = (elf2 + recipes[elf2] + 1) % recipes.Count;

                latestAdded = (latestAdded + newRecipe)[^looseTargetLength..];

                if (latestAdded[1..] == target) return (recipes.Count - targetLength).ToString();
                if (latestAdded[..^1] == target) return (recipes.Count - targetLength - 1).ToString();
            }
        }
    }
}