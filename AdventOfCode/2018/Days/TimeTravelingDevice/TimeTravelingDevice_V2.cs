using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2018.Days.TimeTravelingDevice
{
    public sealed class TimeTravelingDevice_V2
    {
        private const int RegisterSize = 6;
        public static Dictionary<string, Action<long[], long[]>> SupportedOperations = new()
        {
            { "addr", Addr}, { "addi", Addi}, { "mulr", Mulr}, { "muli", Muli}, { "banr", Banr}, { "bani", Bani},
            { "borr", Borr}, { "bori", Bori}, { "setr", Setr}, { "seti", Seti}, { "gtir", Gtir}, { "gtri", Gtri},
            { "gtrr", Gtrr}, { "eqir", Eqir}, { "eqri", Eqri}, { "eqrr", Eqrr},
        };

        public long[] register;
        private int pointerRegistry; 
        public TimeTravelingDevice_V2() => register = new long[6];
        
        public void ExecuteProgram(
            Instruction[] instructions,
            int instructionPointerRegistry,
            long[]? initialRegistryValue = null,
            bool debugMode = false)
        {
            pointerRegistry = instructionPointerRegistry;
            register = initialRegistryValue?.ToArray() ?? new long[RegisterSize];
            
            while(register[pointerRegistry] < instructions.Length)
            {
                var currentInstruction = register[pointerRegistry];
                var instruction = instructions[currentInstruction];
                var function = SupportedOperations[instruction.OperationName];
                function(instruction.Values, register);
                register[pointerRegistry]++;

                if (debugMode) Console.WriteLine($"{instruction}   -   {CurrentRegisters()}"); 
            }
        }

        private string CurrentRegisters() => 
            $"A:{register[0]}, B:{register[1]}, C:{register[2]}, D:{register[3]}, E:{register[4]}, F:{register[5]}";

        private static void Addr(long[] instr, long[] reg) => reg[instr[2]] = reg[instr[0]] + reg[instr[1]];
        private static void Addi(long[] instr, long[] reg) => reg[instr[2]] = reg[instr[0]] + instr[1];
        private static void Mulr(long[] instr, long[] reg) => reg[instr[2]] = reg[instr[0]] * reg[instr[1]];
        private static void Muli(long[] instr, long[] reg) => reg[instr[2]] = reg[instr[0]] * instr[1];
        private static void Banr(long[] instr, long[] reg) => reg[instr[2]] = reg[instr[0]] & reg[instr[1]];
        private static void Bani(long[] instr, long[] reg) => reg[instr[2]] = reg[instr[0]] & instr[1];
        private static void Borr(long[] instr, long[] reg) => reg[instr[2]] = reg[instr[0]] | reg[instr[1]];
        private static void Bori(long[] instr, long[] reg) => reg[instr[2]] = reg[instr[0]] | instr[1];
        private static void Setr(long[] instr, long[] reg) => reg[instr[2]] = reg[instr[0]];
        private static void Seti(long[] instr, long[] reg) => reg[instr[2]] = instr[0];
        private static void Gtir(long[] instr, long[] reg) => reg[instr[2]] = instr[0] > reg[instr[1]] ? 1 : 0;
        private static void Gtri(long[] instr, long[] reg) => reg[instr[2]] = reg[instr[0]] > instr[1] ? 1 : 0;
        private static void Gtrr(long[] instr, long[] reg) => reg[instr[2]] = reg[instr[0]] > reg[instr[1]] ? 1 : 0;
        private static void Eqir(long[] instr, long[] reg) => reg[instr[2]] = instr[0] == reg[instr[1]] ? 1 : 0;
        private static void Eqri(long[] instr, long[] reg) => reg[instr[2]] = reg[instr[0]] == instr[1] ? 1 : 0;
        private static void Eqrr(long[] instr, long[] reg) => reg[instr[2]] = reg[instr[0]] == reg[instr[1]] ? 1 : 0;
    }

    public record Instruction(string OperationName, long[] Values) { 
        public static Instruction FromString(string instruction)
        {
            var split = instruction.Split(" ");
            var operationName = split[0];
            var values = split[1..].Select(long.Parse).ToArray();
            return new Instruction(operationName, values);
        }

        public override string ToString() => $"{OperationName} {string.Join(" ", Values)}";
    }
}
