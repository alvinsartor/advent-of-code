﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2018.Days.TimeTravelingDevice
{
    public sealed class TimeTravelingDevice_V1
    {
        public int[] register;
        public Dictionary<string, Action<int[]>> SupportedOperations;

        public TimeTravelingDevice_V1()
        {
            register = new int[4];
            SupportedOperations = new Dictionary<string, Action<int[]>>
                {
                    { "Addr", Addr},
                    { "Addi", Addi},
                    { "Mulr", Mulr},
                    { "Muli", Muli},
                    { "Banr", Banr},
                    { "Bani", Bani},
                    { "Borr", Borr},
                    { "Bori", Bori},
                    { "Setr", Setr},
                    { "Seti", Seti},
                    { "Gtir", Gtir},
                    { "Gtri", Gtri},
                    { "Gtrr", Gtrr},
                    { "Eqir", Eqir},
                    { "Eqri", Eqri},
                    { "Eqrr", Eqrr},
                };
        }

        public void Initialize(int[] register) => this.register = register.ToArray();
        public void ExecuteOperationsList(int[] initialRegistryValue, List<int[]> operations, Dictionary<int, string> operationCodes)
        {
            Initialize(initialRegistryValue);
            foreach (var operation in operations)
            {
                var operationCode = operation[0];
                var operationValues = operation[1..];
                var functionName = operationCodes[operationCode];
                var function = SupportedOperations[functionName];
                function(operationValues);
            }
        }

        public void Addr(int[] instr) => register[instr[2]] = register[instr[0]] + register[instr[1]];
        public void Addi(int[] instr) => register[instr[2]] = register[instr[0]] + instr[1];
        public void Mulr(int[] instr) => register[instr[2]] = register[instr[0]] * register[instr[1]];
        public void Muli(int[] instr) => register[instr[2]] = register[instr[0]] * instr[1];
        public void Banr(int[] instr) => register[instr[2]] = register[instr[0]] & register[instr[1]];
        public void Bani(int[] instr) => register[instr[2]] = register[instr[0]] & instr[1];
        public void Borr(int[] instr) => register[instr[2]] = register[instr[0]] | register[instr[1]];
        public void Bori(int[] instr) => register[instr[2]] = register[instr[0]] | instr[1];
        public void Setr(int[] instr) => register[instr[2]] = register[instr[0]];
        public void Seti(int[] instr) => register[instr[2]] = instr[0];
        public void Gtir(int[] instr) => register[instr[2]] = instr[0] > register[instr[1]] ? 1 : 0;
        public void Gtri(int[] instr) => register[instr[2]] = register[instr[0]] > instr[1] ? 1 : 0;
        public void Gtrr(int[] instr) => register[instr[2]] = register[instr[0]] > register[instr[1]] ? 1 : 0;
        public void Eqir(int[] instr) => register[instr[2]] = instr[0] == register[instr[1]] ? 1 : 0;
        public void Eqri(int[] instr) => register[instr[2]] = register[instr[0]] == instr[1] ? 1 : 0;
        public void Eqrr(int[] instr) => register[instr[2]] = register[instr[0]] == register[instr[1]] ? 1 : 0;
    }
}
