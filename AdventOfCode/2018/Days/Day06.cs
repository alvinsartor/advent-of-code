﻿using AdventOfCode.helpers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/6

    class Day06 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 50;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 100;

        /// <summary>
        /// Given a list of points, find the biggest finite area obtained by assigning each point to the nearest of the list.
        /// </summary>
        public override string Part1()
        {
            var points = input.Select(x => x.Split(", ")).Select(x => new Point(int.Parse(x[0]),  int.Parse(x[1]))).ToArray();
            var pointsAndIndexes = points.Select((pt, i) => (indx: i, pt)).ToArray();
            (int[][] plane, int minX, int minY) = GetPlane(points);

            Parallel.For(0, plane.Length, (r) =>
            {
                for (var c = 0; c < plane[0].Length; c++)
                {
                    var pt = new Point(r + minY, c + minX);
                    var pointsByDistance = pointsAndIndexes
                        .Select(tuple => (tuple.indx, distance: tuple.pt.ManhattanDistance(pt)))
                        .OrderBy(x => x.distance)
                        .ToArray();

                    var closestPoint = pointsByDistance[0].distance == pointsByDistance[1].distance ? -1 : pointsByDistance[0].indx;
                    plane[r][c] = closestPoint;
                }
            });

            var pointsOnBorders = plane[0].Concat(plane[^1]).Concat(plane.SelectMany(line => new[] { line[0], line[^1] })).ToHashSet();
            Dictionary<int, int> pointsToConsider = pointsAndIndexes
                .Select(t => t.indx)
                .Where(i => !pointsOnBorders.Contains(i))
                .ToDictionary(i => i, i => 0);

            for (var r = 0; r < plane.Length; r++)
                for (var c = 0; c < plane[0].Length; c++)
                    if (pointsToConsider.ContainsKey(plane[r][c]))
                        pointsToConsider[plane[r][c]]++;

            return pointsToConsider.Values.Max().ToString();
        }

        private static (int[][] plane, int minX, int minY) GetPlane(IEnumerable<Point> points)
        {
            (int minX, int minY, int maxX, int maxY) = GetBoundaries(points);

            int planeHeight = maxY - minY;
            int planeWidth = maxX - minX;
            int[][] plane = new int[planeHeight][];
            for (var i = 0; i < plane.Length; i++) plane[i] = new int[planeWidth];

            return (plane, minX, minY);
        }

        private static (int minX, int minY, int maxX, int maxY) GetBoundaries(IEnumerable<Point> points)
        {
            var minX = double.MaxValue;
            var minY = double.MaxValue;
            var maxX = double.MinValue;
            var maxY = double.MinValue;
            foreach(var pt in points)
            {
                if (pt.X > maxX) maxX = pt.X;
                else if (pt.X < minX) minX = pt.X;
                if (pt.Y > maxY) maxY = pt.Y;
                else if (pt.Y < minY) minY = pt.Y;
            }

            return ((int)minX, (int)minY, (int)maxX, (int)maxY);
        }

        /// <summary>
        /// Find the area in which each point's distance to all the other points is less than 10000.
        /// </summary>
        public override string Part2()
        {
            var points = input.Select(x => x.Split(", ")).Select(x => new Point(int.Parse(x[0]), int.Parse(x[1]))).ToArray();
            var pointsAndIndexes = points.Select((pt, i) => (indx: i, pt)).ToArray();
            (int[][] plane, int minX, int minY) = GetPlane(points);

            Parallel.For(0, plane.Length, (r) =>
            {
                for (var c = 0; c < plane[0].Length; c++)
                {
                    var pt = new Point(r + minY, c + minX);
                    plane[r][c] = (int)points.Sum(point => point.ManhattanDistance(pt));
                }
            });

            return plane.Sum(line => line.Count(distance => distance < 10000)).ToString();
        }
    }
}
