﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/8

    class Day08 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 500;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 500;

        /// <summary>
        /// Sum the metadata of all nodes of the license.
        /// </summary>
        public override string Part1()
        {
            var license = input[0].Split(" ").Select(int.Parse).ToArray();
            var licenseRootNode = BuildLicense(license);
            return SumMetadata(licenseRootNode).ToString();
        }

        /// <summary>
        /// Extract the value of the root node using the given formula.
        /// </summary>
        public override string Part2()
        {
            var license = input[0].Split(" ").Select(int.Parse).ToArray();
            var licenseRootNode = BuildLicense(license);
            return GetNodeValue(licenseRootNode).ToString();
        }

        private record LicenseNode(int[] Metadata, List<LicenseNode> SubNodes);
        private static LicenseNode BuildLicense(int[] license) => BuildNode(license, 0).node;
        private static (LicenseNode node, int endIndex) BuildNode(int[] license, int startingIndex)
        {
            int nrChildren = license[startingIndex];
            int nrMetadata = license[startingIndex + 1];
            var childIndex = startingIndex + 2;
            var children = new List<LicenseNode>();
            for (int i = 0; i < nrChildren; i++)
            {
                var (child, endIndex) = BuildNode(license, childIndex);
                children.Add(child);
                childIndex = endIndex;
            }
            var metadataIndexStart = childIndex;
            var metadataIndexEnd = metadataIndexStart + nrMetadata;
            var metadata = license[metadataIndexStart..metadataIndexEnd];
            var licenseNode = new LicenseNode(metadata, children);

            return (licenseNode, metadataIndexEnd);
        }

        private static int SumMetadata(LicenseNode node) => node.Metadata.Sum() + node.SubNodes.Sum(SumMetadata);
        private static int GetNodeValue(LicenseNode node) => node.SubNodes.Count == 0
                ? node.Metadata.Sum()
                : node.Metadata
                .Select(m => m - 1)
                .Where(m => m >= 0 && m < node.SubNodes.Count)
                .Sum(m => GetNodeValue(node.SubNodes[m]));
    }
}
