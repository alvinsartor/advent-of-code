﻿using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/17

    class Day17: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 100;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 100;

        /// <summary>
        /// Given a map of the subsoil, calculate where the water will be able to reach.
        /// </summary>
        public override string Part1()
        {
            (char[][] map, Point source) = BuildMapFromInputPoints(input, new Point(500, 0));
            
            AddWater(map, (int)source.X, (int)source.Y);
            //PrintMap(map); // uncomment to print the map.
            
            var waterTiles = map.Sum(line => line.Count(c => c == '~' || c == '|'));
            return waterTiles.ToString();
        }

        /// <summary>
        /// Now calculate how much water will be retained (so only count the '~' tiles).
        /// </summary>
        public override string Part2()
        {
            (char[][] map, Point source) = BuildMapFromInputPoints(input, new Point(500, 0));

            AddWater(map, (int)source.X, (int)source.Y);
            //PrintMap(map); // uncomment to print the map.

            var waterTiles = map.Sum(line => line.Count(c => c == '~'));
            return waterTiles.ToString();
        }

        private static (char[][] map, Point source) BuildMapFromInputPoints(string[] input, Point source)
        {
            var clayPositions = input.SelectMany(ExtractClayPositionFromInputLine).ToList();
            var (minX, minY, maxX, maxY) = clayPositions.GetBoundingBox();
            var width = (int)(maxX - minX + 1 + 2); // considering an extra column left and right
            var height = (int)(maxY - minY + 1);

            var map = new char[height][];
            for (int r = 0; r < height; r++) 
                map[r] = Enumerable.Repeat('.', width).ToArray();

            foreach (var pt in clayPositions)
                map[(int)(pt.Y - minY)][(int)(pt.X - minX + 1)] = '#';

            var shiftedSource = source - new Point(minX + 1, 0);
            return (map, shiftedSource);
        }

        private static IEnumerable<Point> ExtractClayPositionFromInputLine(string inputLine)
        {
            var split = inputLine.Split(", ");
            bool xFirst = inputLine[0] == 'x';
            var fixedAxis = int.Parse(split[0][2..]);
            var rangedAxis = split[1][2..].Split("..").Select(int.Parse).ToArray();

            return Enumerable
                .Range(rangedAxis[0], rangedAxis[1] - rangedAxis[0] + 1)
                .Select(n => xFirst ? new Point(fixedAxis, n) : new Point(n, fixedAxis));
        }

        private static void PrintMap(char[][] map, int maxRow = -1, int minRow = -1)
        {
            var linesToPrint = maxRow == -1 ? map : map.Take(maxRow);
            linesToPrint = minRow == -1 ? linesToPrint : linesToPrint.Skip(linesToPrint.Count() - minRow);

            linesToPrint.ForEach(line => Console.WriteLine(new string(line)));
            Console.WriteLine();
        }

        private static void AddWater(char[][] map, int column, int row)
        {            
            if (map[row + 1][column] == '|') return;
            row = AddVerticalFall(map, column, row);            
            if (row >= map.Length) return;                        
            AddHorizontalLayer(map, column, row - 1);
        }

        private static int AddVerticalFall(char[][] map, int column, int row)
        {
            while(row < map.Length 
                && map[row][column] != '#' 
                && map[row][column] != '~')
                map[row++][column] = '|';
            return row;
        }

        private static void AddHorizontalLayer(char[][] map, int column, int row)
        {
            char[] mapRow = map[row];
            if (mapRow[column + 1] == '|' && mapRow[column -1] == '|') return;

            int columnToLeft = column;
            while (columnToLeft >= 0 
                && mapRow[columnToLeft] != '#' 
                && row+1 < map.Length 
                && map[row + 1][columnToLeft] != '.'
                && map[row + 1][columnToLeft] != '|')
                mapRow[columnToLeft--] = '~';

            int columnToRight = column + 1;
            while (columnToRight < mapRow.Length 
                && mapRow[columnToRight] != '#' 
                && row + 1 < map.Length 
                && map[row + 1][columnToRight] != '.'
                && map[row + 1][columnToRight] != '|')
                mapRow[columnToRight++] = '~';

            bool hasLeftBoundary = mapRow[columnToLeft] == '#';
            bool hasRightBoundary = mapRow[columnToRight] == '#';

            if (hasLeftBoundary && hasRightBoundary)
            {
                AddHorizontalLayer(map, column, row - 1);
                return;
            }

            // change symbol for non-still water
            for (int c = columnToLeft; c <= columnToRight; c++) 
                mapRow[c] = mapRow[c] != '#' ? '|' : '#';

            if (!hasLeftBoundary) AddWater(map, columnToLeft, row);
            if (!hasRightBoundary) AddWater(map, columnToRight, row);
        }        
    }
}
