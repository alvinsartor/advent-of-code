﻿using AdventOfCode.helpers;
using Graph;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/

    class Day07: DailyChallenge
    {
        /// <summary>
        /// Define the order in which the given steps can be executed.
        /// </summary>
        public override string Part1()
        {
            var steps = ExtractSteps(input);

            var graph = new Graph<char>();
            foreach ((char pre, char post) in steps) graph.TryAdd(pre).AddConnection(graph.TryAdd(post));

            var result = new List<char>();
            while (graph.Count > 0)
            {
                var nodeToRemove = graph.Nodes.Where(node => node.InDegree == 0).OrderBy(x => x.Value).First();
                var neighbors = nodeToRemove.CurrentNeighbors.Select(n => n.Value).ToList();
                nodeToRemove.ClearConnections();
                graph.Remove(nodeToRemove.Value);
                result.Add(nodeToRemove.Value);
            }

            return string.Join("", result);
        }

        /// <summary>
        /// 
        /// </summary>
        public override string Part2()
        {
            var steps = ExtractSteps(input);

            var graph = new Graph<char>();
            foreach ((char pre, char post) in steps) graph.TryAdd(pre).AddConnection(graph.TryAdd(post));

            const int maxWorkers = 5;
            var workers = new List<(char task, int timeLeft)>();

            int elapsedTime = 0;
            while (graph.Count > 0)
            {
                var workedTasks = workers.Select(tuple => tuple.task).ToHashSet();
                var availableTasks = graph.Nodes.Where(node => node.InDegree == 0 && !workedTasks.Contains(node.Value)).Select(x => x.Value).OrderBy(x => x);
                var availableWorkers = maxWorkers - workers.Count;
                availableTasks.Take(availableWorkers).ForEach(task => workers.Add((task, TaskToNeededTime(task))));

                var shortestTask = workers.MinBy(tuple => tuple.timeLeft);
                workers.Remove(shortestTask);
                for (int i = 0; i < workers.Count; i++) workers[i] = (workers[i].task, workers[i].timeLeft - shortestTask.timeLeft);
                elapsedTime += shortestTask.timeLeft;

                var nodeToRemove = graph.GetNode(shortestTask.task);
                var neighbors = nodeToRemove.CurrentNeighbors.Select(n => n.Value).ToList();
                nodeToRemove.ClearConnections();
                graph.Remove(nodeToRemove.Value);                
            }

            return elapsedTime.ToString();
        }

        private static List<(char pre, char post)> ExtractSteps(string[] input) => 
            input.Select(line => (line[5], line[36])).ToList();

        private static int TaskToNeededTime(char task) => task - 'A' + 1 + 60;
    }
}
