﻿using AdventOfCode.helpers;
using Graph;
using Graph.PathFinding;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/22

    class Day22 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 1000;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 1;

        /// <summary>
        /// Find the risk level of the rectangle between the entrance of the cave and the target.
        /// </summary>
        public override string Part1()
        {
            (int depth, int targetX, int targetY) = DecodeInput(input);
            var cave = BuildCave(depth, targetX, targetY);
            var riskLevel = cave.Sum(row => row.Sum());
            return riskLevel.ToString();
        }

        /// <summary>
        /// Calculate the fewest amount of minutes you need to reach the target.
        /// </summary>
        public override string Part2()
        {
            (int depth, int targetX, int targetY) = DecodeInput(input);
            var cave = BuildCave(depth, targetX + 50, targetY + 50);

            bool IsFinalState(CaveExplorationState state) => targetX == state.PosX && targetY == state.PosY && (state.Equipped == EquippableItems.All || state.Equipped == EquippableItems.Torch);
            double Heuristic(CaveExplorationState state) => Math.Abs(targetX - state.PosX) + Math.Abs(targetY - state.PosY);

            var initialState = CaveExplorationState.InitialState();
            DynamicGraph<CaveExplorationState> stateSearchGraph = new((state) => CaveExplorationState.Expand(state, cave));
            stateSearchGraph.Add(initialState);
            var pathToTarget = stateSearchGraph.AStarSearch(initialState, IsFinalState, Heuristic).Result;

            //PrintPath(pathToTarget, cave, (targetX, targetY));
            return pathToTarget.Cost.ToString();
        }

        private static (int depth, int targetX, int targetY) DecodeInput(string[] input)
        {
            int depth = int.Parse(input[0][7..]);
            int[] target = input[1][8..].Split(',').Select(int.Parse).ToArray();
            return (depth, target[0], target[1]);
        }

        private static long[][] BuildCave(int depth, int targetX, int targetY)
        {
            long[][] cave = new long[targetY + 1][];
            for (int i = 0; i < cave.Length; i++) cave[i] = new long[targetX + 1];

            for (int y = 0; y < targetY + 1; y++)
                cave[y][0] = (y * 48271 + depth) % 20183;

            for (int x = 0; x < targetX + 1; x++)
                cave[0][x] = (x * 16807 + depth) % 20183;


            for (int y = 1; y < cave.Length; y++)
                for (int x = 1; x < cave[y].Length; x++)
                    cave[y][x] = x == targetX && y == targetY
                        ? depth
                        : (cave[y - 1][x] * cave[y][x - 1] + depth) % 20183;

            for (int y = 0; y < cave.Length; y++)
                for (int x = 0; x < cave[y].Length; x++)
                    cave[y][x] %= 3;

            return cave;
        }

        private static void PrintPath(Path<CaveExplorationState> path, long[][] cave, (int x, int y) target)
        {
            static char GetTile(long value) => value == 0 ? '.' : value == 1 ? '=' : '|';
            char[][] caveChar = cave.Select(line => line.Select(GetTile).ToArray()).ToArray();
            caveChar[target.y][target.x] = 'T';

            path.Nodes.ForEach(n => PrintState(n.Value, caveChar));
        }

        private static void PrintState(CaveExplorationState state, char[][] cave)
        {
            var caveCopy = cave.Select(line => line.ToArray()).ToArray();
            var tile = caveCopy[state.PosY][state.PosX];
            var tileType = tile == '.' || tile == 'T' ? "Rock" : tile == '=' ? "Wet" : "Narrow";

            caveCopy[state.PosY][state.PosX] = 'X';
            caveCopy.ForEach(Console.WriteLine);
            Console.WriteLine($"{state}, tile: {tileType}");
            Console.WriteLine();
        }

        private enum EquippableItems { None, Torch, Gear, All };

        private record CaveExplorationState(int PosX, int PosY, EquippableItems Equipped)
        {
            public static CaveExplorationState InitialState() => new CaveExplorationState(0, 0, EquippableItems.Torch);
            public override string ToString() => $"[{PosX},{PosY},{Equipped}]";

            public static Task<ImmutableList<(CaveExplorationState, double)>> Expand(CaveExplorationState state, long[][] cave)
            {
                var availableMovements = state.Surroundings(cave)
                    .Select(pos => (new CaveExplorationState(pos.posX, pos.posY, state.Equipped), 1.0));
                var availableEquipments = state.CurrentlyEquippable(cave[state.PosY][state.PosX])
                    .Select(equipment => (new CaveExplorationState(state.PosX, state.PosY, equipment), 7.0));
                return Task.FromResult(availableMovements.Concat(availableEquipments).ToImmutableList());
            }

            private IEnumerable<(int posX, int posY)> Surroundings(long[][] cave)
            {
                return new[] { (PosX + 1, PosY), (PosX, PosY + 1), (PosX - 1, PosY), (PosX, PosY - 1), }
                    .Cast<(int x, int y)>()
                    .Where(pos => IsPositionValid(pos, cave))
                    .Where(pos => IsAccessibleWithCurrentEquipment(cave[pos.y][pos.x], Equipped));
            }

            private static bool IsPositionValid((int posX, int posY) position, long[][] cave) =>
                position.posY >= 0 && position.posY < cave.Length && position.posX >= 0 && position.posX < cave[0].Length;

            private static bool IsAccessibleWithCurrentEquipment(long tile, EquippableItems equipped) => tile switch
            {
                0 => equipped != EquippableItems.None,
                1 => equipped != EquippableItems.Torch && equipped != EquippableItems.All,
                2 => equipped != EquippableItems.Gear && equipped != EquippableItems.All,
                _ => throw new NotImplementedException($"Unrecognized tile: {tile}"),
            };

            private IEnumerable<EquippableItems> CurrentlyEquippable(long tile)
            {
                EquippableItems[] equippable = tile switch
                {
                    0 => new[] { EquippableItems.All, EquippableItems.Torch, EquippableItems.Gear },
                    1 => new[] { EquippableItems.None, EquippableItems.Gear },
                    2 => new[] { EquippableItems.None, EquippableItems.Torch },
                    _ => throw new NotImplementedException(),
                };
                return equippable.Where(item => item != Equipped);
            }
        }
    }
}
