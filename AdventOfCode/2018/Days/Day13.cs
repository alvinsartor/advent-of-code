﻿using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/13

    class Day13: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 100;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 100;

        /// <summary>
        /// Given a map representing rails and carts, find the coordinate of the first crash between two carts.
        /// </summary>
        public override string Part1()
        {
            var carts = FindCartsOnMap(input).ToList();
            var positions = carts.Select(cart => cart.Position).ToHashSet();
            var map = RemoveCartsFromMap(input);

            while (true)
            {
                var sortedCarts = carts.OrderBy(cart => cart.Position.Y).ThenBy(cart => cart.Position.X).ToList();
                foreach(var cart in sortedCarts)
                {
                    var cartOldPosition = cart.Position;
                    cart.Move(map);
                    var cartNewPosition = cart.Position;

                    if (positions.Contains(cartNewPosition)) return $"{cartNewPosition.X},{cartNewPosition.Y}";
                    positions.Remove(cartOldPosition);
                    positions.Add(cartNewPosition);
                }
            }
        }

        /// <summary>
        /// Each time there is an accident, remove those carts and go on. What is the position of the last carts standing?
        /// </summary>
        public override string Part2()
        {
            var carts = FindCartsOnMap(input).ToHashSet();
            var positions = carts.Select(cart => cart.Position).ToHashSet();
            var map = RemoveCartsFromMap(input);

            while (carts.Count > 1)
            {
                var sortedCarts = carts.OrderBy(cart => cart.Position.Y).ThenBy(cart => cart.Position.X).ToList();
                foreach (var cart in sortedCarts)
                {
                    if (!carts.Contains(cart)) continue; // it might have been removed in this tick

                    var cartOldPosition = cart.Position;
                    cart.Move(map);
                    var cartNewPosition = cart.Position;

                    if (positions.Contains(cartNewPosition))
                    {
                        carts.Remove(cart);
                        var hittedCart = carts.First(cart => cart.Position.Equals(cartNewPosition));
                        carts.Remove(hittedCart);
                        positions.Remove(cartNewPosition);
                        positions.Remove(cartOldPosition);
                    }
                    else
                    {
                        positions.Remove(cartOldPosition);
                        positions.Add(cartNewPosition);
                    }
                }
            }

            var lastCartStanding = carts.First().Position;
            return $"{lastCartStanding.X},{lastCartStanding.Y}";
        }

        private static IEnumerable<Cart> FindCartsOnMap(string[] input)
        {
            static bool isCart(char val) => val == 'v' || val == '>' || val == '<' || val == '^';
            for (int r = 0; r < input.Length; r++)
                for (int c = 0; c < input[r].Length; c++)
                    if (input[r][c] != ' ')
                        if (isCart(input[r][c]))
                            yield return new Cart(new Point(c, r), input[r][c]);
        }

        private static string[] RemoveCartsFromMap(string[] input) =>
            input.Select(line => line.Replace('>', '-').Replace('<', '-').Replace('^', '|').Replace('v', '|')).ToArray();

        private class Cart
        {
            private static int ID = -1;

            private readonly int id;
            private Point position;
            private Point direction;
            private Point nextTurnDirection;

            public Point Position => position;
            public Cart(Point position, char symbol)
            {
                id = ID++;
                this.position = position;
                direction = SymbolToDirection(symbol);
                nextTurnDirection = Point.Left;
            }

            private static Point CalculateNextTurningDirection(Point currentDirection) =>
                currentDirection switch {
                    Point(0, -1) => Point.Right,
                    Point(1, 0) => Point.Left,
                    Point(-1, 0) => Point.Up,
                    _ => throw new NotImplementedException($"Direction not recognized: {currentDirection}"),
                };

            private static Point SymbolToDirection(char cartSymbol) =>
                cartSymbol switch
                {
                    '>' => Point.Right,
                    '<' => Point.Left,
                    '^' => Point.Up,
                    'v' => Point.Down,
                    _ => throw new NotImplementedException($"Symbol not recognized: {cartSymbol}"),
                };

            public void Move(string[] map)
            {
                var currentlyOn = map[(int)position.Y][(int)position.X];
                switch (currentlyOn)
                {
                    case '-':
                        if (!(direction.Equals(Point.Right) || direction.Equals(Point.Left))) ThrowDueToIncongruence(map);
                        position += direction;
                        break;
                    case '|':
                        if (!(direction.Equals(Point.Up) || direction.Equals(Point.Down))) ThrowDueToIncongruence(map);
                        position += direction;
                        break;
                    case '\\':
                        direction = DirectionAfterBackslashCurve(direction);   
                        position += direction;
                        break;
                    case '/':
                        direction = DirectionAfterSlashCurve(direction);   
                        position += direction;
                        break;
                    case '+':
                        direction = DirectionAfterCrossing(direction, nextTurnDirection);
                        nextTurnDirection = CalculateNextTurningDirection(nextTurnDirection);
                        position += direction;
                        break;
                    default:
                        throw new NotImplementedException($"Symbol not recognized: {currentlyOn}");
                }   
            }

            private static Point DirectionAfterBackslashCurve(Point currentDirection) =>
                currentDirection switch
                {
                    Point(0, -1) => Point.Left,
                    Point(-1, 0) => Point.Up,
                    Point(0, 1) => Point.Right,
                    Point(1, 0) => Point.Down,
                    _ => throw new NotImplementedException($"Direction not recognized: {currentDirection}"),
                };

            private static Point DirectionAfterSlashCurve(Point currentDirection) =>
                currentDirection switch
                {
                    Point(0, -1) => Point.Right,
                    Point(1, 0) => Point.Up,
                    Point(0, 1) => Point.Left,
                    Point(-1, 0) => Point.Down,
                    _ => throw new NotImplementedException($"Direction not recognized: {currentDirection}"),
                };
            
            private static Point DirectionAfterCrossing(Point currentDirection, Point nextTurnDirection) =>
                nextTurnDirection switch
                {
                    Point(0, -1) => currentDirection,
                    Point(1, 0) => currentDirection.RotatedClockwise(),
                    Point(-1, 0) => currentDirection.RotatedCounterclockwise(),
                    _ => throw new NotImplementedException($"Direction not recognized: {currentDirection}"),
                };

            private void ThrowDueToIncongruence(string[] map) =>
                throw new Exception($"Incongruent position-direction: {map[(int)position.Y][(int)position.X]}, {direction}");
            
                public override bool Equals(object? obj) => obj is Cart cart && id == cart.id;
            public override int GetHashCode() => HashCode.Combine(id);
        }
    }
}
