﻿using AdventOfCode.helpers;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/5

    class Day05 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 100;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 50;

        /// <summary>
        /// Count how many elements are remaining in the polymer after all reactions have happened.
        /// </summary>
        public override string Part1()
        {
            var collapsedPolymerLength = CountCollapsedPolymerUnit(input[0].ToLinkedList());
            return collapsedPolymerLength.ToString();
        }

        /// <summary>
        /// Find the unit that, if removed, allows the polymer to collapse the most.
        /// </summary>
        public override string Part2()
        {
            var polymerRepresentation = input[0];
            var unitsType = polymerRepresentation.ToLower().ToHashSet();
            var shortestPolymer = unitsType
                .Select(c => polymerRepresentation.Where(x => x != c && x != char.ToUpper(c)).ToLinkedList())
                .Select(polymer => CountCollapsedPolymerUnit(polymer))
                .Min();

            return shortestPolymer.ToString();
        }

        private static bool Combine(char unit1, char unit2) => char.ToLower(unit1) == char.ToLower(unit2) && unit1 != unit2;
        private static int CountCollapsedPolymerUnit(LinkedList<char> polymer)
        {
            var unit = polymer.First;

            while (unit != null)
            {
                if (!Combine(unit.Value, unit.Next?.Value ?? '0')) unit = unit.Next;
                else
                {
                    var tmp = unit.Previous;
                    polymer.Remove(unit.Next!);
                    polymer.Remove(unit);
                    unit = tmp ?? polymer.First;
                }
            }
            return polymer.Count;
        }
    }
}