﻿using AdventOfCode.helpers;
using System.Collections.Generic;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/11

    class Day11: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 50;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 10;

        /// <summary>
        /// Given a value that allows to calculate the element of a matrix, give the coordinates of the 3x3 with highest internal sum.
        /// </summary>
        public override string Part1()
        {
            var gridSerial = int.Parse(input[0]);
            var baseGrid = GetGrid(gridSerial);
            var summedAreaTable = GetSummedAreaTable(baseGrid);
            var threeByThreeSums = GetThreeXThreeSums(summedAreaTable);
            var (value, x, y) = threeByThreeSums.MaxBy(area => area.value);

            return $"{x},{y}";
        }

        /// <summary>
        /// Same as before, but now the resulting matrix can be of any size!
        /// </summary>
        public override string Part2()
        {
            var gridSerial = int.Parse(input[0]);
            var baseGrid = GetGrid(gridSerial);
            var summedAreaTable = GetSummedAreaTable(baseGrid);
            var threeByThreeSums = GetAnySizeSums(summedAreaTable);
            var (value, x, y, size) = threeByThreeSums.MaxBy(area => area.value);

            return $"{x},{y},{size}";
        }

        private static int[,] GetGrid(int gridSerial)
        {
            var grid = new int[300,300];
            for (int r = 0; r < 300; r++)
                for (int c = 0; c < 300; c++) 
                    grid[r,c] = CalculatePower(c + 1, r + 1, gridSerial);
            return grid;
        }

        private static int CalculatePower(int x, int y, int gridSerial) => 
            int.Parse((((x + 10) * y + gridSerial) * (x + 10)).ToString().PadLeft(3, '0')[^3].ToString()) - 5;

        private static int[,] GetSummedAreaTable(int[,] grid)
        {
            var summedAreaTable = new int[grid.GetLength(0), grid.GetLength(1)];
            for (int r = 0; r < grid.GetLength(0); r++)
                for (int c = 0; c < grid.GetLength(1); c++)
                {
                    //I(x, y) = i(x, y) + I(x - 1, y) + I(x, y - 1) - I(x - 1, y - 1)
                    //I(x, y) = val0 + val1 + val2 - val3

                    var val0 = grid[r, c];
                    var val1 = c > 0 ? summedAreaTable[r, c - 1] : 0;
                    var val2 = r > 0 ? summedAreaTable[r - 1, c] : 0;
                    var val3 = r > 0 && c > 0 ? summedAreaTable[r - 1, c - 1] : 0;
                    summedAreaTable[r, c] = val0 + val1 + val2 - val3;
                }
            return summedAreaTable;
        }

        private static IEnumerable<(int value, int x, int y)> GetThreeXThreeSums(int[,] sag)
        {
            for (int r = 0; r < sag.GetLength(0) - 3; r++)
                for (int c = 0; c < sag.GetLength(1) - 3; c++)
                    yield return (sag[r, c] + sag[r + 3, c + 3] - sag[r, c + 3] - sag[r + 3, c], c + 2, r + 2);
        }

        private static IEnumerable<(int value, int x, int y, int size)> GetAnySizeSums(int[,] sag)
        {
            for (int size = 1; size < sag.GetLength(0); size++)
                for (int r = 0; r < sag.GetLength(0) - size; r++)
                    for (int c = 0; c < sag.GetLength(1) - size; c++)
                        yield return (
                            sag[r, c] + sag[r + size, c + size] - sag[r, c + size] - sag[r + size, c],
                            c + 2,
                            r + 2,
                            size);
        }
    }
}