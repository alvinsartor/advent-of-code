﻿using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/10

    class Day10: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 10;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 10;

        /// <summary>
        /// 
        /// </summary>
        public override string Part1()
        {
            List<(Point position, Point speed)> points = DecodeInput(input).ToList();
            var time = FindMinimaForViewportSize(points);
            PrintMessage(PointsAtGivenTime(points, time).ToList(), false);
            return "GFANEHKJ";
        }

        /// <summary>
        /// 
        /// </summary>
        public override string Part2()
        {
            List<(Point position, Point speed)> points = DecodeInput(input).ToList();
            var time = FindMinimaForViewportSize(points);
            return time.ToString();
        }

        private static long FindMinimaForViewportSize(List<(Point position, Point speed)> points)
        {
            long functionToMinimize(long x) => ViewportSizeAtGivenTime(points, x);
            return GeneralHelpers.FindMinimaInLinearFunction(functionToMinimize, 0, 1000);
        }

        private static IEnumerable<(Point position, Point speed)> DecodeInput(string[] input)
        {
            foreach(var line in input)
            {
                var rawPosition = line[10..24].Split(", ");
                var position = new Point(int.Parse(rawPosition[0]), int.Parse(rawPosition[1]));
                var rawSpeed = line[36..^1].Split(", ");
                var speed = new Point(int.Parse(rawSpeed[0]), int.Parse(rawSpeed[1]));
                yield return (position, speed);
            }
        }

        private static void PrintMessage(List<Point> points, bool printForReal = true)
        {
            var (maxX, minX, maxY, minY) = GetViewport(points);
            var height = (int)(maxY - minY) + 1;
            var width = (int)(maxX - minX) + 1;

            char[][] sky = new char[height][];
            for (int i = 0; i < sky.Length; i++) 
            { 
                sky[i] = new char[width];
                for (int j = 0; j < sky[i].Length; j++) sky[i][j] = ' ';
            }

            var shift = new Point(minX, minY);
            points.Select(pt => pt - shift).ForEach(pt => sky[(int)pt.Y][(int)pt.X] = '#');

            var pointsInTheSky = sky.Select(line => new string(line));
            if (printForReal) pointsInTheSky.ForEach(Console.WriteLine);
        }

        private static long ViewportSizeAtGivenTime(IEnumerable<(Point position, Point speed)> points, long time) =>
            GetViewportSize(GetViewport(PointsAtGivenTime(points, time)));

        private static IEnumerable<Point> PointsAtGivenTime(IEnumerable<(Point position, Point speed)> points, long time) => 
            points.Select(tuple => tuple.position + (tuple.speed * time));

        private static long GetViewportSize((double maxX, double minX, double maxY, double minY) viewport) =>
            (long)(viewport.maxX - viewport.minX + viewport.maxY - viewport.minY);

        private static (double maxX, double minX, double maxY, double minY) GetViewport(IEnumerable<Point> points)
        {
            var (minX, minY) = (double.MaxValue, double.MaxValue);
            var (maxX, maxY) = (double.MinValue, double.MinValue);
            foreach (var pt in points)
            {
                if (pt.X > maxX) maxX = pt.X;
                else if (pt.X < minX) minX = pt.X;
                if (pt.Y > maxY) maxY = pt.Y;
                else if (pt.Y < minY) minY = pt.Y;
            }

            return (maxX, minX, maxY, minY);
        }
    }
}
