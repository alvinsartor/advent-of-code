﻿using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/24

    class Day24 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 200;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 50;

        /// <summary>
        /// Simulate the fight between the immune system and the infection and see who'd win.
        /// </summary>
        public override string Part1()
        {
            (HashSet<ArmyGroup> immuneSystem, HashSet<ArmyGroup> infection) = DecodeInput(input);
            WageWar(immuneSystem, infection);
            (_, string aftermathMessage) = GetWarAftermath(immuneSystem, infection);
            return aftermathMessage;
        }

        /// <summary>
        /// Find the minimum amount of boost necessary for the immune system to win over the infection.
        /// </summary>
        public override string Part2()
        {
            var minimalBoost = FindMinimalBoostToImmuneSystemToWin(input);
            (HashSet<ArmyGroup> immuneSystem, HashSet<ArmyGroup> infection) = DecodeInput(input, minimalBoost);
            WageWar(immuneSystem, infection);
            (_, string aftermathMessage) = GetWarAftermath(immuneSystem, infection);
            return aftermathMessage;
        }

        private static (HashSet<ArmyGroup> immuneSystem, HashSet<ArmyGroup> infection) DecodeInput(string[] input, int boost = 0)
        {
            HashSet<ArmyGroup> immuneSystem = new();
            HashSet<ArmyGroup> infection = new();

            int i = 1;
            while (!string.IsNullOrEmpty(input[i]))
                immuneSystem.Add(ArmyGroup.ParseFromString(input[i++], "ImmuneSystem", boost));

            i += 2;
            while (i < input.Length && !string.IsNullOrEmpty(input[i]))
                infection.Add(ArmyGroup.ParseFromString(input[i++], "Infection"));

            return (immuneSystem, infection);
        }

        private static void WageWar(HashSet<ArmyGroup> immuneSystem, HashSet<ArmyGroup> infection)
        {
            int turnsInWhichNothingHappened = 0;
            while (immuneSystem.Count > 0 && infection.Count > 0 && turnsInWhichNothingHappened < 5)
            {
                bool hadCasualties = false;
                var immuneSystemSelection = Selection(immuneSystem, infection).ToList();
                var infectionSelection = Selection(infection, immuneSystem).ToList();

                var attackOrder = immuneSystemSelection.Concat(infectionSelection).OrderByDescending(t => t.army.Initiative);
                foreach (var (army, target) in attackOrder)
                {
                    if (!army.IsAlive) continue;
                    if (target == null) continue;

                    if (target.GetAttacked(army)) 
                        hadCasualties = true;

                    if (!target.IsAlive)
                    {
                        var targetSide = target.ArmyType == "ImmuneSystem" ? immuneSystem : infection;
                        targetSide.Remove(target);
                    }
                }

                turnsInWhichNothingHappened = hadCasualties ? 0 : turnsInWhichNothingHappened + 1;
            }
        }

        enum WinningSide { ImmuneSystem, Infection, Draw }
        private static (WinningSide winningSide, string message) GetWarAftermath(
            HashSet<ArmyGroup> immuneSystem, HashSet<ArmyGroup> infection)
        {
            if (immuneSystem.Count > 0 && infection.Count > 0)
                return (WinningSide.Draw, "Draw");

            var winningSide = immuneSystem.Count > 0 ? WinningSide.ImmuneSystem : WinningSide.Infection;
            var unitsLeft = immuneSystem.Sum(a => a.UnitsLeft) + infection.Sum(a => a.UnitsLeft);
            var message = $"{winningSide} won with {unitsLeft} units left.";
            return (winningSide, message);
        }

        private static IEnumerable<(ArmyGroup army, ArmyGroup? target)> Selection(
            IEnumerable<ArmyGroup> attackers, 
            IEnumerable<ArmyGroup> defenders)
        {
            var availableDefenders = defenders.ToHashSet();
            var sortedAttackers = attackers.OrderByDescending(a => a.EffectivePower).ThenByDescending(a => a.Initiative);
            foreach(var army in sortedAttackers)
            {
                var pick = availableDefenders.Count > 0
                    ? availableDefenders
                        .Select(defender => (defender, expectedDamage: army.CalculateExpectedDamage(defender)))
                        .OrderByDescending(defender => defender.expectedDamage)
                        .ThenByDescending(defender => defender.defender.EffectivePower)
                        .ThenByDescending(defender => defender.defender.Initiative)
                        .First()
                    : (army, 0); //there are no available defenders. This will trigger null in the next line.
                var chosenTarget = pick.expectedDamage > 0 ? pick.defender : null;
                if (chosenTarget != null) availableDefenders.Remove(chosenTarget);

                yield return (army, chosenTarget);
            }
        }

        private static int FindMinimalBoostToImmuneSystemToWin(string[] input)
        {
            long function(long boost)
            {
                (HashSet<ArmyGroup> immuneSystem, HashSet<ArmyGroup> infection) = DecodeInput(input, (int)boost);
                WageWar(immuneSystem, infection);
                (var winninSide, _) = GetWarAftermath(immuneSystem, infection);
                return winninSide == WinningSide.ImmuneSystem ? boost : long.MaxValue;
            }
            return (int)GeneralHelpers.FindMinimaInLinearFunction(function, 0, 75);
        }

        private class ArmyGroup
        {
            private int units;
            private readonly int healthPoints;
            private readonly int attackStrength;
            private readonly string attackType;
            private readonly HashSet<string> weaknesses;
            private readonly HashSet<string> immunities;

            public int EffectivePower => units > 0 ? units * attackStrength : 0;
            public int UnitsLeft => units;
            public bool IsAlive => units > 0;
            public int Initiative { get; }
            public string ArmyType { get; }

            public ArmyGroup(int units, int health, int strength, string attackType, int initiative, IEnumerable<string> weakTo, IEnumerable<string> immuneTo, string armyType)
            {
                this.units = units;
                healthPoints = health;
                attackStrength = strength;
                this.attackType = attackType;
                Initiative = initiative;
                ArmyType = armyType;
                weaknesses = weakTo.ToHashSet();
                immunities = immuneTo.ToHashSet();
            }

            public static ArmyGroup ParseFromString(string line, string armyType, int boost = 0)
            {
                var split = line.Split(" ");
                var units = int.Parse(split[0]);
                var health = int.Parse(split[4]);
                var initiative = int.Parse(split[^1]);
                var attackStrength = int.Parse(split[^6]) + boost;
                var attackType = split[^5];
                var weaknesses = ExtractStats(line, "weak to");
                var immunities = ExtractStats(line, "immune to");

                return new ArmyGroup(units, health, attackStrength, attackType, initiative, weaknesses, immunities, armyType);
            }
            private static IEnumerable<string> ExtractStats(string line, string stat)
            {
                var start = line.IndexOf(stat);
                if (start == -1) yield break;

                start += stat.Length + 1;
                var end = line.IndexOf(';', start);
                if (end == -1) end = line.IndexOf(')', start);

                foreach (var value in line[start..end].Split(", ")) yield return value;
            }

            public long CalculateExpectedDamage(ArmyGroup otherGroup) =>
                otherGroup.immunities.Contains(attackType)
                ? 0 : otherGroup.weaknesses.Contains(attackType)
                ? EffectivePower * 2 : EffectivePower;

            public bool GetAttacked(ArmyGroup otherGroup)
            {
                var damage = otherGroup.CalculateExpectedDamage(this);
                var kills = (int)Math.Floor(damage / (double)healthPoints);
                units -= kills;
                return kills > 0;
            }

            public override string ToString() => $"{units} {attackType} guys with <{healthPoints}:hp, {attackStrength}:str, {Initiative}:in>";
        }
    }
}
