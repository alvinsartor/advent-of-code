﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdventOfCode._2018.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2018/day/12

    class Day12 : DailyChallenge
    {
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 500;

        /// <summary>
        /// Given some rules about how plants spread on neighboring vases, calculate the configuration after 20 generations.
        /// </summary>
        public override string Part1()
        {
            var state = GetInitialState(input);
            var rules = GetRules(input);

            int counter = 20;
            int mostLeftVase = 0;
            while (counter-- > 0) (state, mostLeftVase) = ApplyRuleToState(state, mostLeftVase, rules);

            return SumOfVasesWithPlants(state, mostLeftVase).ToString();
        }

        /// <summary>
        /// Same as before, but now with 50 billion generations.
        /// </summary>
        public override string Part2()
        {
            var state = GetInitialState(input);
            var rules = GetRules(input);

            int counter = 200;
            int mostLeftVase = 0;
            var countAt99 = 0;
            var countAt100 = 0;

            while (counter-- > 0)
            {
                (state, mostLeftVase) = ApplyRuleToState(state, mostLeftVase, rules);
                if (counter == 1) countAt99 = SumOfVasesWithPlants(state, mostLeftVase);
                if (counter == 0) countAt100 = SumOfVasesWithPlants(state, mostLeftVase);
            }

            // as the plants increase rate stabilizes after a certain point, 
            // we can use the delta to calculate the number at the 50'000'000'000 th generation

            var delta = countAt100 - countAt99;
            return ((50000000000 - 200) * delta + countAt100).ToString();
        }

        private static string GetInitialState(string[] input) => input[0][15..];
        private static Dictionary<string, char> GetRules(string[] input) =>
            input[2..].Select(line => line.Split(" => ")).ToDictionary(rule => rule[0], rule => rule[1][0]);
        private static int SumOfVasesWithPlants(string state, int mostLeftVase) =>
            state.Select((c, i) => (c, i - mostLeftVase)).Where(t => t.c == '#').Sum(t => t.Item2);

        private static (string state, int mostLeftVase) ApplyRuleToState(string state, int mostLeftVase, Dictionary<string, char> rules)
        {
            state = "......." + state + ".......";
            var newState = new StringBuilder();
            for (int i = 2; i < state.Length - 5; i++)
            {
                var upperBound = i + 5;
                var section = state[i..upperBound];
                var newVaseState = rules.ContainsKey(section) ? rules[section] : '.';
                newState.Append(newVaseState);
            }

            state = newState.ToString();
            var trimmedState = state.TrimStart('.');
            mostLeftVase += 7 - 4 - (state.Length - trimmedState.Length);
            return (trimmedState.TrimEnd('.'), mostLeftVase);
        }
    }
}
