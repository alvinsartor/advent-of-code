﻿using System;
using System.IO;

namespace AdventOfCode
{
    public abstract class DailyChallenge
    {
        private const string DataDirectoryPath = "C:\\Users\\alvin\\Development\\C#\\AdventOfCode\\AdventOfCode\\";
        protected readonly string[] input;

        /// <summary> 
        /// Specifies how many times Part1 should be ran in order to get a proper performances measurement. 
        /// </summary>
        public virtual int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 1000;

        /// <summary> 
        /// Specifies how many times Part2 should be ran in order to get a proper performances measurement. 
        /// </summary>
        public virtual int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 1000;

        /// <summary>
        /// Initializes a new instance of the <see cref="DailyChallenge"/> class, loading the relative input.
        /// </summary>
        public DailyChallenge()
        {
            var classType = GetType();
            var className = classType.Name;
            var challengeYear = classType.FullName!.Split(".")[1].Trim('_');
            
            var filepath = Path.Combine(DataDirectoryPath, challengeYear, "Data", className + ".txt");
            if (!File.Exists(filepath)) throw new Exception("No file could be found at " + filepath);
            input = File.ReadAllLines(filepath);

            Console.WriteLine("Initialized: " + className);
        }

        /// <summary>
        /// Gets the day and year of the current challenge.
        /// </summary>
        public (string day, string year) GetDayAndYear()
        {
            var classType = GetType();
            var challengeDay = classType.Name[3..];
            var challengeYear = classType.FullName!.Split(".")[1].Trim('_');
            return (challengeDay, challengeYear);
        }

        public abstract string Part1();
        public abstract string Part2();
    }
}
