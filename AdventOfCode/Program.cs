using AdventOfCode;
using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Asks the user to input the challenges to be executed.
/// </summary>
static List<DailyChallenge> FetchChallenges()
{
    Console.WriteLine("Which challenge do you want to run?");
    Console.WriteLine("For a whole year, just specify the year [i.e. '2020']");
    Console.WriteLine("For a single challenge, specify year and day [i.e. '2020 01']");
    Console.WriteLine("");
    var userInput = Console.ReadLine()?.Split(" ") ?? Array.Empty<string>();
    var year = userInput[0];
    var day = userInput.Length > 1 ? userInput[1] : null;

    var classesNamespace = $"AdventOfCode._{year}.Days";
    List<Type?> classesToInstantiate = new();
    if (day != null)
    {
        var type = Type.GetType($"{classesNamespace}.Day{day}");
        if (type != null) classesToInstantiate.Add(type);
    } 
    else
    {
        for(int i = 1; i <= 25; i++)
        {
            var type = Type.GetType($"{classesNamespace}.Day{i.ToString().PadLeft(2, '0')}");
            if (type != null) classesToInstantiate.Add(type);
        }
    }

    List<DailyChallenge> instantiatedChallenges = classesToInstantiate
        .Where(type => type != null)
        .Select(type => (DailyChallenge?)Activator.CreateInstance(type!))
        .Where(instance => instance != null)
        .ToList()!;

    return instantiatedChallenges;
}

/// <summary>
/// Runs the challenges and prints results and times.
/// </summary>
static void RunChallenges(List<DailyChallenge> challenges)
{
    decimal counter = 0;
    foreach (var day in challenges)
    {
        (string dayNumber, string yearNumber) = day.GetDayAndYear();

        var part1 = Stopwatcher.Track(day.Part1, day.NumberOfTimesPart1ShouldBeRan);
        Console.WriteLine($"Year {yearNumber} Day {dayNumber} Part 1: {part1.result} | elapsed: {part1.timeInMs}ms");
        
        var part2 = Stopwatcher.Track(day.Part2, day.NumberOfTimesPart2ShouldBeRan);
        Console.WriteLine($"Year {yearNumber} Day {dayNumber} Part 2: {part2.result} | elapsed: {part2.timeInMs}ms");

        counter += part1.timeInMs + part2.timeInMs;
    }

    if (challenges.Count > 1)
    {
        Console.WriteLine();
        Console.WriteLine($"Executed {challenges.Count} days");
        Console.WriteLine($"Total execution time: {counter}ms");
    }
    if (challenges.Count == 0) Console.WriteLine($"No daily challenges could be found.");
}

var challenges = FetchChallenges();
RunChallenges(challenges);