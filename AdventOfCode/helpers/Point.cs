using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.helpers
{
    public record Point(double X, double Y)
    {
        public double ManhattanDistanceFromOrigin => Math.Abs(X) + Math.Abs(Y);
        public double Distance(Point other) => Distance(this, other);
        public double ManhattanDistance(Point other) => Math.Abs(X - other.X) + Math.Abs(Y - other.Y);
        public double Magnitude() => Math.Sqrt(X * X + Y * Y);
        public Point Normalized() => this * (1 / Magnitude());
        public bool IsIntegerPoint() => X == Math.Round(X, 0) && Y == Math.Round(Y, 0);
        public Point Reduced()
        {
            var gcd = MathExtensions.GreatestCommonDivisor(Math.Abs((int)X), Math.Abs((int)Y));
            return new Point(X / gcd, Y / gcd);
        }
        public Point RotatedClockwise() => new Point(-Y, X);
        public Point RotatedCounterclockwise() => new Point(Y, -X);
        public IEnumerable<Point> Surroundings()
        {
            yield return this + Up;
            yield return this + Down;
            yield return this + Left;
            yield return this + Right;
        }
        public IEnumerable<Point> Diagonals()
        {
            yield return this + Up + Left;
            yield return this + Up + Right;
            yield return this + Down + Left;
            yield return this + Down + Right;
        }
        public IEnumerable<Point> ExtendedSurroundings() => Surroundings().Concat(Diagonals());
        public void Deconstruct(out double x, out double y)
        {
            x = X;
            y = Y;
        }

        public static Point Origin { get; } = new Point(0, 0);
        public static Point Up { get; } = new Point(0, -1);
        public static Point Right { get; } = new Point(1, 0);
        public static Point Down { get; } = new Point(0, 1);
        public static Point Left { get; } = new Point(-1, 0);

        public static Point operator +(Point p1, Point p2) => new Point(p1.X + p2.X, p1.Y + p2.Y);
        public static Point operator -(Point p1, Point p2) => new Point(p1.X - p2.X, p1.Y - p2.Y);
        public static Point operator *(Point p1, double scalar) => new Point(p1.X * scalar, p1.Y * scalar);
        public static double Distance(Point p1, Point p2) => Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2));

        public override int GetHashCode() => HashCode.Combine(X, Y);
        public override string ToString() => $"{X},{Y}";
    }

    public record Point3D(double X, double Y, double Z)
    {
        public double Distance(Point3D other) => Distance(this, other);
        public double ManhattanDistance(Point3D other) => Math.Abs(X - other.X) + Math.Abs(Y - other.Y) + Math.Abs(Z - other.Z);
        public double ManhattanDistanceFromOrigin => Math.Abs(X) + Math.Abs(Y) + Math.Abs(Z);

        public static Point3D Origin { get; } = new Point3D(0, 0, 0);        
        public static Point3D operator +(Point3D p1, Point3D p2) => new Point3D(p1.X + p2.X, p1.Y + p2.Y, p1.Z + p2.Z);
        public static Point3D operator +(Point3D p1, Point p2) => new Point3D(p1.X + p2.X, p1.Y + p2.Y, p1.Z);
        public static Point3D operator -(Point3D p1, Point3D p2) => new Point3D(p1.X - p2.X, p1.Y - p2.Y, p1.Z - p2.Z);
        public static Point3D operator -(Point3D p1, Point p2) => new Point3D(p1.X - p2.X, p1.Y - p2.Y, p1.Z);
        public static Point3D operator *(Point3D p, double scalar) => new Point3D(p.X * scalar, p.Y * scalar, p.Z * scalar);
        public static double Distance(Point3D p1, Point3D p2) => 
            Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2) + Math.Pow(p1.Z - p2.Z, 2));

        public override int GetHashCode() => HashCode.Combine(X, Y, Z);
        public override string ToString() => $"{X},{Y},{Z}";
        public Point3D Sign() => new Point3D(Math.Sign(X), Math.Sign(Y), Math.Sign(Z));
    }

    public record Point4D(double X, double Y, double Z, double W)
    {
        public double ManhattanDistance(Point4D p) => Math.Abs(X - p.X) + Math.Abs(Y - p.Y) + Math.Abs(Z - p.Z) + Math.Abs(W - p.W);
        public double ManhattanDistanceFromOrigin => Math.Abs(X) + Math.Abs(Y) + Math.Abs(Z) + Math.Abs(W);

        public static Point4D Origin { get; } = new Point4D(0, 0, 0, 0);
        public static Point4D FromArray(double[] array) => new Point4D(array[0], array[1], array[2], array[3]);

        public override int GetHashCode() => HashCode.Combine(X, Y, Z, W);
        public override string ToString() => $"{X},{Y},{Z},{W}";
    }
}
