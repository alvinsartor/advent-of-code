﻿using System;

namespace AdventOfCode.helpers
{
    public static class GeneralHelpers
    {
        public enum InterpolatorDirection { Up, Down, GoodEnough }

        /// <summary>
        /// Interpolates the specified function.
        /// </summary>
        /// <param name="interpolatingFunction">The function to interpolate.</param>
        /// <param name="fitnessFunction">The function defining the interpolation direction.</param>
        /// <param name="lowerBound">The lower bound.</param>
        /// <param name="upperBound">The upper bound.</param>
        public static long LinearInterpolation(
            Func<long, long> interpolatingFunction, 
            Func<long, InterpolatorDirection> fitnessFunction,
            long lowerBound, 
            long upperBound)
        {
            if (upperBound == lowerBound) return upperBound;

            long middlePoint = (long)Math.Floor((upperBound + lowerBound) / 2d);
            long result = interpolatingFunction(middlePoint);
            var fitness = fitnessFunction(result);

            return fitness switch
            {
                InterpolatorDirection.Up => LinearInterpolation(interpolatingFunction, fitnessFunction, middlePoint + 1, upperBound),
                InterpolatorDirection.Down => LinearInterpolation(interpolatingFunction, fitnessFunction, lowerBound, middlePoint - 1),
                InterpolatorDirection.GoodEnough => middlePoint,
                _ => throw new NotImplementedException(),
            };
        }

        /// <summary>
        /// Finds the minima of the given linear function.
        /// It works by iterating the function until the value stops decreasing; halves and inverts the step repeats.
        /// In this way the minima is found with complexity logN.
        /// </summary>
        /// <param name="function">The function to minimize.</param>
        /// <param name="start">The starting value.</param>
        /// <param name="step">The initial step. The higher, the better.</param>
        public static long FindMinimaInLinearFunction(Func<long, long> function, long start, long step)
        {
            long currentMinimum = start;
            bool positiveMinimumStep = false;
            bool negativeMinimumStep = false;
            do
            {
                positiveMinimumStep = step == 1 || positiveMinimumStep;
                negativeMinimumStep = step == -1 || negativeMinimumStep;
                currentMinimum = FindMinimaInLinearFunctionHelper(function, currentMinimum, step);
                step = (int)Math.Ceiling(Math.Abs(step) / 2d) * -Math.Sign(step); // basically a fancy version of 'step/-2'
            }
            while (!(negativeMinimumStep && positiveMinimumStep));

            return currentMinimum;
        }

        private static long FindMinimaInLinearFunctionHelper(Func<long, long> function, long start, long step)
        {
            var current = start;
            var lastResult = function(start);
            current += step;
            long nextResult;

            while ((nextResult = function(current)) <= lastResult)
            {
                lastResult = nextResult;
                current += step;
            }

            return current - step;
        }
    }
}
