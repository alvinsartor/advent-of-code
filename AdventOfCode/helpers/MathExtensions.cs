﻿using System;

namespace AdventOfCode.helpers
{
    public static class MathExtensions
    {
        /// <summary>
        /// Calculates the GCD for the two given numbers.
        /// </summary>
        /// <param name="a">The first number.</param>
        /// <param name="b">The second number.</param>
        public static long GreatestCommonDivisor(long a, long b) => 
            a == 0 ? b : b == 0 ? a :
            a == b ? a : a > b
                    ? GreatestCommonDivisor(a - b, b)
                    : GreatestCommonDivisor(a, b - a);

        /// <summary>
        /// Calculates the GCD for the two given numbers without the use of recursion.
        /// </summary>
        /// <param name="a">The first number.</param>
        /// <param name="b">The second number.</param>
        public static long FlatGreatestCommonDivisor(long a, long b)
        {
            if (a < b) a *= -1;
            if (b == 0) return a;
            while (a % b != 0)
            {
                a += b;
                b = a - b;
                a -= b;
                b %= a;
            }
            return b;
        }

        /// <summary>
        /// Calculates the LCM for the two given numbers.
        /// </summary>
        /// <param name="a">The first number.</param>
        /// <param name="b">The second number.</param>
        public static long LeastCommonMultiple(long a, long b) => a * b / FlatGreatestCommonDivisor(a, b);

        /// <summary>
        /// Returns the angle between the origin and the two rays.
        /// </summary>
        public static double Angle(Point origin, Point ray1, Point ray2) => Angle(ray1 - origin, ray2 - origin);

        /// <summary>
        /// Returns the angle between the two rays (relative to the origin).
        /// </summary>
        public static double Angle(Point ray1, Point ray2)
        {
            var dot = ray1.X * ray2.X + ray1.Y * ray2.Y;
            var determinant = ray1.X * ray2.Y - ray1.Y * ray2.X;
            return Math.Atan2(determinant, dot);
        }

        public static double AngleToClockwiseValue(double angle)
        {
            var zeroTo2PiValue = angle >= 0 ? angle : Math.PI + Math.PI + angle;
            return zeroTo2PiValue / (Math.PI * 2);
        }
    }
}
