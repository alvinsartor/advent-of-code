using NUnit.Framework;
using System;

namespace AdventOfCode.helpers.NUnit
{
    [TestFixture]
    internal sealed class MathExtensionsFixture
    {
        [TestCase(6, 2, ExpectedResult = 2)]
        [TestCase(100, 18, ExpectedResult = 2)]
        [TestCase(12, 18, ExpectedResult = 6)]
        [TestCase(99, 13, ExpectedResult = 1)]
        public long TestGcd(long n1, long n2) => MathExtensions.GreatestCommonDivisor(n1, n2);

        [TestCase(6, 2, ExpectedResult = 2)]
        [TestCase(100, 18, ExpectedResult = 2)]
        [TestCase(12, 18, ExpectedResult = 6)]
        [TestCase(99, 13, ExpectedResult = 1)]
        public long TestFlatGcd(long n1, long n2) => MathExtensions.FlatGreatestCommonDivisor(n1, n2);

        [Test]
        public void TestAngle()
        {
            Assert.AreEqual(0, MathExtensions.Angle(Point.Origin, new Point(0, -1), new Point(0, -1)), 0.001);
            Assert.AreEqual(Math.PI / 4, MathExtensions.Angle(Point.Origin, new Point(0, -1), new Point(1, -1)), 0.001);
            Assert.AreEqual(Math.PI / 2, MathExtensions.Angle(Point.Origin, new Point(0, -1), new Point(1, 0)), 0.001);
            Assert.AreEqual(Math.PI * 3 / 4, MathExtensions.Angle(Point.Origin, new Point(0, -1), new Point(1, 1)), 0.001);
            Assert.AreEqual(Math.PI, MathExtensions.Angle(Point.Origin, new Point(0, -1), new Point(0, 1)), 0.001);
            Assert.AreEqual(-Math.PI * 3 / 4, MathExtensions.Angle(Point.Origin, new Point(0, -1), new Point(-1, 1)), 0.001);
            Assert.AreEqual(-Math.PI / 2, MathExtensions.Angle(Point.Origin, new Point(0, -1), new Point(-1, 0)), 0.001);
            Assert.AreEqual(-Math.PI / 4, MathExtensions.Angle(Point.Origin, new Point(0, -1), new Point(-1, -1)), 0.001);
        }

        [TestCase(0, ExpectedResult = 0)]
        [TestCase(Math.PI / 4, ExpectedResult = 0.125)]
        [TestCase(Math.PI / 2, ExpectedResult = 0.25)]
        [TestCase(Math.PI * 3 / 4, ExpectedResult = 0.375)]
        [TestCase(Math.PI, ExpectedResult = 0.5)]
        [TestCase(-Math.PI / 2, ExpectedResult = 0.75)]
        [TestCase(-Math.PI / 4, ExpectedResult = 0.875)]
        [TestCase(-Math.PI * 3 / 4, ExpectedResult = 0.625)]
        public double TestClockwiseAngle(double angle) => MathExtensions.AngleToClockwiseValue(angle);
    }
}