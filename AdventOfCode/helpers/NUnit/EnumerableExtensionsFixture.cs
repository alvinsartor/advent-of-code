using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace AdventOfCode.helpers.NUnit
{
    [TestFixture]
    internal sealed class EnumerableExtensionsFixture
    {
        [Test]
        public void LongRangeReturnsTheExpectedValues()
        {
            var range = EnumerableExtensions.Range(0, 5).ToArray();
            Assert.Contains(0, range);
            Assert.Contains(1, range);
            Assert.Contains(2, range);
            Assert.Contains(3, range);
            Assert.Contains(4, range);
        }
    }
}