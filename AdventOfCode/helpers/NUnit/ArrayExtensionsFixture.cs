using NUnit.Framework;
using System.Linq;

namespace AdventOfCode.helpers.NUnit
{
    [TestFixture]
    internal sealed class ArrayExtensionsFixture
    {
        private static char[][] TestArray() =>
            new char[][] {
                new char[] { 'c', 'a', 't' },
                new char[] { 'd', 'o', 'g' },
                new char[] { 'b', 'o', 'b' },
            };

        [Test]
        public void ArrayCanBeFlippedHorizontlly()
        {
            var flipped = TestArray().FlipHorizontally();
            Assert.AreEqual("tac", new string(flipped[0]));
            Assert.AreEqual("god", new string(flipped[1]));
            Assert.AreEqual("bob", new string(flipped[2]));
        }

        [Test]
        public void ArrayCanBeFlippedVertically()
        {
            var flipped = TestArray().FlipVertically();
            Assert.AreEqual("bob", new string(flipped[0]));
            Assert.AreEqual("dog", new string(flipped[1]));
            Assert.AreEqual("cat", new string(flipped[2]));
        }

        [Test]
        public void ArrayCanBeTransposed()
        {
            var transposed = TestArray().Transposed();
            Assert.AreEqual("cdb", new string(transposed[0]));
            Assert.AreEqual("aoo", new string(transposed[1]));
            Assert.AreEqual("tgb", new string(transposed[2]));

            // transposing again brings it back to the original state
            transposed = transposed.Transposed();
            Assert.AreEqual(transposed, TestArray());
        }

        [Test]
        public void ArrayCanBeRotated()
        {
            var rotated = TestArray().Rotate();
            Assert.AreEqual("bdc", new string(rotated[0]));
            Assert.AreEqual("ooa", new string(rotated[1]));
            Assert.AreEqual("bgt", new string(rotated[2]));
            
            rotated = rotated.Rotate();
            Assert.AreEqual("bob", new string(rotated[0]));
            Assert.AreEqual("god", new string(rotated[1]));
            Assert.AreEqual("tac", new string(rotated[2]));
            
            rotated = rotated.Rotate();
            Assert.AreEqual("tgb", new string(rotated[0]));
            Assert.AreEqual("aoo", new string(rotated[1]));
            Assert.AreEqual("cdb", new string(rotated[2]));

            // Rotating again brings it back to the original state
            rotated = rotated.Rotate();
            Assert.AreEqual(rotated, TestArray());
        }

        [Test]
        public void ArrayCanBePermutated()
        {
            var array = new char[] { 'c', 'a', 't' };
            var permutations = array.Permutations();

            Assert.AreEqual(permutations.Count(), 6);

            var toStrings = permutations.Select(arr => new string(arr)).ToList();
            Assert.Contains("cat", toStrings);
            Assert.Contains("cta", toStrings);
            Assert.Contains("act", toStrings);
            Assert.Contains("atc", toStrings);
            Assert.Contains("tac", toStrings);
            Assert.Contains("tca", toStrings);
        }
    }
}