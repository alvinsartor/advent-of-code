using NUnit.Framework;

namespace AdventOfCode.helpers.NUnit
{
    [TestFixture]
    internal sealed class GeneralHelpersFixture
    {
        [TestCase(-100, 1)]
        [TestCase(100, -1)]
        [TestCase(-99, 2)]
        [TestCase(-10, 3)]
        [TestCase(10, -3)]
        [TestCase(33, -10)]
        public void ExactMinimaIsFoundIndependentlyByTheStartingConditions(long start, long step)
        {
            static long function(long x) => x * x; // minima at 0
            var minima = GeneralHelpers.FindMinimaInLinearFunction(function, start, step);
            Assert.AreEqual(0, minima);
        }
    }
}