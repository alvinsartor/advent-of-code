﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.helpers
{
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Finds the minimum of the collection, using the specified formula to extract the comparable value.
        /// </summary>
        /// <param name="enumerable">The enumerable to find the minimum of.</param>
        /// <param name="comparable">The function used to extract the comparable value from each element.</param>
        public static T MinBy<T>(this IEnumerable<T> enumerable, Func<T, IComparable> comparable)
        {
            var list = enumerable.ToList();
            var minimum = list[0];
            var minimumComparable = comparable(minimum);
            foreach(var element in list)
            {
                var elementComparable = comparable(element);
                if (elementComparable.CompareTo(minimumComparable) < 0)
                {
                    minimum = element;
                    minimumComparable = elementComparable;
                }
            }

            return minimum;
        }

        /// <summary>
        /// Finds the maximum of the collection, using the specified formula to extract the comparable value.
        /// </summary>
        /// <param name="enumerable">The enumerable to find the maximum of.</param>
        /// <param name="comparable">The function used to extract the comparable value from each element.</param>
        public static T MaxBy<T>(this IEnumerable<T> enumerable, Func<T, IComparable> comparable)
        {
            var list = enumerable.ToList();
            var maximum = list[0];
            var maximumComparable = comparable(maximum);
            foreach(var element in list)
            {
                var elementComparable = comparable(element);
                if (elementComparable.CompareTo(maximumComparable) > 0)
                {
                    maximum = element;
                    maximumComparable = elementComparable;
                }
            }

            return maximum;
        }

        /// <summary>
        /// Gets the index of the maximum element of the list.
        /// </summary>
        /// <param name="list">The list to search.</param>
        public static int IndexOfMax<T>(this IList<T> list) where T: IComparable
        {
            var maximum = list[0];
            var maxIndex = 0;
            for (var i = 1; i < list.Count; i++)
            {
                var element = list[i];
                if (element.CompareTo(maximum) > 0)
                {
                    maximum = element;
                    maxIndex = i;
                }
            }

            return maxIndex;
        }

        /// <summary>
        /// Converts the given enumberable to a stack.
        /// </summary>
        /// <param name="enumerable">The enumerable to convert to stack.</param>
        public static Stack<T> ToStack<T>(this IEnumerable<T> enumerable)
        {
            Stack<T> result = new Stack<T>();
            foreach (T element in enumerable.Reverse()) result.Push(element);
            return result;
        }

        /// <summary>
        /// Converts the given enumberable to a stack in reverse order.
        /// </summary>
        /// <param name="enumerable">The enumerable to convert to stack.</param>
        public static Stack<T> ToReversedStack<T>(this IEnumerable<T> enumerable)
        {
            Stack<T> result = new Stack<T>();
            foreach (T element in enumerable) result.Push(element);
            return result;
        }

        /// <summary>
        /// Executes the specified action to all elements of the given enumerable.
        /// </summary>
        /// <param name="enumerable">The enumerable the action will be executed on.</param>
        /// <param name="action">The action to execute.</param>
        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            foreach (var item in enumerable) 
                action(item);
        }

        /// <summary>
        /// Converts the enumerable to a linkedlist.
        /// </summary>
        /// <param name="enumerable">The enumerable to convert.</param>
        public static LinkedList<T> ToLinkedList<T>(this IEnumerable<T> enumerable)
        {
            var result = new LinkedList<T>();
            enumerable.ForEach(t => result.AddLast(t));
            return result;
        }

        /// <summary>
        /// Gets the next node on a circular LinkedList.
        /// </summary>
        public static LinkedListNode<T>? CircularNext<T>(this LinkedListNode<T> current) => current.Next ?? current.List!.First;

        /// /// <summary>
        /// Gets the next node on a circular LinkedList.
        /// </summary>
        public static LinkedListNode<T>? CircularPrev<T>(this LinkedListNode<T> current) => current.Previous ?? current.List!.Last;

        /// <summary>
        /// Gets the minimum and max values of the given collection of points.
        /// </summary>
        public static (double minX, double minY, double maxX, double maxY) GetBoundingBox(this IEnumerable<Point> points)
        {
            double minX = double.MaxValue;
            double minY = double.MaxValue;
            double maxX = double.MinValue;
            double maxY = double.MinValue;

            foreach (var pt in points)
            {
                if (pt.X < minX) minX = pt.X;
                else if (pt.X > maxX) maxX = pt.X;
                if (pt.Y < minY) minY = pt.Y;
                else if (pt.Y > maxY) maxY = pt.Y;
            }

            return (minX, minY, maxX, maxY);
        }

        /// <summary>
        /// A range using longs. No discrimination here.
        /// </summary>
        public static IEnumerable<long> Range(long start, long count)
        {
            while (count-- > 0)
                yield return start++;
        }
    }
}
