﻿using System;
using System.Linq;

namespace AdventOfCode.helpers
{
    public static class StringExtensions
    {
        /// <summary>
        /// Returns the reverse string.
        /// </summary>
        /// <param name="s">The string to reverse.</param>
        public static string StringReverse(this string s) => new string(s.Reverse().ToArray());

    }
}
