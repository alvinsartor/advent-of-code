﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.helpers
{
    public static class ArrayExtensions
    {
        /// <summary>
        /// Given a NxM array, it returns the relative transposed MxN array.
        /// </summary>
        /// <param name="array">The two dimensional jagged array to transpose.</param>
        public static T[][] Transposed<T>(this T[][] array)
        {
            var result = new T[array[0].Length][];
            for (int i = 0; i < result.Length; i++)
                result[i] = new T[array.Length];

            for (int i = 0; i < array.Length; i++)
                for (int j = 0; j < array[i].Length; j++)
                    result[j][i] = array[i][j];

            return result;
        }

        /// <summary>
        /// Given an array, it returns a rotated copy.
        /// </summary>
        /// <param name="array">The two dimensional jagged array to rotate.</param>
        public static T[][] Rotate<T>(this T[][] array)
        {
            var result = new T[array[0].Length][];
            for (int i = 0; i < result.Length; i++)
                result[i] = new T[array.Length];

            for (int i = 0; i < array.Length; i++)
                for (int j = 0; j < array[i].Length; j++)
                    result[i][j] = array[array.Length - j - 1][i];

            return result;
        }

        /// <summary>
        /// Given a jaggeed array, it returns a diagonally flipped copy.
        /// </summary>
        /// <param name="array">The two dimensional jagged array to flip.</param>
        public static T[][] FlipHorizontally<T>(this T[][] array) =>
            array.Select(x => x.Reverse().ToArray()).ToArray();

        /// <summary>
        /// Given a jaggeed array, it returns a diagonally flipped copy.
        /// </summary>
        /// <param name="array">The two dimensional jagged array to flip.</param>
        public static T[][] FlipVertically<T>(this T[][] array)
        {
            var result = new T[array.Length][];
            for (int i = 0; i < result.Length; i++) result[i] = new T[array[i].Length];

            for (int i = 0; i < array.Length; i++)
                for (int j = 0; j < array.Length; j++)
                    result[i][j] = array[array.Length - i - 1][j];

            return result;
        }

        /// <summary>
        /// Returns the cartesian product string of the given array.
        /// This will consist on the first element x the second x the third and so on..
        /// </summary>
        /// <param name="array">The array to extract the cartesian product of.</param>
        /// <param name="stringifier">The function to extract a string from each array element.</param>
        public static IEnumerable<string> CartesianProduct<T>(
            this T[] array,
            Func<T, IEnumerable<string>> stringifier)
        {
            if (array.Length == 1) return stringifier(array[0]);

            var firstElement = stringifier(array[0]);
            var rest = CartesianProduct(array[1..], stringifier);
            return firstElement.SelectMany(p2 => rest, (p1, p2) => p1 + p2);
        }

        /// <summary>
        /// Returns a list containing all the possible permutations of the given array.
        /// </summary>
        /// <param name="array">The array to be permutated.</param>
        public static List<T[]> Permutations<T>(this T[] array)
        {
            var result = new List<T[]>();
            HeapPermutation(array, array.Length, result);
            return result;
        }

        private static void HeapPermutation<T>(T[] array, int size, List<T[]> result)
        {
            if (size == 1)
            {
                result.Add(array.ToArray());
                return;
            }
            for (int i = 0; i < size; i++)
            {
                HeapPermutation(array, size - 1, result);
                if (size % 2 == 1) Swap(array, 0, size - 1);
                else Swap(array, i, size - 1);
            }
        }

        private static void Swap<T>(T[] array, int i1, int i2) =>
            (array[i1], array[i2]) = (array[i2], array[i1]);

        /// <summary>
        /// Returns all the possible combinations given the specified elements in the array.
        /// </summary>
        /// <param name="array">The array to extract the combinations from.</param>
        public static IEnumerable<List<T>> Combinations<T>(this T[] array)
        {
            long max = (long)Math.Pow(2, array.Length);
            for(int i = 0; i < max; i++)
            {
                var binary = Convert.ToString(i, 2).PadLeft(8, '0');
                yield return array.Where((x, i) => binary[i] == '1').ToList();
            }
        }
    }
}
