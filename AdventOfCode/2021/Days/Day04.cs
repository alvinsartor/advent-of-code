﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2021.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2021/day/4

    public class Day04 : DailyChallenge
    {
        /// <summary>
        /// Cheat on a Bingo match against the squid by finding the board that will win first.
        /// </summary>
        public override string Part1()
        {
            var numbers = ExtractNumbersFromInput(input);
            var boards = ExtractBingoBoardsFromInput(input);

            var score = 0;
            var indexNumbers = 0;
            var indexBoards = 0;
            while (score == 0)
            {
                score = boards[indexBoards].PlayNumber(numbers[indexNumbers]);

                indexBoards++;
                if (indexBoards >= boards.Length) { 
                    indexBoards = 0; 
                    indexNumbers++; 
                }
            }

            return score.ToString();
        }

        /// <summary>
        /// Be nice and let the squid win the Bingo match by picking the board that will win last.
        /// </summary>
        public override string Part2()
        {
            var numbers = ExtractNumbersFromInput(input);
            var boards = ExtractBingoBoardsFromInput(input).ToList();
            
            var score = 0;
            var indexNumbers = 0;
            var indexBoards = 0;
            while (boards.Count > 0)
            {
                score = boards[indexBoards].PlayNumber(numbers[indexNumbers]);
                if (score > 0) { boards.RemoveAt(indexBoards); indexBoards--; }

                indexBoards++;
                if (indexBoards >= boards.Count)
                {
                    indexBoards = 0;
                    indexNumbers++;
                }
            }

            return score.ToString();
        }

        private static int[] ExtractNumbersFromInput(string[] input) => 
            input[0].Split(',').Select(int.Parse).ToArray();

        private static BingoBoard[] ExtractBingoBoardsFromInput(string[] input)
        {
            List<BingoBoard> list = new();
            for (int r = 2; r < input.Length; r += 6)
            {
                var upperBound = r + 5;
                list.Add(new BingoBoard(input[r..upperBound]));
            }
            return list.ToArray();
        }
        
        private class BingoBoard
        {
            private readonly Dictionary<int, (int row, int col)> numbers;
            private readonly int[][] board;
            private readonly bool[][] marked;

            public BingoBoard(string[] rawBoard)
            {
                numbers = new Dictionary<int, (int row, int col)>();
                board = new int[5][];
                marked = new bool[5][];

                for(var row = 0; row < 5; row++)
                {
                    board[row] = rawBoard[row].Replace("  ", " ").Trim().Split(' ').Select(int.Parse).ToArray();
                    marked[row] = new bool[5];
                    for (var column = 0; column < 5; column++) numbers.Add(board[row][column], (row, column));
                }
            }

            /// <summary>
            /// Marks the number on the board, if possible.
            /// If it was the final move and the board won, it returns the score, otherwise it returns 0.
            /// </summary>
            public int PlayNumber(int number)
            {
                if (!numbers.ContainsKey(number)) return 0;

                var (row, col) = numbers[number];
                marked[row][col] = true;

                if (CheckRowForWin(row) || CheckColForWin(col))
                    return CalculateScore(number);

                return 0;
            }

            private bool CheckRowForWin(int row) => marked[row].All(x => x);
            private bool CheckColForWin(int col) => Enumerable.Range(0, 5).All(i => marked[i][col]);
            
            private int CalculateScore(int lastNumber)
            {
                var sumUnmarked = 0;
                for (var row = 0; row < 5; row++)
                    for (var column = 0; column < 5; column++)
                        if (!marked[row][column])
                            sumUnmarked += board[row][column];

                return sumUnmarked * lastNumber;
            }
        }
    }
}
