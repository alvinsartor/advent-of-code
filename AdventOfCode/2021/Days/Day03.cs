﻿using AdventOfCode.helpers;
using System;
using System.Linq;

namespace AdventOfCode._2021.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2021/day/3

    public class Day03 : DailyChallenge
    {
        /// <summary>
        /// Extract gammma and epsilon from the list of binary values and multiply them.
        /// </summary>
        public override string Part1()
        {
            var mostAndLeastCommon = Enumerable
                .Range(0, input[0].Length)
                .Select(i => ExtractMostAndLeastCommon(input, i))
                .ToArray();

            var gamma = new string(mostAndLeastCommon.Select(n => n.mostCommon).ToArray());
            var epsilon = new string(mostAndLeastCommon.Select(n => n.leastCommon).ToArray());
            var powerConsumption = Convert.ToInt32(gamma, 2) * Convert.ToInt32(epsilon, 2);

            return powerConsumption.ToString();
        }

        /// <summary>
        /// Extract the oxygen generator r. and CO2 scrubbing r. from the list of binary values and multiply them.
        /// </summary>
        public override string Part2()
        {
            var oxygenGeneratorRating = GetOxygenGeneratorRating(input);
            var co2ScrubberRating = GetCo2ScrabbingRating(input);
            var LifeSupportRating = Convert.ToInt32(oxygenGeneratorRating, 2) * Convert.ToInt32(co2ScrubberRating, 2);

            return LifeSupportRating.ToString();
        }

        private static (char mostCommon, char leastCommon) ExtractMostAndLeastCommon(string[] input, int index)
        {
            int[] counter = new int[2];
            input.ForEach(s => { counter[s[index] - '0']++; });
            return counter[0] > counter[1] ? ('0', '1') : ('1', '0');
        }

        private static string GetOxygenGeneratorRating(string[] input)
        {
            int index = 0;
            while(input.Length > 1)
            {
                (char mostCommon, _) = ExtractMostAndLeastCommon(input, index);
                input = input.Where(s => s[index] == mostCommon).ToArray();
                index++;
            }

            return input[0];
        }

        private static string GetCo2ScrabbingRating(string[] input)
        {
            int index = 0;
            while (input.Length > 1)
            {
                (_, char leastCommon) = ExtractMostAndLeastCommon(input, index);
                input = input.Where(s => s[index] == leastCommon).ToArray();
                index++;
            }

            return input[0];
        }
    }
}
