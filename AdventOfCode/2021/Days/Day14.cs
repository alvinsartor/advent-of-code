﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdventOfCode._2021.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2021/day/14

    public class Day14 : DailyChallenge
    {
        /// <summary>
        /// Expand the polymer for a few iterations.
        /// </summary>
        public override string Part1() => RepeatReactionsNTimes(input, 10).ToString();

        /// <summary>
        /// Even more steps!
        /// </summary>
        public override string Part2() => RepeatReactionsNTimes(input, 40).ToString();

        private static long RepeatReactionsNTimes(string[] input, int n)
        {
            Dictionary<string, long> polymer = GetPolymer(input[0]);
            Dictionary<string, (string, string)> reactionsTable = GetReactionsTable(input[2..]);
            for (var i = 0; i < n; i++)
                polymer = ApplyReactionsStep(polymer, reactionsTable);

            var counter = CountOccurrencies(polymer);
            counter[input[0][^1]]++; // add last char as it is not counted
            var sortedOccurrencies = counter.Select(x => (reagent: x.Key, count: x.Value))
                .OrderBy(x => x.count)
                .ToArray();

            return (sortedOccurrencies[^1].count - sortedOccurrencies[0].count);
        }

        private static Dictionary<string, long> GetPolymer(string initialPolymer)
        {
            Dictionary<string, long> result = new();
            for (int i = 0; i < initialPolymer.Length - 1; i++)
            {
                var j = i + 2;
                var reagents = initialPolymer[i..j];
                AddToDictionary(result, reagents, 1);    
            }

            return result;
        }

        private static Dictionary<string, (string, string)> GetReactionsTable(string[] input) =>
            input.Select(ExtractReaction).ToDictionary(l => l.reagents, l => (l.reaction1, l.reaction2));

        private static (string reagents, string reaction1, string reaction2) ExtractReaction(string line)
        {
            var split = line.Split(" -> ");
            return (split[0][0].ToString() + split[0][1].ToString(), 
                    split[0][0].ToString() + split[1][0].ToString(),
                    split[1][0].ToString() + split[0][1].ToString());
        }


        private static Dictionary<string, long> ApplyReactionsStep(
            Dictionary<string, long> polymer, 
            Dictionary<string, (string, string)> reactionsTable)
        {
            Dictionary<string, long> nextPolymer = new();
            foreach(var reagents in polymer.Keys)
            {
                var amount = polymer[reagents];
                (string reaction1, string reaction2) = reactionsTable[reagents];
                AddToDictionary(nextPolymer, reaction1, amount);
                AddToDictionary(nextPolymer, reaction2, amount);
            }

            return nextPolymer;
        }

        private static Dictionary<char, long> CountOccurrencies(Dictionary<string, long> polymer)
        {
            Dictionary<char, long> counter = new();
            foreach (string reaction in polymer.Keys) AddToDictionary(counter, reaction[0], polymer[reaction]);
            return counter;
        }

        private static void AddToDictionary<T>(Dictionary<T, long> dictionary, T key, long amount) where T: notnull
        {
            if (dictionary.ContainsKey(key)) dictionary[key] += amount;
            else dictionary.Add(key, amount);
        }
    }
}
