﻿using System;
using System.Linq;

namespace AdventOfCode._2021.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2021/day/07

    public class Day07: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 10;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 10;

        /// <summary>
        /// Calculate the optimal position for the crab submarines to move to in order to align.
        /// </summary>
        public override string Part1()
        {
            var positions = input[0].Split(',').Select(int.Parse).ToArray();
            var minSteps = Enumerable.Range(0, positions.Max()).Min(p => LinearFuelConsumption(p, positions));
            return minSteps.ToString();
        }

        /// <summary>
        /// Same as above, but using another formula to calculate the fuel consumption.
        /// </summary>
        public override string Part2()
        {
            var positions = input[0].Split(',').Select(int.Parse).ToArray();
            var max = positions.Max();
            var consumptions = CalculateFuelConsumptionTable(max);

            var minSteps = Enumerable.Range(0, max).Min(p => IncreasingFuelConsumption(p, positions, consumptions));
            return minSteps.ToString();
        }

        private static int LinearFuelConsumption(int position, int[] positions) =>
            positions.Select(p => Math.Abs(position - p)).Sum();

        private static int IncreasingFuelConsumption(int position, int[] positions, int[] consumptions) =>
            positions.Select(p => consumptions[Math.Abs(position - p)]).Sum();

        private static int[] CalculateFuelConsumptionTable(int upperBond)
        {
            int[] table = new int[upperBond + 1];
            for (int i = 1; i < table.Length; i++) table[i] = table[i - 1] + i;
            return table;
        }
    }
}
