﻿using System.Collections.Generic;
using System.Linq;
using AdventOfCode.helpers;
using Graph;
using Graph.PathFinding;

namespace AdventOfCode._2021.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2021/day/15

    public class Day15: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 10;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 1;

        /// <summary>
        /// Find the safest path to get to the end of the cave.
        /// </summary>
        public override string Part1()
        {
            var processedInput = ProcessInput(input);
            var graph = GetGraph(processedInput);
            var startPoint = new Point(0, 0);
            var endPoint = new Point(processedInput[0].Length - 1, processedInput.Length - 1);
            var path = graph.AStarSearch(
                startPoint, 
                endPoint,
                (p) => p.Distance(endPoint)
            ).Result;
            
            return path.Cost.ToString();
        }

        /// <summary>
        /// Find the safest path like above, but the cave is 5x bigger!
        /// </summary>
        public override string Part2()
        {
            var processedInput = ProcessInput(input);
            var enlargedInput = EnlargeInput(processedInput);
            var graph = GetGraph(enlargedInput);
            var startPoint = new Point(0, 0);
            var endPoint = new Point(enlargedInput[0].Length - 1, enlargedInput.Length - 1);
            var path = graph.AStarSearch(
                startPoint,
                endPoint,
                (p) => p.Distance(endPoint)
            ).Result;

            return path.Cost.ToString();
        }

        private static int[][] ProcessInput(string[] input) => input.Select(ProcessInputLine).ToArray();
        private static int[] ProcessInputLine(string line) => line.Select(c => c - '0').ToArray();

        private static Graph<Point> GetGraph(int[][] input)
        {
            int GetDangerLevel(Point p) => input[(int)p.Y][(int)p.X];
            
            Graph<Point> graph = new();
            for (int y = 0; y < input.Length; y++)
                for (int x = 0; x < input[0].Length; x++)
                    graph.Add(new Point(x, y));

            Node<Point>? nodeOrNull(Point p) => graph.TryGetNode(p, out var node) ? node : null;
            void TryToConnect(Node<Point> p1, Node<Point>? p2, int w21)
            {
                if (p2 == null) return; 
                p1.AddBidirectionalConnection(p2, GetDangerLevel(p2.Value), w21);
            }

            for (int y = 0; y < input.Length; y++)
                for (int x = 0; x < input[0].Length; x++)
                {
                    var pt = new Point(x, y);
                    var ptDangerLevel = GetDangerLevel(pt);

                    var rightPt = pt + Point.Right;
                    TryToConnect(graph[pt], nodeOrNull(rightPt), ptDangerLevel);

                    var downPt = pt + Point.Down;
                    TryToConnect(graph[pt], nodeOrNull(downPt), ptDangerLevel);
                }

            return graph;
        }

        private static int[][] EnlargeInput(int[][] input)
        {
            int height = input.Length;
            int width = input[0].Length;

            static int Modda(int n) => n <= 9 ? n : (n % 9) + 1;
            int GetOriginal(int row, int col) => input[row % height][col % width];

            var output = new int[height * 5][];
            for (int r = 0; r < output.Length; r++)
            {
                output[r] = new int[width * 5];
                var rMultiplier = r / height;
                for (int c = 0; c < output[r].Length; c++)
                {
                    var cMultiplier = c / height;
                    output[r][c] = Modda(GetOriginal(r, c) + rMultiplier + cMultiplier);
                }
            }

            return output;
        }
    }
}
