﻿using System;
using System.Linq;

namespace AdventOfCode._2021.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2021/day/1

    public class Day01 : DailyChallenge
    {
        /// <summary>
        ///
        /// </summary>
        public override string Part1()
        {
            var measuredDepth = input.Select(int.Parse).ToArray();
            var decreases = Enumerable
                .Range(0, measuredDepth.Length - 1)
                .Count(i => measuredDepth[i] < measuredDepth[i + 1]);

            return decreases.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        public override string Part2()
        {
            var measuredDepth = input.Select(int.Parse).ToArray();
            var slidingWindow = Enumerable
                .Range(2, measuredDepth.Length - 2)
                .Select(i => measuredDepth[i-2] + measuredDepth[i - 1] + measuredDepth[i])
                .ToArray();
            var decreases = Enumerable
                .Range(0, slidingWindow.Length - 1)
                .Count(i => slidingWindow[i] < slidingWindow[i + 1]);

            return decreases.ToString();
        }
    }
}
