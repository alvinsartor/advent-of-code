﻿using AdventOfCode.helpers;
using System.Linq;

namespace AdventOfCode._2021.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2021/day/6

    public class Day06 : DailyChallenge
    {

        /// <summary>
        /// Simulate how many lanterfishes there will be in 80 days.
        /// </summary>
        public override string Part1()
        {
            var fishSchool = DecodeInput(input[0]);
            for (int i = 0; i < 80; i++) ApplyDayOfLife(fishSchool);
            return fishSchool.Sum().ToString();
        }

        /// <summary>
        /// Simulate how many lanterfishes there will be in 256 days.
        /// </summary>
        public override string Part2()
        {
            var fishSchool = DecodeInput(input[0]);
            for (int i = 0; i < 256; i++) ApplyDayOfLife(fishSchool);
            return fishSchool.Sum().ToString();
        }

        private static long[] DecodeInput(string input)
        {
            var result = new long[9];
            input.Split(',')
                .Select(long.Parse)
                .GroupBy(x => x)
                .ForEach(x => result[x.Key] = x.Count());
            return result;
        }

        private static void ApplyDayOfLife(long[] fishSchool)
        {
            long readyToBreed = fishSchool[0];
            for (int i = 0; i < fishSchool.Length - 1; i++) fishSchool[i] = fishSchool[i + 1];
            fishSchool[6] += readyToBreed;
            fishSchool[8] = readyToBreed;
        }
    }
}
