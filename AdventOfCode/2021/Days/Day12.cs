﻿using System;
using System.Collections.Generic;
using System.Linq;
using Graph;

namespace AdventOfCode._2021.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2021/day/12

    public class Day12 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 10;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 1;

        static List<string> AllPaths = new();

        private readonly Graph<string> caves;
        public Day12() : base() => caves = DecodeInput(input);

        /// <summary>
        /// Count all the possible paths through the caves.
        /// </summary>
        public override string Part1()
        {
            AllPaths = new List<string>();
            FindAllPaths(caves, "start", "", Part1PathAlgorithm);
            return AllPaths.Count.ToString();
        }

        /// <summary>
        /// Count all the possible paths through the caves, allowing yourself to pass to one small cave twice.
        /// </summary>
        public override string Part2()
        {
            AllPaths = new List<string>();
            FindAllPaths(caves, "start", "", Part2PathAlgorithm, true);
            return AllPaths.Count.ToString();
        }

        private static Graph<string> DecodeInput(string[] input)
        {
            Graph<string> graph = new();
            foreach (var line in input)
            {
                var split = line.Split('-');
                var n1 = graph.TryAdd(split[0]);
                var n2 = graph.TryAdd(split[1]);
                n1.TryAddBidirectionalConnection(n2);
            }
            return graph;
        }

        private static void FindAllPaths(
            Graph<string> caves,
            string start,
            string current,
            Func<string, string, bool> choosingAlgorithm,
            bool allowVisingSmallCavesTwice = false)
        {
            if (allowVisingSmallCavesTwice && char.IsLower(start[0]) && current.Contains(start))
            {
                // we have already visited this small cave, so we add an "X-" at the beginning of the path to mark it
                current = "X-" + current;
            }

            current += "-" + start;
            if (start == "end")
            {
                AllPaths.Add(current);
                return;
            }

            var possibleNextStep = caves[start]
                .GetAllNeighbors().Result
                .Select(x => x.Value)
                .Where(x => choosingAlgorithm(current, x))
                .ToArray();

            foreach (var next in possibleNextStep)
                FindAllPaths(caves, next, current, choosingAlgorithm, allowVisingSmallCavesTwice);
        }

        private static bool Part1PathAlgorithm(string currentPath, string cave) =>
            cave.ToUpper() == cave || !currentPath.Contains(cave);

        private static bool Part2PathAlgorithm(string currentPath, string cave) =>
            cave != "start" &&
            (cave.ToUpper() == cave || !currentPath.Contains(cave) || !HasVisitedSmallCaveTwice(currentPath));

        private static bool HasVisitedSmallCaveTwice(string currentPath) =>
            currentPath.StartsWith("X-");
    }
}
