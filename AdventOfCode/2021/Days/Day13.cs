﻿using AdventOfCode.helpers;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdventOfCode._2021.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2021/day/13

    public class Day13: DailyChallenge
    {
        private readonly HashSet<Point> points;
        private readonly FoldInstruction[] instructions;
        public Day13() : base() => (points, instructions) = DecodeInput(input);

        /// <summary>
        /// Count the dots on the transparent sheet after the first fold instruction.
        /// </summary>
        public override string Part1()
        {
            var foldedOnce = Fold(points, instructions[0]);
            return foldedOnce.Count.ToString();
        }

        /// <summary>
        /// Follow all the instructions and print the result.
        /// </summary>
        public override string Part2()
        {
            (HashSet<Point> points, FoldInstruction[] instructions) = DecodeInput(input);
            instructions.ForEach(i => points = Fold(points, i));
            return PrintImage(points);
        }

        private enum Side { Horizontal, Vertical }
        private struct FoldInstruction {
            public readonly Side Side;
            public readonly int Position;
            public FoldInstruction(char side, int pos)
            {
                Side = side == 'x' ? Side.Vertical : Side.Horizontal;
                Position = pos;
            }
        }

        private static (HashSet<Point> points, FoldInstruction[] instructions) DecodeInput(string[] input)
        {
            int index = -1;
            while (input[++index] != "") ;
            var instructionsIndex = index + 1;

            var points = input[0..index].Select(DecodePoint).ToHashSet();
            var instructions = input[instructionsIndex..^0].Select(DecodeInstruction).ToArray();
            return (points, instructions);
        }

        private static Point DecodePoint(string line)
        {
            var split = line.Split(',');
            return new Point(double.Parse(split[0]), double.Parse(split[1]));
        }

        private static FoldInstruction DecodeInstruction(string line)
        {
            var split = line.Split('=');
            return new FoldInstruction(split[0][^1], int.Parse(split[1]));
        }

        private static HashSet<Point> Fold(HashSet<Point> points, FoldInstruction instruction) =>
            points.Select(p => Fold(p, instruction)).ToHashSet();

        private static Point Fold(Point point, FoldInstruction instruction) =>
            instruction.Side == Side.Horizontal
            ? FoldHorizontally(point, instruction.Position)
            : FoldVertically(point, instruction.Position);

        private static Point FoldHorizontally(Point point, int position)
        {
            if (point.Y < position) return point;
            var distance = point.Y - position;
            return new Point(point.X, point.Y - distance * 2);
        }

        private static Point FoldVertically(Point point, int position)
        {
            if (point.X < position) return point;
            var distance = point.X - position;
            return new Point(point.X - distance * 2, point.Y);
        }

        private static string PrintImage(HashSet<Point> points)
        {
            var builder = new StringBuilder("\n");

            var furthestPoint = new Point(points.Max(p => p.X), points.Max(p => p.Y));
            for (var r = 0; r <= furthestPoint.Y; r++)
            {
                for (var c = 0; c <= furthestPoint.X; c++)
                    if (points.Contains(new Point(c, r)))
                        builder.Append('█');
                    else
                        builder.Append(' ');
                builder.Append('\n');
            }
            builder.Append('\n');

            return builder.ToString();
        }
    }
}
