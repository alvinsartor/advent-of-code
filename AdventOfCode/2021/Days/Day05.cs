﻿using AdventOfCode.helpers;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2021.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2021/day/5

    public class Day05 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 50;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 50;

        /// <summary>
        /// Count the points in which multiple (horizontal and vertical only) vents overlap.
        /// </summary>
        public override string Part1()
        {
            var segments = DecodeInput(input).Where(s => !s.IsOblique);
            var overlapping = CountOverlappingPoints(segments);
            return overlapping.ToString();
        }

        /// <summary>
        /// Count the points in which multiple vents overlap.
        /// </summary>
        public override string Part2()
        {
            var segments = DecodeInput(input);
            var overlapping = CountOverlappingPoints(segments);
            return overlapping.ToString();
        }

        private static int CountOverlappingPoints(IEnumerable<Segment> segments)
        {
            int count = 0;
            var hashset = new HashSet<Point>();
            var duplicates = new HashSet<Point>();
            foreach (var point in segments.SelectMany(s => s.GetPoints()))
                if (!hashset.Add(point))
                {
                    count++;
                    if (duplicates.Add(point))
                        count++;
                }

            return count;
        }

        private static Segment[] DecodeInput(string[] input) => input.Select(DecodeInputLine).ToArray();
        private static Segment DecodeInputLine(string input)
        {
            var split = input.Split(" -> ")
                .SelectMany(partial => partial.Split(','))
                .Select(int.Parse)
                .ToArray();

            return new Segment(new Point(split[0], split[1]), new Point(split[2], split[3]));
        }        

        private record Segment(Point P1, Point P2)
        {
            public bool IsOblique => !(P1.X == P2.X || P1.Y == P2.Y);
            public IEnumerable<Point> GetPoints()
            {
                var vector = (P2 - P1) * (1 / P1.ManhattanDistance(P2));
                if (IsOblique) vector *= 2;                
                var current = P1;

                while (!current.Equals(P2))
                {
                    yield return current;
                    current += vector;
                }

                yield return P2;
            }
        }
    }
}
