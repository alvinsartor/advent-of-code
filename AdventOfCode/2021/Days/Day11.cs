﻿using System;
using System.Linq;

namespace AdventOfCode._2021.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2021/day/11

    public class Day11: DailyChallenge
    {
        /// <summary>
        /// Count how many dumbo octopuses flashes there have been after 100 units of time.
        /// </summary>
        public override string Part1()
        {
            var field = DecodeInput(input);
            var helper = PrepareHelper(field);

            var accumulator = 0;
            for (int i = 0; i < 100; i++) 
                accumulator += FlashesAfterTick(field, helper);

            return accumulator.ToString();
        }

        /// <summary>
        /// Find the first moment in which all octopuses flash simultaneously.
        /// </summary>
        public override string Part2()
        {
            var field = DecodeInput(input);
            var helper = PrepareHelper(field);
            var nrOctopuses = field.Length * field[0].Length;
            var tick = 0;

            while (tick++ < int.MaxValue)
                if (FlashesAfterTick(field, helper) == nrOctopuses)
                    return tick.ToString();
            
            return "-1";
        }

        private static int[][] DecodeInput(string[] input) =>
            input.Select(line => line.ToCharArray().Select(c => c - '0').ToArray()).ToArray();

        private static bool[][] PrepareHelper(int[][] field) =>
            field.Select(l => new bool[l.Length]).ToArray();

        private static int FlashesAfterTick(int[][] field, bool[][] flashed)
        {
            for (var r = 0; r < field.Length; r++)
                for (var c = 0; c < field[r].Length; c++)
                {
                    field[r][c]++;
                    flashed[r][c] = false;
                }

            for (var r = 0; r < field.Length; r++)
                for (var c = 0; c < field[r].Length; c++)
                    if (field[r][c] > 9)
                        TriggerFlash(field, flashed, r, c);

            return flashed.Sum(line => line.Count(x => x));
        }

        private static void TriggerFlash(int[][] field, bool[][] flashed, int r, int c)
        {
            field[r][c] = 0;
            flashed[r][c] = true;
            PropagateFlash(field, flashed, r, c);
        }

        private static void PropagateFlash(int[][] field, bool[][] flashed, int r, int c)
        {
            for (var k = Math.Max(0, r - 1); k < Math.Min(field.Length, r + 2); k++)
                for (var l = Math.Max(0, c - 1); l < Math.Min(field.Length, c + 2); l++)
                {
                    if (!flashed[k][l]) field[k][l]++;
                    if (field[k][l] > 9)
                        TriggerFlash(field, flashed, k, l);
                }
        }
    }
}
