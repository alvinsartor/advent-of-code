﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2021.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2021/day/08

    public class Day08: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 500;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 200;

        /// <summary>
        /// Count the number of displays showing known digits (the ones with a predictable number of segments)
        /// </summary>
        public override string Part1()
        {
            static bool isKnownDigit(string s) => s.Length == 2 || s.Length == 3 || s.Length == 4 || s.Length == 7;

            var decodedInput = DecodeInput(input);
            var countKnownDigits = decodedInput.Sum(line => line.secondPart.Count(isKnownDigit));

            return countKnownDigits.ToString();
        }

        /// <summary>
        /// Decode the input results
        /// </summary>
        public override string Part2()
        {
            var decodedInput = DecodeInput(input);
            var accumulator = 0;

            foreach(var (firstPart, secondPart) in decodedInput)
            {
                var display = SevenSegmentsDisplay.FromRandomizedLine(firstPart);
                var decoded = display.Decode(secondPart);
                accumulator += decoded;
            }

            return accumulator.ToString();
        }

        private static (string[] firstPart, string[] secondPart)[] DecodeInput(string[] input) =>
            input.Select(DecodeInputLine).ToArray();

        private static (string[] firstPart, string[] secondPart) DecodeInputLine(string line)
        {
            var split = line.Split(" | ");
            return (split[0].Split(' '), split[1].Split(' '));
        }

        private class SevenSegmentsDisplay
        {            
            private readonly Dictionary<string, char> Map;
            private SevenSegmentsDisplay(Dictionary<string, char> map) => Map = map;

            public static SevenSegmentsDisplay FromRandomizedLine(string[] line)
            {
                // sort the chars in each word to be able to compare them more easily later on
                var normalized = line.Select(word => new string(word.OrderBy(c => c).ToArray())).ToHashSet();

                // order by length so to quickly find the known digits
                var sorted = normalized.OrderBy(x => x.Length).ToArray();

                // pick the known ones (1: 2segments, 7: 3s, 8: 7s, 4: 4s)
                var one = sorted[0];
                normalized.Remove(one);
                var seven = sorted[1];
                normalized.Remove(seven);
                var four = sorted[2];
                normalized.Remove(four);
                var eight = sorted[9];
                normalized.Remove(eight);

                // three is the only 5s containing the segments of 1
                var three = normalized.Single(c => c.Length == 5 && c.Contains(one[0]) && c.Contains(one[1]));
                normalized.Remove(three);

                // nine is the combination of 3 and 4
                var nine = new string(three.Union(four).OrderBy(x => x).ToArray());
                normalized.Remove(nine);

                // zero is the only 6s containing the segments of 1
                var zero = normalized.Single(c => c.Length == 6 && c.Contains(one[0]) && c.Contains(one[1]));
                normalized.Remove(zero);

                // six is the remaining 6s
                var six = normalized.Single(c => c.Length == 6);
                normalized.Remove(six);

                // Excluding 6 from 8 we obtain the segment belonging to 2, but not to 5
                var topRightSegment = eight.Except(six).Single();
                var two = normalized.Single(d => d.Contains(topRightSegment));
                normalized.Remove(two);

                // The remaining one is 5
                var five = normalized.Single();

                return new SevenSegmentsDisplay(new Dictionary<string, char>
                {
                    { zero, '0' },
                    { one, '1' },
                    { two, '2' },
                    { three, '3' },
                    { four, '4' },
                    { five, '5' },
                    { six, '6' },
                    { seven, '7' },
                    { eight, '8' },
                    { nine, '9' },
                });
            }

            public int Decode(string[] encodedDigits) => 
                int.Parse(new string(DecodeMultipleDigits(encodedDigits)));

            private char[] DecodeMultipleDigits(string[] digits) =>
                digits.Select(DecodeDigit).ToArray();

            private char DecodeDigit(string digitCode) =>
                Map[Normalize(digitCode)];

            private static string Normalize(string s) =>
                new(s.OrderBy(x => x).ToArray());
        }
    }
}
