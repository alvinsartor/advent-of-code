﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2021.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2021/day/10

    public class Day10: DailyChallenge
    {
        /// <summary>
        /// Find the total score of the corrupted lines.
        /// </summary>
        public override string Part1() => input
                .Select(AnalyzeLine)
                .Where(x => x.state == LineState.Corrupted)
                .Select(x => x.score)
                .Sum()
                .ToString();

        /// <summary>
        /// Find the middle score of the incomplete lines.
        /// </summary>
        public override string Part2()
        {
            var incompleteScores = input
                .Select(AnalyzeLine)
                .Where(x => x.state == LineState.Incomplete)
                .Select(x => x.score)
                .OrderBy(x => x)
                .ToArray();

            return incompleteScores[incompleteScores.Length / 2].ToString();
        }

        enum LineState { Corrupted, Incomplete }

        private (LineState state, long score) AnalyzeLine(string line)
        {
            var stack = new Stack<char>();
            foreach (var character in line)
            {
                if (Opens(character)) 
                    // opening parenthesis. We add to the stack
                    stack.Push(character);
                else if (Match(character, stack.Peek())) 
                    // matching closing parenthesis. We pop it from the stack
                    stack.Pop();
                else 
                    // nonmatching parenthesis. Calculate corruption score
                    return (LineState.Corrupted, CorruptionScore(character));               
            }

            return (LineState.Incomplete, IncompleteScore(stack));
        }

        private static bool Opens(char c) => 
            c == '(' || c == '[' || c == '{' || c == '<';

        private static bool Match(char close, char open) => close switch
        {
            ')' => open == '(',
            ']' => open == '[',
            '}' => open == '{',
            '>' => open == '<',
            _ => throw new Exception($"Illegal character: {close}"),
        };

        private static int CorruptionScore(char c) => c switch
        {
            ')' => 3,
            ']' => 57,
            '}' => 1197,
            '>' => 25137,
            _ => throw new Exception($"Illegal character: {c}"),
        };

        private static long IncompleteScore(Stack<char> stack, long currentScore = 0)
        {
            if (stack.Count == 0) return currentScore;
            var newScore = currentScore * 5 + IncompleteScore(stack.Pop());
            return IncompleteScore(stack, newScore);
        }

        private static int IncompleteScore(char c) => c switch
        {
            '(' => 1,
            '[' => 2,
            '{' => 3,
            '<' => 4,
            _ => throw new Exception($"Illegal character: {c}"),
        };
    }
}
