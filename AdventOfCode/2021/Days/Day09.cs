﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2021.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2021/day/09

    public class Day09: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 1000;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 100;

        private static List<(int r, int c)> LowPoints;

        /// <summary>
        /// Find the local minima in the map.
        /// </summary>
        public override string Part1()
        {
            LowPoints = new();
            HashSet<string> coords = new();
            char[][] map = DecodeInput(input);

            bool isLocalMinima(int r, int c)
            {
                var value = map[r][c];
                if (r > 0 && map[r - 1][c] <= value) return false;
                if (r < map.Length - 1 && map[r + 1][c] <= value) return false;
                if (c > 0 && map[r][c - 1] <= value) return false;
                if (c < map[r].Length - 1 && map[r][c + 1] <= value) return false;
                return true;
            }

            for (var r = 0; r < map.Length; r++)
                for (var c = 0; c < map[r].Length; c++)
                    if (isLocalMinima(r, c))
                        LowPoints.Add((r, c));

            return LowPoints.Select(coord => int.Parse(map[coord.r][coord.c].ToString()) + 1).Sum().ToString();
        }

        /// <summary>
        /// Find the sizes of the biggest basins.
        /// </summary>
        public override string Part2()
        {
            char[][] map = DecodeInput(input);
            var basins = LowPoints!
                .Select(coord => FindBasinCoordinatesFrom(map, coord.r, coord.c))
                .Select(b => b.Count)
                .OrderByDescending(b => b)
                .ToArray();
            return (basins[0] * basins[1] * basins[2]).ToString();
        }

        private static char[][] DecodeInput(string[] input) => 
            input.Select(l => l.ToCharArray()).ToArray();

        private HashSet<string> FindBasinCoordinatesFrom(char[][] map, int r, int c)
        {
            var basin = new HashSet<string>();
            FindBasinCoordinatesFrom(map, r, c, basin);
            return basin;
        }

        private void FindBasinCoordinatesFrom(char[][] map, int r, int c, HashSet<string> basin)
        {
            if (map[r][c] == '9') return;
            if (!basin.Add(CoordToString(r, c))) return;

            if (r > 0) FindBasinCoordinatesFrom(map, r - 1, c, basin);
            if (r < map.Length - 1) FindBasinCoordinatesFrom(map, r + 1, c, basin);
            if (c > 0) FindBasinCoordinatesFrom(map, r, c - 1, basin);
            if (c < map[r].Length - 1) FindBasinCoordinatesFrom(map, r, c + 1, basin);
        }

        private static string CoordToString(int r, int c) => $"{r}.{c}";
    }
}
