﻿using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2021.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2021/day/2

    public class Day02 : DailyChallenge
    {
        /// <summary>
        /// Find the final destination of the submarine.
        /// </summary>
        public override string Part1()
        {
            var directions = DecodeInput(input);
            var position = directions.Aggregate(Point.Origin, (curr, next) => curr += next);
            
            var result = position.X * position.Y;
            return result.ToString();
        }

        /// <summary>
        /// Find the final destination of the submarine, with the updated rule.
        /// </summary>
        public override string Part2()
        {
            var directions = DecodeInput(input);
            var position = Point.Origin;
            var aim = 0d;

            foreach (var direction in directions)
            {
                aim += direction.Y;
                position = new Point(position.X + direction.X, position.Y + direction.X * aim); 
            }
            
            var result = position.X * position.Y;
            return result.ToString();
        }

        private static IEnumerable<Point> DecodeInput(string[] input) => input.Select(InstructionToPoint);

        private static Point InstructionToPoint(string instruction)
        {
            var split = instruction.Split(' ');
            var amount = int.Parse(split[1]);
            var direction = split[0];

            return direction switch
            {
                "up" => new Point(0, -amount),
                "down" => new Point(0, amount),
                "forward" => new Point(amount, 0),
                _ => throw new Exception($"Unsupported direction {direction}")
            };
        }
    }
}
