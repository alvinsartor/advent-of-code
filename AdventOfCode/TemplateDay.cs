﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2021.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2021/day/

    public class Day: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 0;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 0;

        /// <summary>
        /// 
        /// </summary>
        public override string Part1() => "NOT IMPLEMENTED";

        /// <summary>
        /// 
        /// </summary>
        public override string Part2() => "NOT IMPLEMENTED";
    }
}
