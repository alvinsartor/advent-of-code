﻿using AdventOfCode.helpers;
using Graph;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/12

    class Day12: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 50;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 50;

        /// <summary>
        /// Find the number of programs that are connected to program 0;
        /// </summary>
        public override string Part1()
        {
            var graph = BuildGraphFromInput(input);
            var connectedTo0 = GetGroupOfGivenNode(graph, 0).Count;
            return connectedTo0.ToString();
        }

        /// <summary>
        /// Count the number of groups.
        /// </summary>
        public override string Part2()
        {
            var graph = BuildGraphFromInput(input);
            var groups = GetAllGroups(graph);
            return groups.Count.ToString();
        }

        private static Graph<int> BuildGraphFromInput(string[] input)
        {
            var graph = new Graph<int>();

            var upperBound = int.Parse(input[^1].Split(" ")[0]);
            Enumerable.Range(0, upperBound + 1).ForEach(n => graph.Add(n));

            foreach(var line in input)
            {
                var split = line.Split(" <-> ");
                var connectorNode = graph[int.Parse(split[0])];
                var connectedNodes = split[1].Split(", ").Select(n => graph[int.Parse(n)]);
                connectedNodes.ForEach(n => n.TryAddBidirectionalConnection(connectorNode));
            }

            return graph;
        }

        private static HashSet<int> GetGroupOfGivenNode(Graph<int> graph, int initialNode)
        {
            var processedNodes = new HashSet<int>();
            var frontierNodes = new HashSet<int> { initialNode };

            while(frontierNodes.Count > 0)
            {
                var newFrontier = frontierNodes
                    .SelectMany(n => graph[n].CurrentNeighbors)
                    .Select(node => node.Value)
                    .Where(n => !processedNodes.Contains(n))
                    .ToHashSet();

                frontierNodes.ForEach(n => processedNodes.Add(n));
                frontierNodes = newFrontier;
            }

            return processedNodes;
        }

        private static List<HashSet<int>> GetAllGroups(Graph<int> graph)
        {
            List<HashSet<int>> groups = new();
            bool IsInAGroup(int n) => groups.Any(group => group.Contains(n));

            foreach (var element in graph.Nodes.Select(n => n.Value))
                if (!IsInAGroup(element))
                    groups.Add(GetGroupOfGivenNode(graph, element));

            return groups;
        }
    }
}
