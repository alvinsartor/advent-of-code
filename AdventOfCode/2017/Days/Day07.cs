﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdventOfCode.helpers;
using Graph;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/7

    class Day07: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 200;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 200;

        /// <summary>
        /// Build the tower of programs and find the name of the program at the bottom.
        /// </summary>
        public override string Part1() => GetTreeRoot(BuildProgramsTowerFromInput(input).programs);

        /// <summary>
        /// Find the program's weight that has to be adjusted in order to balance the whole structure.
        /// </summary>
        public override string Part2()
        {
            (Graph<string> programs, Dictionary<string, int> weights) = BuildProgramsTowerFromInput(input);
            var rootProgram = GetTreeRoot(programs);
            var cumulativeWeights = BuildCumulativeWeights(rootProgram, programs, weights);
            var unbalancedProgram = FindDeltaOfUnbalancedProgram(rootProgram, programs, cumulativeWeights);

            if (!unbalancedProgram.HasValue) throw new Exception("Couldn't find an imbalanced program!");
            var correctedWeight = weights[unbalancedProgram.Value.program] + unbalancedProgram.Value.delta;

            return correctedWeight.ToString();
        }

        private static string GetTreeRoot(Graph<string> tree) => tree.Nodes.First(n => n.InDegree == 0).Value;

        private static (Graph<string> programs, Dictionary<string, int> weights) BuildProgramsTowerFromInput(string[] input)
        {
            Graph<string> graph = new();
            Dictionary<string, int> weights = new();
            ExtractPrograms(input).ForEach(p => { graph.Add(p.name); weights.Add(p.name, p.weight); });
            ExtractConnections(input).ForEach(c => c.programsTop.ForEach(top => graph[c.programBase].AddConnection(graph[top])));

            return (graph, weights);
        }

        private static IEnumerable<(string name, int weight)> ExtractPrograms(string[] input)
        {
            foreach(var line in input)
            {
                var split = line.Split(" ");
                var name = split[0];
                var weight = int.Parse(split[1][1..^1]);
                yield return (name, weight);
            }
        }

        private static IEnumerable<(string programBase, IEnumerable<string> programsTop)> ExtractConnections(string[] input)
        {
            foreach (var line in input)
            {
                var split = line.Split(" -> ");
                if (split.Length < 2) continue; // no connection

                var programBase = split[0].Split(" ")[0];
                var programsTop = split[1].Split(", ");
                yield return (programBase, programsTop);
            }
        }

        private static Dictionary<string, int> BuildCumulativeWeights(
            string rootProgram,
            Graph<string> programs, 
            Dictionary<string, int> weights)
        {
            Dictionary<string, int> cumulativeWeights = new();
            FindWeightOfNodeRecursively(rootProgram, programs, weights, cumulativeWeights);
            return cumulativeWeights;
        }

        private static void FindWeightOfNodeRecursively(
            string node,
            Graph<string> programs, 
            Dictionary<string, int> weights,
            Dictionary<string, int> cumulativeWeights)
        {
            if (cumulativeWeights.ContainsKey(node)) return;

            var children = programs[node].CurrentNeighbors.Select(n => n.Value).ToList();
            children.ForEach(childNode => FindWeightOfNodeRecursively(childNode, programs, weights, cumulativeWeights));

            var childrenWeight = children.Sum(childNode => cumulativeWeights[childNode]);
            var nodeWeight = weights[node];

            cumulativeWeights.Add(node, nodeWeight + childrenWeight);
        }

        private static (string program, int delta)? FindDeltaOfUnbalancedProgram(
            string rootNode, 
            Graph<string> programs, 
            Dictionary<string, int> cumulativeWeights)
        {
            var children = programs[rootNode].CurrentNeighbors.Select(n => n.Value).ToList();
            if (children.Count == 0) return null; 

            foreach (string sibiling in children)
            {
                var sibilingsDelta = FindDeltaOfUnbalancedProgram(sibiling, programs, cumulativeWeights);
                if (sibilingsDelta != null) 
                    return sibilingsDelta;
            }

            int value = cumulativeWeights[children[0]];
            foreach (string sibiling in children.Skip(1))
                if (value != cumulativeWeights[sibiling])
                    return FindDeltaInSibilings(children, cumulativeWeights);            

            return null;
        }

        private static (string program, int delta) FindDeltaInSibilings(List<string> sibilings, Dictionary<string, int> cumulativeWeights)
        {
            var values = sibilings.Select(s => cumulativeWeights[s]).ToList();
            var distinctValues = values.ToHashSet().ToList();
            int weight1 = distinctValues[0];                       
            int weight2 = distinctValues[1];

            (int commonWeight, int differentWeight) = values.Count(v => v == weight1) > 1 ? (weight1, weight2) : (weight2, weight1);
            var unbalancedSibiling = sibilings.Single(s => cumulativeWeights[s] == differentWeight);
            var delta = commonWeight - differentWeight;
            return (unbalancedSibiling, delta);
        }
    }
}
