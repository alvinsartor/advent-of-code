﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/4

    class Day04: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 100;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 100;

        /// <summary>
        /// Count the number of passprashes that have no duplicate words.
        /// </summary>
        public override string Part1() => input.Count(HasNoDuplicates).ToString();

        /// <summary>
        /// Count the number of passprashes that have no anagrams words.
        /// </summary>
        public override string Part2() => input.Count(HasNoAnagrams).ToString();

        public bool HasNoDuplicates(string line)
        {
            HashSet<string> container = new();
            foreach (var word in line.Split(" "))
                if (!container.Contains(word)) container.Add(word);
                else return false;

            return true;
        }

        public bool HasNoAnagrams(string line)
        {
            HashSet<string> container = new();
            foreach (var word in line.Split(" "))
            {
                var sortedWord = new string(word.ToCharArray().OrderBy(c => c).ToArray());
                if (!container.Contains(sortedWord)) container.Add(sortedWord);
                else return false;
            }

            return true;
        }
    }
}
