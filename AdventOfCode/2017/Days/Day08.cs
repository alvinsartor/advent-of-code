﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/8

    class Day08: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 500;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 500;

        /// <summary>
        /// Apply the operations to the registers and find the highest value afterwards.
        /// </summary>
        public override string Part1()
        {
            var registers = InitializeRegisters(input);
            foreach (var instruction in input) ApplyInstruction(instruction, registers);
            return registers.Values.Max().ToString();
        }

        /// <summary>
        /// Find the highest value any time during the execution of the instructions.
        /// </summary>
        public override string Part2()
        {
            var registers = InitializeRegisters(input);
            var allValues = new List<int>();
            foreach (var instruction in input) allValues.Add(ApplyInstruction(instruction, registers));
            return allValues.Max().ToString();
        }

        private static Dictionary<string, int> InitializeRegisters(string[] input) => 
            input.Select(line => line.Split(" ")[0]).ToHashSet().ToDictionary(s => s, s => 0);
        
        private static int ApplyInstruction(string instruction, Dictionary<string, int> registers)
        {
            var split = instruction.Split(" ");
            var register = split[0];
            Func<int, int, int> operation = split[1] == "inc" ? Sum : Subtract;
            int value = int.Parse(split[2]);
            var comparingRegister = split[4];
            var comparingFunction = GetComparingFunction(split[5]);
            int comparingValue = int.Parse(split[6]);

            if (comparingFunction(registers[comparingRegister], comparingValue))
                registers[register] = operation(registers[register], value);

            return registers[register];
        }

        private static Func<int, int, bool> GetComparingFunction(string sign) => sign switch
        {
            "==" => Equal,
            "!=" => NotEqual,
            ">=" => GreaterOrEqual,
            "<=" => SmallerOrEqual,
            ">" => Greater,
            "<" => Smaller,
            _ => throw new NotImplementedException($"Operation '{sign}' not found."),
        };

        private static int Sum(int v1, int v2) => v1 + v2;
        private static int Subtract(int v1, int v2) => v1 - v2;
        private static bool Equal(int v1, int v2) => v1 == v2; 
        private static bool NotEqual(int v1, int v2) => v1 != v2; 
        private static bool Greater(int v1, int v2) => v1 > v2; 
        private static bool Smaller(int v1, int v2) => v1 < v2; 
        private static bool GreaterOrEqual(int v1, int v2) => v1 >= v2; 
        private static bool SmallerOrEqual(int v1, int v2) => v1 <= v2; 
    }
}
