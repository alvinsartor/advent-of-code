﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/25

    class Day25: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 50;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 0;

        /// <summary>
        /// Execute the instructions and count the 1s at the end.
        /// </summary>
        public override string Part1()
        {
            var inputs = FillInputs();
            int steps = 12425180;
            var tape = new byte[100000];
            int currentIndex = 50000;
            char state = 'a';

            while(steps-- > 0)
            {
                var instruction = inputs[state][tape[currentIndex]];
                tape[currentIndex] = instruction.Write;
                currentIndex += instruction.Move;
                state = instruction.Next;
            }
            
            return tape.Count(v => v == 1).ToString();
        }

        /// <summary>
        /// Nothing to do here..
        /// </summary>
        public override string Part2() => "Nothing to do! Yay!";

        private static Dictionary<char, Dictionary<int, Instruction>> FillInputs()
        {
            var result = new Dictionary<char, Dictionary<int, Instruction>>
            {
                { 'a', new Dictionary<int, Instruction>() },
                { 'b', new Dictionary<int, Instruction>() },
                { 'c', new Dictionary<int, Instruction>() },
                { 'd', new Dictionary<int, Instruction>() },
                { 'e', new Dictionary<int, Instruction>() },
                { 'f', new Dictionary<int, Instruction>() }
            };

            result['a'][0] = new Instruction(1, 1, 'b');
            result['a'][1] = new Instruction(0, 1, 'f');
            result['b'][0] = new Instruction(0, -1, 'b');
            result['b'][1] = new Instruction(1, -1, 'c');
            result['c'][0] = new Instruction(1, -1, 'd');
            result['c'][1] = new Instruction(0, 1, 'c');
            result['d'][0] = new Instruction(1, -1, 'e');
            result['d'][1] = new Instruction(1, 1, 'a');
            result['e'][0] = new Instruction(1, -1, 'f');
            result['e'][1] = new Instruction(0, -1, 'd');
            result['f'][0] = new Instruction(1, 1, 'a');
            result['f'][1] = new Instruction(0, -1, 'e');

            return result;
        }

        private record Instruction(byte Write, int Move, char Next) {}
    }
}
