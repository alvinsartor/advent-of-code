﻿using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/16

    class Day16: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 50;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 10;

        /// <summary>
        /// Perform the dance moves on the programs and print the final dispositions.
        /// </summary>
        public override string Part1()
        {
            var programs = new List<char>(new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'});
            var moves = ExtractMovesSequence(input[0]).ToList();            
            moves.ForEach(move => move(programs));

            return string.Join("", programs);
        }

        /// <summary>
        /// Ok now repeat the moves 1Billion times. What is the final result?
        /// </summary>
        public override string Part2()
        {
            var programs = new List<char>(new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p' });
            var moves = ExtractMovesSequence(input[0]).ToList();
            var cycle = ContinueUntilCycles(programs, moves);
            
            long goal = 1_000_000_000;
            return cycle[(int)(goal % cycle.Count)];
        }

        private static List<string> ContinueUntilCycles(List<char> programs, List<Action<List<char>>> moves)
        {
            var initial = string.Join("", programs);
            var turns = new List<string> { string.Join("", programs) };

            do
            {
                moves.ForEach(move => move(programs));
                turns.Add(string.Join("", programs));
            } while (turns[^1] != initial);

            turns.RemoveAt(turns.Count - 1);
            return turns;
        }

        private static IEnumerable<Action<List<char>>> ExtractMovesSequence(string input) => 
            input.Split(",").Select(move => DecodeMove(move));

        private static Action<List<char>> DecodeMove(string move) => move[0] switch
        {
            's' => DecodeSpin(move[1..]),
            'x' => DecodeExchange(move[1..]),
            'p' => DecodePartner(move[1..]),
            _ => throw new NotImplementedException(),
        };

        private static Action<List<char>> DecodeSpin(string move) => (List<char> l) => Spin(l, int.Parse(move));
        private static Action<List<char>> DecodeExchange(string move)
        {
            var indexes = ExtractIndexes(move);
            return (List<char> l) => Exchange(l, indexes[0], indexes[1]);
        }
        private static Action<List<char>> DecodePartner(string move)
        {
            var indexes = ExtractPartners(move);
            return (List<char> l) => Partner(l, indexes[0][0], indexes[1][0]);
        }

        private static int[] ExtractIndexes(string move) => ExtractPartners(move).Select(int.Parse).ToArray();
        private static string[] ExtractPartners(string move) => move.Split('/');

        private static void Spin(List<char> arr, int n) => Enumerable.Range(0, n).ForEach(_ =>
        {
            var last = arr[^1];
            arr.RemoveAt(arr.Count - 1);
            arr.Insert(0, last);
        });
        private static void Exchange(List<char> arr, int n1, int n2) => (arr[n1], arr[n2]) = (arr[n2], arr[n1]);
        private static void Partner(List<char> arr, char p1, char p2)
        {
            var indx1 = -1;
            var indx2 = -1;
            for (int i = 0; i < arr.Count && (indx1 == -1 || indx2 == -1); i++)
            {
                if (indx1 == -1 && arr[i] == p1) indx1 = i;
                if (indx2 == -1 && arr[i] == p2) indx2 = i;
            }
            Exchange(arr, indx1, indx2);
        }
    }
}
