﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/23

    class Day23: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 50;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 1000;

        private static long MulCalls = 0;

        /// <summary>
        /// Execute the program and count how many time the Mul instruction is called.
        /// </summary>
        public override string Part1()
        {
            var registers = GetRegistersList(input).ToDictionary(r => r, _ => (long)0);
            long currentLine = 0;
            while (currentLine < input.Length)
                currentLine = ExecuteLine(input, currentLine, registers);
            
            return MulCalls.ToString();
        }

        /// <summary>
        /// Exit from debug mode and determine what the register H would contain if the program could end.
        /// </summary>
        public override string Part2()
        {
            // the program counts the prime numbers from B to C, with a step of 17

            static bool isPrime(int n)
            {
                double upperLimit = Math.Sqrt(n);
                for (var i = 2; i <= upperLimit; i++)
                    if (n % i == 0)
                        return false;
                return true;
            }

            int counter = 0;
            const int lowerLimit = 109900;
            const int upperLimit = lowerLimit + 17 * 1000;
            for (int b = lowerLimit; b <= upperLimit; b += 17)
                if (!isPrime(b))
                    counter++;

            return counter.ToString();
        }

        private static long ExecuteLine(string[] input, long currentLine, Dictionary<string, long> regs)
        {
            var split = input[currentLine].Split(" ");
            var operation = split[0];
            var reg1 = split[1];
            var value = split.Length <= 2 ? 0 : regs.ContainsKey(split[2]) ? regs[split[2]] : long.Parse(split[2]);

            switch (operation)
            {
                case "set": Set(regs, reg1, value); break;
                case "sub": Sub(regs, reg1, value); break;
                case "mul": MulCalls++; Mul(regs, reg1, value); break;                                
                // jgz can receive a value as first argument.
                case "jnz": long check = regs.ContainsKey(reg1) ? regs[reg1] : int.Parse(reg1); return currentLine + Jnz(check, value);
                default: throw new NotImplementedException();
            }
            return currentLine + 1;
        }

        private static IEnumerable<string> GetRegistersList(string[] input) => input
            .Select(line => line.Split(" ")[1]).Where(r => !int.TryParse(r, out int _)).ToHashSet();

        private static long Jnz(long check, long val) => check != 0 ? val : 1;
        private static void Set(Dictionary<string, long> regs, string reg1, long val) => regs[reg1] = val;
        private static void Sub(Dictionary<string, long> regs, string reg1, long val) => regs[reg1] -= val;
        private static void Mul(Dictionary<string, long> regs, string reg1, long val) => regs[reg1] *= val;

    }
}
