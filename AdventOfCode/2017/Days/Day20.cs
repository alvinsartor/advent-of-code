﻿using AdventOfCode.helpers;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/20

    class Day20: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 10;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 10;

        /// <summary>
        /// Find the particle that will stay closer to origin in the long run
        /// </summary>
        public override string Part1()
        {
            const int MANUAL_TRIALS = 350; // simply tweak the value until the number does not change anymore;
            var particles = input.Select(Particle.FromString).ToList();
            Enumerable.Range(0, MANUAL_TRIALS).ForEach(_ => particles.ForEach(p => p.Tick()));
            var closestToOrigin = particles.Select((p, index) => (p, index)).MinBy(t => t.p.DistanceFromOrigin).index;
            return closestToOrigin.ToString();
        }

        /// <summary>
        /// Count the number of particles remaining after the colliding ones have been removed.
        /// </summary>
        public override string Part2()
        {
            const int MANUAL_TRIALS = 150; // simply tweak the value until the number does not change anymore;
            var particles = input.Select(Particle.FromString).ToList();
            Enumerable.Range(0, MANUAL_TRIALS).ForEach(_ => {
                particles.ForEach(p => p.Tick());
                particles = RemoveCollidingParticles(particles);
            });
            return particles.Count.ToString();
        }

        private static List<Particle> RemoveCollidingParticles(IEnumerable<Particle> particles)
        {
            HashSet<Particle> particlesToKeep = particles.ToHashSet();
            Dictionary<Point3D, Particle> positions = new();
            foreach(var particle in particles)
            {
                if (positions.ContainsKey(particle.Position))
                {
                    particlesToKeep.Remove(particle);
                    particlesToKeep.Remove(positions[particle.Position]);
                }
                else
                {
                    positions.Add(particle.Position, particle);
                }
            }
            return particlesToKeep.ToList();
        }


        private class Particle
        {
            private Point3D position;
            private Point3D velocity;
            private readonly Point3D acceleration;

            public Point3D Position => position;
            public double DistanceFromOrigin => position.ManhattanDistanceFromOrigin;

            private Particle(Point3D p, Point3D v, Point3D a) => (position, velocity, acceleration) = (p, v, a);
            public static Particle FromString(string line)
            {
                static Point3D ExtractPoint(string s)
                {
                    var split = s.Split(",").Select(double.Parse).ToArray();
                    return new Point3D(split[0], split[1], split[2]);
                }

                var split = line.Split(", ").Select(pt => pt[3..^1]).ToArray();
                var position = ExtractPoint(split[0]);
                var velocity = ExtractPoint(split[1]);
                var acceleration = ExtractPoint(split[2]);
                return new Particle(position, velocity, acceleration);
            }

            public void Tick()
            {
                velocity += acceleration;
                position += velocity;
            }
        }
    }
}
