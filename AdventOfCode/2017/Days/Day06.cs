﻿using AdventOfCode.helpers;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/6

    class Day06: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 50;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 50;

        /// <summary>
        /// Count the number of redistributions before an already seen configuration is created.
        /// </summary>
        public override string Part1()
        {
            int[] memory = input[0].Split("\t").Select(int.Parse).ToArray();
            return RedistributeUntilLoops(memory).instructionsToCycle.ToString();
        }

        /// <summary>
        /// Count the size of the loop.
        /// </summary>
        public override string Part2()
        {
            int[] memory = input[0].Split("\t").Select(int.Parse).ToArray();
            return RedistributeUntilLoops(memory).cycleSize.ToString();
        }

        private static (int instructionsToCycle, int cycleSize) RedistributeUntilLoops(int[] memory)
        {
            string snapshot() => string.Join(".", memory);
            Dictionary<string, int> snapshots = new() { { snapshot(), 0 } };
            int counter = 0;

            while (true)
            {
                counter++;
                Redistribute(memory);
                var currentSnapshot = snapshot();
                if (snapshots.ContainsKey(currentSnapshot)) break;
                snapshots.Add(currentSnapshot, counter);
            }

            int instructionsToCycle = counter;
            int cycleSize = counter - snapshots[snapshot()];
            return (instructionsToCycle, cycleSize);
        }

        private static void Redistribute(int[] memory)
        {
            var i = memory.IndexOfMax();
            var toRedistribute = memory[i];
            memory[i] = 0;

            while(toRedistribute > 0)
            {
                i = (i + 1) % memory.Length;
                memory[i]++;
                toRedistribute--;
            }
        }
    }
}
