﻿using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/3

    class Day03: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 100;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 1000;

        /// <summary>
        /// Find the Manhattan distance between the input and the 1, following a spiral pattern.
        /// </summary>
        public override string Part1()
        {
            var inputNumber = int.Parse(input[0]);
            var inputPosition = PositionInTheSpiral(inputNumber);
            return inputPosition.ManhattanDistanceFromOrigin.ToString();
        }

        private static MutablePoint PositionInTheSpiral(int n)
        {
            MutablePoint point = new(0, 0);
            var directions = new[] { new MutablePoint(1, 0), new MutablePoint(0, -1), new MutablePoint(-1, 0), new MutablePoint(0, 1) };
            
            int currentDirection = 0;
            int currentStepsPerDirection = 1;
            int stepsTillChangeOfDirection = 1;
            int turnsBeforeIncreasingSteps = 2;
            int currentNumber = 1;
            
            while (++currentNumber <= n)
            {
                point.Add(directions[currentDirection]);                
                if (--stepsTillChangeOfDirection == 0)
                {
                    currentDirection = (currentDirection + 1) % 4;
                    if (--turnsBeforeIncreasingSteps == 0)
                    {
                        turnsBeforeIncreasingSteps = 2;
                        currentStepsPerDirection++;
                    }
                    stepsTillChangeOfDirection = currentStepsPerDirection;
                }
            }

            return point;
        }

        private class MutablePoint
        {
            private int x;
            private int y;
            public double ManhattanDistanceFromOrigin => Math.Abs(x) + Math.Abs(y);

            public MutablePoint(int x, int y) => (this.x, this.y) = (x, y);
            public void Add(MutablePoint other)
            {
                x += other.x;
                y += other.y;
            }
            public override string ToString() => $"{x},{y}";
        }

        /// <summary>
        /// Find the first value obtained by incrementing each step in the spiral with the sum of the neighboring points.
        /// </summary>
        public override string Part2()
        {
            var inputNumber = int.Parse(input[0]);
            var stressTestResult = SpiralStressTest(inputNumber);
            return stressTestResult.ToString();
        }

        private static int SpiralStressTest(int n)
        {
            Point point = new(0, 0);
            var directions = new[] { Point.Right, Point.Up, Point.Left, Point.Down };
            var addedPoints = new Dictionary<Point, int>() { { point, 1 } };

            int currentDirection = 0;
            int currentStepsPerDirection = 1;
            int stepsTillChangeOfDirection = 1;
            int turnsBeforeIncreasingSteps = 2;
            int latestAddedValue = 1;

            while (latestAddedValue < n)
            {
                point += directions[currentDirection];
                latestAddedValue = point.ExtendedSurroundings().Sum(p => addedPoints.ContainsKey(p) ? addedPoints[p] : 0);
                addedPoints.Add(point, latestAddedValue);

                if (--stepsTillChangeOfDirection == 0)
                {
                    currentDirection = (currentDirection + 1) % 4;
                    if (--turnsBeforeIncreasingSteps == 0)
                    {
                        turnsBeforeIncreasingSteps = 2;
                        currentStepsPerDirection++;
                    }
                    stepsTillChangeOfDirection = currentStepsPerDirection;
                }
            }
            return latestAddedValue;
        }
    }
}
