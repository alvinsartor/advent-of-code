﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/13

    class Day13: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 100;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 10;

        /// <summary>
        /// Calculate the severity of your trip across the firewall.
        /// </summary>
        public override string Part1()
        {
            var firewallMap = DecodeFirewall(input);
            var layersInWhichPackageIsCaught = firewallMap.Where(CatchesPackage);
            var gravities = layersInWhichPackageIsCaught.Select(slice => slice.layer * slice.depth);

            return gravities.Sum().ToString();
        }

        /// <summary>
        /// Calculate the minimum amount of delay to not get caught by the firewall.
        /// </summary>
        public override string Part2()
        {
            var firewallMap = DecodeFirewall(input);
            int delay = 1;
            while (firewallMap.Any(slice => CatchesDelayedPackage(slice, delay))) delay++;

            return delay.ToString();
        }

        private static List<(int layer, int depth)> DecodeFirewall(string[] input) => input
            .Select(line => { var split = line.Split(": ").Select(int.Parse).ToArray(); return (split[0], split[1]); })
            .ToList();

        private static bool CatchesPackage((int layer, int depth) slice) => 
            CatchesDelayedPackage(slice, 0);  

        private static bool CatchesDelayedPackage((int layer, int depth) slice, int delay) => 
            (slice.layer + delay) % ((slice.depth - 1) * 2) == 0;
    }
}
