﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/10

    class Day10 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 500;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 500;

        /// <summary>
        /// Calculate the sparse hash of the numbers [0..255] using the input lengths.
        /// </summary>
        public override string Part1()
        {
            var lengths = input[0].Split(",").Select(int.Parse).ToList();
            var sparseHash = SimpleKnotHash(lengths);
            return (sparseHash[0] * sparseHash[1]).ToString();
        }

        /// <summary>
        /// Now apply the whole algorithm and hash the number from 0 to 255.
        /// </summary>
        public override string Part2() => KnotHash(input[0]);

        private static List<int> SimpleKnotHash(List<int> lengths)
        {
            var numbers = Enumerable.Range(0, 256).ToList();
            var currentPosition = 0;
            var skipSize = 0;
            foreach (var length in lengths)
            {
                KnotHashPassage(numbers, length, currentPosition);
                currentPosition = (currentPosition + length + skipSize) % numbers.Count;
                skipSize++;
            }

            return numbers;
        }

        public static string KnotHash(string sequenceToHash)
        {
            var sparseHash = CalculateSparseHash(sequenceToHash);
            var denseHash = CalculateDenseHash(sparseHash).ToList();
            var hexRepresentation = string.Join("", denseHash.Select(n => n.ToString("X").PadLeft(2, '0')));
            return hexRepresentation;
        }

        private static List<int> CalculateSparseHash(string sequenceToHash)
        {
            const int timesToRepeat = 64;
            var numbers = Enumerable.Range(0, 256).ToList();
            var lengths = sequenceToHash.ToCharArray().Select(c => (int)c).Concat(new[] { 17, 31, 73, 47, 23 }).ToList();

            var currentPosition = 0;
            var skipSize = 0;

            for (int i = 0; i < timesToRepeat; i++)
                foreach (var length in lengths)
                {
                    KnotHashPassage(numbers, length, currentPosition);
                    currentPosition = (currentPosition + length + skipSize) % numbers.Count;
                    skipSize++;
                }

            return numbers;
        }

        private static void KnotHashPassage(List<int> numbers, int length, int currentPosition)
        {
            int indxStart = currentPosition;
            int indxEnd = (currentPosition + length - 1) % numbers.Count;
            int count = 0;
            int stopAt = length / 2;

            while (count++ < stopAt)
            {
                (numbers[indxStart], numbers[indxEnd]) = (numbers[indxEnd], numbers[indxStart]);
                indxStart = (indxStart + 1) % numbers.Count;
                indxEnd = indxEnd > 0 ? indxEnd - 1 : numbers.Count - 1;
            }
        }

        private static IEnumerable<int> CalculateDenseHash(IEnumerable<int> sparseHash)
        {
            const int size = 16;
            var accumulator = 0;
            var count = 0;

            foreach (var n in sparseHash)
            {
                accumulator ^= n;
                count++;
                if (count == size)
                {
                    count = 0;
                    yield return accumulator;
                    accumulator = 0;
                }
            }
        }
    }
}
