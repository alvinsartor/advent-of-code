﻿using AdventOfCode.helpers;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/19

    class Day19: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 100;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 100;

        /// <summary>
        /// Help the packet arrive at its destination and display the letters it went through.
        /// </summary>
        public override string Part1() => CrossMap(input).Path;

        /// <summary>
        /// Count how many steps are needed to cross the whole map.
        /// </summary>
        public override string Part2() => CrossMap(input).StepCounter.ToString();

        private static Package CrossMap(string[] input)
        {
            var map = input.Select(line => line.ToCharArray()).ToArray();
            var numMilestones = map.Sum(line => line.Count(c => char.IsUpper(c)));
            var package = new Package(map, numMilestones);
            while (!package.IsArrived) package.NextStep();
            return package;
        }

        private class Package
        {
            private readonly char[][] map;
            private readonly int numMilestones;
            private Point position;
            private Point direction;
            private readonly List<char> milestones;

            public bool IsArrived { get; private set; }
            public int StepCounter { get; private set; } = 1;
            public string Path => new string(milestones.ToArray());

            public Package(char[][] map, int numMilestones)
            {
                this.map = map;
                this.numMilestones = numMilestones;
                var startingX = map[0].ToList().IndexOf('|');
                position = new Point(startingX, 0);
                direction = Point.Down;
                milestones = new();
            }

            public void NextStep()
            {
                if (IsArrived) return;

                position += direction;
                StepCounter++;
                var currentlyOn = GetAt(position);

                if (char.IsUpper(currentlyOn))
                {
                    milestones.Add(currentlyOn);
                    if (milestones.Count == numMilestones) IsArrived = true;
                    return;
                }

                if (currentlyOn == '+')
                    direction = GetNewDirection(position, direction);
            }

            private char GetAt(Point p) => map[(int)p.Y][(int)p.X];
            private Point GetNewDirection(Point current, Point direction) => IsDirectionUpOrDown(direction)
                    ? IsLeftNonEmpty(current) ? Point.Left : Point.Right
                    : IsUpNonEmpty(current) ? Point.Up : Point.Down;

            private static bool IsDirectionUpOrDown(Point direction) => direction.Equals(Point.Up) || direction.Equals(Point.Down);
            private bool IsLeftNonEmpty(Point current) => GetAt(current + Point.Left) != ' ';
            private bool IsUpNonEmpty(Point current) => GetAt(current + Point.Up) != ' ';
        }
    }
}
