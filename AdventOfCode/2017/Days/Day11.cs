﻿using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/11

    class Day11: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 500;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 500;

        private static readonly Point3D North = new Point3D(0, 1, -1);
        private static readonly Point3D NorthEast = new Point3D(1, 0, -1);
        private static readonly Point3D SouthEast = new Point3D(1, -1, 0);
        private static readonly Point3D South = new Point3D(0, -1, +1);
        private static readonly Point3D SouthWest = new Point3D(-1, 0, 1);
        private static readonly Point3D NorthWest = new Point3D(-1, 1, 0);

        /// <summary>
        /// Calculate the distance of the program in the HEX grid.
        /// </summary>
        public override string Part1()
        {
            var steps = input[0].Split(',').Select(StepToPoint);
            var finalPosition = steps.Aggregate(Point3D.Origin, (curr, next) => curr + next);
            return HexDistanceFromOrigin(finalPosition).ToString();
        }

        /// <summary>
        /// Calculate the maximum distance ever reached.
        /// </summary>
        public override string Part2()
        {
            var steps = input[0].Split(',').Select(StepToPoint).ToArray();
            var furthestDistance = CalculateAllPositions(steps).Max(HexDistanceFromOrigin);
            return furthestDistance.ToString();
        }

        private static Point3D StepToPoint(string step) =>
            step switch
            {
                "n" => North,
                "s" => South,
                "se" => SouthEast,
                "sw" => SouthWest,
                "ne" => NorthEast,
                "nw" => NorthWest,
                _ => throw new NotImplementedException($"Unknown direction: {step}"),
            };

        private static double HexDistanceFromOrigin(Point3D p1) => (Math.Abs(p1.X) + Math.Abs(p1.Y) + Math.Abs(p1.Z)) / 2;
        private static IEnumerable<Point3D> CalculateAllPositions(IEnumerable<Point3D> steps)
        {
            var current = Point3D.Origin;
            foreach(var step in steps)
            {
                current += step;
                yield return current;
            }
        }
    }
}
