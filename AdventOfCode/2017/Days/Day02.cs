﻿using System;
using System.Linq;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/2

    class Day02: DailyChallenge
    {
        /// <summary>
        /// Calculate the checksum of the given spreadsheet by finding the delta between max and min in each row.
        /// </summary>
        public override string Part1()
        {
            var rows = input.Select(line => line.Split("\t").Select(int.Parse).ToArray());
            var checksum = rows.Select(line => line.Max() - line.Min()).Sum();
            return checksum.ToString();
        }

        /// <summary>
        /// Calculate the checksum of the given spreadsheet by finding quotient of the only two divisible numbers in each row.
        /// </summary>
        public override string Part2()
        {
            var rows = input.Select(line => line.Split("\t").Select(int.Parse).ToArray());
            var checksum = rows.Select(FindQuotientOfDivisibleNumbers).Sum();
            return checksum.ToString();            
        }

        private int FindQuotientOfDivisibleNumbers(int[] row)
        {
            for (int i = 0; i < row.Length - 1; i++)
                for (int j = i + 1; j < row.Length; j++)
                    if (row[i] % row[j] == 0) return row[i] / row[j];
                    else if (row[j] % row[i] == 0) return row[j] / row[i];

            throw new Exception("I could not find two values in this row that were evenly divisible!");
        }
    }
}
