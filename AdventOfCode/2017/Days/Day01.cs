﻿using System.Linq;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/1

    class Day01: DailyChallenge
    {
        /// <summary>
        /// Find the sum of digits that match the next digit in this circular input.
        /// </summary>
        public override string Part1()
        {
            static bool matchesNext(int[] array, int i) => array[i] == array[(i + 1) % array.Length]; 
            var digits = input[0].ToCharArray().Select(c => int.Parse(c.ToString())).ToArray();
            var digitsThatMatchNext = digits.Where((_, i) => matchesNext(digits, i));
            return digitsThatMatchNext.Sum().ToString();
        }

        /// <summary>
        /// Find the sum of digits that match the digit halfway in the circular input.
        /// </summary>
        public override string Part2()
        {
            static bool matchesTheDigitHalfwayAway(int[] array, int i) => array[i] == array[(i + array.Length / 2) % array.Length];
            var digits = input[0].ToCharArray().Select(c => int.Parse(c.ToString())).ToArray();
            var digitsThatMatchNext = digits.Where((_, i) => matchesTheDigitHalfwayAway(digits, i));
            return digitsThatMatchNext.Sum().ToString();
        }
    }
}
