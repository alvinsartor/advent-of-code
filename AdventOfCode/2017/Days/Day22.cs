﻿using AdventOfCode.helpers;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/22

    class Day22: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 100;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 1;

        /// <summary>
        /// Count the bursts of infection caused by the virus.
        /// </summary>
        public override string Part1()
        {
            var map = GetInitialStateFromInput(input).ToHashSet();
            var initialPosition = new Point(input[0].Length / 2, input.Length / 2);
            var virus = new Virus(initialPosition, map);

            int counter = 0;
            while (counter++ < 10000) virus.Move();

            return virus.InfectionBursts.ToString();
        }

        /// <summary>
        /// Count the bursts of infection caused by modified version of the virus.
        /// </summary>
        public override string Part2()
        {
            var map = GetInitialStateFromInput(input).ToHashSet();
            var initialPosition = new Point(input[0].Length / 2, input.Length / 2);
            var virus = new Virus_V2(initialPosition, map);

            int counter = 0;
            while (counter++ < 10000000) virus.Move();

            return virus.InfectionBursts.ToString();
        }

        private static IEnumerable<Point> GetInitialStateFromInput(string[] input)
        {
            for (int r = 0; r < input.Length; r++)
                for (int c = 0; c < input[r].Length; c++)
                    if (input[r][c] == '#')
                        yield return new Point(c, r);
        }

        private class Virus
        {
            private Point position;
            private Point direction;
            private readonly HashSet<Point> currentlyInfectedCells;

            public int InfectionBursts { get; private set; } = 0;

            public Virus(Point initialPosition, HashSet<Point> currentlyInfectedCells)
            {
                position = initialPosition;
                direction = Point.Up;
                this.currentlyInfectedCells = currentlyInfectedCells;
            }

            public void Move()
            {
                bool isInfected = currentlyInfectedCells.Contains(position);
                direction = isInfected ? direction.RotatedClockwise() : direction.RotatedCounterclockwise();

                if (isInfected) currentlyInfectedCells.Remove(position);
                else
                {
                    currentlyInfectedCells.Add(position);
                    InfectionBursts++;
                }

                position += direction;
            }
        }

        private class Virus_V2
        {
            enum NodeState { Clean, Weakened, Infected, Flagged }

            private Point position;
            private Point direction;
            private readonly HashSet<Point> currentlyInfectedCells;
            private readonly HashSet<Point> currentlyWeakenedCells;
            private readonly HashSet<Point> currentlyFlaggedCells;

            public int InfectionBursts { get; private set; } = 0;

            public Virus_V2(Point initialPosition, HashSet<Point> currentlyInfectedCells)
            {
                position = initialPosition;
                direction = Point.Up;
                this.currentlyInfectedCells = currentlyInfectedCells;
                currentlyFlaggedCells = new();
                currentlyWeakenedCells = new();
            }

            public void Move()
            {
                var currentNodeState = GetCurrentNodeState();
                direction = GetNewDirectionUsingState(currentNodeState);

                if (currentNodeState == NodeState.Clean)
                {
                    currentlyWeakenedCells.Add(position);
                }
                else if (currentNodeState == NodeState.Weakened)
                {
                    currentlyWeakenedCells.Remove(position);
                    currentlyInfectedCells.Add(position);
                    InfectionBursts++;
                }
                else if (currentNodeState == NodeState.Infected)
                {
                    currentlyInfectedCells.Remove(position);
                    currentlyFlaggedCells.Add(position);
                }
                else
                {
                    currentlyFlaggedCells.Remove(position);
                }
                
                position += direction;
            }

            private NodeState GetCurrentNodeState() => currentlyInfectedCells.Contains(position)
                    ? NodeState.Infected
                    : currentlyWeakenedCells.Contains(position)
                        ? NodeState.Weakened
                        : currentlyFlaggedCells.Contains(position)
                            ? NodeState.Flagged
                            : NodeState.Clean;

            private Point GetNewDirectionUsingState(NodeState state) => state switch
            {
                NodeState.Clean => direction.RotatedCounterclockwise(),
                NodeState.Weakened => direction,
                NodeState.Infected => direction.RotatedClockwise(),
                NodeState.Flagged => direction.RotatedClockwise().RotatedClockwise(),
                _ => throw new System.NotImplementedException(),
            };
        }
    }
}
