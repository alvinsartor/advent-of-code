﻿using System.Collections.Generic;
using AdventOfCode.helpers;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/17

    class Day17: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 10;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 10;

        /// <summary>
        /// Run the spinlock by adding numbers until 2017. What is the following number?
        /// </summary>
        public override string Part1()
        {
            int jumpSize = int.Parse(input[0]);
            LinkedList<int> buffer = new();
            buffer.AddFirst(0);
            LinkedListNode<int> current = buffer.First!;
            int index = 1;

            while (buffer.Count < 2018)
            {
                int minimumJumps = jumpSize % buffer.Count;
                while (minimumJumps-- > 0) current = current.CircularNext()!;
                current = buffer.AddAfter(current, index++);
            }

            return current!.CircularNext()!.Value.ToString();
        }

        /// <summary>
        /// Same as before, but run 50M times and find the number after 0.
        /// </summary>
        public override string Part2()
        {
            int jumpSize = int.Parse(input[0]);
            int currPos = 0;
            int result = 0;
            int limit = 50_000_000;
            int n = 0;
            while (n < limit)
            {
                if (currPos == 1) result = n;
                int fits = (n - currPos) / jumpSize;
                n += fits + 1;
                currPos = (currPos + (fits + 1) * (jumpSize + 1) - 1) % n + 1;
            }
            return result.ToString();
        }
    }
}
