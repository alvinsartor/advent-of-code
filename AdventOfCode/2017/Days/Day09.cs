﻿using System.Collections.Generic;
using System.Text;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/9

    class Day09: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 100;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 100;

        /// <summary>
        /// Calculate the stream level by measuring the nesting level of all groups.
        /// </summary>
        public override string Part1()
        {
            var (cleanedStream, _) = CleanStream(input[0]);
            var streamScore = CalculateStreamScore(cleanedStream);
            return streamScore.ToString();
        }

        /// <summary>
        /// Count the amount of garbage in the stream.
        /// </summary>
        public override string Part2()
        {
            var (_, removedGarbage) = CleanStream(input[0]);
            return removedGarbage.ToString();
        }

        private static (IEnumerable<char> stream, int removedGarbage) CleanStream(string dirtyStream)
        {
            var stream = new StringBuilder(dirtyStream);
            int pointer = 0;
            int garbageCounter = 0;
            bool isRubbish = false;

            while(pointer < stream.Length)
            {
                var current = stream[pointer];
                if (!isRubbish && current == '<')
                {
                    stream.Remove(pointer, 1);
                    isRubbish = true;
                    continue;
                }
                if (isRubbish && current == '>')
                {
                    stream.Remove(pointer, 1);
                    isRubbish = false;
                    continue;
                }
                if (isRubbish && current == '!')
                {
                    stream.Remove(pointer, 2);
                    continue;
                }
                if (isRubbish)
                {
                    stream.Remove(pointer, 1);
                    garbageCounter++;
                }
                else pointer++;
            }

            return (stream.ToString(), garbageCounter);
        }

        private static int CalculateStreamScore(IEnumerable<char> stream)
        {
            int currentLevel = 0;
            int currentScore = 0;

            foreach(var c in stream)
            {
                if (c == '{')
                {
                    currentLevel++;
                    currentScore += currentLevel;
                }
                else if (c == '}')
                {
                    currentLevel--;
                }
            }
            return currentScore;
        }
    }
}
