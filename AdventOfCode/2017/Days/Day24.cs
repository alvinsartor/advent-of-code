﻿using System.Linq;
using Graph;
using Graph.PathFinding;
using System.Collections.Immutable;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/24

    class Day24: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 2;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 100;

        private static List<State>? bridges;

        /// <summary>
        /// Build the strongest possible bridge with the given connecting pieces
        /// </summary>
        public override string Part1()
        {
            bridges = new List<State>();

            var pieces = ParseInput(input);
            var initials = pieces
                .Where(piece => piece.ConnectsWith(0))
                .Select(piece => new State(
                    0,
                    piece.GetOtherSide(0),
                    piece.Strength,
                    ImmutableList<State>.Empty,
                    pieces.Remove(piece)));

            foreach (var state in initials) GetStrongestBridgeValue(state);

            return bridges.Max(b => b.strength).ToString();
        }

        /// <summary>
        /// Get the longest bridge
        /// </summary>
        public override string Part2()
        {
            int maxLength = bridges!.Select(b => b.previousPieces.Count).Max();
            var longestBridges = bridges!.Where(b => b.previousPieces.Count == maxLength);
            return longestBridges.Max(b => b.strength).ToString();
        }

        private static ImmutableHashSet<Piece> ParseInput(string[] input) => 
            input.Select(line => line.Split('/'))
                 .Select(split => new Piece(int.Parse(split[0]), int.Parse(split[1])))
                 .ToImmutableHashSet();


        private static void GetStrongestBridgeValue(State current)
        {
            var compatiblePieces = current.otherPieces.Where(p => p.ConnectsWith(current.connector2)).ToList();
            if (compatiblePieces.Count == 0)
            {
                bridges.Add(current);
                return;
            }

            var newStates = compatiblePieces
                .Select(piece => new State(
                    current.connector2,
                    piece.GetOtherSide(current.connector2),
                    current.strength + piece.Strength,
                    current.previousPieces.Add(current),
                    current.otherPieces.Remove(piece)));

            foreach (var state in newStates) GetStrongestBridgeValue(state);
        }

        private record Piece(int Connector1, int Connector2)
        {
            public bool ConnectsWith(int connector) => connector == Connector1 || connector == Connector2;
            public int GetOtherSide(int connector) => connector == Connector1 ? Connector2 : Connector1;
            public int Strength => Connector1 + Connector2;
        }

        private sealed class State
        {
            public readonly int connector1;
            public readonly int connector2;
            public readonly int strength;
            public readonly ImmutableList<State> previousPieces;
            public readonly ImmutableHashSet<Piece> otherPieces;

            public State(
                int connector1, 
                int connector2, 
                int strength, 
                ImmutableList<State> previousPieces,
                ImmutableHashSet<Piece> otherPieces)
            {
                this.connector1 = connector1;
                this.connector2 = connector2;
                this.strength = strength;
                this.previousPieces = previousPieces;
                this.otherPieces = otherPieces;
            }

            public static string ToString(State s) => $"[{s.connector1}/{s.connector2}]({s.strength})";
            public string Remaining() => string.Join('-', otherPieces.Select(p => $"[{p.Connector1}/{p.Connector2}]").ToArray());
        }
    }
}
