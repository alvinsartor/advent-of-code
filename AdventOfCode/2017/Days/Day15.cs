﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/15

    class Day15: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 3;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 3;

        /// <summary>
        /// Count the matches for the first 40M numbers made by the two generators.
        /// </summary>
        public override string Part1()
        {
            var starters = DecodeInput(input);
            var generator1 = GenerateNumbers(starters.s1, 16807);
            var generator2 = GenerateNumbers(starters.s2, 48271);
            var matchs = Enumerable.Zip(generator1, generator2).Count(zipped => (zipped.First & 0xffff) == (zipped.Second & 0xffff));
            return matchs.ToString();
        }

        /// <summary>
        /// Count the matches for the first 5M numbers made by the two generators following the new rules.
        /// </summary>
        public override string Part2()
        {
            var starters = DecodeInput(input);
            var generator1 = GenerateNumbersWithMod(starters.s1, 16807, 4);
            var generator2 = GenerateNumbersWithMod(starters.s2, 48271, 8);
            var matchs = Enumerable.Zip(generator1, generator2).Count(zipped => (zipped.First & 0xffff) == (zipped.Second & 0xffff));
            return matchs.ToString();
        }

        private static (int s1, int s2) DecodeInput(string[] input) => (GetGeneratorStart(input[0]), GetGeneratorStart(input[1]));
        private static int GetGeneratorStart(string input) => int.Parse(input.Split(" ")[4]);

        private static IEnumerable<long> GenerateNumbers(long initial, long factor, int amountToGenerate = 40_000_000)
        {
            while(amountToGenerate-- > 0)
            {
                initial *= factor;                
                while (initial >= 0x7fffffff) initial = (initial & 0x7fffffff) + (initial >> 31);                
                yield return initial;
            }
        }

        private static IEnumerable<long> GenerateNumbersWithMod(long initial, long factor, int modVal, int amountToGenerate = 5_000_000)
        {
            while (amountToGenerate > 0)
            {
                initial *= factor;
                while (initial >= 0x7fffffff) initial = (initial & 0x7fffffff) + (initial >> 31);
                if (initial % modVal == 0)
                {
                    amountToGenerate--;
                    yield return initial;
                }
            }
        }
    }
}
