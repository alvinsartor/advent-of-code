﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/18

    class Day18 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 1000;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 10;

        private static long LastSound = -1;
        private static long LastRetrieved = -1;

        /// <summary>
        /// Execute the program and return the first retrieved sound.
        /// </summary>
        public override string Part1()
        {
            var registers = GetRegistersList(input).ToDictionary(r => r, _ => (long)0);
            long currentLine = 0;
            while (currentLine < input.Length && LastRetrieved == -1)
                currentLine = ExecuteLine(input, currentLine, registers);
            return LastRetrieved.ToString();
        }

        private static long ExecuteLine(string[] input, long currentLine, Dictionary<string, long> regs)
        {
            var split = input[currentLine].Split(" ");
            var operation = split[0];
            var reg1 = split[1];
            var value = split.Length <= 2 ? 0 : regs.ContainsKey(split[2]) ? regs[split[2]] : long.Parse(split[2]);

            switch (operation)
            {
                case "set": Set(regs, reg1, value); break;
                case "add": Add(regs, reg1, value); break;
                case "mul": Mul(regs, reg1, value); break;
                case "mod": Mod(regs, reg1, value); break;
                // jgz can receive a value as first argument.
                case "jgz": long check = regs.ContainsKey(reg1) ? regs[reg1] : int.Parse(reg1); return currentLine + Jgz(check, value);
                case "snd": Snd(regs, reg1); break;
                case "rcv": Rcv(regs, reg1); break;
                default: throw new NotImplementedException();
            }
            return currentLine + 1;
        }

        private static IEnumerable<string> GetRegistersList(string[] input) => input
            .Select(line => line.Split(" ")[1]).Where(r => !int.TryParse(r, out int _)).ToHashSet();

        private static long Jgz(long check, long val) => check > 0 ? val : 1;
        private static void Set(Dictionary<string, long> regs, string reg1, long val) => regs[reg1] = val;
        private static void Add(Dictionary<string, long> regs, string reg1, long val) => regs[reg1] += val;
        private static void Mul(Dictionary<string, long> regs, string reg1, long val) => regs[reg1] *= val;
        private static void Mod(Dictionary<string, long> regs, string reg1, long val) => regs[reg1] %= val;
        private static void Snd(Dictionary<string, long> regs, string reg1) => LastSound = regs[reg1];
        private static void Rcv(Dictionary<string, long> regs, string reg1) => LastRetrieved = regs[reg1] != 0 ? LastSound : LastRetrieved;

        /// <summary>
        /// Run two programs at the same time and let them communicate until they cannot run anymore.
        /// </summary>
        public override string Part2()
        {
            var registersList = GetRegistersList(input).ToList();
            var p0ToP1 = new Queue<long>();
            var p1ToP0 = new Queue<long>();
            var p0 = new Program(0, registersList, input, p1ToP0, p0ToP1);
            var p1 = new Program(1, registersList, input, p0ToP1, p1ToP0);

            var programs = new[] { p0, p1 };
            var cursor = 0;
            while(!programs.All(p => p.CannotContinue))
            {
                programs[cursor].ExecuteTillStops();
                cursor = cursor == 1 ? 0 : 1;
            }

            return p1.SendCounter.ToString();
        }

        private class Program
        {
            public Queue<long> InputQueue;
            public Queue<long> OutputQueue;
            private readonly Dictionary<string, long> registers;
            private readonly string[] instructions;
            private bool isWaiting = false;
            private long index = 0;

            public int SendCounter { get; private set; } = 0;
            public bool CannotContinue => index < 0 || index >= instructions.Length || (isWaiting && InputQueue.Count == 0);

            public Program(int id, IEnumerable<string> registers, string[] instructions, Queue<long> InputQueue, Queue<long> OutputQueue)
            {
                this.registers = registers.ToDictionary(r => r, _ => (long)0);
                this.registers["p"] = id;
                this.InputQueue = InputQueue;
                this.OutputQueue = OutputQueue;
                this.instructions = instructions;
            }

            public void ExecuteTillStops()
            {
                while (!CannotContinue)
                    ExecuteNextLine();
            }

            private void ExecuteNextLine()
            {
                isWaiting = false;
                var split = instructions[index].Split(" ");
                var operation = split[0];
                var reg1 = split[1];
                var value = split.Length <= 2 ? 0 : registers.ContainsKey(split[2]) ? registers[split[2]] : long.Parse(split[2]);

                var deltaIndex = operation switch
                {
                    "set" => Set(registers, reg1, value),
                    "add" => Add(registers, reg1, value),
                    "mul" => Mul(registers, reg1, value),
                    "mod" => Mod(registers, reg1, value),
                    "jgz" => Jgz(registers.ContainsKey(reg1) ? registers[reg1] : int.Parse(reg1), value),
                    "snd" => Snd(registers, reg1),
                    "rcv" => Rcv(registers, reg1),
                    _ => throw new NotImplementedException(),
                };

                index += deltaIndex;
            }

            private long Jgz(long check, long val) => check > 0 ? val : 1;
            private long Set(Dictionary<string, long> regs, string reg1, long val) { regs[reg1] = val; return 1; }
            private long Add(Dictionary<string, long> regs, string reg1, long val) { regs[reg1] += val; return 1; }
            private long Mul(Dictionary<string, long> regs, string reg1, long val) { regs[reg1] *= val; return 1; }
            private long Mod(Dictionary<string, long> regs, string reg1, long val) { regs[reg1] %= val; return 1; }
            private long Snd(Dictionary<string, long> regs, string reg1) { SendCounter++; OutputQueue.Enqueue(regs[reg1]); return 1; }
            private long Rcv(Dictionary<string, long> regs, string reg1)
            {
                if (InputQueue.Count == 0)
                {
                    isWaiting = true;
                    return 0;
                }

                regs[reg1] = InputQueue.Dequeue();
                return 1;
            }
        }
    }
}
