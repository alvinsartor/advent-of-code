﻿using System.Linq;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/5

    class Day05: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 100;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 10;

        /// <summary>
        /// Follow the jumps and count the steps to get out of the array.
        /// </summary>
        public override string Part1()
        {
            var instructions = input.Select(int.Parse).ToArray();
            var pointer = 0;
            var steps = 0;
            var relativeJump = 0;

            while(pointer < instructions.Length)
            {
                relativeJump = instructions[pointer];
                instructions[pointer]++;
                pointer += relativeJump;
                steps++;
            }

            return steps.ToString();
        }

        /// <summary>
        /// Same as before, but now the steps decrease if they are greater than 3.
        /// </summary>
        public override string Part2()
        {
            var instructions = input.Select(int.Parse).ToArray();
            var pointer = 0;
            var steps = 0;
            var relativeJump = 0;

            while (pointer < instructions.Length)
            {
                relativeJump = instructions[pointer];
                instructions[pointer] += relativeJump >= 3 ? -1 : 1;
                pointer += relativeJump;
                steps++;
            }

            return steps.ToString();
        }
    }
}
