﻿using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/14

    class Day14 : DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 10;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 10;

        /// <summary>
        /// Count the number of used squares in a disk mapped using the knot hash algorithm.
        /// </summary>
        public override string Part1()
        {
            var disk = MapInputToDisk(input[0]);
            var usedCells = disk.Sum(line => line.Count(c => c == '1'));
            return usedCells.ToString();
        }

        /// <summary>
        /// Find the number of partitions in the disk.
        /// </summary>
        public override string Part2()
        {
            var disk = MapInputToDisk(input[0]);
            var blocks = GetBlocksPositions(disk).ToHashSet();
            var partitions = GetDiskPartitions(blocks);
            return partitions.Count.ToString();
        }

        private static char[][] MapInputToDisk(string input) =>
            Enumerable.Range(0, 128).Select(n => MapLineFromInput(input, n)).ToArray();

        private static char[] MapLineFromInput(string input, int line)
        {
            var hashed = Day10.KnotHash($"{input}-{line}");
            var binary = hashed.SelectMany(c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0'));
            return binary.ToArray();
        }

        private static IEnumerable<Point> GetBlocksPositions(char[][] map)
        {
            for (int r = 0; r < map.Length; r++)
                for (int c = 0; c < map[r].Length; c++)
                    if (map[r][c] == '1')
                        yield return new Point(c, r);
        }

        private static List<HashSet<Point>> GetDiskPartitions(HashSet<Point> blocks)
        {
            List<HashSet<Point>> partitions = new();
            bool IsInAPartition(Point block) => partitions.Any(group => group.Contains(block));

            foreach (var element in blocks)
                if (!IsInAPartition(element))
                    partitions.Add(GetPartitionOf(blocks, element));

            return partitions;
        }

        private static HashSet<Point> GetPartitionOf(HashSet<Point> blocks, Point initialPoint)
        {
            if (!blocks.Contains(initialPoint)) throw new Exception("The specified block does not exist.");

            var processedBlocks = new HashSet<Point>();
            var frontierBlocks = new HashSet<Point> { initialPoint };

            while (frontierBlocks.Count > 0)
            {
                var newFrontier = frontierBlocks
                    .SelectMany(n => n.Surroundings().Where(neighbor => blocks.Contains(neighbor)))
                    .Where(n => !processedBlocks.Contains(n) && !frontierBlocks.Contains(n))
                    .ToHashSet();

                frontierBlocks.ForEach(n => processedBlocks.Add(n));
                frontierBlocks = newFrontier;
            }

            return processedBlocks;
        }
    }
}
