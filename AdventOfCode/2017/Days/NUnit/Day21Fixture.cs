﻿using System.Linq;
using NUnit.Framework;

namespace AdventOfCode._2017.Days.NUnit
{
    [TestFixture]
    internal sealed class Day21Fixture
    {
        [TestCase("#..#/..../..../#..#", 0, 0, 2, "#./..")]
        [TestCase("#..#/..../..../#..#", 0, 2, 2, ".#/..")]
        [TestCase("#..#/..../..../#..#", 2, 0, 2, "../#.")]
        [TestCase("#..#/..../..../#..#", 2, 2, 2, "../.#")]
        [TestCase("##.##./#..#../....../##.##./#..#../......", 0, 0, 2, "##/#.")]
        [TestCase("##.##./#..#../....../##.##./#..#../......", 0, 2, 2, ".#/.#")]
        [TestCase("##.##./#..#../....../##.##./#..#../......", 0, 4, 2, "#./..")]
        [TestCase("#.##.##.#/..#..#..#/........./#######.#/#.##.#..#/..#..#.../#.##.##.#/..#..#..#/#.##.##.#", 0, 0, 3, "#.#/..#/...")]
        [TestCase("#.##.##.#/..#..#..#/........./#######.#/#.##.#..#/..#..#.../#.##.##.#/..#..#..#/#.##.##.#", 3, 0, 3, "###/#.#/..#")]
        public void SubImagesAreCorrectlyExtracted(string image, int R, int C, int len, string expectedResult)
        {
            char[][] rows = image.Split("/").Select(line => line.ToCharArray()).ToArray();
            var result = Day21.ExtractSubImage(rows, R, C, len);
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase(".#/..", ".#/..")]
        [TestCase(".#./..#/###", ".#./..#/###")]
        [TestCase("#..#/..../..../#..#", "#./..|.#/..|../#.|../.#")]
        public void ImageIsCorrectlySplitInSubImages(string image, string expectedResultRaw)
        {
            var expectedResult = expectedResultRaw.Split('|');

            var result = Day21.SplitInSubImages(image);
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase(".#./..#/###", "#..#/..../..../#..#")]
        [TestCase("#..#/..../..../#..#", "##.##./#..#../....../##.##./#..#../......")]
        public void ImageIsCorrectlyEnhanced(string image, string expectedResult)
        {
            var baseRules = new string[] { "../.# => ##./#../...", ".#./..#/### => #..#/..../..../#..#" };
            var allRules = Day21.ExpandRules(Day21.ExtractRulesFromInput(baseRules));

            var result = Day21.EnhanceImage(image, allRules);
            Assert.AreEqual(expectedResult, result);
        }
    }
}
