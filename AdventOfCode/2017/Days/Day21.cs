﻿using AdventOfCode.helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdventOfCode._2017.Days
{
    // Get the original challenge and data here: https://adventofcode.com/2017/day/21

    class Day21: DailyChallenge
    {
        public override int NumberOfTimesPart1ShouldBeRan { get; protected set; } = 100;
        public override int NumberOfTimesPart2ShouldBeRan { get; protected set; } = 5;

        /// <summary>
        /// Count the number of pixels that stay on after 5 iterations of the artist algorithm.
        /// </summary>
        public override string Part1()
        {
            string image = ".#./..#/###";
            var extendedRules = ExpandRules(ExtractRulesFromInput(input));
            
            var iterations = 5;
            while (iterations-- > 0) 
                image = EnhanceImage(image, extendedRules);
    
            return image.Count(c => c == '#').ToString();
        }

        /// <summary>        
        /// Count the number of pixels that stay on after 18 iterations of the artist algorithm.
        /// </summary>
        public override string Part2()
        {
            string image = ".#./..#/###";
            var extendedRules = ExpandRules(ExtractRulesFromInput(input));

            var iterations = 18;
            while (iterations-- > 0)
                image = EnhanceImage(image, extendedRules);

            return image.Count(c => c == '#').ToString();
        }

        internal static string EnhanceImage(string image, Dictionary<string, string> rules)
        {
            // Small image, a rule can be applied directly
            if (image.Replace("/", "").Length <=9)
            {
                return rules[image];
            }

            var split = SplitInSubImages(image);
            var enhanced = split.Select(img => EnhanceImage(img, rules)).ToArray();
            var recomposed = RecomposeImage(enhanced);
            return recomposed;
        }

        internal static string[] SplitInSubImages(string image)
        {
            /*   #.|.#      "#..#/..../..../#..#",
             *   ..|..                  v
             *   --+--   =>             v
             *   ..|..                  v
             *   #.|.#      ["#./..", ".#/..", "../#.", "../.#"]
             */

            char[][] rows = image.Split("/").Select(line => line.ToCharArray()).ToArray();
            var len = rows[0].Length;
            var divisor = len % 2 == 0 ? 2 : 3;
            var squaresNumber = len / divisor;

            if (squaresNumber == 1) return new string[]{image};

            List<string> result = new List<string>();
            for (int r = 0; r < squaresNumber; r++)
                for (int c = 0; c < squaresNumber; c++)
                    result.Add(ExtractSubImage(rows, r * divisor, c * divisor, divisor));

            return result.ToArray();
        }

        internal static string ExtractSubImage(char[][] rows, int R, int C, int len)
        {
            /*   #.|.#              
             *   ..|..               
             *   --+--  + 0,0,2  => "#./.." 
             *   ..|..              
             *   #.|.#              
             */

            var columnTo = C + len;
            List<string> result = new List<string>();
            for (int r = R; r < R + len; r++)                
                result.Add(new string(rows[r][C..columnTo]));

            return string.Join("/", result);
        }

        private static string RecomposeImage(string[] subImages)
        {
            /*  ["#./..", ".#/..", "../#.", "../.#"]     #.|.#
             *              v                            ..|..
             *              v                        =>  --+-- 
             *              v                            ..|..
             *   "#..#/..../..../#..#"                   #.|.#
             */

            var sideLength = (int)Math.Sqrt(subImages.Length);
            string[][] squaredSuperImage = new string[sideLength][];
            for (int i = 0; i < sideLength; i++) squaredSuperImage[i] = new string[sideLength];

            for (int r = 0; r < sideLength; r++)
                for (int c = 0; c < sideLength; c++)
                    squaredSuperImage[r][c] = subImages[c + r * sideLength];

            var rows = squaredSuperImage.SelectMany(ExtractRowsFromSquaredSuperImageRow).ToArray();
            return string.Join("/", rows);
        }

        private static IEnumerable<string> ExtractRowsFromSquaredSuperImageRow(string[] squaredSuperImageRow)
        {
            /*  ["#./..", ".#/.."]      #.|.#
             *        v                 ..|..
             *        v             =>  --+-- 
             *        v                 ..|..
             *    "#..#/...."            #.|.#
             */
            
            static string recomposeLine(string[][] blocks, int row)
            {
                var builder = new StringBuilder();
                blocks.ForEach(block => builder.Append(block[row]));
                return builder.ToString();
            }

            var splitBlocks = squaredSuperImageRow.Select(block => block.Split('/')).ToArray();
            var rows = Enumerable.Range(0, splitBlocks[0].Length)
                .Select(i => recomposeLine(splitBlocks, i)).ToArray();
            return rows;          
        }

        internal static Dictionary<string, string> ExtractRulesFromInput(string[] input) =>
            input.ToDictionary(l => l.Split(" => ")[0], l => l.Split(" => ")[1]);

        internal static Dictionary<string, string> ExpandRules(Dictionary<string, string> rules)
        {
            Dictionary<string, string> expandedRules = new();
            foreach(string rule in rules.Keys)
            {
                var transformations = TransformRule(rule).Select(RecomposeSquare).ToHashSet();
                transformations.ForEach(newRule => expandedRules.Add(newRule, rules[rule]));
            }
            return expandedRules;
        }

        private static IEnumerable<char[][]> TransformRule(string rule) {
            var squared = rule.Split('/').Select(line => line.ToCharArray()).ToArray();
            var rotated90 = squared.Rotate();
            var rotated180 = rotated90.Rotate();
            var rotated270 = rotated180.Rotate();
            var flipped = squared.FlipVertically();
            var flippedRotated90 = flipped.Rotate();
            var flippedRotated180 = flippedRotated90.Rotate();
            var flippedRotated270 = flippedRotated180.Rotate();
            return new[] { squared, rotated90, rotated180, rotated270, flipped, flippedRotated90, flippedRotated180, flippedRotated270 };
        }

        private static string RecomposeSquare(char[][] square) => string.Join("/", square.Select(line => string.Join("", line)));
    }
}
